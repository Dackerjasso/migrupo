//
//  Student.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso on 22/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import CVCalendar

class Student: NSObject, NSCopying {
    var name: String!
    var dbid: Int!
    var dbidString : String!
    var gender : String!
    var birthday: String!
    var birthdayDate : CVDate!
    var emailTutor : String!
    var classe : Class!
    var uuid : String!
    var claseId : Int!
    var isHere : Bool!
    var isAlmostNotHere: Bool!
    var img : UIImage!
    var imgUrl : String!
    var asked = false
    var criterios : [Criterio] = []
    var updated: Bool!
    var localId : Int!

    override init (){
        
    }
    
    init(name: String?,  gender: String?, birthdayDate: CVDate?, emailTutor: String? ) {
        self.name = name
        self.gender = gender
        self.birthdayDate = birthdayDate
        self.emailTutor = emailTutor
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = Student(name: name, gender: gender, birthdayDate: birthdayDate, emailTutor: emailTutor)
        return copy
    }
    
    
}
