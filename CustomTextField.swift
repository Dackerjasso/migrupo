//
//  CustomTextField.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 13/07/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

class CustomTextField: UITextField {

    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 45, dy: 0)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return self.textRect(forBounds: bounds)
    }
    
}
