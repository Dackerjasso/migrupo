//
//  RegisterViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso on 22/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Photos
import PhotosUI
import ActiveLabel

class RegisterViewController: UIViewController, UIImagePickerControllerDelegate,
UINavigationControllerDelegate {
    
    @IBOutlet weak var labelLink: ActiveLabel!

    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var usrnameTF: UITextField!
    @IBOutlet weak var schoolTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var button: UIButton!
    let imagePicker = UIImagePickerController()
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var viewText: UIView!
    let defaults = UserDefaults()
    var localP : URL!
    
    var tWS = TeacherWS()
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            internetProvider.sharedInstance.isVertical = false
        } else {
            internetProvider.sharedInstance.isVertical = true
            
        }
        self.view.layoutIfNeeded()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {

        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
        self.view.isUserInteractionEnabled = true
        if (self.isBeingPresented) {
            // being presented
            print("Presented")
            backButton.alpha = 1
        } else if (self.isMovingToParentViewController) {
            // being pushed
            print("Pushed")
            backButton.alpha = 0
        } else {
            // simply showing again because another VC was dismissed
            print("No sé")
        }
        labelLink.attributedText = NSAttributedString(string: Localization("PrivacyPolicy"))
        addImageButotn.text = Localization("AddImage")
        nameTF.placeholder = Localization("Name")
        lastNameTF.placeholder = Localization("SecondName")
        usrnameTF.placeholder = Localization("Alias")
        schoolTF.placeholder = Localization("School")
        emailTF.placeholder = Localization("Email")
        passwordTF.placeholder = Localization("Password")
        acceptButton.setTitle(Localization("Aceptar"), for: .normal)
    }
    
    @IBAction func acceptTerms(_ sender: Any) {
        (sender as! UIButton).isSelected = !(sender as! UIButton).isSelected
        if (sender as! UIButton).isSelected{
            acceptButton.isEnabled = true
        }else{
            acceptButton.isEnabled = false
        }
    }
    
    @IBOutlet weak var addImageButotn: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.img.image = #imageLiteral(resourceName: "espacioFoto-1")


        labelLink.customize { label in
            let customType = ActiveType.custom(pattern: Localization("PrivacyPolicyLink")) //Regex that looks for "with"
            labelLink.enabledTypes = [.mention, .hashtag, .url, customType]
            label.handleCustomTap(for: customType) {_ in self.a(UIButton()) }
            labelLink.textColor = UIColor(red: 102.0/255, green: 117.0/255, blue: 127.0/255, alpha: 1)
            labelLink.hashtagColor = UIColor(red: 85.0/255, green: 172.0/255, blue: 238.0/255, alpha: 1)
            labelLink.mentionColor = UIColor(red: 238.0/255, green: 85.0/255, blue: 96.0/255, alpha: 1)
            labelLink.URLColor = UIColor(red: 85.0/255, green: 238.0/255, blue: 151.0/255, alpha: 1)
            label.handleHashtagTap { _ in self.a(UIButton()) }

        }
        
        
        
        
        IQKeyboardManager.sharedManager().disabledToolbarClasses.append(RegisterViewController.self)
        img.clipsToBounds = true
        tWS.delegate = self
        img.layer.cornerRadius = img.frame.size.width / 2
        nameTF.setLeftPaddingPoints(30)
        nameTF.setRightPaddingPoints(30)
        lastNameTF.setLeftPaddingPoints(30)
        lastNameTF.setRightPaddingPoints(30)
        usrnameTF.setLeftPaddingPoints(30)
        usrnameTF.setRightPaddingPoints(30)
        schoolTF.setLeftPaddingPoints(30)
        schoolTF.setRightPaddingPoints(30)
        emailTF.setLeftPaddingPoints(30)
        emailTF.setRightPaddingPoints(30)
        passwordTF.setLeftPaddingPoints(30)
        passwordTF.setRightPaddingPoints(30)
        nameTF.setRightPaddingPoints(30, image: #imageLiteral(resourceName: "check1"))
        lastNameTF.setRightPaddingPoints(30, image: #imageLiteral(resourceName: "check1"))
        usrnameTF.setRightPaddingPoints(30, image: #imageLiteral(resourceName: "check1"))
        schoolTF.setRightPaddingPoints(30, image: #imageLiteral(resourceName: "check1"))
        emailTF.setRightPaddingPoints(30, image: #imageLiteral(resourceName: "check1"))
        passwordTF.setRightPaddingPoints(30, image: #imageLiteral(resourceName: "check1"))
        // Do any additional setup after loading the view.
    }

    @IBOutlet weak var backButton: UIButton!
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true) { 
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func a(_ sender: Any) {
        let url = URL(string: "http://migrupo.redlarousse.mx/politicas/")!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @IBAction func selectPhotoFrom(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cameraOption = UIAlertAction(title: Localization("camera"), style: .default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
        let libraryOption = UIAlertAction(title: Localization("gallery"), style: .default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
        let cancelOption = UIAlertAction(title: Localization("Cancelar"), style: .cancel) { (action) in
            
        }
        alert.addAction(cameraOption)
        alert.addAction(libraryOption)
//        alert.addAction(cancelOption)
        
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = button
            presenter.sourceRect = button.bounds
        }
        
        self.present(alert, animated: true) {
            
        }
    }
    func getDocumentsURL() -> URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsURL
    }
    
    func fileInDocumentsDirectory(_ filename: String) -> URL{
        let fileURL = getDocumentsURL().appendingPathComponent(filename)
        return fileURL
    }
    
    func saveImage (_ image: UIImage, path: URL ) -> Bool{
        
        let pngImageData = UIImageJPEGRepresentation(image, 1.0)
        UIGraphicsBeginImageContext(image.size)
        image.draw(in: CGRect(x:0, y:0, width:image.size.width, height:image.size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let jpgImageData = UIImageJPEGRepresentation(newImage!, 1.0)
        
        let result = (try? jpgImageData!.write(to: URL(fileURLWithPath: path.path), options: [.atomic])) != nil
        self.localP = path
        return result
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        img.image =  info["UIImagePickerControllerOriginalImage"] as? UIImage
        if picker.sourceType == .camera{
            picker.dismiss(animated: true, completion: {
                let myImageName = "asset.JPG"
                let imagePath = self.fileInDocumentsDirectory(myImageName)
                if self.saveImage((info["UIImagePickerControllerOriginalImage"] as? UIImage)!, path: imagePath){
                    self.localP = imagePath
                    do {
                        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                        let documentDirectory = URL(fileURLWithPath: path)
                        let originPath = documentDirectory.appendingPathComponent("asset.JPG")
                        let destinationPath = documentDirectory.appendingPathComponent("assetUser.jpg")
                        self.localP = destinationPath
                        try FileManager.default.moveItem(at: originPath, to: destinationPath)
                    } catch {
                        print(error)
                        
                    }
                }else{
                    self.img.image = #imageLiteral(resourceName: "espacioFoto")
                }
                
                
            })
        }else{
            let imageUrl          = info[UIImagePickerControllerReferenceURL] as! NSURL
            let imageName         = imageUrl.lastPathComponent?.replacingOccurrences(of: ".PNG", with: ".JPG")
            let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
            let photoURL          = NSURL(fileURLWithPath: documentDirectory)
            let localPath         = photoURL.appendingPathComponent(imageName!)
            guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else { return }
            let data              = UIImageJPEGRepresentation(image, 1.0)

            
            do
            {
                try data?.write(to: localPath!, options: Data.WritingOptions.atomic)
                self.localP = localPath
            }
            catch
            {
                // Catch exception here and act accordingly
            }
            do {
                let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                let documentDirectory = URL(fileURLWithPath: path)
                let originPath = documentDirectory.appendingPathComponent("asset.JPG")
                let destinationPath = documentDirectory.appendingPathComponent("assetUser.jpg")
                try FileManager.default.moveItem(at: originPath, to: destinationPath)
            } catch {
                print(error)
                
            }
            picker.dismiss(animated: true) {
                
            }
        }

    }
    
    @IBAction func registerTeacher(_ sender: Any) {
        let errorString = NSMutableString()
        var error = false
        if (nameTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
            error = true
        }
        if (lastNameTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
            error = true
        }
        if (emailTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)! || !emailTF.isValidEmail(){
            
            let errorAlert = self.errorAlert(message: Localization("EmailNotValid"))
            self.present(errorAlert, animated: true, completion: {
                
            })
            error = true
            return
        }
        if (passwordTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
            let errorAlert = self.errorAlert(message: Localization("LenghtPassAlert"))
            self.present(errorAlert, animated: true, completion: {
                
            })
            error = true
            return
        }
        if (usrnameTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
            error = true
        }
        if (schoolTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
            error = true
        }
        
        if (passwordTF.text?.characters.count)! < 5{
            error = true
        }
        
        if error{
            errorString.append(Localization("ErrorRegisterUser"))
            let errorAlert = self.errorAlert(message: errorString as String)
            self.present(errorAlert, animated: true, completion: { 
                self.view.isUserInteractionEnabled = true
            })
        }else{
            let teacher = Teacher()
            teacher.name = nameTF.text
            teacher.lastName = lastNameTF.text
            teacher.email = emailTF.text
            teacher.password = passwordTF.text
            teacher.userName = usrnameTF.text
            teacher.schoolName = schoolTF.text
            teacher.localPath = localP
            teacher.image = img.image
            tWS.register(teacher: teacher)
            self.startSpinner()
            self.view.isUserInteractionEnabled = false
        }
        

    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension RegisterViewController: teacherDelegate{
    func didSuccessRegister(teacher: Teacher) {
        self.stopSpinner()
        nameTF.text = ""
        lastNameTF.text = ""
        usrnameTF.text = ""
        schoolTF.text = ""
        emailTF.text = ""
        passwordTF.text = ""
        UserProvider.sharedInstance.image = img.image
        self.img.image = #imageLiteral(resourceName: "espacioFoto-1")

        
        self.view.isUserInteractionEnabled = true
        defaults.set(teacher.email, forKey: "email")
        defaults.set(teacher.uuid, forKey: "uuid")
        defaults.set(teacher.password, forKey: "password")
        defaults.set("1", forKey: "logued")
        defaults.synchronize()
        let rootSB = UIStoryboard(name: "Root", bundle: nil)
        let rvc = rootSB.instantiateViewController(withIdentifier: "rootView") as! RootViewController
        
        
        let navController = UINavigationController.init(rootViewController: rvc)
        
        present(navController, animated: true, completion: nil)

    }
    
    func didFailRegisterTeacher(statusCode: Int, message: String!) {
        self.stopSpinner()
        self.view.isUserInteractionEnabled = true
        let alert = self.errorAlert(message: message)
        self.present(alert, animated: true) {
            
        }
    }
    
    func userAlreadyExists() {
        self.view.isUserInteractionEnabled = true
        let errorvc = self.errorAlert(message: "Este correo ya está registrado")
        self.present(errorvc, animated: true) { 
            self.view.isUserInteractionEnabled = true
        }
    }
}

extension RegisterViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 35 // Bool
    }
}

extension RegisterViewController: UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
    }
}
