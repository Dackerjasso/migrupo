//
//  ListEditStudentTableViewCell.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 31/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

protocol editListButtonsDelegate {
    func changeStatus()
}

class ListEditStudentTableViewCell: UITableViewCell {
    var delegate: editListButtonsDelegate?
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var faltaButton: UIButton!
    @IBOutlet weak var retardoButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var asistenciaButton: UIButton!
    var student: Student!
    override func awakeFromNib() {
        super.awakeFromNib()
        userImg.clipsToBounds = true
        userImg.layer.cornerRadius = userImg.frame.size.width / 2
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func faltaAction(_ sender: Any) {
        student.isHere = false
        student.isAlmostNotHere = false
        self.delegate?.changeStatus()
    }
    
    @IBAction func retardoAction(_ sender: Any) {
        student.isHere = false
        student.isAlmostNotHere = true
        self.delegate?.changeStatus()
    }

    @IBAction func assistenciaAction(_ sender: Any) {
        student.isHere = true
        student.isAlmostNotHere = false
        self.delegate?.changeStatus()
    }
}
