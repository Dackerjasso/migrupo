//
//  ListNewStudentViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 31/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import CVCalendar
import Nuke
import Toucan


class ListNewStudentViewController: UIViewController {
    var classe: Class!
    @IBOutlet weak var tableView: UITableView!
    var toDelete : [Student] = []
    let btn1 = UIButton(type: .custom)
    @IBOutlet weak var btnAddBottom: UIButton!
    var currentStudent = 0
    var tWS = TeacherWS()
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
        toDelete.removeAll()
        btn1.isEnabled = false
        self.stopSpinner()
        tWS.delegate = self
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func deleteStudents(){
        print("Delete")
        let alert = UIAlertController(title: nil, message: Localization("DeleteStudents"), preferredStyle: .alert)
        let okAction = UIAlertAction(title: Localization("yes"), style: .default) { (action) in
            self.startSpinner()
            self.tWS.remove(students: self.toDelete, toClass: self.classe)
            
        }
        let cancelAction = UIAlertAction(title: Localization("no"), style: .cancel) { (action) in
            
        }
        alert.addAction(cancelAction)
        alert.addAction(okAction)
        self.present(alert, animated: true) { 
            
        }

    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btn1.setImage(#imageLiteral(resourceName: "btnEliminar"), for: .normal)
        btn1.isEnabled = false
        btn1.frame = CGRect(x: 0, y: 0, width: 46.5, height: 46.5)
        btn1.addTarget(self, action: #selector(self.deleteStudents), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        self.navigationItem.setRightBarButtonItems([item1], animated: true)
        tableView.isMultipleTouchEnabled = true

        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addOtherSegue"{
            let vc = segue.destination as! RegisterStudentViewController
            vc.toRoot = true
            vc.classe = self.classe
        }
        if segue.identifier == "editStudentSegue"{
            let vc = segue.destination as! EditStudentViewController
            vc.toRoot = true
            vc.student = classe.students[currentStudent]
//            vc.student = self.classe
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    func getDocumentsURL() -> URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsURL
    }
    
    func fileInDocumentsDirectory(_ filename: String) -> URL{
        let fileURL = getDocumentsURL().appendingPathComponent(filename)
        return fileURL
    }
    
    func saveImage (_ image: UIImage, path: URL ) -> Bool{
        
        let pngImageData = UIImageJPEGRepresentation(image, 1.0)
        UIGraphicsBeginImageContext(image.size)
        image.draw(in: CGRect(x:0, y:0, width:image.size.width, height:image.size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let jpgImageData = UIImageJPEGRepresentation(newImage!, 1.0)
        
        let result = (try? jpgImageData!.write(to: URL(fileURLWithPath: path.path), options: [.atomic])) != nil
        return result
        
    }
}


extension ListNewStudentViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if classe.students.count == 0{
            btnAddBottom.alpha = 0
            tableView.isScrollEnabled = false
            return 1
        }
        btnAddBottom.alpha = 1
        tableView.isScrollEnabled = true
       return classe.students.count
    }
    
    func addStudent(){
        print("Add")
        self.performSegue(withIdentifier: "addOtherSegue", sender: nil)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if classe.students.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "noDataCell") as! NoStudentsTableViewCell
            cell.addButton.addTarget(self, action: #selector(self.addStudent), for: .touchUpInside)
            cell.materiaLabel.text = self.classe.name
            return cell
        }
        if indexPath.row < classe.students.count{
            let cell = tableView.dequeueReusableCell(withIdentifier: "seeListStudentCell") as! StudentInformationTableViewCell
            let student = classe.students[indexPath.row]
//            userImg
            if let imag = student.img{
                cell.userImg.image = imag
            }else{
                cell.userImg.image = nil
                let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
                let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
                let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
                if let dirPath          = paths.first
                {
                    let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("\(student.uuid!).jpg")
                    let image    = UIImage(contentsOfFile: imageURL.path)
                    if image != nil{
                        student.img = image
                    }else{
                        student.img = nil
                    }
                }else{
                    student.img = nil
                }
            }
            if let imag = student.img{
                cell.userImg.image = imag
            }else{
                cell.userImg.image = nil
                if let urlString = student.imgUrl ,!urlString.isEmpty {
                    switch urlString {
                    case "emoti1.jpg":
                        student.img = #imageLiteral(resourceName: "avatar1G")
                        cell.userImg.image = #imageLiteral(resourceName: "avatar1G")
                        break
                    case "emoti2.jpg":
                        student.img = #imageLiteral(resourceName: "avatar2G")
                        cell.userImg.image = #imageLiteral(resourceName: "avatar2G")
                        break
                    case "emoti3.jpg":
                        student.img = #imageLiteral(resourceName: "avatar3G")
                        cell.userImg.image = #imageLiteral(resourceName: "avatar3G")
                        break
                    case "emoti4.jpg":
                        student.img = #imageLiteral(resourceName: "avatar4G")
                        cell.userImg.image = #imageLiteral(resourceName: "avatar4G")
                        break
                    case "emoti5.jpg":
                        student.img = #imageLiteral(resourceName: "avatar5G")
                        cell.userImg.image = #imageLiteral(resourceName: "avatar5G")
                        break
                    case "emoti6.jpg":
                        student.img = #imageLiteral(resourceName: "avatar6G")
                        cell.userImg.image = #imageLiteral(resourceName: "avatar6G")
                        break
                    case "emoti7.jpg":
                        student.img = #imageLiteral(resourceName: "avatar7G")
                        cell.userImg.image = #imageLiteral(resourceName: "avatar7G")
                        break
                    case "emoti8.jpg":
                        student.img = #imageLiteral(resourceName: "avatar8G")
                        cell.userImg.image = #imageLiteral(resourceName: "avatar8G")
                        break
                    case "emoti9.jpg":
                        student.img = #imageLiteral(resourceName: "avatar9G")
                        cell.userImg.image = #imageLiteral(resourceName: "avatar9G")
                        break
                    default:
                        let request = Request(url: URL(string:urlString)!)
                        
                        Nuke.loadImage(with: request, into: cell.userImg) { [weak view] response, _ in
                            //                view?.image = response.value
                            if let error = response.error{
                                
                            }else{
                            print(response.value!)
                            let resizedImage = Toucan.Resize.resizeImage(response.value!, size: CGSize(width: 200, height: 200))
                            cell.userImg.image = resizedImage
                            student.img = resizedImage
                                _ = self.saveImage(resizedImage, path: self.fileInDocumentsDirectory("\(student.uuid!).jpg"))

                            }
                        }
                        break
                    }

                }
            }
            
            
            if toDelete.contains(student){
                cell.selectedImg.alpha = 1
                cell.fondoImg.image = #imageLiteral(resourceName: "fondoAlumnoNoDisponible")
                cell.editButton.isEnabled = false
                
            }else{
                cell.selectedImg.alpha = 0
                cell.fondoImg.image = #imageLiteral(resourceName: "fondoAlumno1")
                cell.editButton.isEnabled = true
            }
            if let sex = student.gender{
                if sex == Localization("man") || sex == "M"{
                    cell.sexLabel.text = Localization("man")
                }else{
                    cell.sexLabel.text = Localization("woman")
                }
            }else{
                cell.sexLabel.text = " - "
            }
            cell.nameLabel.text = student.name
            if let birthday = student.birthdayDate{
                cell.birthdayLabel.text = birthday.commonDescription
            }else{
                cell.birthdayLabel.text = " - "
            }
            if let email = student.emailTutor{
                cell.emailLabel.text = student.emailTutor
            }else{
                cell.emailLabel.text = " - "
            }
            cell.editButton.tag = indexPath.row
            cell.editButton.addTarget(self, action: #selector(self.editStudent(_:)), for: .touchUpInside)
            cell.backgroundColor  = UIColor.clear
            return cell
        }else{
            return tableView.dequeueReusableCell(withIdentifier: "plusCell") as! PlusTableViewCell
        }
    }
    
    func editStudent(_ sender : UIButton){
        print(sender.tag)
        currentStudent = sender.tag
        self.performSegue(withIdentifier: "editStudentSegue", sender: self)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if classe.students.count == 0{
            return tableView.frame.size.height
        }
        if indexPath.row < classe.students.count{
            return 130
        }else{
            return 65
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if classe.students.count > 0{
            let student = classe.students[indexPath.row]
            if toDelete.contains(student){
                toDelete.remove(at: toDelete.index(of: student)!)
            }else{
                toDelete.append(student)
            }
            if toDelete.count > 0{
                btn1.isEnabled = true
            }else{
                btn1.isEnabled = false
            }
            tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let student = classe.students[indexPath.row]
        if toDelete.contains(student){
            toDelete.remove(at: toDelete.index(of: student)!)
        }else{
            toDelete.append(student)
        }
        if toDelete.count > 0{
            btn1.isEnabled = true
        }else{
            btn1.isEnabled = false
        }
    }
}

extension ListNewStudentViewController: teacherDelegate{
    func didSuccessDelete(students: [Student]) {
        self.stopSpinner()
        for student in self.toDelete{
            if classe.students.contains(student){
                let fileManager = FileManager.default
                let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
                let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
                let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
                guard let dirPath = paths.first else {
                    return
                }
                let filePath = "\(dirPath)/\(student.uuid!).jpg"
                do {
                    try fileManager.removeItem(atPath: filePath)
                } catch let error as NSError {
                    print(error.debugDescription)
                }
                self.classe.students.remove(at: self.classe.students.index(of: student)!)
            }
        }
        self.toDelete.removeAll()
        self.btn1.isEnabled = false
        self.tableView.reloadData()
    }
    
    func didFailDeleteStudents(message: String!) {
        self.stopSpinner()
        let error = self.errorAlert(message: message)
        self.present(error, animated: true) {
            
        }
    }
}
