//
//  RegisterStudentViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso on 25/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import CVCalendar
import Nuke
import PhotosUI
import Photos
import Toucan

class EditStudentViewController: UIViewController, UIImagePickerControllerDelegate,
UINavigationControllerDelegate {
    var date : CVDate!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var studentImg: UIImageView!
    @IBOutlet weak var birthdayButton: UIButton!
    let imagePicker = UIImagePickerController()
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var genderTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var birthdayTF: UITextField!
    @IBOutlet weak var imgButton: UIButton!
    @IBOutlet weak var nameTF: UITextField!
    var tWS = TeacherWS()
    var toRoot = false
    var student: Student!
    let bcvDate : CVDate! = nil
    var edit = false
    var updated = false
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
        if !internetProvider.sharedInstance.isVertical{
            print("Landscape")
            self.stackView.axis = .horizontal
            titleLabel.font = UIFont.boldSystemFont(ofSize: 28)
        } else {
            self.stackView.axis = .vertical
            titleLabel.font = UIFont.boldSystemFont(ofSize: 50)
        }
        self.view.layoutIfNeeded()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            self.stackView.axis = .horizontal
            internetProvider.sharedInstance.isVertical = false
            titleLabel.font = UIFont.boldSystemFont(ofSize: 28)
        } else {
            self.stackView.axis = .vertical
            internetProvider.sharedInstance.isVertical = true
            titleLabel.font = UIFont.boldSystemFont(ofSize: 50)
        }
        self.view.layoutIfNeeded()
        
    }
    
    
    func back(sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go back to the previous ViewController
        if edit{
            let error = UIAlertController(title: nil, message: Localization("LostData"), preferredStyle: .alert)
            let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default) { (action) in
                _ = self.navigationController?.popViewController(animated: true)
            }
            let cancelAction = UIAlertAction(title: Localization("Cancelar"), style: .cancel) { (action) in
                
            }
            error.addAction(okAction)
            error.addAction(cancelAction)
            self.present(error, animated: true) {
                
            }
        }else{
            _ = self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    
    @IBOutlet weak var acceptButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(self.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        titleLabel.text = Localization("EditStudent")
        nameTF.placeholder = Localization("Name")
        genderTF.placeholder = Localization("Gender")
        birthdayTF.placeholder = Localization("Birthday")
        emailTF.placeholder = Localization("ParentEmail")
        acceptButton.setTitle(Localization("Aceptar"), for: .normal)
        
        if let imag = student.img{
            studentImg.image = imag
        }else{
            studentImg.image = #imageLiteral(resourceName: "espacioAvatar")
            if let urlString = student.imgUrl ,!urlString.isEmpty {
                switch urlString {
                case "emoti1.jpg":
                    student.img = #imageLiteral(resourceName: "avatar1G")
                    studentImg.image = #imageLiteral(resourceName: "avatar1G")
                    break
                case "emoti2.jpg":
                    student.img = #imageLiteral(resourceName: "avatar2G")
                    studentImg.image = #imageLiteral(resourceName: "avatar2G")
                    break
                case "emoti3.jpg":
                    student.img = #imageLiteral(resourceName: "avatar3G")
                    studentImg.image = #imageLiteral(resourceName: "avatar3G")
                    break
                case "emoti4.jpg":
                    student.img = #imageLiteral(resourceName: "avatar4G")
                    studentImg.image = #imageLiteral(resourceName: "avatar4G")
                    break
                case "emoti5.jpg":
                    student.img = #imageLiteral(resourceName: "avatar5G")
                    studentImg.image = #imageLiteral(resourceName: "avatar5G")
                    break
                case "emoti6.jpg":
                    student.img = #imageLiteral(resourceName: "avatar6G")
                    studentImg.image = #imageLiteral(resourceName: "avatar6G")
                    break
                case "emoti7.jpg":
                    student.img = #imageLiteral(resourceName: "avatar7G")
                    studentImg.image = #imageLiteral(resourceName: "avatar7G")
                    break
                case "emoti8.jpg":
                    student.img = #imageLiteral(resourceName: "avatar8G")
                    studentImg.image = #imageLiteral(resourceName: "avatar8G")
                    break
                case "emoti9.jpg":
                    student.img = #imageLiteral(resourceName: "avatar9G")
                    studentImg.image = #imageLiteral(resourceName: "avatar9G")
                    break
                default:
                    let request = Request(url: URL(string:urlString)!)
                    
                    Nuke.loadImage(with: request, into: studentImg) { [weak view] response, _ in
                        //                view?.image = response.value
                        if let error = response.error{
                            
                        }else{
                        print(response.value!)
                        let resizedImage = Toucan.Resize.resizeImage(response.value!, size: CGSize(width: 200, height: 200))
                        self.studentImg.image = resizedImage
                            self.student.img = resizedImage
                            _ = self.saveImage(resizedImage, path: self.fileInDocumentsDirectory("\(self.student.uuid!).jpg"))
                        }
                    }
                    break
                }


            }
        }

        print(student.name)
        nameTF.text = student.name
        emailTF.text = student.emailTutor
        if let g = student.gender{
            if g == Localization("man") || g == "M"{
                genderTF.text = Localization("man")
            }else{
                genderTF.text = Localization("woman")
            }
        }
        if let b = student.birthdayDate{
            birthdayTF.text = b.commonDescription
            date = b
        }
        tWS.delegate = self
        nameTF.setLeftPaddingPoints(30)
        nameTF.setRightPaddingPoints(30)
        genderTF.setLeftPaddingPoints(30)
        genderTF.setRightPaddingPoints(30)
        birthdayTF.setLeftPaddingPoints(30)
        birthdayTF.setRightPaddingPoints(30)
        emailTF.setLeftPaddingPoints(30)
        emailTF.setRightPaddingPoints(30)
        studentImg.clipsToBounds = true
        studentImg.layer.cornerRadius = studentImg.frame.size.width / 2
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addImage(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cameraOption = UIAlertAction(title: Localization("camera"), style: .default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
        let libraryOption = UIAlertAction(title: Localization("gallery"), style: .default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
        let avatarOption = UIAlertAction(title: "Avatar", style: .default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                //                self.present(self.imagePicker, animated: true, completion: nil)
                let avc = self.storyboard?.instantiateViewController(withIdentifier: "avatarsView") as! AvatarsViewController
                avc.delegate = self
                self.navigationController?.pushViewController(avc, animated: true)
            }
        }
        let cancelOption = UIAlertAction(title: "Cancelar", style: .cancel) { (action) in
            
        }
        alert.addAction(cameraOption)
        alert.addAction(libraryOption)
        alert.addAction(avatarOption)
//        alert.addAction(cancelOption)
        
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = imgButton
            presenter.sourceRect = imgButton.bounds
        }
        
        self.present(alert, animated: true) {
            
        }
        
    }
    
    func getDocumentsURL() -> URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsURL
    }
    
    func fileInDocumentsDirectory(_ filename: String) -> URL{
        let fileURL = getDocumentsURL().appendingPathComponent(filename)
        return fileURL
    }
    
    func saveImage (_ image: UIImage, path: URL ) -> Bool{
        
        let pngImageData = UIImageJPEGRepresentation(image, 1.0)
        UIGraphicsBeginImageContext(image.size)
        image.draw(in: CGRect(x:0, y:0, width:image.size.width, height:image.size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let jpgImageData = UIImageJPEGRepresentation(newImage!, 1.0)
        
        let result = (try? jpgImageData!.write(to: URL(fileURLWithPath: path.path), options: [.atomic])) != nil
        return result
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        studentImg.image =  info["UIImagePickerControllerOriginalImage"] as? UIImage
        edit = true
        var localPath : URL!
        if picker.sourceType == .camera{
            picker.dismiss(animated: true, completion: {
                let myImageName = "asset.JPG"
                let imagePath = self.fileInDocumentsDirectory(myImageName)
                if self.saveImage((info["UIImagePickerControllerOriginalImage"] as? UIImage)!, path: imagePath){
                    self.tWS.updateImage(url: imagePath, student: self.student)
                    self.startSpinner()

                }else{
                    self.studentImg.image = #imageLiteral(resourceName: "espacioFoto")
                }
                
            })
        }else{
            let imageUrl          = info[UIImagePickerControllerReferenceURL] as! NSURL
            let imageName         = imageUrl.lastPathComponent?.replacingOccurrences(of: ".PNG", with: ".JPG")
            let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
            let photoURL          = NSURL(fileURLWithPath: documentDirectory)
             localPath         = photoURL.appendingPathComponent(imageName!)
            let image             = info[UIImagePickerControllerOriginalImage]as! UIImage
            let data              = UIImageJPEGRepresentation(image, 1.0)
            
            do
            {
                try data?.write(to: localPath!, options: Data.WritingOptions.atomic)
            }
            catch
            {
                // Catch exception here and act accordingly
                print(error)
                return
            }
            do {
                let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                let documentDirectory = URL(fileURLWithPath: path)
                let originPath = documentDirectory.appendingPathComponent("asset.JPG")
                let destinationPath = documentDirectory.appendingPathComponent("\(student.uuid!).jpg")
                localPath = destinationPath
                try FileManager.default.moveItem(at: originPath, to: destinationPath)
            
            } catch {
                print(error)
                
            }
            tWS.updateImage(url: localPath!, student: self.student)
            picker.dismiss(animated: true) {
                self.startSpinner()
            }
        }

    }
    
    @IBAction func genderAction(_ sender: Any) {
        nameTF.resignFirstResponder()
        emailTF.resignFirstResponder()
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let mAction = UIAlertAction(title: Localization("man"), style: .default) { (action) in
            self.genderTF.text = action.title
            self.edit = true
        }
        let fAction = UIAlertAction(title: Localization("woman"), style: .default) { (action) in
            self.genderTF.text = action.title
            self.edit = true
        }
        alert.addAction(mAction)
        alert.addAction(fAction)
        
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = genderTF
            presenter.sourceRect = genderTF.bounds
        }
        
        self.present(alert, animated: true) {
            
        }
    }
    
    @IBAction func birthdayAction(_ sender: Any) {
        nameTF.resignFirstResponder()
        emailTF.resignFirstResponder()
        let calendarStoryBoard = UIStoryboard(name: "Calendar", bundle: nil)
        let cvc = calendarStoryBoard.instantiateInitialViewController() as! CalendarJTViewController
        cvc.modalPresentationStyle = .overCurrentContext
        cvc.modalTransitionStyle = .crossDissolve
        cvc.delegate = self
        self.present(cvc, animated: true) {
            
        }
    }
    
    @IBAction func registerAction(_ sender: Any) {
        let errorString = NSMutableString(string: "")
        var isError = false
        if nameTF.text?.trimmingCharacters(in: NSCharacterSet.whitespaces).characters.count == 0{
            isError = true
        }
        if genderTF.text?.trimmingCharacters(in: NSCharacterSet.whitespaces).characters.count == 0{
            isError = true
        }
        if birthdayTF.text?.trimmingCharacters(in: NSCharacterSet.whitespaces).characters.count == 0{
            isError = true
        }
        if (emailTF.text?.characters.count)! > 0{
            if !emailTF.isValidEmail(){
                let alert = self.errorAlert(message: Localization("EmailNotValid"))
                self.present(alert, animated: true, completion: {
                    
                })
                return
            }
        }
        if studentImg.image != nil && studentImg.image != #imageLiteral(resourceName: "espacioAvatar"){
            student.img = studentImg.image
        }
        if isError{
            let alert = self.errorAlert(message: Localization("ErrorEditStudent"))
            self.present(alert, animated: true, completion: { })
        }else{
            print("Right")
            let s = Student()
            s.name = nameTF.text?.trimmingCharacters(in: NSCharacterSet.whitespaces)
            s.gender = genderTF.text?.trimmingCharacters(in: NSCharacterSet.whitespaces) == Localization("man") ? "M" : "F"
            s.birthday = date.commonDescription
            s.birthdayDate = date
            s.emailTutor = emailTF.text?.trimmingCharacters(in: NSCharacterSet.whitespaces)
            s.uuid = student.uuid
            s.dbid = student.dbid
            s.claseId = student.claseId
            self.startSpinner()
            tWS.update(student: s)
            //            self.view.isUserInteractionEnabled = false
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension EditStudentViewController: teacherDelegate{
    func didSuccessUpdate(student: Student) {
        if !updated {
            updated = true
            self.student.name = student.name
            self.student.gender = student.gender
            self.student.birthday = student.birthday
            self.student.birthdayDate = student.birthdayDate
            self.student.emailTutor = student.emailTutor
            let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.stopSpinner()
                self.navigationController?.popViewController(animated: true)
            }
        }

    }
    
    func didFailUpdateStudent(message: String) {
        self.stopSpinner()
        updated = false
        let error = self.errorAlert(message: message)
        self.present(error, animated: true) { 
            
        }
    }
    

    func didSuccessUploadImage() {
        self.stopSpinner()
        do {
            let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let documentDirectory = URL(fileURLWithPath: path)
            let originPath = documentDirectory.appendingPathComponent("asset.JPG")
            let destinationPath = documentDirectory.appendingPathComponent("\(self.student.uuid!)")
            try FileManager.default.moveItem(at: originPath, to: destinationPath)
        } catch {
            print(error)
            
        }
        self.student.img = studentImg.image
        _ = self.saveImage(studentImg.image!, path: self.fileInDocumentsDirectory("\(student.uuid!).jpg"))
        do {
            let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let documentDirectory = URL(fileURLWithPath: path)
            let originPath = documentDirectory.appendingPathComponent("asset.JPG")
            try FileManager.default.removeItem(at: originPath)
            
        } catch {
            print(error)
            
        }
        student.img = studentImg.image
        tWS.updateOnDB(student: student)
    }
    
    func didFailUpdateImage(message: String) {
        self.stopSpinner()
        if let imag = student.img{
            studentImg.image = imag
        }else{
            let error = self.errorAlert(message: message)
            self.present(error, animated: true) {
                
            }
            studentImg.image = #imageLiteral(resourceName: "espacioAvatar")
        }
    }
}

extension EditStudentViewController: calendarDeleate{
    func selectedDay(date: CVDate) {
        edit = true
        self.date = date
        birthdayTF.text = date.commonDescription
    }
}

extension EditStudentViewController: AvatarsDelegate{
    func selected(image: UIImage) {
        edit = true
        studentImg.image = image
        var myImageName = "\(student.uuid!).jpg"
        switch image {
        case #imageLiteral(resourceName: "avatar1G"):
            myImageName = "emoti1.jpg"
            break
        case #imageLiteral(resourceName: "avatar2G"):
            myImageName = "emoti2.jpg"
            break
        case #imageLiteral(resourceName: "avatar3G"):
            myImageName = "emoti3.jpg"
            break
        case #imageLiteral(resourceName: "avatar4G"):
            myImageName = "emoti4.jpg"
            break
        case #imageLiteral(resourceName: "avatar5G"):
            myImageName = "emoti5.jpg"
            break
        case #imageLiteral(resourceName: "avatar6G"):
            myImageName = "emoti6.jpg"
            break
        case #imageLiteral(resourceName: "avatar7G"):
            myImageName = "emoti7.jpg"
            break
        case #imageLiteral(resourceName: "avatar8G"):
            myImageName = "emoti8.jpg"
            break
        case #imageLiteral(resourceName: "avatar9G"):
            myImageName = "emoti9.jpg"
            break
        default:
            break
        }
        
        myImageName = "\(student.uuid!).jpg"
        let imagePath = self.fileInDocumentsDirectory(myImageName)
        if self.saveImage(image, path: imagePath){
            self.tWS.updateImage(url: imagePath, student: self.student)
            self.startSpinner()
        }else{
            self.studentImg.image = #imageLiteral(resourceName: "espacioFoto")
        }
    }
}

extension EditStudentViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        edit = true
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 45 // Bool
    }
}
