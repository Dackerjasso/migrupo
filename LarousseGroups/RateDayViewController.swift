//
//  RateDayViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 13/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import Nuke
import CVCalendar
import Toucan

class RateDayViewController: UIViewController {

    @IBOutlet weak var criterio1Label: UILabel!
    @IBOutlet weak var criterio2Label: UILabel!
    @IBOutlet weak var criterio3Label: UILabel!
    @IBOutlet weak var criterio1Switch: UISwitch!
    @IBOutlet weak var criterio2Switch: UISwitch!
    @IBOutlet weak var criterio3Switch: UISwitch!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var studentLabel: UILabel!
    var tWS = TeacherWS()
    var students : [Student] = []

    var c1IsAvalible = true
    var c2IsAvalible = true
    var c3IsAvalible = true
    
    var crit1User : [Student] = []
    var crit2User : [Student] = []
    var crit3User : [Student] = []
    var item1 : UIBarButtonItem!
    var classe : Class!
    var period: Period!
    var date: CVDate!
    
    func back(sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go back to the previous ViewController
        if crit1User.count > 0 || crit2User.count > 0 || crit3User.count > 0{
            let error = UIAlertController(title: nil, message: Localization("LostData"), preferredStyle: .alert)
            let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default) { (action) in
                _ = self.navigationController?.popViewController(animated: true)
            }
            let cancelAction = UIAlertAction(title: Localization("Cancelar"), style: .cancel) { (action) in
                
            }
            error.addAction(okAction)
            error.addAction(cancelAction)
            self.present(error, animated: true) {
                
            }
        }else{
            _ = self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    func getDocumentsURL() -> URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsURL
    }
    
    func fileInDocumentsDirectory(_ filename: String) -> URL{
        let fileURL = getDocumentsURL().appendingPathComponent(filename)
        return fileURL
    }
    
    func saveImage (_ image: UIImage, path: URL ) -> Bool{
        
        let pngImageData = UIImageJPEGRepresentation(image, 1.0)
        UIGraphicsBeginImageContext(image.size)
        image.draw(in: CGRect(x:0, y:0, width:image.size.width, height:image.size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let jpgImageData = UIImageJPEGRepresentation(newImage!, 1.0)
        
        let result = (try? jpgImageData!.write(to: URL(fileURLWithPath: path.path), options: [.atomic])) != nil
        return result
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tWS.delegate = self
        self.startSpinner()
        for s in classe.students{
            s.criterios.removeAll()
            for c in classe.criterios{
                c.calif = 0.0
                s.criterios.append(c.copy() as! Criterio)
            }
        }
        tWS.getReportBy(date: date.convertedDate(calendar: .current)!, classe: classe, period: period)
        studentLabel.text = Localization("Student")
        
        

        switch classe.criterios.count {
        case 0,1,2:
            criterio1Switch.isOn = false
            criterio2Switch.isOn = false
            criterio3Switch.isOn = false
            criterio1Switch.isEnabled = false
            criterio2Switch.isEnabled = false
            criterio3Switch.isEnabled = false
            break
        case 3:
            criterio1Switch.isOn = true
            criterio2Switch.isOn = false
            criterio3Switch.isOn = false
            criterio1Switch.isEnabled = true
            criterio2Switch.isEnabled = false
            criterio3Switch.isEnabled = false
            criterio1Label.text = classe.criterios[2].name
            c1IsAvalible = true
            c2IsAvalible = false
            c3IsAvalible = false
            break
        case 4:
            criterio1Switch.isOn = true
            criterio2Switch.isOn = true
            criterio3Switch.isOn = false
            criterio1Switch.isEnabled = true
            criterio2Switch.isEnabled = true
            criterio3Switch.isEnabled = false
            criterio1Label.text = classe.criterios[2].name
            criterio2Label.text = classe.criterios[3].name
            c1IsAvalible = true
            c2IsAvalible = true
            c3IsAvalible = false
            break
        case 5:
            criterio1Switch.isOn = true
            criterio2Switch.isOn = true
            criterio3Switch.isOn = true
            criterio1Switch.isEnabled = true
            criterio2Switch.isEnabled = true
            criterio3Switch.isEnabled = true
            criterio1Label.text = classe.criterios[2].name
            criterio2Label.text = classe.criterios[3].name
            criterio3Label.text = classe.criterios[4].name
            c1IsAvalible = true
            c2IsAvalible = true
            c3IsAvalible = true
            break
        default:
            break
        }
        tableView.reloadData()

        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(self.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        let btn1 = UIButton(type: .custom)
        btn1.setImage(#imageLiteral(resourceName: "btnFinalizar"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 46.5, height: 46.5)
        btn1.addTarget(self, action: #selector(self.done), for: .touchUpInside)
        item1 = UIBarButtonItem(customView: btn1)
        if !c1IsAvalible && !c2IsAvalible && !c3IsAvalible{
            item1.isEnabled = false
        }else{
            item1.isEnabled = true
        }
        if classe.criterios.count > 2{
            self.navigationItem.setRightBarButtonItems([item1], animated: true)
        }
        criterio1Switch.transform = CGAffineTransform.init(scaleX: 0.7, y: 0.7)
        criterio2Switch.transform = CGAffineTransform.init(scaleX: 0.7, y: 0.7)
        criterio3Switch.transform = CGAffineTransform.init(scaleX: 0.7, y: 0.7)
        self.automaticallyAdjustsScrollViewInsets = false
        tableView.contentInset = UIEdgeInsets.zero

        // Do any additional setup after loading the view.
    }

    func done(){
        print("Done")
        self.startSpinner()
        tWS.rateDay(students: students, classe: self.classe, period: self.period, c1: c1IsAvalible,  c2: c2IsAvalible,  c3: c3IsAvalible, date: date.convertedDate(calendar: .current)!)
        
        
//        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func disableCriterio1(_ sender: Any) {
        c1IsAvalible = (sender as! UISwitch).isOn
        crit1User.removeAll()
        tableView.reloadData()
        if !c1IsAvalible && !c2IsAvalible && !c3IsAvalible{
            item1.isEnabled = false
        }else{
            item1.isEnabled = true
        }
    }
    
    @IBAction func disableCriterio2(_ sender: Any) {
        c2IsAvalible = (sender as! UISwitch).isOn
        crit2User.removeAll()
        tableView.reloadData()
        if !c1IsAvalible && !c2IsAvalible && !c3IsAvalible{
            item1.isEnabled = false
        }else{
            item1.isEnabled = true
        }
    }
    
    @IBAction func disableCriterio3(_ sender: Any) {
        c3IsAvalible = (sender as! UISwitch).isOn
        crit3User.removeAll()
        tableView.reloadData()
        if !c1IsAvalible && !c2IsAvalible && !c3IsAvalible{
            item1.isEnabled = false
        }else{
            item1.isEnabled = true
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension RateDayViewController: UITableViewDelegate, UITableViewDataSource{
    
    func criterio1(_ sender: Any){
        print((sender as! UIButton).tag)
         let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        for index in 1...10{
            let a = UIAlertAction(title: "\(index   )", style: .default, handler: { (action) in
                let studnt = self.students.filter{ $0.uuid == (sender as! UIButton).accessibilityIdentifier }.first
                studnt?.criterios[2].calif = Double((action.title! as NSString).floatValue)
                (sender as! UIButton).setTitle("\(action.title!).0", for: .normal)
                (sender as! UIButton).imageEdgeInsets = UIEdgeInsets(top: 0.0, left: 15, bottom: 0.0, right: 0)
            })
            alert.addAction(a)
        }
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = sender as! UIView
            presenter.sourceRect = (sender as! UIButton).bounds
        }
        
        self.present(alert, animated: true) {
            
        }
    }
    
    func criterio2(_ sender: Any){
        print((sender as! UIButton).tag)
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        for index in 1...10{
            let a = UIAlertAction(title: "\(index)", style: .default, handler: { (action) in
                let studnt = self.students.filter{ $0.uuid == (sender as! UIButton).accessibilityIdentifier }.first
                studnt?.criterios[3].calif = Double((action.title! as NSString).floatValue)
                (sender as! UIButton).setTitle("\(action.title!).0", for: .normal)
                (sender as! UIButton).imageEdgeInsets = UIEdgeInsets(top: 0.0, left: 15, bottom: 0.0, right: 0)
            })
            alert.addAction(a)
        }
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = sender as! UIView
            presenter.sourceRect = (sender as! UIButton).bounds
        }
        
        self.present(alert, animated: true) {
            
        }
    }
    
    func criterio3(_ sender: Any){
        print((sender as! UIButton).tag)
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        for index in 1...10{
            let a = UIAlertAction(title: "\(index   )", style: .default, handler: { (action) in
                let studnt = self.students.filter{ $0.uuid == (sender as! UIButton).accessibilityIdentifier }.first
                studnt?.criterios[4].calif = Double((action.title! as NSString).floatValue)
                (sender as! UIButton).setTitle("\(action.title!).0", for: .normal)

            })
            alert.addAction(a)
        }
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = sender as! UIView
            presenter.sourceRect = (sender as! UIButton).bounds
        }
        
        self.present(alert, animated: true) {
            
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return students.count + 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "studentCell") as! StudentRateDayTableViewCell
        if indexPath.section == students.count{
            return cell
        }
        let student = students[indexPath.section]
        cell.student = student
        if let imag = student.img{
            cell.imgUser.image = imag
        }else{
            cell.imgUser.image = nil
            let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
            let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
            let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
            if let dirPath          = paths.first
            {
                let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("\(student.uuid!).jpg")
                let image    = UIImage(contentsOfFile: imageURL.path)
                if image != nil{
                    student.img = image
                }else{
                    student.img = nil
                }
            }else{
                student.img = nil
            }
            if student.img != nil{
                if let urlString = student.imgUrl ,!urlString.isEmpty {
                    switch urlString {
                    case "emoti1.jpg":
                        student.img = #imageLiteral(resourceName: "avatar1G")
                        cell.imgUser.image = #imageLiteral(resourceName: "avatar1G")
                        break
                    case "emoti2.jpg":
                        student.img = #imageLiteral(resourceName: "avatar2G")
                        cell.imgUser.image = #imageLiteral(resourceName: "avatar2G")
                        break
                    case "emoti3.jpg":
                        student.img = #imageLiteral(resourceName: "avatar3G")
                        cell.imgUser.image = #imageLiteral(resourceName: "avatar3G")
                        break
                    case "emoti4.jpg":
                        student.img = #imageLiteral(resourceName: "avatar4G")
                        cell.imgUser.image = #imageLiteral(resourceName: "avatar4G")
                        break
                    case "emoti5.jpg":
                        student.img = #imageLiteral(resourceName: "avatar5G")
                        cell.imgUser.image = #imageLiteral(resourceName: "avatar5G")
                        break
                    case "emoti6.jpg":
                        student.img = #imageLiteral(resourceName: "avatar6G")
                        cell.imgUser.image = #imageLiteral(resourceName: "avatar6G")
                        break
                    case "emoti7.jpg":
                        student.img = #imageLiteral(resourceName: "avatar7G")
                        cell.imgUser.image = #imageLiteral(resourceName: "avatar7G")
                        break
                    case "emoti8.jpg":
                        student.img = #imageLiteral(resourceName: "avatar8G")
                        cell.imgUser.image = #imageLiteral(resourceName: "avatar8G")
                        break
                    case "emoti9.jpg":
                        student.img = #imageLiteral(resourceName: "avatar9G")
                        cell.imgUser.image = #imageLiteral(resourceName: "avatar9G")
                        break
                    default:
                        let request = Request(url: URL(string:urlString)!)
                        
                        Nuke.loadImage(with: request, into: cell.imgUser) { [weak view] response, _ in
                            //                view?.image = response.value
                            if let error = response.error{
                                cell.imgUser.backgroundColor = UIColor(red: 6.0/255.0, green: 169.0/255.0, blue: 244.0/255.0, alpha: 1)
                            }else{
                                print(response.value!)
                                let resizedImage = Toucan.Resize.resizeImage(response.value!, size: CGSize(width: 200, height: 200))
                                cell.imgUser.image = resizedImage
                                student.img = resizedImage
                                _ = self.saveImage(resizedImage, path: self.fileInDocumentsDirectory("\(student.uuid!).jpg"))
                                
                            }
                        }
                        break
                    }
                }
            }
        }


        


        if c1IsAvalible{
            cell.btnCriterio1.semanticContentAttribute = .forceRightToLeft
            cell.btnCriterio1.setTitle("\(student.criterios[2].calif)", for: .normal)
            cell.btnCriterio1.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: 15, bottom: 0.0, right: 0)
            cell.btnCriterio1.isEnabled = true

            if crit1User.contains(student){
                cell.btnCriterio1.isSelected = true
            }else{
                cell.btnCriterio1.isSelected = false
            }
        }else{
            cell.btnCriterio1.isEnabled = false
            cell.btnCriterio1.isSelected = false
        }
        if c2IsAvalible{
            cell.btnCriterio2.semanticContentAttribute = .forceRightToLeft
            cell.btnCriterio2.setTitle("\(student.criterios[3].calif)", for: .normal)
            cell.btnCriterio2.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: 15, bottom: 0.0, right: 0)
            cell.btnCriterio2.isEnabled = true
            if crit2User.contains(student){
                cell.btnCriterio2.isSelected = true
            }else{
                cell.btnCriterio2.isSelected = false
            }
        }else{
            cell.btnCriterio2.isEnabled = false
            cell.btnCriterio2.isSelected = false
        }
        if c3IsAvalible{
            
            cell.btnCriterio3.semanticContentAttribute = .forceRightToLeft
            cell.btnCriterio3.setTitle("\(student.criterios[4].calif)", for: .normal)
            cell.btnCriterio3.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: 15, bottom: 0.0, right: 0)
            cell.btnCriterio3.isEnabled = true
            if crit3User.contains(student){
                cell.btnCriterio3.isSelected = true
            }else{
                cell.btnCriterio3.isSelected = false
            }
        }else{
            cell.btnCriterio3.isEnabled = false
            cell.btnCriterio3.isSelected = false
        }
        cell.btnCriterio1.addTarget(self, action: #selector(self.criterio1(_:)), for: .touchUpInside)
        cell.btnCriterio1.accessibilityIdentifier = student.uuid
        cell.btnCriterio2.addTarget(self, action: #selector(self.criterio2(_:)), for: .touchUpInside)
        cell.btnCriterio2.accessibilityIdentifier = student.uuid
        cell.btnCriterio3.addTarget(self, action: #selector(self.criterio3(_:)), for: .touchUpInside)
        cell.btnCriterio3.accessibilityIdentifier = student.uuid
        cell.nameLabel.text = student.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == students.count{
            return 0
        }
        return 123
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor(red: 6.0/255.0, green: 169.0/255.0, blue: 244.0/255.0, alpha: 1)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 3
    }
    
}

extension RateDayViewController: teacherDelegate{
    func didSuccessRateDay() {
        self.stopSpinner()
        let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.stopSpinner()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func didFailRateDay(message: String) {
        self.stopSpinner()
        let errorvc = self.errorAlert(message: message)
        self.present(errorvc, animated: true) { 
            
        }
    }
    
    func didSuccessGetReportByDay(students: [Student]) {
        self.stopSpinner()
        if students.count == 0{
            self.students = students
        }else{
            self.students = students
        }
        for s in students{
            switch s.criterios.count {
            case 5:
                criterio1Label.text = s.criterios[2].name
                criterio2Label.text = s.criterios[3].name
                criterio3Label.text = s.criterios[4].name
                s.criterios[2].uuid = classe.criterios[2].uuid
                s.criterios[3].uuid = classe.criterios[3].uuid
                s.criterios[4].uuid = classe.criterios[4].uuid
                s.criterios[2].dbid = classe.criterios[2].dbid
                s.criterios[3].dbid = classe.criterios[3].dbid
                s.criterios[4].dbid = classe.criterios[4].dbid
                break
            case 4:
                criterio1Label.text = s.criterios[2].name
                criterio2Label.text = s.criterios[3].name
                s.criterios[2].uuid = classe.criterios[2].uuid
                s.criterios[3].uuid = classe.criterios[3].uuid
                s.criterios[2].dbid = classe.criterios[2].dbid
                s.criterios[3].dbid = classe.criterios[3].dbid
                break
            case 3:
                criterio1Label.text = s.criterios[2].name
                s.criterios[2].uuid = classe.criterios[2].uuid
                s.criterios[2].dbid = classe.criterios[2].dbid
                break
            default: break
            }
        }
        tableView.reloadData()
    }
    
    func didFailGetReportByDay(message: String) {
        self.stopSpinner()
        let errorvc = self.errorAlert(message: message)
        self.present(errorvc, animated: true) {
            
        }
        self.students = classe.students
        tableView.reloadData()
    }

}
