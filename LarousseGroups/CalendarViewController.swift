//
//  CalendarViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso on 23/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import CVCalendar

protocol calendarDeleate {
    func selectedDay(date: CVDate)
}

class CalendarViewController: UIViewController, CVCalendarViewDelegate, CVCalendarMenuViewDelegate {

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
    }
    var delegate: calendarDeleate?
    @IBOutlet weak var menuView: CVCalendarMenuView!
    @IBOutlet weak var calendarView: CVCalendarView!
    @IBOutlet weak var monthLabel: UILabel!
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        menuView.delegate = self
        calendarView.delegate = self
        calendarView.calendarAppearanceDelegate = self
        menuView.commitMenuViewUpdate()
        calendarView.commitCalendarViewUpdate()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let cvdat = CVDate(date: Date(), calendar: .current)
        monthLabel.text = cvdat.globalDescription
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func presentationMode() -> CalendarMode {
        return CVCalendarViewPresentationMode.monthView
    }
    
    func firstWeekday() -> Weekday {
        return CVCalendarWeekday.sunday
    }
    
    func shouldShowWeekdaysOut() -> Bool {
        return false
    }
    
    func shouldAutoSelectDayOnMonthChange() -> Bool {
        return false
    }
    
    func didSelectDayView(_ dayView: DayView, animationDidFinish: Bool) {
        print(dayView.date)
        print(dayView.isOut)
        if self.navigationController != nil{
            self.delegate?.selectedDay(date: dayView.date)
        }else{
            self.dismiss(animated: false) {
                self.delegate?.selectedDay(date: dayView.date)
            }
        }
    }
    
    func presentedDateUpdated(_ date: CVDate) {
        print(date)
        monthLabel.text = "\(date.globalDescription)"
    }
    
    func dayLabelWeekdayOutTextColor() -> UIColor {
        return UIColor.red
    }
    
    @IBAction func nextMonth(_ sender: Any) {
        calendarView.loadNextView()
    }
    
    @IBAction func prevMonth(_ sender: Any) {
        calendarView.loadPreviousView()
    }
    
    @IBAction func nextYear(_ sender: Any) {
        let date = Calendar.current.date(byAdding: .year, value: 1, to: calendarView.presentedDate.convertedDate(calendar: .current)!)
        calendarView.toggleViewWithDate(date!)
    }
    
    @IBAction func prevYear(_ sender: Any) {
        let date = Calendar.current.date(byAdding: .year, value: -1, to: calendarView.presentedDate.convertedDate(calendar: .current)!)
        calendarView.toggleViewWithDate(date!)
    }
}

extension CalendarViewController : CVCalendarViewAppearanceDelegate{
    
}
