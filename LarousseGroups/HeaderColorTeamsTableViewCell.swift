//
//  HeaderColorTeamsTableViewCell.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 07/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

class HeaderColorTeamsTableViewCell: UITableViewCell {

    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var imgHeadr: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgHeadr.clipsToBounds = true
        imgHeadr.layer.cornerRadius = imgHeadr.frame.size.width / 2
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
