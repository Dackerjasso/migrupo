//
//  EditPeriodViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 12/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import CVCalendar

class EditPeriodViewController: UIViewController {

    @IBOutlet weak var startDateTF: UITextField!
    @IBOutlet weak var endDateTF: UITextField!
    var star: Bool!
    var startDate: CVDate!
    var endDate: CVDate!
    var classe : Class!
    var period: Period!
    @IBOutlet weak var titleLabel: UILabel!
    var edit = false
    var tWS = TeacherWS()
    @IBOutlet weak var notButton: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
        if !internetProvider.sharedInstance.isVertical {
            print("Landscape")
            titleLabel.font = UIFont.boldSystemFont(ofSize: 28)
        } else {
            titleLabel.font = UIFont.boldSystemFont(ofSize: 50)
        }
        self.view.layoutIfNeeded()
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            internetProvider.sharedInstance.isVertical = false
            titleLabel.font = UIFont.boldSystemFont(ofSize: 28)
        } else {
            internetProvider.sharedInstance.isVertical = true
            titleLabel.font = UIFont.boldSystemFont(ofSize: 50)
            
        }
        self.view.layoutIfNeeded()
        
    }
    
    func back(sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go back to the previous ViewController
        if edit{
            let error = UIAlertController(title: nil, message: Localization("LostData"), preferredStyle: .alert)
            let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default) { (action) in
                _ = self.navigationController?.popToRootViewController(animated: true)
            }
            let cancelAction = UIAlertAction(title: Localization("Cancelar"), style: .cancel) { (action) in
                
            }
            error.addAction(okAction)
            error.addAction(cancelAction)
            self.present(error, animated: true) {
                
            }
        }else{
            _ = self.navigationController?.popViewController(animated: true)
        }
        
    }
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var activateNotify: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        notButton.isSelected = period.notifications
        tWS.delegate = self
        titleLabel.text = Localization("EditPeriod")
        startDateTF.placeholder = Localization("StartDate")
        endDateTF.placeholder = Localization("EndDate")
        acceptButton.setTitle(Localization("Aceptar"), for: .normal)
        activateNotify.text = Localization("ActivatedNotify")
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(self.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        startDateTF.setLeftPaddingPoints(30)
        startDateTF.setRightPaddingPoints(30)
        endDateTF.setLeftPaddingPoints(30)
        endDateTF.setRightPaddingPoints(30)
        endDateTF.setRightPaddingPoints(10, image: #imageLiteral(resourceName: "flecha3"))
        startDateTF.setRightPaddingPoints(10, image: #imageLiteral(resourceName: "flecha3"))

        startDateTF.text = period.startDate.commonDescription
        endDateTF.text = period.endDate.commonDescription
        startDate = period.startDate
        endDate = period.endDate
        
        
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func starDateAction(_ sender: Any) {
        let calendarStoryBoard = UIStoryboard(name: "Calendar", bundle: nil)
        let cvc = calendarStoryBoard.instantiateInitialViewController() as! CalendarJTViewController
        cvc.modalPresentationStyle = .overCurrentContext
        cvc.modalTransitionStyle = .crossDissolve
        cvc.delegate = self
        star = true
        self.present(cvc, animated: true) {
            
        }
    }

    @IBAction func endDateAction(_ sender: Any) {
        let calendarStoryBoard = UIStoryboard(name: "Calendar", bundle: nil)
        let cvc = calendarStoryBoard.instantiateInitialViewController() as! CalendarJTViewController
        cvc.modalPresentationStyle = .overCurrentContext
        cvc.modalTransitionStyle = .crossDissolve
        cvc.delegate = self
        star = false
        self.present(cvc, animated: true) {
            
        }
    }
    @IBAction func acceptAction(_ sender: Any) {
        if self.endDate.convertedDate(calendar: .current)! < self.startDate.convertedDate(calendar: .current)!{
            let alert = UIAlertController(title: nil, message: Localization("AlertDates"), preferredStyle: .alert)
            let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default, handler: { (action) in
                
            })
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: {
                
            })
            return
        }
        period.notifications = notButton.isSelected
        switch currentReachabilityStatus {
        case .notReachable:
        
            break
        case .reachableViaWiFi, .reachableViaWWAN:
            break
        }
        var can = true
        for p in classe.periods{
            if p.uuid == period.uuid{
                continue
            }else{
                let start = p.startDate.convertedDate(calendar: .current)
                let end = p.endDate.convertedDate(calendar: .current)
                let start1 = Calendar.current.date(byAdding: .day, value: 0, to: start!)
                let end1 = Calendar.current.date(byAdding: .day, value: 0, to: end!)
                if ((start1!...end1!).contains(self.startDate.convertedDate(calendar: .current)!)){//errorPeriodsDate
                    self.stopSpinner()
                    let alert = UIAlertController(title: nil, message: Localization("errorPeriodsDate"), preferredStyle: .alert)
                    let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default, handler: { (action) in
                        
                    })
                    alert.addAction(okAction)
                    self.present(alert, animated: true, completion: {
                        
                    })
                    can = false
                    return
                }else if ((start1!...end1!).contains(self.endDate.convertedDate(calendar: .current)!)){
                    self.stopSpinner()
                    let alert = UIAlertController(title: nil, message: Localization("errorPeriodsDate"), preferredStyle: .alert)
                    let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default, handler: { (action) in
                        
                    })
                    alert.addAction(okAction)
                    self.present(alert, animated: true, completion: {
                        
                    })
                    can = false
                    return
                }
                
                if ((self.startDate.convertedDate(calendar: .current)!...self.endDate.convertedDate(calendar: .current)!).contains(start1!)){//errorPeriodsDate
                    self.stopSpinner()
                    let alert = UIAlertController(title: nil, message: Localization("errorPeriodsDate"), preferredStyle: .alert)
                    let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default, handler: { (action) in
                        
                    })
                    alert.addAction(okAction)
                    self.present(alert, animated: true, completion: {
                        
                    })
                    can = false
                    return
                }else  if ((self.startDate.convertedDate(calendar: .current)!...self.endDate.convertedDate(calendar: .current)!).contains(end1!)) {
                    self.stopSpinner()
                    let alert = UIAlertController(title: nil, message: Localization("errorPeriodsDate"), preferredStyle: .alert)
                    let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default, handler: { (action) in
                        
                    })
                    alert.addAction(okAction)
                    self.present(alert, animated: true, completion: {
                        
                    })
                    can = false
                    return
                }
            }
        }
        if can{
            period.startDate = self.startDate
            period.endDate = self.endDate
            self.startSpinner()
            tWS.update(period: period, strtDate: startDate, ndDate: endDate, classe: classe)
        }

    }
    
    @IBAction func notificationsAction(_ sender: Any) {
        (sender as! UIButton).isSelected = !(sender as! UIButton).isSelected
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}

extension EditPeriodViewController: teacherDelegate{
    
    func didSuccessUpdate(period: Period) {
        period.endDate = self.endDate
        period.startDate = self.startDate
        let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.stopSpinner()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func didFailUpdatePeriod(message: String) {
        self.stopSpinner()
        let error = self.errorAlert(message: message)
        self.present(error, animated: true) { 
            
        }
    }
    
    
}


extension EditPeriodViewController: calendarDeleate{
    func selectedDay(date: CVDate) {
        edit = true
        if star{
            startDateTF.text = date.commonDescription
            startDate = date
        }else{
            endDateTF.text = date.commonDescription
            endDate = date
        }
    }
}
