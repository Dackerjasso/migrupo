//
//  StudentSeeListTableViewCell.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 30/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
protocol StudentListDelegate {
    func customAction(tag: Int)
}

class StudentSeeListTableViewCell: UITableViewCell {
    var delegate: StudentListDelegate?
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var fondoImg: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var button: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        userImg.clipsToBounds = true
        userImg.layer.cornerRadius = userImg.frame.size.width / 2
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
            userImg.backgroundColor = UIColor(red: 6.0/255.0, green: 169.0/255.0, blue: 244.0/255.0, alpha: 1)
    }
    
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        
            userImg.backgroundColor = UIColor(red: 6.0/255.0, green: 169.0/255.0, blue: 244.0/255.0, alpha: 1)
    }
    

    @IBAction func action(_ sender: Any) {
        self.delegate?.customAction(tag: button.tag)
    }
}
