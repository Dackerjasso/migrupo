//
//  LogoutViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso on 25/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

protocol logoutDelegate {
    func logout()
}

class LogoutViewController: UIViewController {
    var delegate: logoutDelegate?
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var acceptButton: UIButton!
    
    @IBOutlet weak var titileLabel: UILabel!
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titileLabel.text = Localization("LogOutQ")
        cancelButton.setTitle(Localization("Cancelar"), for: .normal)
        acceptButton.setTitle(Localization("Aceptar"), for: .normal)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: false) {
            
        }
    }

    @IBAction func okAction(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.logout()
        }
    }
    
    @IBAction func tapAction(_ sender: Any) {
        self.dismiss(animated: false) {
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
