//
//  HeaderReportPeriodTableViewCell.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 04/07/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

class HeaderReportPeriodTableViewCell: UITableViewCell {

    @IBOutlet weak var StudentLabel: UILabel!
    @IBOutlet weak var AsistenceLabel: UILabel!
    @IBOutlet weak var criterioLabel3: UILabel!
    @IBOutlet weak var criterioLabel4: UILabel!
    @IBOutlet weak var ParticipationLabel: UILabel!
    @IBOutlet weak var criterioLabel5: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        ParticipationLabel.text = Localization("Participation")
        StudentLabel.text = Localization("Student")
        AsistenceLabel.text = Localization("Asistence")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
