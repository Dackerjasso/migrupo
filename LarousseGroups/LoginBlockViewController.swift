//
//  LoginBlockViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso on 23/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import Nuke
import IQKeyboardManagerSwift
import SQLite
import Toucan

protocol loginBlockViewDelegate {
    func loginWith(teacher: Teacher)
}

class LoginBlockViewController: UIViewController {
    var delegate : loginBlockViewDelegate?
    
    @IBOutlet weak var imageLabel: UIButton!
    let defaults = UserDefaults()
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var imgTeacher: UIImageView!
    @IBOutlet weak var  passwordTF: UITextField!
    var isBlockedTime = false
    let tWS = TeacherWS()
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            internetProvider.sharedInstance.isVertical = false
        } else {
            internetProvider.sharedInstance.isVertical = true
        }
        self.view.layoutIfNeeded()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        let defaults = UserDefaults()
       
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
        if isBlockedTime{
            let svcc = self.storyboard?.instantiateViewController(withIdentifier: "screenSaverView") as! ScreenSaverViewController
            self.present(svcc, animated: true, completion: { 
                self.isBlockedTime = false
            })
        }
        forgotButton.setTitle(Localization("Forgot"), for: .normal)
        imageLabel.setTitle(Localization("StartSesionOther"), for: .normal)
        emailTF.placeholder = Localization("Email")
        passwordTF.placeholder = Localization("Password")
        acceptButton.setTitle(Localization("Aceptar"), for: .normal)
        if let x = defaults.object(forKey: "logued"){
            if x as! String == "0"{
                let mst = UIStoryboard(name: "Main", bundle: nil)
                let vc = mst.instantiateInitialViewController()
                self.present(vc!, animated: true, completion: {
                    
                    
                })
            }
        }
    }
    
    func keyboardWillShow(notification: NSNotification) {
        print("show")
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            if self.view.frame.origin.y == 0{
//                self.view.frame.origin.y -= keyboardSize.height
//            }
//        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        print("hidde")
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            if self.view.frame.origin.y != 0{
//                self.view.frame.origin.y += keyboardSize.height
//            }
//        }
    }
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var forgotButton: UIButton!
    
    func backNot(){
        self.dismissAction(UIButton())
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.backNot), name: NSNotification.Name(rawValue: "BackPop"), object: nil)
        imgTeacher.clipsToBounds = true
        imgTeacher.layer.cornerRadius = imgTeacher.frame.size.width / 2
        imgTeacher.image = #imageLiteral(resourceName: "espacioFoto")
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            if let uuidString = defaults.object(forKey: "uuid") as? String{
                let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("user\(uuidString).jpg")
                let image    = UIImage(contentsOfFile: imageURL.path)
                imgTeacher.image = image
                if image != nil{
                    UserProvider.sharedInstance.image = image
                }else{
                    imgTeacher.image = #imageLiteral(resourceName: "espacioFoto")
                }
            }else{
                imgTeacher.image = #imageLiteral(resourceName: "espacioFoto")
            }

            // Do whatever you want with the image
        }


        tWS.delegate = self
        emailTF.text = defaults.object(forKey: "email") as? String
        emailTF.setLeftPaddingPoints(30)
        emailTF.setRightPaddingPoints(30)
        passwordTF.setLeftPaddingPoints(30)
        passwordTF.setRightPaddingPoints(30)
        passwordTF.setRightPaddingPoints(30, image: #imageLiteral(resourceName: "check1"))
        emailTF.setRightPaddingPoints(30, image: #imageLiteral(resourceName: "check1"))
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func dismissAction(_ sender: Any) {
        if let dbid = UserProvider.sharedInstance.uuid{
            UserProvider.sharedInstance.classes = []
            UserProvider.sharedInstance.dbid = 0
            UserProvider.sharedInstance.dbidString = ""
            UserProvider.sharedInstance.email = ""
            UserProvider.sharedInstance.image = nil
            UserProvider.sharedInstance.imageUrl = ""
            UserProvider.sharedInstance.isnew = false
            UserProvider.sharedInstance.lastName = ""
            UserProvider.sharedInstance.name = ""
            UserProvider.sharedInstance.password = ""
            UserProvider.sharedInstance.schoolName = ""
            UserProvider.sharedInstance.token = ""
            UserProvider.sharedInstance.uuid = ""
            UserProvider.sharedInstance.localPath = nil
            let defaults = UserDefaults()
            defaults.set("0", forKey: "logued")
            defaults.set("", forKey: "password")
            defaults.set("", forKey: "email")
            defaults.set("", forKey: "uuid")
            defaults.synchronize()
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "VC") as! ViewController
            self.present(vc, animated: true, completion: {
                self.navigationController?.popToRootViewController(animated: false)
            })
        }else{
            self.dismiss(animated: true) {
                let defaults = UserDefaults()
                defaults.set("0", forKey: "logued")
                defaults.set("", forKey: "password")
                defaults.set("", forKey: "email")
                defaults.set("", forKey: "uuid")
                defaults.synchronize()
            }
        }

    }
    
    @IBAction func okAction(_ sender: Any) {
        
        let email = defaults.object(forKey: "email") as! String
        let pass = defaults.object(forKey: "password") as! String
        if emailTF.text == email && pass == passwordTF.text{
            if isBlockedTime{
                self.dismiss(animated: true, completion: { 
                    
                })
            }else{
                let teacher = Teacher()
                teacher.email = emailTF.text
                teacher.password = passwordTF.text
                tWS.login(teacher: teacher)
                self.startSpinner()
                self.view.isUserInteractionEnabled = false
            }
        }else{
            let alert = self.errorAlert(message: Localization("MailOrPasswordErrorAlert"))
            self.present(alert, animated: true) {
                
            }
        }
    }
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LoginBlockViewController: teacherDelegate{
    func didSuccessLogin(teacher: Teacher) {
        self.view.isUserInteractionEnabled = true
        self.dismiss(animated: false) {
            self.defaults.set(teacher.email, forKey: "email")
            self.defaults.set(teacher.password, forKey: "password")
            self.defaults.set(teacher.uuid, forKey: "uuid")
            self.defaults.set("1", forKey: "logued")
            self.defaults.synchronize()
            self.tWS.getClassesByDB()
        }
    }
    
    func didSuccessGet(classes: [Class]) {
        self.stopSpinner()
        let rootSB = UIStoryboard(name: "Root", bundle: nil)
        let rvc = rootSB.instantiateViewController(withIdentifier: "rootView") as! RootViewController
        
        
        let navController = UINavigationController.init(rootViewController: rvc)
        
        self.present(navController, animated: true, completion: nil)
        self.delegate?.loginWith(teacher: UserProvider.sharedInstance)
    }
    
    func didFailGetClasses(statusCode: Int, message: String!) {
        self.stopSpinner()
        print("error")
    }
    
    func didFailSuccessLogin(statusCode: Int, message: String!) {
        self.view.isUserInteractionEnabled = true
        self.stopSpinner()
        self.view.isUserInteractionEnabled = true
        let alert = self.errorAlert(message: message)
        self.present(alert, animated: true) {
            
        }
    }
}

extension LoginBlockViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 35 // Bool
    }
}
