//
//  CustomCell.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 16/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CustomCell: JTAppleCell {

    @IBOutlet weak var dateLabel : UILabel!
    
    @IBOutlet weak var selectedView: UIView!
    
    override func awakeFromNib() {
        selectedView.clipsToBounds = true
        selectedView.layer.cornerRadius = selectedView.frame.size.width / 2
    }

}
