//
//  AvatarsViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 02/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

protocol AvatarsDelegate {
    func selected(image: UIImage)
}

class AvatarsViewController: UIViewController {
    var delegate : AvatarsDelegate?
    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            titleLabel.font = UIFont.boldSystemFont(ofSize: 28)
        } else {
            titleLabel.font = UIFont.boldSystemFont(ofSize: 50)
        }
        self.view.layoutIfNeeded()
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            internetProvider.sharedInstance.isVertical = false
            titleLabel.font = UIFont.boldSystemFont(ofSize: 28)
        } else {
            internetProvider.sharedInstance.isVertical = true
            titleLabel.font = UIFont.boldSystemFont(ofSize: 50)
        }
        self.view.layoutIfNeeded()
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func action(_ sender: Any) {
        let button = sender as! UIButton
        print (button.tag)
        self.navigationController?.popViewController(animated: true)
        self.delegate?.selected(image: UIImage(named: "avatar\(button.tag)G")!)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
