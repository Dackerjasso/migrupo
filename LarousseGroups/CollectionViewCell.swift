//
//  CollectionViewCell.swift
//  InfiniteScrolling
//
//  Created by Vishal Singh on 1/22/17.
//  Copyright © 2017 Vishal Singh. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var content: UIView!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var fondo: UIImageView!
    var student : Student!
    override func awakeFromNib() {
        super.awakeFromNib()
        userImg.clipsToBounds = true
        userImg.layer.cornerRadius = userImg.frame.size.width / 2
        // Initialization code
    }

}
