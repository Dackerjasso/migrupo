//
//  StudentsTeamTableViewCell.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 09/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

class StudentsTeamTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imgUser: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        imgUser.clipsToBounds = true
        imgUser.layer.cornerRadius = imgUser.frame.size.width / 2
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if(selected) {
            imgUser.image = imgUser.image
            imgUser.backgroundColor = UIColor(red: 6.0/255.0, green: 169.0/255.0, blue: 244.0/255.0, alpha: 1)
        }
    }
    
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        
        if(highlighted) {
            imgUser.image = imgUser.image
            imgUser.backgroundColor = UIColor(red: 6.0/255.0, green: 169.0/255.0, blue: 244.0/255.0, alpha: 1)
        }
    }
    

    
}
