//
//  PopUpRemovePeriodViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 12/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

protocol RemoveAnyDelegate {
    func remove(period: Any)
}

class PopUpRemovePeriodViewController: UIViewController {
    var delegate: RemoveAnyDelegate?
    @IBOutlet weak var textLabel: UILabel!
    var period: Period!
    var classe: Class!
    override func viewWillAppear(_ animated: Bool) {
        if let p = period{
            print (p.startDate.commonDescription)
        }else{
            textLabel.attributedText = NSAttributedString(string:"¿Seguro que quieres \r\n eliminar esta \r\n clase?")
        }
    }
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var acceptButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textLabel.attributedText = NSAttributedString(string: Localization("DeletePeriod"))
        cancelButton.setTitle(Localization("Cancel"), for: .normal)
        acceptButton.setTitle(Localization("Aceptar"), for: .normal)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: false) {
            
        }
    }

    @IBAction func acceptAction(_ sender: Any) {
        self.dismiss(animated: false) {
            if let p = self.period{
                self.delegate?.remove(period: p)
            }else{
                self.delegate?.remove(period: self.classe)
            }
        }
    }
    
    @IBAction func tapAction(_ sender: Any) {
        self.dismiss(animated: false) {
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
