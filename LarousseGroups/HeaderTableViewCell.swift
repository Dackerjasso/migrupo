//
//  HeaderTableViewCell.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 27/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

class HeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var criterioLabel1: UILabel!
    @IBOutlet weak var criterioLabel2: UILabel!
    @IBOutlet weak var criterioLabel3: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
