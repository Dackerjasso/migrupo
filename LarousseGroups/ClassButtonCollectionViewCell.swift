//
//  ClassButtonCollectionViewCell.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso on 22/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import CircleMenu

class ClassButtonCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var button: CircleMenu!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var circleImg: UIImageView!
    var distance : Float!
    override func awakeFromNib() {
        circleImg.clipsToBounds = true
        circleImg.layer.cornerRadius = circleImg.frame.size.width / 1.1
        self.button.clipsToBounds = true
        self.button.layer.cornerRadius = self.button.frame.size.width / 1.1
        if let d = distance{
            button.distance = d
        }
    }
}
