//
//  NoStudentsTableViewCell.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 21/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

class NoStudentsTableViewCell: UITableViewCell {

    @IBOutlet weak var materiaLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var subtitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        subtitleLabel.attributedText = NSAttributedString(string: Localization("AddStudents"))
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
