//
//  CreateTeamsViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 06/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

protocol CreateTeamsDelegate {
    func selected(teams: [Team])
}

class CreateTeamsViewController: UIViewController, UITextFieldDelegate {
    var delegate : CreateTeamsDelegate?
    @IBOutlet weak var teamsOrStudentsTF: UITextField!
    @IBOutlet weak var numberTF: UITextField!
    var classe : Class!
    var teams : [Team] = []
    var colors : [Constants.colorClass] = []

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
    }
    @IBOutlet weak var acceptButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = Localization("MakeTeams")
        acceptButton.setTitle(Localization("Aceptar"), for: .normal)
        colors = [Constants.colorClass.red, Constants.colorClass.orange, Constants.colorClass.blue1, Constants.colorClass.pink, Constants.colorClass.purple, Constants.colorClass.yellow, Constants.colorClass.aqua, Constants.colorClass.green, Constants.colorClass.green2, Constants.colorClass.blue2, Constants.colorClass.purple2]
        
        
        teamsOrStudentsTF.setLeftPaddingPoints(30)
        teamsOrStudentsTF.setRightPaddingPoints(10, image: #imageLiteral(resourceName: "flecha3"))
        numberTF.setLeftPaddingPoints(30)
        numberTF.setRightPaddingPoints(30)
        teamsOrStudentsTF.text = Localization("Teams")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBAction func teamsOrStudentsAction(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let studentsAction = UIAlertAction(title: Localization("Students"), style: .default) { (action) in
            self.teamsOrStudentsTF.text = action.title
        }
        let teamsAction = UIAlertAction(title: Localization("Teams"), style: .default) { (action) in
            self.teamsOrStudentsTF.text = action.title
        }
        alert.addAction(teamsAction)
        alert.addAction(studentsAction)
        
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = teamsOrStudentsTF
            presenter.sourceRect = teamsOrStudentsTF.bounds
        }
        
        self.present(alert, animated: true) {
            
        }
    }
    
    
    @IBAction func acceptAction(_ sender: Any) {
        teams.removeAll()
        let copyClass = self.classe.copy() as! Class
        copyClass.students.shuffle()
        if (numberTF.text?.characters.count)! > 0 && Int(numberTF.text!)! <= classe.students.count{
            if teamsOrStudentsTF.text == Localization("Teams"){
                let total : Int = classe.students.count / Int(numberTF.text!)!
                var a = (classe.students.count / total)
                if a > 11{
                    a = 11
                }
                for index in 1...a{
                    let team = Team()
                    team.color = colors[index - 1]
                    team.name = team.color.nameTeam
                    let studentsAux = copyClass.students.prefix(total)
                    team.students.append(contentsOf: studentsAux)
                    let range = 0..<total
                    copyClass.students.removeSubrange(range)
                    self.teams.append(team)
                }
                if copyClass.students.count > 0{
                    while copyClass.students.count > 0 {
                        for index in 0...a - 1{
                            if copyClass.students.count > 0{
                                teams[index].students.append(copyClass.students[0])
                                copyClass.students.remove(at: 0)
                            }else{
                                break
                            }
                        }
                    }
                }
                self.dismiss(animated: false, completion: {
                    self.delegate?.selected(teams: self.teams)
                })
            }else{
                var total = classe.students.count / Int(numberTF.text!)!
                if total > 11 {
                    total = 11
                }
                let residuo = classe.students.count % Int(numberTF.text!)!
                print("\(total) Equipos - \(residuo) Sin equipo")
                for index in 1...total{
                    let team = Team()
                    team.color = colors[index - 1]
                    team.name = team.color.nameTeam
                    let studentsAux = copyClass.students.prefix(classe.students.count / total)
                    team.students.append(contentsOf: studentsAux)
                    let range = 0..<classe.students.count / total
                    copyClass.students.removeSubrange(range)
                    teams.append(team)
                }
                if copyClass.students.count > 0{
                    while copyClass.students.count > 0 {
                        for index in 0...copyClass.students.count - 1{
                            teams[index].students.append(copyClass.students[0])
                            copyClass.students.remove(at: 0)
                        }
                    }
                }
                self.dismiss(animated: false, completion: {
                    self.delegate?.selected(teams: self.teams)
                })
            }
        }else{
            let alert = self.errorAlert(message: Localization("ErrorNoTeams"))
            self.present(alert, animated: true, completion: { 
                
            })
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let aSet = NSCharacterSet(charactersIn:"0123456789").inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")
        return string == numberFiltered
    }
    @IBAction func dismissAction(_ sender: Any) {
        self.dismiss(animated: false) {
            
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
