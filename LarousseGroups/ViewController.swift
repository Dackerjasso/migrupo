//
//  ViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso on 22/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class ViewController: UIViewController {

    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    let tWS = TeacherWS()
    let defaults = UserDefaults()
    @IBOutlet weak var userTFConstraints: NSLayoutConstraint!
    @IBOutlet weak var viewLogo: UIView!
    @IBOutlet weak var upconstraint: NSLayoutConstraint!

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
        self.view.isUserInteractionEnabled = true
        forgotButton.setTitle(Localization("Forgot"), for: .normal)
        registerButton.setTitle(Localization("Register"), for: .normal)
        emailTF.placeholder = Localization("Email")
        passwordTF.placeholder = Localization("Password")
        loginButton.setTitle(Localization("StartSession"), for: .normal)

    }
    
    func keyboardWillShow(notification: NSNotification) {
       print("show")

    }
    
    func keyboardWillHide(notification: NSNotification) {
        print("hidde")

    }
    @IBOutlet weak var registerButton: UIButton!
    
    @IBOutlet weak var forgotButton: UIButton!
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            internetProvider.sharedInstance.isVertical = false
        } else {
            internetProvider.sharedInstance.isVertical = true
            
        }
        self.view.layoutIfNeeded()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tWS.hasInternet { (result) in
            internetProvider.sharedInstance.hasInternet = result
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        if let log = defaults.object(forKey: "logued"){
            if log as! String == "1"{
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "loginBlockView") as! LoginBlockViewController
                vc.delegate = self
                self.present(vc, animated: true, completion: {
                    self.viewLogo.alpha = 0
                })
            }else{
                viewLogo.alpha = 01
            }
        }else{
            viewLogo.alpha = 0
        }
        
        emailTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        passwordTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        tWS.delegate = self
        loginButton.isEnabled = false
        passwordTF.setLeftPaddingPoints(30)
        passwordTF.setRightPaddingPoints(30)
        emailTF.setLeftPaddingPoints(30)
        emailTF.setRightPaddingPoints(30)
        passwordTF.setRightPaddingPoints(30, image: #imageLiteral(resourceName: "check1"))
        emailTF.setRightPaddingPoints(30, image: #imageLiteral(resourceName: "check1"))
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func textFieldDidChange(_ textField: UITextField) {
        if emailTF.isValidEmail() && (passwordTF.text?.characters.count)! > 4{
            loginButton.isEnabled = true
        }else{
            loginButton.isEnabled = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func login(_ sender: Any) {
        let teacher = Teacher()
        teacher.email = emailTF.text
        teacher.password = passwordTF.text
        tWS.login(teacher: teacher)
        self.startSpinner()
        self.view.isUserInteractionEnabled = false
    }
}


extension ViewController: teacherDelegate{
    func didSuccessLogin(teacher: Teacher){
        self.stopSpinner()
        self.view.isUserInteractionEnabled = true
       print("Correcto")
        self.emailTF.text = ""
        self.passwordTF.text = ""
        self.view.isUserInteractionEnabled = true
        defaults.set(teacher.email, forKey: "email")
        defaults.set(teacher.uuid, forKey: "uuid")
        defaults.set(teacher.password, forKey: "password")
        defaults.set("1", forKey: "logued")
        defaults.synchronize()
        let rootSB = UIStoryboard(name: "Root", bundle: nil)
        let rvc = rootSB.instantiateViewController(withIdentifier: "rootView") as! RootViewController
        
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            if let uuidString = defaults.object(forKey: "uuid") as? String{
                let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("\(uuidString).jpg")
                let image    = UIImage(contentsOfFile: imageURL.path)
                UserProvider.sharedInstance.image = image
            }
            
            // Do whatever you want with the image
        }

        let navController = UINavigationController.init(rootViewController: rvc)
        
        present(navController, animated: true, completion: nil)
        
    }
    
    func didFailSuccessLogin(statusCode: Int, message: String!) {
        self.view.isUserInteractionEnabled = true
        self.stopSpinner()
        let alert = self.errorAlert(message: message)
        self.present(alert, animated: true) {
            self.view.isUserInteractionEnabled = true
        }
    }
}

extension ViewController: loginBlockViewDelegate{
    func loginWith(teacher: Teacher) {
        self.emailTF.text = ""
        self.passwordTF.text = ""
        didSuccessLogin(teacher: teacher)
        self.stopSpinner()
        
        self.view.isUserInteractionEnabled = true
    }
}

extension ViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 35 // Bool
    }
}
