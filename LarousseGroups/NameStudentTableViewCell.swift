//
//  NameStudentTableViewCell.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 29/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

protocol listDelegate {
    func actionListFor(student: Student, cell: NameStudentTableViewCell)
    func reloadTable()
}

class NameStudentTableViewCell: UITableViewCell {
    var delegate: listDelegate?
    @IBOutlet weak var userIMg: UIImageView!
    @IBOutlet weak var nameStudentLabel: UILabel!
    @IBOutlet weak var notHereButton: UIButton!
    @IBOutlet weak var almostNotHereButton: UIButton!
    @IBOutlet weak var hereButton: UIButton!
    var student: Student!
    var index : IndexPath!
    var xFromCenter: CGFloat = 0
    var initpoint = CGPoint(x: 0, y: 0)
    enum PanDirection {
        case vertical
        case horizontal
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        let gesture = UIPanGestureRecognizer(target: self, action: #selector(self.wasDragged(gesture:)))
        userIMg.addGestureRecognizer(gesture)
        userIMg.isUserInteractionEnabled = true
        initpoint = userIMg.center
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func wasDragged(gesture: UIPanGestureRecognizer) {
        // translation vector origin -> destination
        
        let translation = gesture.translation(in: self) // get the translation
        let label = gesture.view! // the view inside the gesture
        
        xFromCenter += translation.x
        
        
        // move the label with the translation
        label.center = CGPoint(x: label.center.x - translation.x, y: label.center.y - translation.y)
        
        // reset the translation that now, is already applied to the label
        gesture.setTranslation(CGPoint.zero, in: self)
        
        let point = gesture.location(in: self)
        
        if point.x > 600{
//            print("Is Here")
            UIView.animate(withDuration: 0.4, animations: {
                self.hereButton.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                self.almostNotHereButton.transform = CGAffineTransform.identity
                self.notHereButton.transform = CGAffineTransform.identity
            }, completion: nil)
        }
        
        if point.x < 165{
//            print("Is Not Here")
            UIView.animate(withDuration: 0.4, animations: {
                self.notHereButton.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                self.hereButton.transform = CGAffineTransform.identity
                self.almostNotHereButton.transform = CGAffineTransform.identity
            }, completion: nil)
        }
        
        
        if point.y < 190{
//            print("Is Almost Not Here")
            UIView.animate(withDuration: 0.4, animations: {
                self.almostNotHereButton.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                self.hereButton.transform = CGAffineTransform.identity
                self.notHereButton.transform = CGAffineTransform.identity
            }, completion: nil)
        }
        
        if point.x < 600 && point.x > 165 && point.y > 190{
            UIView.animate(withDuration: 0.2, animations: {
                self.almostNotHereButton.transform = CGAffineTransform.identity
                self.hereButton.transform = CGAffineTransform.identity
                self.notHereButton.transform = CGAffineTransform.identity
            }, completion: nil)
        }

        
        if gesture.state == UIGestureRecognizerState.ended {
            if point.x > 600{
                label.center = hereButton.center
                userIMg.center = hereButton.center
                student.isHere = false
                student.isAlmostNotHere = false
                label.center = initpoint
                self.delegate?.actionListFor(student: student, cell: self)
            }
            if point.x < 165{
                label.center = notHereButton.center
                userIMg.center = notHereButton.center
                student.isHere = true
                student.isAlmostNotHere = false
                label.center = initpoint
                self.delegate?.actionListFor(student: student, cell: self)
            }
            if point.y < 190{
                label.center = almostNotHereButton.center
                userIMg.center = almostNotHereButton.center
                student.isHere = false
                student.isAlmostNotHere = true
                label.center = initpoint
                self.delegate?.actionListFor(student: student, cell: self)
            }
            
            if point.x < 600 && point.x > 165 && point.y > 190{
                label.center = initpoint
            }
            // restore the inital value before drag the item
            
//            label.center = initpoint
            UIView.animate(withDuration: 0.2, animations: {
                self.almostNotHereButton.transform = CGAffineTransform.identity
                self.hereButton.transform = CGAffineTransform.identity
                self.notHereButton.transform = CGAffineTransform.identity
            }, completion: nil)

            
        }

    }



    @IBAction func hereAction(_ sender: Any) {
        
    }
    
    @IBAction func nopeHere(_ sender: Any) {
        
    }
    
    @IBAction func almostNopeHere(_ sender: Any) {
        
    }
}

extension UIPanGestureRecognizer {
    
    enum GestureDirection {
        case Up
        case Down
        case Left
        case Right
    }
    
    /// Get current vertical direction
    ///
    /// - Parameter target: view target
    /// - Returns: current direction
    func verticalDirection(target: UIView) -> GestureDirection {
        return self.velocity(in: target).y > 0 ? .Down : .Up
    }
    
    /// Get current horizontal direction
    ///
    /// - Parameter target: view target
    /// - Returns: current direction
    func horizontalDirection(target: UIView) -> GestureDirection {
        return self.velocity(in: target).x > 0 ? .Right : .Left
    }
    
    /// Get a tuple for current horizontal/vertical direction
    ///
    /// - Parameter target: view target
    /// - Returns: current direction
    func versus(target: UIView) -> (horizontal: GestureDirection, vertical: GestureDirection) {
        return (self.horizontalDirection(target: target), self.verticalDirection(target: target))
    }
    
}


