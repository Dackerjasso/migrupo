//
//  SpinnerViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 07/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

class SpinnerViewController: UIViewController {

    @IBOutlet weak var spinnerimg: UIImageView!
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
        let imgListArray :NSMutableArray = []
        for countValue in 1...10{
            
            let strImageName : String = "\(countValue)"
            let image  = UIImage(named:strImageName)
            imgListArray .add(image!)
        }
        
        self.spinnerimg.animationImages = (imgListArray as! [UIImage]);
        self.spinnerimg.animationDuration = 0.8
        self.spinnerimg.startAnimating()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.spinnerimg.stopAnimating()
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
