//
//  StudentRateDayTableViewCell.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 13/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

class StudentRateDayTableViewCell: UITableViewCell {
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var btnCriterio1: UIButton!
    @IBOutlet weak var btnCriterio2: UIButton!
    @IBOutlet weak var btnCriterio3: UIButton!
    var student : Student!

    override func awakeFromNib() {
        super.awakeFromNib()
        imgUser.clipsToBounds = true
        imgUser.layer.cornerRadius = imgUser.frame.size.width / 2
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
