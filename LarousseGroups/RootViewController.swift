//
//  RootViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso on 22/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import CircleMenu
import CVCalendar
import SwiftMessages
import Nuke
import SQLite
import Toucan

class RootViewController: UIViewController {
    
    let margin: CGFloat = 10
    @IBOutlet weak var buttonAddBottom: UIButton!
    let cellsPerRow = 2
    @IBOutlet weak var buttonAddUp: UIButton!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var imgTeacher: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var textToAdd: UILabel!
    @IBOutlet weak var topConstraintsBlur: NSLayoutConstraint!
    @IBOutlet weak var textHi: UILabel!
    @IBOutlet weak var topmenuConstraints: NSLayoutConstraint!
    var isLong = false
    @IBOutlet weak var menuView2: UIView!
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var blurView: UIView!
    var currentClass : Class!
    let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()
    var tWS = TeacherWS()
    var currentPeriod = 0
    var alert = true
    var sincronizando = false
    var sincronizado2 = false
    var sincronizar2 = false
    let sWS = Sincronizacion()
    @IBOutlet weak var helpMenuButton: UIButton!
    var typeCalendar = 0
    // MARK: - Notification methods
    
    let yourAttributes : [String : Any] = [
        NSForegroundColorAttributeName : UIColor.white,
        NSUnderlineStyleAttributeName : NSUnderlineStyle.styleSingle.rawValue]

    func receiveLanguageChangedNotification(notification:NSNotification) {
        if notification.name == kNotificationLanguageChanged {
            configureViewFromLocalisation()
            let defaults = UserDefaults()
        }
    }
    
    // MARK: - Memory management
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: kNotificationLanguageChanged, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        menuView.alpha = 0
//        menuView2.transform = CGAffineTransform.identity
    }
    
    @IBAction func helpMenuAction(_ sender: Any) {
        let url = URL(string: "http://migrupo.redlarousse.mx/manual-de-uso/")!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        sWS.delegate = self
//        tWS.getClassesByDB()
        self.stopSpinner()
        configureViewFromLocalisation()
        collectionView.reloadData()
//        if UIDevice.current.orientation.isLandscape {
//            print("Landscape")
//            UserProvider.sharedInstance.isVertical = false
//            let mutableString = NSMutableAttributedString(string: String(format: "¡%@ %@!",Localization("Saludo") , UserProvider.sharedInstance.name!), attributes: [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 30)])
//            textHi.attributedText = mutableString
//        } else {
//            UserProvider.sharedInstance.isVertical = true
//            let mutableString = NSMutableAttributedString(string: String(format: "¡%@ %@!",Localization("Saludo") , UserProvider.sharedInstance.name!), attributes: [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 50)])
//            textHi.attributedText = mutableString
//            
//        }
        if !internetProvider.sharedInstance.isVertical {
            print("Landscape")
            self.stackView.axis = .horizontal
            let mutableString = NSMutableAttributedString(string: String(format: "¡%@ %@!",Localization("Saludo") , UserProvider.sharedInstance.name!), attributes: [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 30)])
            textHi.attributedText = mutableString
        } else {
            self.stackView.axis = .vertical
            let mutableString = NSMutableAttributedString(string: String(format: "¡%@ %@!",Localization("Saludo") , UserProvider.sharedInstance.name!), attributes: [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 50)])
            textHi.attributedText = mutableString
        }
        self.view.layoutIfNeeded()
        if let imag = UserProvider.sharedInstance.image{
            imgTeacher.image = imag
        }
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.backgroundColor = UIColor(red: 239.0/255.0, green: 243.0/255.0, blue: 244.0/255.0, alpha: 1)

        if UserProvider.sharedInstance.classes.count == 0{
            self.textToAdd.alpha = 1
            self.buttonAddUp.alpha = 1
            self.buttonAddBottom.alpha = 0
            self.collectionView.alpha = 0
        }else{
            self.textToAdd.alpha = 0
            self.buttonAddUp.alpha = 0
            self.buttonAddBottom.alpha = 1
            self.collectionView.alpha = 1
        }
        collectionView.reloadData()
    }
    
    func configureViewFromLocalisation() {
//        helpMenuButton.setAttributedTitle(NSAttributedString(string: Localization("menuAyuda")), for: .normal)
        let attributeString = NSMutableAttributedString(string: Localization("menuAyuda"),
                                                        attributes: yourAttributes)
        helpMenuButton.setAttributedTitle(attributeString, for: .normal)
        if !internetProvider.sharedInstance.isVertical {
            let mutableString = NSMutableAttributedString(string: String(format: "¡%@ %@!",Localization("Saludo") , UserProvider.sharedInstance.name!), attributes: [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 30)])
            textHi.attributedText = mutableString
        } else {
            let mutableString = NSMutableAttributedString(string: String(format: "¡%@ %@!",Localization("Saludo") , UserProvider.sharedInstance.name!), attributes: [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 50)])
            textHi.attributedText = mutableString
        }
        toAddStudent.attributedText = NSAttributedString(string: Localization("ToAddStudents"))
        ToseeCLass.attributedText = NSAttributedString(string: Localization("ToseeMaterias"))
        toSelect.attributedText = NSAttributedString(string: Localization("ToSelect"))
        toAddStudent.textAlignment = .center
        ToseeCLass.textAlignment = .center
        toSelect.textAlignment = .center

        assignLabel.text = Localization("AsignStudents")
        textToAdd.attributedText = NSAttributedString(string: Localization("MakeClassA"))
        editProfileButton.setTitle(Localization("EditProfile"), for: .normal)
        changeLanguageButton.setTitle(Localization("ChangeLanguaget"), for: .normal)
        logOutButton.setTitle(Localization("Logout"), for: .normal)

        
    }
    
    func runAlert(title: String?, body: String?){
        if alert{
            let view = MessageView.viewFromNib(layout: .CardView)
            view.configureTheme(.warning)
            view.configureDropShadow()
            var config = SwiftMessages.Config()
            config.duration = .seconds(seconds: 5)
            config.dimMode = .gray(interactive: true)
            view.button?.alpha = 0
            view.configureContent(title: title!, body: body!)
            //            SwiftMessages.sho
            SwiftMessages.pauseBetweenMessages = 0.5
            SwiftMessages.show(config: config, view: view)
        }
    }
    
    
    func reloadClasses(){
        alert = false
        self.stopSpinner()
        self.textToAdd.alpha = 0
        self.buttonAddUp.alpha = 0
        UserProvider.sharedInstance.classes.removeAll()
        tWS.getClasses()
        collectionView.reloadData()
    }
    
    @IBOutlet weak var assignLabel: UILabel!
    @IBOutlet weak var editProfileButton: UIButton!
    @IBOutlet weak var changeLanguageButton: UIButton!
    @IBOutlet weak var logOutButton: UIButton!
    @IBOutlet weak var toAddStudent: UILabel!
    @IBOutlet weak var ToseeCLass: UILabel!
    @IBOutlet weak var toSelect: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()


        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("user\(UserProvider.sharedInstance.uuid!).jpg")
            let image    = UIImage(contentsOfFile: imageURL.path)
            imgTeacher.image = image
            if image != nil{
                UserProvider.sharedInstance.image = image
                self.tWS.updateImage(url: imageURL, student: nil)
            }else{
                imgTeacher.image = #imageLiteral(resourceName: "espacioFoto")
            }
            
            
            // Do whatever you want with the image
        }
        
        
        
        
        
        if let imag = UserProvider.sharedInstance.image{
            imgTeacher.image = imag
            if let urlString = UserProvider.sharedInstance.imageUrl,!urlString.isEmpty {
                let request = Request(url: URL(string:urlString)!)
                
//                Nuke.loadImage(with: request, into: imgTeacher) { [weak view] response, _ in
//                    //                view?.image = response.value
//                    if let error = response.error{
//                        
//                    }else{
//                        let resizedImage = Toucan.Resize.resizeImage(response.value!, size: CGSize(width: 200, height: 200))
//                        self.imgTeacher.image = resizedImage
//                        UserProvider.sharedInstance.image = resizedImage
//                        _ = self.saveImage(resizedImage, path: self.fileInDocumentsDirectory("user\(UserProvider.sharedInstance.uuid!).jpg"))
//                        
//                    }
//                }
            }
        }else{
            imgTeacher.image = #imageLiteral(resourceName: "espacioFoto")
            if let urlString = UserProvider.sharedInstance.imageUrl,!urlString.isEmpty {
                let request = Request(url: URL(string:urlString)!)
                
//                Nuke.loadImage(with: request, into: imgTeacher) { [weak view] response, _ in
//                    //                view?.image = response.value
//                    if let error = response.error{
//                        
//                    }else{
//                        let resizedImage = Toucan.Resize.resizeImage(response.value!, size: CGSize(width: 200, height: 200))
//                        self.imgTeacher.image = resizedImage
//                        UserProvider.sharedInstance.image = resizedImage
//                        _ = self.saveImage(resizedImage, path: self.fileInDocumentsDirectory("user\(UserProvider.sharedInstance.uuid!).jpg"))
//                    }
//                }
            }
        }
        do {
            let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let documentDirectory = URL(fileURLWithPath: path)
            let originPath = documentDirectory.appendingPathComponent("assetUser.jpg")
            let destinationPath = documentDirectory.appendingPathComponent("user\(UserProvider.sharedInstance.uuid!).jpg")
            try FileManager.default.moveItem(at: originPath, to: destinationPath)
        } catch {
            print(error)
            
        }
        
        
        
//        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
//        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
//        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
//        if let dirPath          = paths.first
//        {
//            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("editUser\(UserProvider.sharedInstance.uuid!).jpg")
//            let image    = UIImage(contentsOfFile: imageURL.path)
//            if image != nil{
//                UserProvider.sharedInstance.image = image
//                imgTeacher.image = image
//                tWS.updateImage(url: imageURL, student: nil)
//            }else{
//            }
//        }else{
//        }
        toAddStudent.attributedText = NSAttributedString(string: Localization("ToAddStudents"))
        ToseeCLass.attributedText = NSAttributedString(string: Localization("ToseeMaterias"))
        toSelect.attributedText = NSAttributedString(string: Localization("ToSelect"))
//        helpMenuButton.setTitle(Localization("menuAyuda"), for: .normal)
        let attributeString = NSMutableAttributedString(string: Localization("menuAyuda"),
                                                        attributes: yourAttributes)
        helpMenuButton.setAttributedTitle(attributeString, for: .normal)

        assignLabel.text = Localization("AsignStudents")
        textToAdd.attributedText = NSAttributedString(string: Localization("MakeClassA"))
        editProfileButton.setTitle(Localization("EditProfile"), for: .normal)
        changeLanguageButton.setTitle(Localization("ChangeLanguaget"), for: .normal)
        logOutButton.setTitle(Localization("Logout"), for: .normal)
        self.tWS.delegate = self
//        if UserProvider.sharedInstance.isnew{
//            if UserProvider.sharedInstance.localPath != nil{
//                tWS.updateImage(url: UserProvider.sharedInstance.localPath, student: nil)
//            }
//        }
        collectionView.clipsToBounds = true
//        if let lp = UserProvider.sharedInstance.localPath {
//            tWS.updateImage(url: lp, student: nil)
//        }
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reloadClasses),
                                               name: NSNotification.Name(rawValue: "BackFromPercent"),
                                               object: nil)
        
        
        imgTeacher.clipsToBounds = true
        imgTeacher.layer.cornerRadius = imgTeacher.frame.size.width / 2
    

//        let falls = (datestart...dateend).contains((date.convertedDate(calendar: .current))!)
//        print(falls)
        
//        blurView.addGradients(colorTop: UIColor(red: 239.0/255.0, green: 243.0/255.0, blue: 244.0/255.0, alpha: 1).cgColor, and: UIColor.clear.cgColor)
        if UserProvider.sharedInstance.isnew{
            self.view.isUserInteractionEnabled = true
        }else{
            self.view.isUserInteractionEnabled = true
        }
        if let u = UserProvider.sharedInstance.updated{
            if !UserProvider.sharedInstance.updated && UserProvider.sharedInstance.updated != nil{
                sWS.delegate = self
                sWS.register(teacher: UserProvider.sharedInstance)
            }else{
                self.tWS.getClassesByDB()
            }
        }else{
            self.tWS.getClassesByDB()
        }
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem?.tintColor = UIColor(red: 0, green: 170.0/255.0, blue: 240.0/255.0, alpha: 1)

        
        NotificationCenter.default.addObserver(self, selector: #selector(RootViewController.receiveLanguageChangedNotification(notification:)), name: kNotificationLanguageChanged, object: nil)
        configureViewFromLocalisation()

        
        let classe = Class()
        classe.colorClass = Constants.colorClass.aqua
        classe.name = "Prueba Provider"
//        UserProvider.sharedInstance.classes.append(classe)
//        UserProvider.sharedInstance.classes.append(classe)
//        UserProvider.sharedInstance.classes.append(classe)
        guard let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        flowLayout.minimumInteritemSpacing = margin
        flowLayout.minimumLineSpacing = margin
        flowLayout.sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)
        
        let btn1 = UIButton(type: .custom)
        btn1.setImage(#imageLiteral(resourceName: "ajustes"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 46.5, height: 46.5)
        btn1.addTarget(self, action: #selector(self.settings), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        
        let btn2 = UIButton(type: .custom)
        btn2.setImage(#imageLiteral(resourceName: "ayuda"), for: .normal)
        btn2.frame = CGRect(x: 0, y: 0, width: 46.5, height: 46.5)
        btn2.addTarget(self, action: #selector(self.ayuda), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)
        
        self.navigationItem.setRightBarButtonItems([item1, item2], animated: true)

        self.stopSpinner()
        
        // Do any additional setup after loading the view.
    }
    
    func ayuda(){
        self.navigationController?.navigationBar.isHidden = true
        menuView.alpha = 0
        blurView.alpha = 1
        topConstraintsBlur.constant = -44
        self.view.layoutIfNeeded()
    }
    
    @IBAction func helpAction(_ sender: Any) {

    }
    
    @IBAction func closeHelpAction(_ sender: Any) {
        self.navigationController?.navigationBar.isHidden = false
        topConstraintsBlur.constant = 0
        blurView.alpha = 0
        self.view.layoutIfNeeded()

        
    }
    
    func settings(){
        if menuView.alpha == 0{
//            menuView2.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
            menuView.alpha = 1
        }else{
//            menuView2.transform = CGAffineTransform.identity
            menuView.alpha = 0
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func addClassUpAction(_ sender: Any) {
        let cvc = self.storyboard?.instantiateViewController(withIdentifier: "createView") as! CreateClassViewController
        self.navigationController?.pushViewController(cvc, animated: true)
    }
    
    @IBAction func addClassBottomAction(_ sender: Any) {
        let cvc = self.storyboard?.instantiateViewController(withIdentifier: "createView") as! CreateClassViewController
        self.navigationController?.pushViewController(cvc, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func editProfileAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "editProfileView") as! EditTeacherProfileViewController
        self.navigationController?.pushViewController(vc, animated: true)
        menuView.alpha = 0
//        menuView2.transform = CGAffineTransform.identity
    }

    @IBAction func languajeAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "languageModalView") as! LanguageViewController
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true) {
            
        }
        menuView.alpha = 0
//        menuView2.transform = CGAffineTransform.identity
    }

    @IBAction func logOutAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "modalLogoutView") as! LogoutViewController
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.delegate = self
        self.present(vc, animated: true) {
            
        }
        menuView.alpha = 0
//        menuView2.transform = CGAffineTransform.identity
    }
    
    @IBAction func hideMenu(_ sender: Any) {
        menuView.alpha = 0
//        menuView2.transform = CGAffineTransform.identity
//        let csb = UIStoryboard(name: "Calendar", bundle: nil)
//        let c2v = csb.instantiateViewController(withIdentifier: "CJTview") as! CalendarJTViewController
//        self.navigationController?.pushViewController(c2v, animated: true)
    }
    
}

extension RootViewController: logoutDelegate{
    func logout() {
        tWS.hasInternet { (result) in
            internetProvider.sharedInstance.hasInternet = result
        }
        SwiftMessages.hideAll()
        self.dismiss(animated: false) {
            UserProvider.sharedInstance.classes = []
            UserProvider.sharedInstance.dbid = 0
            UserProvider.sharedInstance.dbidString = ""
            UserProvider.sharedInstance.email = ""
            UserProvider.sharedInstance.image = nil
            UserProvider.sharedInstance.imageUrl = ""
            UserProvider.sharedInstance.isnew = false
            UserProvider.sharedInstance.lastName = ""
            UserProvider.sharedInstance.userName = ""
            UserProvider.sharedInstance.name = ""
            UserProvider.sharedInstance.password = ""
            UserProvider.sharedInstance.schoolName = ""
            UserProvider.sharedInstance.token = ""
            UserProvider.sharedInstance.uuid = ""
            UserProvider.sharedInstance.localPath = nil
            let defaults = UserDefaults()
            defaults.set("0", forKey: "logued")
            defaults.set("", forKey: "password")
            defaults.set("", forKey: "email")
            defaults.set("", forKey: "uuid")
            defaults.synchronize()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "BackPop"), object: nil)
        }
    }
}

extension RootViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let marginsAndInsets = flowLayout.sectionInset.left + flowLayout.sectionInset.right + flowLayout.minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
        let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
        return CGSize(width: itemWidth, height: itemWidth)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView?.collectionViewLayout.invalidateLayout()
        super.viewWillTransition(to: size, with: coordinator)
        self.blurView.alpha = 0
        self.navigationController?.navigationBar.isHidden = false
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            self.stackView.axis = .horizontal
            internetProvider.sharedInstance.isVertical = false
            let mutableString = NSMutableAttributedString(string: String(format: "¡%@ %@!",Localization("Saludo") , UserProvider.sharedInstance.name!), attributes: [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 30)])
            textHi.attributedText = mutableString
        } else {
            self.stackView.axis = .vertical
            internetProvider.sharedInstance.isVertical = true
            let mutableString = NSMutableAttributedString(string: String(format: "¡%@ %@!",Localization("Saludo") , UserProvider.sharedInstance.name!), attributes: [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 50)])
            textHi.attributedText = mutableString
        }
        self.view.layoutIfNeeded()
        collectionView.reloadData()

    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if UserProvider.sharedInstance.classes.count == 0{
            self.textToAdd.alpha = 1
            self.buttonAddUp.alpha = 1
            self.buttonAddBottom.alpha = 0
            self.collectionView.alpha = 0
        }else{
            self.textToAdd.alpha = 0
            self.buttonAddUp.alpha = 0
            self.buttonAddBottom.alpha = 1
            self.collectionView.alpha = 1
        }

        return UserProvider.sharedInstance.classes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ClassButtonCollectionViewCell
        let classe = UserProvider.sharedInstance.classes[indexPath.row]
        cell.button.delegate = self
        cell.button.duration = 2
        cell.button.distance = 130
//        cell.backgroundColor = UIColor.green
        cell.button.setBackgroundImage(UIImage(), for: .selected)
//        cell.button.backgroundColor = UIColor.yellow
        cell.nameLabel.text = classe.name
        cell.nameLabel.textColor = UIColor.white
        if classe.colorClass == nil{
            classe.colorClass = Constants.colorClass.aqua
        }
        cell.circleImg.image = classe.colorClass.image
        if cell.button.buttonsIsShown(){
            cell.button.onTap()
        }
        cell.circleImg.tag = indexPath.row
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(normalTap(_:)))
        cell.circleImg.isUserInteractionEnabled = true
        cell.button.isUserInteractionEnabled = false
        cell.nameLabel.isUserInteractionEnabled = false
        cell.circleImg.tag = indexPath.row
        cell.circleImg.addGestureRecognizer(longGesture)
        cell.circleImg.addGestureRecognizer(tapGesture)
        return cell
    }
    
    func normalTap(_ sender: UIGestureRecognizer){
        let cell = collectionView.cellForItem(at: IndexPath(row: (sender.view as! UIImageView).tag, section: 0)) as! ClassButtonCollectionViewCell
        cell.nameLabel.clipsToBounds = true
        cell.nameLabel.layer.cornerRadius = 20
        if cell.button.buttonsIsShown(){
            cell.button.onTap()
        }else{
            
            for cell in collectionView.visibleCells{
                if (cell as! ClassButtonCollectionViewCell).button.buttonsIsShown(){
                    (cell as! ClassButtonCollectionViewCell).button.onTap()
                }
            }
            isLong = false
            currentClass = UserProvider.sharedInstance.classes[(sender.view as! UIImageView).tag]
            cell.button.buttonsCount = currentClass.periods.count <= 6 ? currentClass.periods.count : 6
            cell.button.onTap()
            print(currentClass.name)
            print((sender.view as! UIImageView).tag)
            print("Normal tap")
            cell.sendSubview(toBack: cell.circleImg)
            cell.sendSubview(toBack: cell.button)
            if currentClass.periods.count == 0{
                let alert = self.errorAlert(message: Localization("NoPeriods"))
                self.present(alert, animated: true, completion: { 
                    
                })
            }
        }
        
        //        if cell.button.buttonsIsShown(){
        //            if cell.button.buttonsCount == 5{
        //                cell.button.onTap()
        //            }else{
        //
        //            }
        //
        //        }

    }
    
    func longTap(_ sender: UIGestureRecognizer){
        print("Long tap \((sender.view as! UIImageView).tag)")
        if sender.state == .ended {
            print("UIGestureRecognizerStateEnded")
            //Do Whatever You want on End of Gesture
        }else if sender.state == .began {
            print("UIGestureRecognizerStateBegan.")
            let cell = collectionView.cellForItem(at: IndexPath(row: (sender.view as! UIImageView).tag, section: 0)) as! ClassButtonCollectionViewCell
            if cell.button.buttonsCount == 5 && cell.button.buttonsIsShown(){
                
            }else{
                for cell in collectionView.visibleCells{
                    if (cell as! ClassButtonCollectionViewCell).button.buttonsIsShown(){
                        (cell as! ClassButtonCollectionViewCell).button.onTap()
                    }
                }
                isLong = true
                cell.button.buttonsCount = 5
                currentClass = UserProvider.sharedInstance.classes[(sender.view as! UIImageView).tag]
                print(currentClass.name)
                print((sender.view as! UIImageView).tag)
                cell.button.onTap()
            }
            
            

            //Do Whatever You want on Began of Gesture
        }
    }
    
    

}


extension RootViewController: CircleMenuDelegate{
    func circleMenu(_ circleMenu: CircleMenu, willDisplay button: UIButton, atIndex: Int) {
        button.backgroundColor = UIColor.clear
        button.setBackgroundImage(UIImage(), for: .selected)
        print(circleMenu.tag)
        if isLong{
            if atIndex == 0{
                button.setBackgroundImage(#imageLiteral(resourceName: "opAgregarOff"), for: .normal)
            }
            if atIndex == 1{
                button.setBackgroundImage(#imageLiteral(resourceName: "opPorcentajeOff"), for: .normal)
            }
            if atIndex == 2{
                button.setBackgroundImage(#imageLiteral(resourceName: "opDuplicarOff"), for: .normal)
            }
            if atIndex == 3{
                button.setBackgroundImage(#imageLiteral(resourceName: "opEditarOff"), for: .normal)
            }
            if atIndex == 4{
                button.setBackgroundImage(#imageLiteral(resourceName: "opEliminarOff"), for: .normal)
            }
        }else{
            if atIndex == 0{
                button.setBackgroundImage(#imageLiteral(resourceName: "periodo1Off"), for: .normal)
            }
            if atIndex == 1{
                button.setBackgroundImage(#imageLiteral(resourceName: "periodo2Off"), for: .normal)
            }
            if atIndex == 2{
                button.setBackgroundImage(#imageLiteral(resourceName: "periodo3Off"), for: .normal)
            }
            if atIndex == 3{
                button.setBackgroundImage(#imageLiteral(resourceName: "periodo4Off"), for: .normal)
            }
            if atIndex == 4{
                button.setBackgroundImage(#imageLiteral(resourceName: "periodo5Off"), for: .normal)
            }
            if atIndex == 5{
                button.setBackgroundImage(#imageLiteral(resourceName: "periodo6Off"), for: .normal)
            }
            if atIndex == 6{
                button.setBackgroundImage(UIImage(named:"periodo7Off"), for: .normal)
            }
        }
        
    }
    
    func circleMenu(_ circleMenu: CircleMenu, buttonDidSelected button: UIButton, atIndex: Int) {
        print(atIndex)
    }
    
    func circleMenu(_ circleMenu: CircleMenu, buttonWillSelected button: UIButton, atIndex: Int) {
        print(atIndex)
        if isLong{
            if atIndex == 0{
                let studentSB = UIStoryboard(name: "Student", bundle: nil)
                let nvc = studentSB.instantiateViewController(withIdentifier: "seeNewList") as! ListNewStudentViewController
                nvc.classe = currentClass
                self.navigationController?.pushViewController(nvc, animated: true)
            }
            if atIndex == 1{
                let percentSB = UIStoryboard(name: "Percent", bundle: nil)
                let pvc = percentSB.instantiateViewController(withIdentifier: "percentView") as! PercentViewController
                pvc.classe = currentClass.copy() as! Class
                pvc.classe.criterios.removeAll()
                for c in currentClass.criterios{
                    pvc.classe.criterios.append(c.copy() as! Criterio)
                }
                self.navigationController?.pushViewController(pvc, animated: true)
            }
            if atIndex == 2{
                let ppcovc = self.storyboard?.instantiateViewController(withIdentifier: "popUpClassView") as! PopUpClassOptionsViewController
                ppcovc.delegate = self
                ppcovc.modalPresentationStyle = .overCurrentContext
                ppcovc.modalTransitionStyle = .crossDissolve
                self.present(ppcovc, animated: true, completion: {
                    
                })
            }
            if atIndex == 3{
                let ecvc = self.storyboard?.instantiateViewController(withIdentifier: "editClassView") as! EditClassViewController
                ecvc.classe = currentClass
                self.navigationController?.pushViewController(ecvc, animated: true)
            }
            if atIndex == 4{
//                let poprv = self.storyboard?.instantiateViewController(withIdentifier: "removePeriodPopUpView") as! PopUpRemovePeriodViewController
//                poprv.delegate = self
//                poprv.classe = currentClass
//                self.navigationController?.present(poprv, animated: true, completion: {
//                    
//                })
                let alert = UIAlertController(title: nil, message: Localization("DeleteClassAlert"), preferredStyle: .alert)
                let okAction = UIAlertAction(title: Localization("yes"), style: .default, handler: { (action) in
                    self.startSpinner()
                    self.tWS.delete(classe: self.currentClass)
                })
                let cancelAction = UIAlertAction(title: Localization("no"), style: .cancel, handler: { (action) in
                    
                })
                alert.addAction(okAction)
                alert.addAction(cancelAction)
                self.present(alert, animated: true, completion: {
                        
                })
            }
        }else{
            if currentClass.students.count > 0{
                let popUpView = self.storyboard?.instantiateViewController(withIdentifier: "popUpListView") as! PopupListViewController
                popUpView.modalPresentationStyle = .overCurrentContext
                popUpView.modalTransitionStyle = .crossDissolve
                popUpView.delegate = self
                currentPeriod = atIndex + 1
                self.present(popUpView, animated: true, completion: {
                    
                })
            }else{
                let error = self.errorAlert(message: Localization("AddStudent"))
                self.present(error, animated: true, completion: {
                    
                })
            }
        }
    }
    
    func menuCollapsed(_ circleMenu: CircleMenu) {
        circleMenu.alpha = 1
    }
}


extension RootViewController: PopUpListDelegate{
    func repsorts() {
        print("Reposrts")
        let rppvc = self.storyboard?.instantiateViewController(withIdentifier: "resportePopUpView") as! ReportePopUpViewController
        rppvc.delegate = self
        rppvc.modalPresentationStyle = .overCurrentContext
        rppvc.modalTransitionStyle = .crossDissolve
        self.present(rppvc, animated: true) {
            
        }
    }
    
    func rateDay() {
        let start = currentClass.periods[currentPeriod - 1].startDate.convertedDate(calendar: .current)!
        let end = currentClass.periods[currentPeriod - 1].endDate.convertedDate(calendar: .current)!
        let eEnd = Calendar.current.date(byAdding: .day, value: 0, to: end)!

        if (start...eEnd).contains(Date()) || Calendar.current.isDateInToday(start) || Calendar.current.isDateInToday(end){
            let calendarStoryBoard = UIStoryboard(name: "Calendar", bundle: nil)
            let cvc = calendarStoryBoard.instantiateInitialViewController() as! CalendarJTViewController
            cvc.modalPresentationStyle = .overCurrentContext
            cvc.modalTransitionStyle = .crossDissolve
            typeCalendar = 1
            cvc.delegate = self
            self.navigationController?.pushViewController(cvc, animated: true)
        }else{
            let errorVC = self.errorAlert(message: Localization("DayNotInPeriodAlert"))
            self.present(errorVC, animated: true, completion: { 
                
            })
        }
  
    }

    func seeList() {
        let start = currentClass.periods[currentPeriod - 1].startDate.convertedDate(calendar: .current)!
        let end = currentClass.periods[currentPeriod - 1].endDate.convertedDate(calendar: .current)!
        let eEnd = Calendar.current.date(byAdding: .day, value: 0, to: end)!
        if (start...eEnd).contains(Date()) || Calendar.current.isDateInToday(start) || Calendar.current.isDateInToday(end){
            let actStoryBoard = UIStoryboard(name: "Activity", bundle: nil)
            let tlvc = actStoryBoard.instantiateViewController(withIdentifier: "seeListView") as! SeeListViewController
            tlvc.classe = currentClass
            self.navigationController?.pushViewController(tlvc, animated: true)
        }else{
            let errorVC = self.errorAlert(message: Localization("DayNotInPeriodAlert"))
            self.present(errorVC, animated: true, completion: {
                
            })
        }

    }

    func takeList() {
        let start = currentClass.periods[currentPeriod - 1].startDate.convertedDate(calendar: .current)!
        let end = currentClass.periods[currentPeriod - 1].endDate.convertedDate(calendar: .current)!
        let eEnd = Calendar.current.date(byAdding: .day, value: 0, to: end)!

        if (start...eEnd).contains(Date()) || Calendar.current.isDateInToday(start) || Calendar.current.isDateInToday(end){
            let calendarStoryBoard = UIStoryboard(name: "Calendar", bundle: nil)
            let cvc = calendarStoryBoard.instantiateInitialViewController() as! CalendarJTViewController
            typeCalendar = 0
            cvc.modalPresentationStyle = .overCurrentContext
            cvc.modalTransitionStyle = .crossDissolve
            cvc.delegate = self
            self.navigationController?.pushViewController(cvc, animated: true)
        }else{
            let errorVC = self.errorAlert(message: Localization("DayNotInPeriodAlert"))
            self.present(errorVC, animated: true, completion: {
                
            })
        }
    }
    
    func activities() {
        let start = currentClass.periods[currentPeriod - 1].startDate.convertedDate(calendar: .current)!
        let end = currentClass.periods[currentPeriod - 1].endDate.convertedDate(calendar: .current)!
        if end < start{
            return
        }
        let eEnd = Calendar.current.date(byAdding: .day, value: 0, to: end)!
        if (start...eEnd).contains(Date()) || Calendar.current.isDateInToday(start) || Calendar.current.isDateInToday(end){
            let pvc = self.storyboard?.instantiateViewController(withIdentifier: "popUpActivitiesView") as! PopUpActivitiesViewController
            pvc.delegate = self
            pvc.modalPresentationStyle = .overCurrentContext
            pvc.modalTransitionStyle = .crossDissolve
            self.present(pvc, animated: true) {
                
            }
        }else{
            let errorVC = self.errorAlert(message: Localization("DayNotInPeriodAlert"))
            self.present(errorVC, animated: true, completion: {
                
            })
        }
    }
    
    func getDocumentsURL() -> URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsURL
    }
    
    func fileInDocumentsDirectory(_ filename: String) -> URL{
        let fileURL = getDocumentsURL().appendingPathComponent(filename)
        return fileURL
    }
    
    func saveImage (_ image: UIImage, path: URL ) -> Bool{
        
        let pngImageData = UIImageJPEGRepresentation(image, 1.0)
        UIGraphicsBeginImageContext(image.size)
        image.draw(in: CGRect(x:0, y:0, width:image.size.width, height:image.size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let jpgImageData = UIImageJPEGRepresentation(newImage!, 1.0)
        
        let result = (try? jpgImageData!.write(to: URL(fileURLWithPath: path.path), options: [.atomic])) != nil
        return result
        
    }

    
}

extension RootViewController: teacherDelegate{
    
    func didSuccessUpdateCriteriosOn(classe: Class) {
        
    }
    
    func didFailUpdateCriteriosOnClasse(message: String) {
        self.stopSpinner()
    }
    
    func didSuccessRegister(classe: Class) {
        
    }
    
    func didFailRegisterClass(statusCode: Int, message: String!) {
        self.stopSpinner()
    }
    
    func didSuccessUpdate(student: Student) {
        
    }
    
    func didFailUpdateStudent(message: String) {
        
    }
    
    func didSuccessDelete(students: [Student]) {
        
    }
    
    func didFailDeleteStudents(message: String!) {
        
    }
    
    func didSuccessRegister(student: Student, classe: Class) {
        
    }
    
    func didFailRegisterStudent(statusCode: Int, message: String!) {
        
    }
    
    func didSuccessUploadImage() {
        do {
            let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let documentDirectory = URL(fileURLWithPath: path)
            let originPath = documentDirectory.appendingPathComponent("asset.JPG")
            let destinationPath = documentDirectory.appendingPathComponent("editUser\(UserProvider.sharedInstance.uuid!).jpg")
            try FileManager.default.removeItem(at: destinationPath)
            
        } catch {
            print(error)
            
            
        }
        
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("user\(UserProvider.sharedInstance.uuid!).jpg")
            let image    = UIImage(contentsOfFile: imageURL.path)
            if image != nil{
                UserProvider.sharedInstance.image = image
                imgTeacher.image = image
            }else{
            }
        }else{
        }

        
        self.stopSpinner()
    }
    
    func didFailUpdateImage(message: String) {
        self.stopSpinner()
    }
    
    func didSuccessGet(classes: [Class]) {
        self.stopSpinner()
        if let x = UserProvider.sharedInstance.updated{
            //        self.startSpinner()
            if !sincronizando{
                sWS.sincronizar(classes: UserProvider.sharedInstance.classes)
                sincronizando = true
            }
        }
        self.view.isUserInteractionEnabled = true
        UserProvider.sharedInstance.classes.reverse()
        var cs : [Class] = []
        var students : [Student]
        var periods: [Period]
        var criterio: [Criterio]
        for cl in UserProvider.sharedInstance.classes{
                            cs.append(cl)

        }
        for clss in UserProvider.sharedInstance.classes{
            for prd in clss.periods{
                if prd.notifications {
                    let de = prd.endDate.convertedDate(calendar: .current)
                    let date = Date()
                    let date5 = Calendar.current.date(byAdding: .day, value: -5, to: de!)
                    if (date5!...de!).contains(date) || Calendar.current.isDateInToday(de!) || Calendar.current.isDateInToday(de!) {
                        let calendar = NSCalendar.current
                        var components = calendar.dateComponents([.day], from: date, to: de!)
                        if calendar.isDateInToday(de!){
                            let aStr = String(format: Localization("periodAlertToday"),"\(clss.periods.index(of: prd)! + 1)","\(clss.name!)")
                            self.runAlert(title: "", body:aStr)
                        }else{
                            let aStr = String(format: Localization("periodalert1"),"\(clss.periods.index(of: prd)! + 1)","\(clss.name!)","\(components.day! + 1)")
                            self.runAlert(title: "", body: aStr)
                        }
                    }
                }
            }
            for student in clss.students{
                let bDate = student.birthdayDate.convertedDate(calendar: .current)
                let calendar = Calendar.current
                let year = calendar.component(.year, from: Date())
                let month = calendar.component(.month, from: bDate!)
                let day = calendar.component(.day, from: bDate!)
                
                var newDateCompoents = DateComponents()
                newDateCompoents.year = year
                newDateCompoents.month = month
                newDateCompoents.day = day
                let newDate = calendar.date(from:newDateCompoents)
                print(newDateCompoents)
                if calendar.isDateInToday(newDate!){
                    let aStr = String(format: Localization("birthday"), "\(student.name!)")
                    self.runAlert(title: "", body: aStr)
                }
            }
        }
        alert  = false
        sincronizando = true
        collectionView.reloadData()
        self.stopSpinner()
        if sincronizar2{
            sWS.updateDB()
            sincronizar2 = false
        }
    }
        
    func didFailGetClasses(statusCode: Int, message: String!) {
        self.stopSpinner()
        self.view.isUserInteractionEnabled = true
        let alert = self.errorAlert(message: message)
        self.present(alert, animated: true) {
        }
    }
    
    func didSuccessDelete(classe: Class) {
        if let x =  UserProvider.sharedInstance.classes.index(of: self.currentClass){
            UserProvider.sharedInstance.classes.remove(at: x)
        }
        self.self.collectionView.reloadData()
        self.stopSpinner()
    }
    
    func didFailDeleteClass(message: String!) {
        self.stopSpinner()
        let error = errorAlert(message: message)
        self.present(error, animated: true) { 
            
        }
    }
    
    func didSuccessRegister(teacher: Teacher) {
        //        self.startSpinner()
        self.stopSpinner()
        if !sincronizando{
            self.startSpinner()
            sWS.sincronizar(classes: UserProvider.sharedInstance.classes)
            sincronizando = true
            let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
            let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
            let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
            if let dirPath          = paths.first
            {
                let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("user\(UserProvider.sharedInstance.uuid!).jpg")
                let image    = UIImage(contentsOfFile: imageURL.path)
                if image != nil{
                    self.tWS.updateImage(url: imageURL, student: nil)
                }else{
                }
            }else{
            }

        }
    }
}


extension RootViewController: ActivitiesDelegate{
    func roulette() {
        let activitiesStoryBoard = UIStoryboard(name: "Activity", bundle: nil)
        let rvc = activitiesStoryBoard.instantiateViewController(withIdentifier: "rouletteView") as! RouletteViewController
        rvc.classe = currentClass.copy() as! Class
        rvc.period = currentClass.periods[currentPeriod - 1].dbid
        self.navigationController?.pushViewController(rvc, animated: true)
    }
    
    func groups() {
        let actStoryBoard = UIStoryboard(name: "Activity", bundle: nil)
        let gvc = actStoryBoard.instantiateViewController(withIdentifier: "TeamsView") as! TeamsViewController
        gvc.classe = currentClass.copy() as! Class
        gvc.period = currentClass.periods[currentPeriod - 1].dbid
        self.navigationController?.pushViewController(gvc, animated: true)
    }
}

extension RootViewController: ClassOptionsDelegate{
    func newClass() {
        print("new class")
        let ecvc = self.storyboard?.instantiateViewController(withIdentifier: "editClassView") as! EditClassViewController
        ecvc.classe = currentClass
        ecvc.isNew = true
        self.navigationController?.pushViewController(ecvc, animated: true)
    }
    
    func newPeriod() {
        if currentClass.periods.count < 6{
            let npvc = self.storyboard?.instantiateViewController(withIdentifier: "newPeriodView") as! NewPeriodViewController
            npvc.classe = currentClass
            self.navigationController?.pushViewController(npvc, animated: true)
            print("new period")
        }else{
            let alert = self.errorAlert(message: Localization("6Periods"))
            self.present(alert, animated: true, completion: { 
                
            })
        }
    }
}



extension RootViewController: PopUpReporteDelegate{
    func day() {
        print("Day")
        let calendarStoryBoard = UIStoryboard(name: "Calendar", bundle: nil)
        let cvc = calendarStoryBoard.instantiateInitialViewController() as! CalendarJTViewController
        cvc.modalPresentationStyle = .overCurrentContext
        cvc.modalTransitionStyle = .crossDissolve
        typeCalendar = 2
        cvc.delegate = self
        self.navigationController?.pushViewController(cvc, animated: true)
}
    
    func period() {
        print("Periodo")
        let rsb = UIStoryboard(name: "Reportes", bundle: nil)
        let rdvc = rsb.instantiateViewController(withIdentifier: "periodReporteView") as! PeriodReportViewController
        rdvc.classe = currentClass
        rdvc.period = currentClass.periods[currentPeriod - 1]
        rdvc.title = String(format: "%@ %i", Localization("Period"), currentPeriod)
        self.navigationController?.pushViewController(rdvc, animated: true)
    }
}

extension RootViewController: calendarDeleate{
    func selectedDay(date: CVDate) {
        print(date)
        let start = currentClass.periods[currentPeriod - 1].startDate.convertedDate(calendar: .current)!
        let end = currentClass.periods[currentPeriod - 1].endDate.convertedDate(calendar: .current)!
        if ((start...end).contains(date.convertedDate(calendar: .current)!) || Calendar.current.isDate(start, inSameDayAs: date.convertedDate(calendar: .current)!) || Calendar.current.isDate(end, inSameDayAs: date.convertedDate(calendar: .current)!)){
            if date.convertedDate(calendar: .current)! > Date(){
                let errorVC = self.errorAlert(message: Localization("dayAfter"))
                self.present(errorVC, animated: true, completion: {
                    
                })
                return
            }else{
                if typeCalendar == 2{
                    let rsb = UIStoryboard(name: "Reportes", bundle: nil)
                    let rdvc = rsb.instantiateViewController(withIdentifier: "dayReporteView") as! DayReporteViewController
                    rdvc.classe = currentClass
                    rdvc.period = currentClass.periods[currentPeriod - 1]
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeStyle = DateFormatter.Style.none
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    dateFormatter.dateFormat = "dd / MMM / y"
                    let bDate = dateFormatter.string(from: date.convertedDate(calendar: .current)!)  // "2010-01-27"
                    rdvc.title = bDate
                    rdvc.date = date
                    self.navigationController?.pushViewController(rdvc, animated: true)
                }
                if typeCalendar == 0{
                    let actStoryBoard = UIStoryboard(name: "Activity", bundle: nil)
                    let tlvc = actStoryBoard.instantiateViewController(withIdentifier: "takeListView") as! TakeListViewController
                    tlvc.classe = currentClass.copy() as! Class
                    tlvc.period = currentClass.periods[currentPeriod - 1]
                    tlvc.date = date
                    self.navigationController?.pushViewController(tlvc, animated: true)
                }
                if typeCalendar == 1{
                    print("Rate Day")
                    let rdvc = self.storyboard?.instantiateViewController(withIdentifier: "rateDayView") as! RateDayViewController
                    rdvc.classe = currentClass
                    rdvc.period = currentClass.periods[currentPeriod - 1]
                    rdvc.date = date
                    switch currentClass.criterios.count {
                    case 3:
                        rdvc.c1IsAvalible = true
                        rdvc.c2IsAvalible = false
                        rdvc.c3IsAvalible = false
                        break
                    case 4:
                        rdvc.c1IsAvalible = true
                        rdvc.c2IsAvalible = true
                        rdvc.c3IsAvalible = false
                        break
                    case 5:
                        rdvc.c1IsAvalible = true
                        rdvc.c2IsAvalible = true
                        rdvc.c3IsAvalible = true
                        break
                    default:
                        rdvc.c1IsAvalible = false
                        rdvc.c2IsAvalible = false
                        rdvc.c3IsAvalible = false
                        break
                    }
                    self.navigationController?.pushViewController(rdvc, animated: true)
                }
            }
        }else{
            let errorVC = self.errorAlert(message: Localization("DayNotInPeriodAlert"))
            self.present(errorVC, animated: true, completion: {
                
            })
        }
    }
}

extension RootViewController: sincronizacionDelegate{
    func didsuccessSincronizacion2() {
        self.stopSpinner()
        sincronizando = true
        if !sincronizado2{
            sincronizado2 = true
            tWS.getClasses()
        }
    }
    
    func didSuccessCLassesSincronizacion(classes: [Class]) {
        self.stopSpinner()
        sincronizando = true
        self.view.isUserInteractionEnabled = true
        tWS.getClasses()
        sincronizar2 = true
    }

    func noInternetconnection() {
        sincronizando = true
        self.view.isUserInteractionEnabled = true
        self.stopSpinner()
    }

    func didSuccessSincronizado(teacher: Teacher){
        self.view.isUserInteractionEnabled = true
        sincronizando = true
        self.checkClasses()
        self.stopSpinner()
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
        let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        if let dirPath          = paths.first
        {
            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("user\(UserProvider.sharedInstance.uuid!).jpg")
            let image    = UIImage(contentsOfFile: imageURL.path)
            if image != nil{
                self.tWS.updateImage(url: imageURL, student: nil)
            }else{
            }
        }else{
        }
        do {
            let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let documentDirectory = URL(fileURLWithPath: path)
            let originPath = documentDirectory.appendingPathComponent("user\(UserProvider.sharedInstance.localID!).jpg")
            let destinationPath = documentDirectory.appendingPathComponent("user\(UserProvider.sharedInstance.uuid!).jpg")
            try FileManager.default.moveItem(at: originPath, to: destinationPath)
            self.tWS.updateImage(url: destinationPath, student: nil)
        } catch {
            print(error)
            
        }
//        tWS.getClasses()
//        if !sincronizado2{
//            sincronizado2 = true
//            tWS.getClasses()
//        }
    }
    
    func checkClasses(){
        self.stopSpinner()
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            db = nil
        }
        
        let classesTable = Table("classes")
        let criteriosTable = Table("criterios")
        let periodsTable = Table("periods")
        let participTable = Table("participation")
        let rateDayTable = Table("rateDay")
        let studentsTable = Table("students")
        let listTable = Table("list")
        var countFunction = 0
        do{
            if db != nil{
                countFunction = try db.scalar( classesTable.filter( Constants.tUuid == "\(UserProvider.sharedInstance.localID!)").count )
            }
        }catch{
            print(error)
        }
        
        
        if countFunction > 0{
            do {
                if db != nil{
                    let query = classesTable.filter( Constants.tUuid == "\(UserProvider.sharedInstance.localID!)")
                    try db.run(query.update(Constants.tUuid <- UserProvider.sharedInstance.uuid))
                    let querryList = listTable.filter(Constants.tUuid == "\(UserProvider.sharedInstance.localID!)")
                    let querryParticipation = participTable.filter(Constants.tUuid == "\(UserProvider.sharedInstance.localID!)")
                    let querryrateDay = rateDayTable.filter(Constants.tUuid == "\(UserProvider.sharedInstance.localID!)")
                    try db.run(querryList.update(Constants.tUuid <- UserProvider.sharedInstance.uuid))
                    try db.run(querryParticipation.update(Constants.tUuid <- UserProvider.sharedInstance.uuid))
                    try db.run(querryrateDay.update(Constants.tUuid <- UserProvider.sharedInstance.uuid))
                }
                print("update")
            } catch {
                print(error)
            }
        }
        sincronizando = false
        tWS.getClassesByDB()
    }
    
    func didFailSincronizadoTeacher(message: String) {
        self.view.isUserInteractionEnabled = true
        sincronizando = true
        tWS.getClasses()
        self.view.isUserInteractionEnabled = true
        self.stopSpinner()
    }
    
    func updatedDB() {
        sWS.sincronizacion2()
        for cls in UserProvider.sharedInstance.classes{
            for student in cls.students{
                let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
                let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
                let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
                if let dirPath          = paths.first
                {
                    let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("\(student.uuid!).jpg")
                    let image    = UIImage(contentsOfFile: imageURL.path)
                    if image != nil{
                        self.tWS.updateImage(url: imageURL, student: student)
                        do {
                            let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                            let documentDirectory = URL(fileURLWithPath: path)
                            let originPath = documentDirectory.appendingPathComponent("\(student.localId!).jpg")
                            let destinationPath = documentDirectory.appendingPathComponent("\(student.uuid!).jpg")
                            let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("\(student.uuid!).jpg")
                            let image    = UIImage(contentsOfFile: imageURL.path)
                            student.img = image
                            try FileManager.default.moveItem(at: originPath, to: destinationPath)
                        } catch {
                            print(error)
                            
                        }
                        
                    }else{
                    }
                }else{
                }
            }
        }
    }
}

