//
//  PopUpListCompletedViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 31/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

protocol EditListPopUpDelegate {
    func editList()
    func finishList()
}

class PopUpListCompletedViewController: UIViewController {
    var delegate: EditListPopUpDelegate?
    var classe : Class!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var editListButton: UIButton!
    @IBOutlet weak var finishListButton: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editListButton.setTitle(Localization("EditList"), for: .normal)
        finishListButton.setTitle(Localization("FinishList"), for: .normal)
titleLabel.text = Localization("Todo")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func editAction(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.editList()
        }
    }

    @IBAction func finishAction(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.finishList()
        }
    }
    @IBAction func tapAction(_ sender: Any) {
        self.dismiss(animated: false) {
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
