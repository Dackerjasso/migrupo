//
//  PercentTableViewCell.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 26/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

protocol nameChangedDelegate {
    func nameChanged()
}

class PercentTableViewCell: UITableViewCell {
    var delegate : nameChangedDelegate?
    @IBOutlet weak var percentBar: G8SliderStep!
    @IBOutlet weak var titleTF: UITextField!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var percentLAbel: UILabel!
    var criterio: Criterio!
    override func awakeFromNib() {
        super.awakeFromNib()
        okButton.alpha = 0
        percentBar.setThumbImage(#imageLiteral(resourceName: "circuloAzul"), for: .normal)
        percentBar.setThumbImage(#imageLiteral(resourceName: "circuloAzul"), for: .highlighted)
        percentBar.setThumbImage(#imageLiteral(resourceName: "circuloAzul"), for: .selected)
        percentBar.setMaximumTrackImage(#imageLiteral(resourceName: "campoTareas")
            .resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 0.0, bottom: 0, right: 6.0)),
                                        for: .normal)
        percentBar.setMinimumTrackImage(#imageLiteral(resourceName: "azul")
            .withRenderingMode(.alwaysTemplate)
            .resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 6.0, bottom: 0, right: 0)),
                                        for: .normal)
        
        // Initialization code
    }
    
    @IBAction func editAction(_ sender: Any) {
        self.delegate?.nameChanged()
        (sender as! UIButton).isSelected = !(sender as! UIButton).isSelected
        if !(sender as! UIButton).isSelected {
            criterio.name = titleTF.text
            editButton.alpha = 1
            okButton.alpha = 0
            titleTF.isEnabled = false
            titleTF.textColor = UIColor(red: 173.0/255.0, green: 173.0/255.0, blue: 173.0/255.0, alpha: 1)
            titleTF.resignFirstResponder()
        }else{
            titleTF.isEnabled = true
            editButton.alpha = 1
            okButton.alpha = 1
            titleTF.textColor = UIColor.black
            titleTF.becomeFirstResponder()
        }

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

