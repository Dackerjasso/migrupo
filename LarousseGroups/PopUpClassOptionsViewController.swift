//
//  PopUpClassOptionsViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 09/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

protocol ClassOptionsDelegate {
    func newPeriod()
    func newClass()
}

class PopUpClassOptionsViewController: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var newPeriodButton: UIButton!
    @IBOutlet weak var newClassButton: UIButton!
    var delegate: ClassOptionsDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = Localization("Todo")
        newPeriodButton.setTitle(Localization("NewPeriod"), for: .normal)
        newClassButton.setTitle(Localization("NewMateria"), for: .normal)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func newPeriod(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.newPeriod()
        }
    }

    @IBAction func newClass(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.newClass()
        }
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        self.dismiss(animated: false) { 
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
