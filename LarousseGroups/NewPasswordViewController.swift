//
//  NewPasswordViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 15/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

class NewPasswordViewController: UIViewController {

    @IBOutlet weak var currentPassTF: UITextField!
    @IBOutlet weak var newPassTF: UITextField!
    @IBOutlet weak var newPass2TF: UITextField!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var acceptButton: UIButton!
    let tWS = TeacherWS()
    var first = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cancelButton.setTitle(Localization("Cancelar"), for: .normal)
        acceptButton.setTitle(Localization("Aceptar"), for: .normal)
        currentPassTF.placeholder = Localization("CurrentPass")
        newPassTF.placeholder = Localization("NewPAssword")
        newPass2TF.placeholder = Localization("ConfirmPass")
        tWS.delegate = self
        currentPassTF.setLeftPaddingPoints(30)
        currentPassTF.setRightPaddingPoints(30)
        newPassTF.setLeftPaddingPoints(30)
        newPassTF.setRightPaddingPoints(30)
        newPass2TF.setLeftPaddingPoints(30)
        newPass2TF.setRightPaddingPoints(30)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func okAction(_ sender: Any) {
        if currentPassTF.text?.trimmingCharacters(in: .whitespacesAndNewlines) == UserProvider.sharedInstance.password{
            if newPassTF.text?.trimmingCharacters(in: .whitespacesAndNewlines) == newPass2TF.text?.trimmingCharacters(in: .whitespacesAndNewlines){
                if (newPassTF.text?.trimmingCharacters(in: .whitespacesAndNewlines).characters.count)! > 4{
                    print("Mandar ws")
                    self.startSpinner()
                    tWS.updateProfile(password: (newPassTF.text?.trimmingCharacters(in: .whitespacesAndNewlines))!)
                }else{
                    let errorAlert = self.errorAlert(message: Localization("LenghtPassAlert"))
                    self.present(errorAlert, animated: true, completion: {
                        
                    })
                }
            }else{
                let errorAlert = self.errorAlert(message: Localization("ErrorPasswords"))
                self.present(errorAlert, animated: true, completion: {
                    
                })
            }
        }else{
            let errorAlert = self.errorAlert(message: Localization("ErrorCurrentPassword"))
            self.present(errorAlert, animated: true, completion: { 
                
            })
        }
//            self.dismiss(animated: true) { 
//                
//        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true) { 
            
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NewPasswordViewController: teacherDelegate{
    func didSuccessChangePassword() {
        self.stopSpinner()
        let alert = UIAlertController(title: nil, message: Localization("passwordUpdated"), preferredStyle: .alert)
        let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default) { (action) in
            self.dismiss(animated: true) {
                self.navigationController?.popViewController(animated: true)
            }
        }

        alert.addAction(okAction)
        self.present(alert, animated: true) { 
            
        }
    }
    
    func didFailChangePassword(message: String!) {
        if first{
            first = false
            self.dismiss(animated: true) {
                self.stopSpinner()
            }
        }
    }
}

extension NewPasswordViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 20 // Bool
    }
}
