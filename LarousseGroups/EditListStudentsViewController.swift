//
//  EditListStudentsViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 31/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import Nuke
import CVCalendar
import Toucan

class EditListStudentsViewController: UIViewController {
    var classe: Class!
    var tWS = TeacherWS()
    var period: Period!
    var date: CVDate!

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    func checkAll(){
        self.startSpinner()
//        let x = DispatchTime.now() + 3
//        DispatchQueue.main.asyncAfter(deadline: x, execute: {
//            self.stopSpinner()
//            self.navigationController?.popToRootViewController(animated: true)
//        })
        tWS.asistencia(classe: classe, period: self.period, date: date.convertedDate(calendar: .current)!)
    }
    
    func getDocumentsURL() -> URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsURL
    }
    
    func fileInDocumentsDirectory(_ filename: String) -> URL{
        let fileURL = getDocumentsURL().appendingPathComponent(filename)
        return fileURL
    }
    
    func saveImage (_ image: UIImage, path: URL ) -> Bool{
        
        let pngImageData = UIImageJPEGRepresentation(image, 1.0)
        UIGraphicsBeginImageContext(image.size)
        image.draw(in: CGRect(x:0, y:0, width:image.size.width, height:image.size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let jpgImageData = UIImageJPEGRepresentation(newImage!, 1.0)
        
        let result = (try? jpgImageData!.write(to: URL(fileURLWithPath: path.path), options: [.atomic])) != nil
        return result
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            internetProvider.sharedInstance.isVertical = false
            titleLabel.font = UIFont.boldSystemFont(ofSize: 28)
        } else {
            internetProvider.sharedInstance.isVertical = true
            titleLabel.font = UIFont.boldSystemFont(ofSize: 50)
        }
        self.view.layoutIfNeeded()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
        if !internetProvider.sharedInstance.isVertical{
            print("Landscape")
            titleLabel.font = UIFont.boldSystemFont(ofSize: 28)
        } else {
            titleLabel.font = UIFont.boldSystemFont(ofSize: 50)
        }
        self.view.layoutIfNeeded()
    }
    
    
        
        
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = Localization("EditListM")
        tWS.delegate = self
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(self.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        
        let btn1 = UIButton(type: .custom)
        btn1.setImage(#imageLiteral(resourceName: "btnFinalizar"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 46.5, height: 46.5)
        btn1.addTarget(self, action: #selector(self.checkAll), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        
        self.navigationItem.setRightBarButtonItems([item1], animated: true)
        // Do any additional setup after loading the view.
    }
    
    func back(sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go back to the previous ViewController
        let error = UIAlertController(title: nil, message:Localization("LostData"), preferredStyle: .alert)
        let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default) { (action) in
            _ = self.navigationController?.popToRootViewController(animated: true)
        }
        let cancelAction = UIAlertAction(title: Localization("Cancelar"), style: .cancel) { (action) in
            
        }
        error.addAction(okAction)
        error.addAction(cancelAction)
        self.present(error, animated: true) { 
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EditListStudentsViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return classe.students.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "seeListStudentCell") as! ListEditStudentTableViewCell
        cell.delegate = self
        let student = classe.students[indexPath.row]
        cell.student = student
        if let imag = student.img{
            cell.userImg.image = imag
        }else{
            cell.userImg.image = nil
            if let urlString = student.imgUrl ,!urlString.isEmpty {
                switch urlString {
                case "emoti1.jpg":
                    student.img = #imageLiteral(resourceName: "avatar1G")
                    cell.userImg.image = #imageLiteral(resourceName: "avatar1G")
                    break
                case "emoti2.jpg":
                    student.img = #imageLiteral(resourceName: "avatar2G")
                    cell.userImg.image = #imageLiteral(resourceName: "avatar2G")
                    break
                case "emoti3.jpg":
                    student.img = #imageLiteral(resourceName: "avatar3G")
                    cell.userImg.image = #imageLiteral(resourceName: "avatar3G")
                    break
                case "emoti4.jpg":
                    student.img = #imageLiteral(resourceName: "avatar4G")
                    cell.userImg.image = #imageLiteral(resourceName: "avatar4G")
                    break
                case "emoti5.jpg":
                    student.img = #imageLiteral(resourceName: "avatar5G")
                    cell.userImg.image = #imageLiteral(resourceName: "avatar5G")
                    break
                case "emoti6.jpg":
                    student.img = #imageLiteral(resourceName: "avatar6G")
                    cell.userImg.image = #imageLiteral(resourceName: "avatar6G")
                    break
                case "emoti7.jpg":
                    student.img = #imageLiteral(resourceName: "avatar7G")
                    cell.userImg.image = #imageLiteral(resourceName: "avatar7G")
                    break
                case "emoti8.jpg":
                    student.img = #imageLiteral(resourceName: "avatar8G")
                    cell.userImg.image = #imageLiteral(resourceName: "avatar8G")
                    break
                case "emoti9.jpg":
                    student.img = #imageLiteral(resourceName: "avatar9G")
                    cell.userImg.image = #imageLiteral(resourceName: "avatar9G")
                    break
                default:
                    let request = Request(url: URL(string:urlString)!)
                    
                    Nuke.loadImage(with: request, into: cell.userImg) { [weak view] response, _ in
                        //                view?.image = response.value
                        if let error = response.error{
                            
                        }else{
                        print(response.value!)
                        let resizedImage = Toucan.Resize.resizeImage(response.value!, size: CGSize(width: 200, height: 200))
                        cell.userImg.image = resizedImage
                            student.img = resizedImage
                            _ = self.saveImage(resizedImage, path: self.fileInDocumentsDirectory("\(student.uuid!).jpg"))
                        }
                    }
                    break
                }


            }
        }

        if student.isHere == nil{
            student.isHere = false
        }
        if student.isAlmostNotHere == nil{
            student.isAlmostNotHere = false
        }
        if student.isHere  && !student.isAlmostNotHere{
            cell.asistenciaButton.isSelected = true
            cell.faltaButton.isSelected = false
            cell.retardoButton.isSelected = false
        }
        if !student.isHere && student.isAlmostNotHere{
            cell.asistenciaButton.isSelected = false
            cell.faltaButton.isSelected = false
            cell.retardoButton.isSelected = true
        }
        
        if !student.isAlmostNotHere && !student.isHere{
            cell.asistenciaButton.isSelected = false
            cell.faltaButton.isSelected = true
            cell.retardoButton.isSelected = false
        }

        cell.nameLabel.text = student.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
}

extension EditListStudentsViewController: editListButtonsDelegate{
    func changeStatus() {
        tableView.reloadData()
    }
}

extension EditListStudentsViewController: teacherDelegate{
    func didSuccessTakeList() {
        let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.stopSpinner()
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func didFailTakeList(message: String) {
        self.stopSpinner()
        let error = self.errorAlert(message: message)
        self.present(error, animated: true) {
            
        }
    }
}
