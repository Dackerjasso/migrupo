//
//  NewPeriodViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 09/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import CVCalendar

class NewPeriodViewController: UIViewController {

    @IBOutlet weak var startDateTF: UITextField!
    @IBOutlet weak var endDateTF: UITextField!
    var star: Bool!
    var startDate: CVDate!
    var endDate: CVDate!
    @IBOutlet weak var titleLabel: UILabel!
    var classe : Class!
    var edit = false
    var tWS = TeacherWS()
    @IBOutlet weak var notButton: UIButton!
    let period = Period()

    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
        if !internetProvider.sharedInstance.isVertical {
            print("Landscape")
            titleLabel.font = UIFont.boldSystemFont(ofSize: 28)
        } else {
            titleLabel.font = UIFont.boldSystemFont(ofSize: 50)
        }
        self.view.layoutIfNeeded()
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            internetProvider.sharedInstance.isVertical = false
            titleLabel.font = UIFont.boldSystemFont(ofSize: 28)
        } else {
            internetProvider.sharedInstance.isVertical = true
            titleLabel.font = UIFont.boldSystemFont(ofSize: 50)
            
        }
        self.view.layoutIfNeeded()
        
    }
    
    func back(sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go back to the previous ViewController
        if edit{
            let error = UIAlertController(title: nil, message: Localization("LostData"), preferredStyle: .alert)
            let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default) { (action) in
                _ = self.navigationController?.popViewController(animated: true)
            }
            let cancelAction = UIAlertAction(title: Localization("Cancelar"), style: .cancel) { (action) in
                
            }
            error.addAction(okAction)
            error.addAction(cancelAction)
            self.present(error, animated: true) {
                
            }
        }else{
            _ = self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var antivateLabel: UILabel!
    
    override func viewDidLoad() {
        tWS.delegate = self
        notButton.isSelected = true
        titleLabel.text = Localization("MAkePeriod")
        acceptButton.setTitle(Localization("Aceptar"), for: .normal)
        antivateLabel.text = Localization("ActivatedNotify")
        startDateTF.placeholder = Localization("StartDate")
        endDateTF.placeholder = Localization("EndDate")
        startDateTF.setLeftPaddingPoints(30)
        startDateTF.setRightPaddingPoints(30)
        endDateTF.setLeftPaddingPoints(30)
        endDateTF.setRightPaddingPoints(30)
        endDateTF.setRightPaddingPoints(10, image: #imageLiteral(resourceName: "flecha3"))
        startDateTF.setRightPaddingPoints(10, image: #imageLiteral(resourceName: "flecha3"))
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(self.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func starDateAction(_ sender: Any) {
        let calendarStoryBoard = UIStoryboard(name: "Calendar", bundle: nil)
        let cvc = calendarStoryBoard.instantiateInitialViewController() as! CalendarJTViewController
        cvc.modalPresentationStyle = .overCurrentContext
        cvc.modalTransitionStyle = .crossDissolve
        cvc.delegate = self
        star = true
        self.present(cvc, animated: true) {
            
        }
    }

    @IBAction func endDateAction(_ sender: Any) {
        let calendarStoryBoard = UIStoryboard(name: "Calendar", bundle: nil)
        let cvc = calendarStoryBoard.instantiateInitialViewController() as! CalendarJTViewController
        cvc.modalPresentationStyle = .overCurrentContext
        cvc.modalTransitionStyle = .crossDissolve
        cvc.delegate = self
        star = false
        self.present(cvc, animated: true) {
            
        }
    }
    
    @IBAction func notificationsAction(_ sender: Any) {
        (sender as! UIButton).isSelected = !(sender as! UIButton).isSelected
    }
    
    @IBAction func acceptAction(_ sender: Any) {
        if !internetProvider.sharedInstance.hasInternet{
        if startDate == nil || endDate == nil{
                let alert = UIAlertController(title: nil, message: Localization("CreatePeriodAlert"), preferredStyle: .alert)
                let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default, handler: { (action) in
                    
                })
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: {
                    
                })
            }else{
                if endDate.convertedDate(calendar: .current)! < startDate.convertedDate(calendar: .current)!{
                    let alert = UIAlertController(title: nil, message: Localization("AlertDates"), preferredStyle: .alert)
                    let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default, handler: { (action) in
                        
                    })
                    alert.addAction(okAction)
                    self.present(alert, animated: true, completion: {
                        
                    })
                    return
                }
                for p in classe.periods{
                    let start = p.startDate.convertedDate(calendar: .current)
                    let end = p.endDate.convertedDate(calendar: .current)
                    let start1 = Calendar.current.date(byAdding: .day, value: 0, to: start!)
                    let end1 = Calendar.current.date(byAdding: .day, value: 0, to: end!)
                    if ((start1!...end1!).contains(period.startDate.convertedDate(calendar: .current)!)){
                        let alert = UIAlertController(title: nil, message: Localization("errorPeriodsDate"), preferredStyle: .alert)
                        let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default, handler: { (action) in
                            
                        })
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: {
                            
                        })
                        return
                    }else if ((start1!...start!).contains(period.endDate.convertedDate(calendar: .current)!)){
                        let alert = UIAlertController(title: nil, message: Localization("errorPeriodsDate"), preferredStyle: .alert)
                        let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default, handler: { (action) in
                            
                        })
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: {
                            
                        })
                        return
                    }
                    if ((period.startDate.convertedDate(calendar: .current)!...period.endDate.convertedDate(calendar: .current)!).contains(p.startDate.convertedDate(calendar: .current)!)){
                        let alert = UIAlertController(title: nil, message: Localization("errorPeriodsDate"), preferredStyle: .alert)
                        let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default, handler: { (action) in
                            
                        })
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: {
                            
                        })
                        return
                    }else if ((period.startDate.convertedDate(calendar: .current)!...period.endDate.convertedDate(calendar: .current)!).contains(p.endDate.convertedDate(calendar: .current)!)){
                        let alert = UIAlertController(title: nil, message: Localization("errorPeriodsDate"), preferredStyle: .alert)
                        let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default, handler: { (action) in
                            
                        })
                        alert.addAction(okAction)
                        self.present(alert, animated: true, completion: {
                            
                        })
                        return
                    }
                }
            }
        }
        if startDate == nil || endDate == nil{
            let alert = UIAlertController(title: nil, message: Localization("CreatePeriodAlert"), preferredStyle: .alert)
            let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default, handler: { (action) in
                
            })
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: {
                    
            })
            return
        }else{
            if endDate.convertedDate(calendar: .current)! < startDate.convertedDate(calendar: .current)!{
                let alert = UIAlertController(title: nil, message: Localization("AlertDates"), preferredStyle: .alert)
                let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default, handler: { (action) in
                    
                })
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: {
                    
                })
                return
            }
            period.startDate = startDate
            period.endDate = endDate
            period.notifications = notButton.isSelected
            self.startSpinner()
            tWS.add(period: period, toClass: self.classe)
//            self.navigationController?.popViewController(animated: true)
        }

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension NewPeriodViewController: calendarDeleate{
    func selectedDay(date: CVDate) {
        edit = true
        if star{
            startDateTF.text = date.commonDescription
            period.startDate = date
            startDate = date
        }else{
            endDateTF.text = date.commonDescription
            period.endDate = date
            endDate = date
        }
    }
}


extension NewPeriodViewController: teacherDelegate{
    func didSuccessAddPeriodToClass() {
        let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.stopSpinner()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func didFailAddPeriodToClass(message: String) {
        self.stopSpinner()
        let error = self.errorAlert(message: message)
        self.present(error, animated: true) { 
            
        }
    }
}
