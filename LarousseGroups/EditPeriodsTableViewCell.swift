//
//  EditPeriodsTableViewCell.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 12/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

protocol EditPeriodsDelegate {
    func delete(period: Period)
    func edit(period: Period)
}

class EditPeriodsTableViewCell: UITableViewCell {
    var delegate: EditPeriodsDelegate?
    var period : Period!
    @IBOutlet weak var namePeriodLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func deleteAction(_ sender: Any) {
        delegate?.delete(period: self.period)
    }

    @IBAction func editAction(_ sender: Any) {
        delegate?.edit(period: self.period)
    }
}
