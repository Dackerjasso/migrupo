//
//  ReportePopUpViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 14/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

protocol PopUpReporteDelegate {
    func day()
    func period()
}

class ReportePopUpViewController: UIViewController {
    var delegate : PopUpReporteDelegate?
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dayButton: UIButton!
    @IBOutlet weak var periodButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = Localization("Report")
        dayButton.setTitle(Localization("DayM"), for: .normal)
        periodButton.setTitle(Localization("PeriodM"), for: .normal)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dayAction(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.day()
        }
    }

    @IBAction func periodAction(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.period()
        }
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        self.dismiss(animated: false) {
            
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
