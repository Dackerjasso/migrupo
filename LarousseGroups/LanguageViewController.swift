//
//  LanguageViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso on 25/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

class LanguageViewController: UIViewController {
    
    let arrayLanguages = Localisator.sharedInstance.getArrayAvailableLanguages()

    @IBOutlet weak var titleLabel: UILabel!
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
    }
    @IBOutlet weak var spanishButton: UIButton!
    @IBOutlet weak var englishButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = Localization("ChangeLanguaget")
        spanishButton.setTitle(Localization("SpanishM"), for: .normal)
        englishButton.setTitle(Localization("InglesM"), for: .normal)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func spanishAction(_ sender: Any) {
        self.dismiss(animated: false) {
            SetLanguage(self.arrayLanguages[1])
        }
    }

    @IBAction func englishAction(_ sender: Any) {
        self.dismiss(animated: false) {
            SetLanguage(self.arrayLanguages[2])
        }
    }
    
    @IBAction func tapAction(_ sender: Any) {
        self.dismiss(animated: false) {
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
