//
//  RegisterViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso on 22/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import Photos
import SQLite

class EditTeacherProfileViewController: UIViewController, UIImagePickerControllerDelegate,
UINavigationControllerDelegate {
    
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var usrnameTF: UITextField!
    @IBOutlet weak var schoolTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var button: UIButton!
    let imagePicker = UIImagePickerController()
    let defaults = UserDefaults()
    var edit = false
    var tWS = TeacherWS()
    @IBOutlet weak var addImageLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var changePassButton: UIButton!
    @IBOutlet weak var acceptButton: UIButton!
    
    
    override func viewWillAppear(_ animated: Bool) {
        if !internetProvider.sharedInstance.hasInternet{
            changePassButton.isHidden = true
        }else{
            changePassButton.isHidden = false
        }
        switch currentReachabilityStatus {
        case .notReachable:
            changePassButton.alpha = 0
            break
        case .reachableViaWiFi, .reachableViaWWAN:
            break
        }
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func back(sender: UIBarButtonItem){
        if nameTF.text != UserProvider.sharedInstance.name{
            edit = true
        }
        if lastNameTF.text != UserProvider.sharedInstance.lastName{
            edit = true
        }
        if usrnameTF.text != UserProvider.sharedInstance.userName{
            edit = true
        }
        if schoolTF.text != UserProvider.sharedInstance.schoolName{
            edit = true
        }
        if edit{
            let error = UIAlertController(title: nil, message: Localization("LostData"), preferredStyle: .alert)
            let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default) { (action) in
                self.navigationController?.popViewController(animated: true)
            }
            let cancelAction = UIAlertAction(title: Localization("Cancelar"), style: .cancel) { (action) in
                
            }
            error.addAction(okAction)
            error.addAction(cancelAction)
            self.present(error, animated: true) {
                
            }
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        do {
            let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let documentDirectory = URL(fileURLWithPath: path)
            let destinationPath = documentDirectory.appendingPathComponent("asset.JPG")
            try FileManager.default.removeItem(at: destinationPath)
            
        } catch {
            print(error)
            
            
        }
        titleLabel.text = Localization("EditProfile")
        addImageLabel.text = Localization("AddImage")
        nameTF.placeholder = Localization("Name")
        lastNameTF.placeholder = Localization("Apellidos")
        usrnameTF.placeholder = Localization("ALias")
        schoolTF.placeholder = Localization("School")
        changePassButton.setTitle(Localization("ChangePass"), for: .normal)
        acceptButton.setTitle(Localization("Aceptar"), for: .normal)
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(self.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        tWS.delegate = self
        img.clipsToBounds = true
//        tWS.delegate = self
        img.layer.cornerRadius = img.frame.size.width / 2
        nameTF.setLeftPaddingPoints(30)
        nameTF.setRightPaddingPoints(30)
        lastNameTF.setLeftPaddingPoints(30)
        lastNameTF.setRightPaddingPoints(30)
        usrnameTF.setLeftPaddingPoints(30)
        usrnameTF.setRightPaddingPoints(30)
        schoolTF.setLeftPaddingPoints(30)
        schoolTF.setRightPaddingPoints(30)
        nameTF.setRightPaddingPoints(30, image: #imageLiteral(resourceName: "check1"))
        lastNameTF.setRightPaddingPoints(30, image: #imageLiteral(resourceName: "check1"))
        usrnameTF.setRightPaddingPoints(30, image: #imageLiteral(resourceName: "check1"))
        schoolTF.setRightPaddingPoints(30, image: #imageLiteral(resourceName: "check1"))
        nameTF.text = UserProvider.sharedInstance.name
        lastNameTF.text = UserProvider.sharedInstance.lastName
        usrnameTF.text = UserProvider.sharedInstance.userName
        schoolTF.text = UserProvider.sharedInstance.schoolName
        if let imag = UserProvider.sharedInstance.image{
            img.image = imag
        }else{
            img.image = #imageLiteral(resourceName: "espacioFoto-1")
        }
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func selectPhotoFrom(_ sender: Any) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cameraOption = UIAlertAction(title: Localization("camera"), style: .default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
        let libraryOption = UIAlertAction(title: Localization("gallery"), style: .default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
        let cancelOption = UIAlertAction(title: "Cancelar", style: .cancel) { (action) in
            
        }
        alert.addAction(cameraOption)
        alert.addAction(libraryOption)
//        alert.addAction(cancelOption)
        
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = button
            presenter.sourceRect = button.bounds
        }
        
        self.present(alert, animated: true) {
            
        }
    }
    
    func getDocumentsURL() -> URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsURL
    }
    
    func fileInDocumentsDirectory(_ filename: String) -> URL{
        let fileURL = getDocumentsURL().appendingPathComponent(filename)
        return fileURL
    }
    
    func saveImage (_ image: UIImage, path: URL ) -> Bool{
        
        let pngImageData = UIImageJPEGRepresentation(image, 1.0)
        UIGraphicsBeginImageContext(image.size)
        image.draw(in: CGRect(x:0, y:0, width:image.size.width, height:image.size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let jpgImageData = UIImageJPEGRepresentation(newImage!, 1.0)
        
        let result = (try? jpgImageData!.write(to: URL(fileURLWithPath: path.path), options: [.atomic])) != nil
        return result
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        img.image =  info["UIImagePickerControllerOriginalImage"] as? UIImage
        if picker.sourceType == .camera{
            picker.dismiss(animated: true, completion: {
                let myImageName = "user\(UserProvider.sharedInstance.uuid!).jpg"
                let imagePath = self.fileInDocumentsDirectory(myImageName)
                if self.saveImage((info["UIImagePickerControllerOriginalImage"] as? UIImage)!, path: imagePath){
                    self.tWS.updateImage(url: imagePath, student: nil)
                    let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        self.startSpinner()
                    }
                }else{
                    self.img.image = #imageLiteral(resourceName: "espacioFoto")
                }
                
            })
        }else{
//            let imageUrl          = info[UIImagePickerControllerReferenceURL] as! NSURL
//            let imageName         = imageUrl.lastPathComponent?.replacingOccurrences(of: ".PNG", with: ".JPG")
//            let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
//            let photoURL          = NSURL(fileURLWithPath: documentDirectory)
//            let localPath         = photoURL.appendingPathComponent(imageName!)
//            guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else { return }
//            let data              = UIImageJPEGRepresentation(image, 1.0)
//            
//            let a : URL!
//            do {
//                let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
//                let documentDirectory = URL(fileURLWithPath: path)
//                let originPath = documentDirectory.appendingPathComponent("asset.JPG")
//                let destinationPath = documentDirectory.appendingPathComponent("user\(UserProvider.sharedInstance.uuid!).jpg")
//                a = originPath
//
//            } catch {
//                print(error)
//                self.stopSpinner()
//                picker.dismiss(animated: true) {
//                    
//                }
//                return
//                
//            }
//            let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
//            DispatchQueue.main.asyncAfter(deadline: when) {
//                self.startSpinner()
//            }
//            self.tWS.updateImage(url: a, student: nil)
//            picker.dismiss(animated: true) {
//                
//            }
//            picker.dismiss(animated: true, completion: {
//                
//            })
            var localPath : URL!
            let imageUrl          = info[UIImagePickerControllerReferenceURL] as! NSURL
            let imageName         = imageUrl.lastPathComponent?.replacingOccurrences(of: ".PNG", with: ".JPG")
            let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
            let photoURL          = NSURL(fileURLWithPath: documentDirectory)
            localPath         = photoURL.appendingPathComponent(imageName!)
            let image             = info[UIImagePickerControllerOriginalImage]as! UIImage
            let data              = UIImageJPEGRepresentation(image, 1.0)
            
            do
            {
                try data?.write(to: localPath!, options: Data.WritingOptions.atomic)
            }
            catch
            {
                // Catch exception here and act accordingly
                print(error)
                return
            }
            do {
                let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                let documentDirectory = URL(fileURLWithPath: path)
                let originPath = documentDirectory.appendingPathComponent("asset.JPG")
                let destinationPath = documentDirectory.appendingPathComponent("user\(UserProvider.sharedInstance.uuid!).jpg")
                localPath = destinationPath
                try FileManager.default.moveItem(at: originPath, to: destinationPath)
                
            } catch {
                print(error)
                
            }
            tWS.updateImage(url: localPath!, student: nil)
            picker.dismiss(animated: true) {
                self.startSpinner()
            }

        }
        


    }
    
    @IBAction func registerTeacher(_ sender: Any) {
        let errorString = NSMutableString(string: "")
        var isError = false
        if nameTF.text?.trimmingCharacters(in: NSCharacterSet.whitespaces).characters.count == 0{
            isError = true
            errorString.append("Error Nombre \n\r")
        }
        if lastNameTF.text?.trimmingCharacters(in: NSCharacterSet.whitespaces).characters.count == 0{
            errorString.append("Error Apellido \n\r")
            isError = true
        }
        if usrnameTF.text?.trimmingCharacters(in: NSCharacterSet.whitespaces).characters.count == 0{
            errorString.append("Error Alias \n\r")
            isError = true
        }
        if schoolTF.text?.trimmingCharacters(in: .whitespaces).characters.count == 0{
            errorString.append("Error Escuela")
            isError = true
        }
        if isError{
            let alert = self.errorAlert(message: Localization("ErrorEditUser"))
            self.present(alert, animated: true, completion: { })
        }else{
            let t = Teacher()
            t.name = nameTF.text
            t.email = UserProvider.sharedInstance.email
            t.lastName = lastNameTF.text
            t.userName = usrnameTF.text
            t.schoolName = schoolTF.text
            t.uuid = UserProvider.sharedInstance.uuid
            t.dbid = UserProvider.sharedInstance.dbid
            self.startSpinner()
            tWS.update(teacher: t)
        }

    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension EditTeacherProfileViewController: teacherDelegate{
    func didSuccessUserUpdated(teacher: Teacher) {
        UserProvider.sharedInstance.name = teacher.name
        UserProvider.sharedInstance.lastName = teacher.lastName
        UserProvider.sharedInstance.userName = teacher.userName
        UserProvider.sharedInstance.schoolName = teacher.schoolName
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            db = nil
        }
        
        let users = Table("users")
        
        var countFunction = 0
        do{
            if db != nil{
                countFunction = try db.scalar( users.filter( Constants.email == UserProvider.sharedInstance.email ).count )
            }
        }catch{
            print(error)
        }
        
        
        if countFunction > 0{
            do {
                if db != nil{
                    let query = users.filter( Constants.email == UserProvider.sharedInstance.email)
                    try db.run(query.update(Constants.id <- teacher.dbid, Constants.name <- teacher.name, Constants.email <- teacher.email, Constants.lastName <- teacher.lastName, Constants.username <- teacher.userName, Constants.schoolName <- teacher.schoolName ,Constants.uuid <- teacher.uuid, Constants.updated <- true))
                }
                print("update")
            } catch {
                print(error)
            }
        }
        let when = DispatchTime.now() + 2 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.stopSpinner()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func didFailUserUpdated(message: String) {
        self.stopSpinner()
        let errorV = self.errorAlert(message: message)
        self.present(errorV, animated: true) {
            
        }
    }
    
    func didSuccessUploadImage() {
        self.stopSpinner()
        
        _ = self.saveImage(img.image!, path: self.fileInDocumentsDirectory("user\(UserProvider.sharedInstance.uuid!).jpg"))
        
        do {
            let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let documentDirectory = URL(fileURLWithPath: path)
            let originPath = documentDirectory.appendingPathComponent("asset.JPG")
            let destinationPath = documentDirectory.appendingPathComponent("editUser\(UserProvider.sharedInstance.uuid!).jpg")
            try FileManager.default.removeItem(at: destinationPath)
            try FileManager.default.moveItem(at: originPath, to: destinationPath)
            
        } catch {
            print(error)
            
            
        }
        
        UserProvider.sharedInstance.image = img.image
        let when = DispatchTime.now() + 2 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func didFailUpdateImage(message: String) {
        self.stopSpinner()
        if let imag = UserProvider.sharedInstance.image{
            img.image = imag
        }else{
            img.image = #imageLiteral(resourceName: "espacioFoto-1")
        }
    }
}

extension EditTeacherProfileViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 35 // Bool
    }
}
