//
//  RouletteViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 31/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import InfiniteScrolling
import Nuke
import Toucan

extension Student: InfiniteScollingData {}


class RouletteViewController: UIViewController {
    var isb: InfiniteScrollingBehaviour!
    @IBOutlet weak var arraowView: UIImageView!
    @IBOutlet weak var rateButton: UIButton!
    var currentStudent : Student!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    var classe : Class!
    var copyClass : Class!
    var temaName = "fondoAlumno1"
    var period: Int!
    var btn1 : UIButton!
    var btn2 : UIButton!
    var btn3 : UIButton!
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func getDocumentsURL() -> URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsURL
    }
    
    func fileInDocumentsDirectory(_ filename: String) -> URL{
        let fileURL = getDocumentsURL().appendingPathComponent(filename)
        return fileURL
    }
    
    func saveImage (_ image: UIImage, path: URL ) -> Bool{
        
        let pngImageData = UIImageJPEGRepresentation(image, 1.0)
        UIGraphicsBeginImageContext(image.size)
        image.draw(in: CGRect(x:0, y:0, width:image.size.width, height:image.size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let jpgImageData = UIImageJPEGRepresentation(newImage!, 1.0)
        
        let result = (try? jpgImageData!.write(to: URL(fileURLWithPath: path.path), options: [.atomic])) != nil
        return result
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
//        btn1.isEnabled = true
//        btn2.isEnabled = true
//        btn3.isEnabled = true
    }
    
    private func registerCell() {
        collectionView.register(UINib.init(nibName: "CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CellID")
    }
    
//    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
//        for cll in collectionView.visibleCells{
//            (cll as! CollectionViewCell).fondo.transform = CGAffineTransform.identity
//            (cll as! CollectionViewCell).fondo.transform = CGAffineTransform.identity
//            (cll as! CollectionViewCell).userImg.transform = CGAffineTransform.identity
//            (cll as! CollectionViewCell).titleLabel.transform = CGAffineTransform.identity
//        }
//    }
    
    func random(){
        rateButton.isEnabled = true
        if (!isb.collectionView.isDragging || !isb.collectionView.isDecelerating){
            currentStudent = nil
            let student = classe.students[Int(arc4random_uniform(UInt32(classe.students.count)))]
            let index = classe.students.index(of: student)
            
            let indexPath = IndexPath(row: index!, section: 0)
            let cell = isb.collectionView.cellForItem(at: indexPath)
            let at = self.isb.collectionView.layoutAttributesForItem(at: indexPath)
            isb.collectionView.scrollRectToVisible((at?.frame)!, animated: true)
        }else{
            print("Can't random")
        }

    }
    

    
//    func getCenterCell(){
//        let row = (collectionView.contentOffset.y + collectionView.frame.size.height / 2) / (collectionView.frame.size.height / 5.1);
//        let indexPath = IndexPath(row: Int(row), section: 0)
//        if let cell = collectionView.cellForItem(at: indexPath){
//            print ("**** \((cell as! CollectionViewCell).student.name!)")
//            rateButton.isEnabled = true
//            currentStudent = (cell as! CollectionViewCell).student
//        }
//    }
    
    @IBAction func cancelTap(_ sender: Any) {
        self.navigationController?.navigationBar.isHidden = false
        blurView.alpha = 0
    }
    
    func changeColor(){
        if (!isb.collectionView.isDragging || !isb.collectionView.isDecelerating){
            self.navigationController?.navigationBar.isHidden = true
            blurView.alpha = 1
        }else{
            print("Can't change color")
        }

    }
    
    func refresh(){
        if (!isb.collectionView.isDragging || !isb.collectionView.isDecelerating){
            classe.students.removeAll()
            classe = copyClass.copy() as! Class
            isb.reload(withData: classe.students)
            collectionView.alpha = 1
        }else{
            print("Scrolling")
        }

    }
    
    
    
    @IBOutlet weak var skinLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        skinLabel.text = Localization("SelectTheme")
        copyClass = classe.copy() as! Class
        btn1 = UIButton(type: .custom)
        btn1.setImage(#imageLiteral(resourceName: "random"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 46.5, height: 46.5)
        btn1.addTarget(self, action: #selector(self.random), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        
        btn2 = UIButton(type: .custom)
        btn2.setImage(#imageLiteral(resourceName: "cambiarTema"), for: .normal)
        btn2.frame = CGRect(x: 0, y: 0, width: 46.5, height: 46.5)
        btn2.addTarget(self, action: #selector(self.changeColor), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)
        
        btn3 = UIButton(type: .custom)
        btn3.setImage(#imageLiteral(resourceName: "recargar"), for: .normal)
        btn3.frame = CGRect(x: 0, y: 0, width: 46.5, height: 46.5)
        btn3.addTarget(self, action: #selector(self.refresh), for: .touchUpInside)
        let item3 = UIBarButtonItem(customView: btn3)
        
        
        self.navigationItem.setRightBarButtonItems([item3,item2, item1 ], animated: true)
        
        registerCell()
        if let _ = isb {}
        else {
            let configuration = CollectionViewConfiguration(layoutType: .fixedSize(sizeValue: (self.collectionView.frame.size.height / 5), lineSpacing: 0), scrollingDirection: .horizontal)
            isb = InfiniteScrollingBehaviour(withCollectionView: collectionView, andData: classe.students, delegate: self, configuration: configuration)
            let layout = isb.collectionConfiguration.layoutType
            let configuration2 = CollectionViewConfiguration(layoutType: layout, scrollingDirection: .vertical)
            isb.updateConfiguration(configuration: configuration2)
        }
        collectionView.reloadData()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - TEMAS
    @IBAction func temaAction(_ sender: Any) {
        
        self.navigationController?.navigationBar.isHidden = false
        temaName = "fondoAlumno\((sender as! UIButton).tag)"
        isb.reload(withData: classe.students)
        blurView.alpha = 0
        arraowView.image = UIImage(named: "flecha-\((sender as! UIButton).tag)")
    }
    
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "rateSegue"{
            return true
            
        }else{
            return false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "rateSegue"{
            if currentStudent == nil{
                if let centerCellIndexPath: NSIndexPath  = isb.collectionView.centerCellIndexPath! as NSIndexPath {
                    let cell = isb.collectionView.cellForItem(at: centerCellIndexPath as IndexPath)
                    print((cell as! CollectionViewCell).student.name)
                    currentStudent = (cell as! CollectionViewCell).student
                }
            }
            let vc = segue.destination as! RateViewController
            vc.delegate = self
            vc.period = self.period
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            vc.student = currentStudent
            vc.classe = self.classe
            
            
        }
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
}


extension RouletteViewController: RateDelegate{
    func rated() {
        if classe.students.count > 1{
            classe.students.remove(at: classe.students.index(of: currentStudent)!)
        }else{
            collectionView.alpha = 0
            classe.students.removeAll()
            classe = copyClass.copy() as! Class
            isb.reload(withData: classe.students)
            collectionView.alpha = 1
        }
        currentStudent = nil
        isb.reload(withData: classe.students)
    }
}


extension RouletteViewController: InfiniteScrollingBehaviourDelegate {
    func centerCellAt(indexPath: IndexPath) {
        print(">>>>>>>>> Scrolling!!!")
//        btn1.isEnabled = false
//        btn2.isEnabled = false
//        btn3.isEnabled = false
        for cll in collectionView.visibleCells{
            let centerCellIndexPath: NSIndexPath  = collectionView.centerCellIndexPath! as NSIndexPath
            if let cell = collectionView.cellForItem(at: centerCellIndexPath as IndexPath){
                if cll == cell as! CollectionViewCell{
                    (cll as! CollectionViewCell).fondo.transform = CGAffineTransform(scaleX: 1.2, y:1.2)
                    (cll as! CollectionViewCell).fondo.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                    (cll as! CollectionViewCell).userImg.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                    (cll as! CollectionViewCell).titleLabel.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                }else{

                    (cll as! CollectionViewCell).fondo.transform = CGAffineTransform.identity
                    (cll as! CollectionViewCell).fondo.transform = CGAffineTransform.identity
                    (cll as! CollectionViewCell).userImg.transform = CGAffineTransform.identity
                    (cll as! CollectionViewCell).titleLabel.transform = CGAffineTransform.identity
                }
            }
        }
    }
    
    func didEndScrolling(inInfiniteScrollingBehaviour behaviour: InfiniteScrollingBehaviour) {
//        let row = (behaviour.collectionView.contentOffset.y + collectionView.frame.size.height / 2) / (collectionView.frame.size.height / 5);
//        let ip = IndexPath(row: Int(row), section: 0)
//        collectionView.scrollToItem(at: ip, at: .centeredVertically, animated: true)
//        let cell = collectionView.cellForItem(at: ip) as! CollectionViewCell
//        print ("------------- \(cell.student.name!)")
//        rateButton.isEnabled = true
//        currentStudent = cell.student
        print("FIN-----")
//        btn1.isEnabled = true
//        btn2.isEnabled = true
//        btn3.isEnabled = true
        if let centerCellIndexPath: NSIndexPath  = behaviour.collectionView.centerCellIndexPath! as NSIndexPath {
            let cell = behaviour.collectionView.cellForItem(at: centerCellIndexPath as IndexPath)
            print((cell as! CollectionViewCell).student.name)
            rateButton.isEnabled = true
            currentStudent = (cell as! CollectionViewCell).student
        }
        
    }
    
    func configuredCell(forItemAtIndexPath indexPath: IndexPath, originalIndex: Int, andData data: InfiniteScollingData, forInfiniteScrollingBehaviour behaviour: InfiniteScrollingBehaviour) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CellID", for: indexPath)
        if let collectionCell = cell as? CollectionViewCell,
            let card = data as? Student {
            if let imag = card.img{
                collectionCell.userImg.image = imag
            }else{
                collectionCell.userImg.image = nil
                if let urlString = card.imgUrl ,!urlString.isEmpty {
                    let request = Request(url: URL(string:urlString)!)
                    
                    Nuke.loadImage(with: request, into: collectionCell.userImg) { [weak view] response, _ in
                        //                view?.image = response.value
                        if let error = response.error{
                            
                        }else{
                        print(response.value!)
                        let resizedImage = Toucan.Resize.resizeImage(response.value!, size: CGSize(width: 200, height: 200))
                        collectionCell.userImg.image = resizedImage
                            card.img = resizedImage
                            _ = self.saveImage(resizedImage, path: self.fileInDocumentsDirectory("\(card.uuid!).jpg"))

                        }
                    }
                }
            }
            collectionCell.fondo.image = UIImage(named: temaName)
            collectionCell.titleLabel.text = card.name
            collectionCell.student = card
        }
        return cell
    }
}

extension UICollectionView {
    
    var centerPoint : CGPoint {
        
        get {
            return CGPoint(x: self.center.x + self.contentOffset.x, y: self.center.y + self.contentOffset.y);
        }
    }
    
    var centerCellIndexPath: IndexPath? {
        
        if let centerIndexPath = self.indexPathForItem(at: self.centerPoint) {
            return centerIndexPath
        }
        return nil
    }
}



