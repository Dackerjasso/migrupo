//
//  DayReporteViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 14/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import Photos
import MessageUI
import Nuke
import CVCalendar
import Toucan

class DayReporteViewController: UIViewController {

    @IBOutlet weak var headerConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    var classe: Class!
    var shouldShowTitle = false
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var criterLabel1: UILabel!
    @IBOutlet weak var criterLabel3: UILabel!
    @IBOutlet weak var criterLabel2: UILabel!
    var period: Period!
    var date: CVDate!
    @IBOutlet weak var participationLabel: UILabel!
    @IBOutlet weak var asistenceLabel: UILabel!
    @IBOutlet weak var studentLabel: UILabel!
    var tWS = TeacherWS()
    var students : [Student] = []

    override func viewWillAppear(_ animated: Bool) {
        headerConstraint.constant = -60
        self.view.layoutIfNeeded()
    }
    
    func pdfDataWithTableView(tableView: UITableView) {
        shouldShowTitle = true
        tableView.reloadData()
        let priorBounds = tableView.bounds
        let fittedSize = tableView.sizeThatFits(CGSize(width:priorBounds.size.width, height:tableView.contentSize.height))
        tableView.bounds = CGRect(x:0, y:0, width:fittedSize.width, height:fittedSize.height)
        let pdfPageBounds = CGRect(x:0, y:0, width:tableView.frame.width, height:self.view.frame.height)
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, pdfPageBounds,nil)
        var pageOriginY: CGFloat = 0
        while pageOriginY < fittedSize.height {
            UIGraphicsBeginPDFPageWithInfo(pdfPageBounds, nil)
            UIGraphicsGetCurrentContext()!.saveGState()
            UIGraphicsGetCurrentContext()!.translateBy(x: 0, y: -pageOriginY)
            tableView.layer.render(in: UIGraphicsGetCurrentContext()!)
            UIGraphicsGetCurrentContext()!.restoreGState()
            pageOriginY += pdfPageBounds.size.height
        }
        UIGraphicsEndPDFContext()
        tableView.bounds = priorBounds
        var docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
        docURL = docURL.appendingPathComponent("myDocument.pdf")
        pdfData.write(to: docURL as URL, atomically: true)
        print(docURL)
//        let url = URL.init(fileURLWithPath: docURL.absoluteString)
        
//        let activityViewController = UIActivityViewController(activityItems: [url] , applicationActivities: nil)
//        
//        if let popover = activityViewController.popoverPresentationController {
//            popover.barButtonItem = self.navigationItem.rightBarButtonItem
//        }
//
//        present(activityViewController,
//                animated: true,
//                completion: nil)
        
        
        let title = "Reporte"
        let messageBody = ""
        let recives = [UserProvider.sharedInstance.email]
        let mvc = MFMailComposeViewController()
        mvc.mailComposeDelegate = self
        mvc.setMessageBody(messageBody, isHTML: false)
        mvc.setSubject(title)
        mvc.setToRecipients(recives as! [String])
        let data = NSData(contentsOf: docURL)
        mvc.addAttachmentData(data! as Data, mimeType: "application/pdf", fileName: "myDocument.pdf")
        if MFMailComposeViewController.canSendMail(){
            self.present(mvc, animated: true) {
                
            }
        }else{
            shouldShowTitle = false
            tableView.reloadData()
        }

    }
    
    func save(){
        print("Save")
        let image = self.view.screenshot
        let im = UIImageView(frame: CGRect(x: 0, y: 0, width: 100, height: 200))
        im.image = image
        pdfDataWithTableView(tableView: tableView)
    }
    
    func getDocumentsURL() -> URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsURL
    }
    
    func fileInDocumentsDirectory(_ filename: String) -> URL{
        let fileURL = getDocumentsURL().appendingPathComponent(filename)
        return fileURL
    }
    
    func saveImage (_ image: UIImage, path: URL ) -> Bool{
        
        let pngImageData = UIImageJPEGRepresentation(image, 1.0)
        UIGraphicsBeginImageContext(image.size)
        image.draw(in: CGRect(x:0, y:0, width:image.size.width, height:image.size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let jpgImageData = UIImageJPEGRepresentation(newImage!, 1.0)
        
        let result = (try? jpgImageData!.write(to: URL(fileURLWithPath: path.path), options: [.atomic])) != nil
        return result
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        participationLabel.text = Localization("Participation")
        studentLabel.text = Localization("Student")
        asistenceLabel.text = Localization("Asistence")
        tWS.delegate = self
        self.startSpinner()
        for student in classe.students{
            for c in student.criterios{
                c.calif = 0
            }
        }
        tWS.getReportBy(date: date.convertedDate(calendar: .current)!, classe: classe, period: period)
        let btn1 = UIButton(type: .custom)
        btn1.setImage(#imageLiteral(resourceName: "enviar"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 46.5, height: 46.5)
        btn1.addTarget(self, action: #selector(self.save), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        
        self.navigationItem.setRightBarButtonItems([item1], animated: true)
        
        
        self.automaticallyAdjustsScrollViewInsets = false
        tableView.contentInset = UIEdgeInsets.zero
        self.navigationController?.navigationBar.titleTextAttributes =  [NSForegroundColorAttributeName: UIColor(red: 6.0/255.0, green: 169.0/255.0, blue: 244.0/255.0, alpha: 1), NSFontAttributeName: UIFont.boldSystemFont(ofSize: 20)]
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


    
}

extension DayReporteViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if shouldShowTitle{
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if shouldShowTitle{
            if section == 0{
                return 1
            }else{
                return students.count + 1
            }
        }
        return students.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if shouldShowTitle{
            if indexPath.section == 0{
                let cell =  tableView.dequeueReusableCell(withIdentifier: "titleCell") as! TitleReportTableViewCell
                cell.titleLabel.text = "\(self.title!) - \(classe.name!)"
                cell.backgroundColor = UIColor.clear
                cell.titleLabel.backgroundColor = UIColor.clear
                return cell
            }else{
                if indexPath.row == 0{
                    let cell =  tableView.dequeueReusableCell(withIdentifier: "cellHeader") as!HeaderReportPeriodTableViewCell
                    if let s = students.first{
                        switch s.criterios.count {
                        case 5:
                            cell.criterioLabel3.text = "\(s.criterios[2].name!)"
                            cell.criterioLabel4.text = "\(s.criterios[3].name!)"
                            cell.criterioLabel5.text = "\(s.criterios[4].name!)"
                            break
                        case 4:
                            cell.criterioLabel3.text = "\(s.criterios[2].name!)"
                            cell.criterioLabel4.text = "\(s.criterios[3].name!)"
                            break
                        case 3:
                            cell.criterioLabel3.text = "\(s.criterios[2].name!)"
                            break
                        default: break
                        }
                        
                    }else{
                        
                    }
                    return cell                }
                let cell = tableView.dequeueReusableCell(withIdentifier: "studentCell") as! StudentDayReportTableViewCell
                let student = students[indexPath.row - 1]
                if let imag = student.img{
                    cell.imgUser.image = imag
                }else{
                    let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
                    let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
                    let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
                    if let dirPath          = paths.first
                    {
                        let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("\(student.uuid!).jpg")
                        let image    = UIImage(contentsOfFile: imageURL.path)
                        if image != nil{
                            student.img = image
                            students[indexPath.row - 1].img = image
                            cell.imgUser.image = image
                        }else{
                            student.img = nil
                        }
                    }else{
                        student.img = nil
                    }
                    cell.imgUser.image = nil
                    if let urlString = student.imgUrl ,!urlString.isEmpty {
                        switch urlString {
                        case "emoti1.jpg":
                            student.img = #imageLiteral(resourceName: "avatar1G")
                            cell.imgUser.image = #imageLiteral(resourceName: "avatar1G")
                            break
                        case "emoti2.jpg":
                            student.img = #imageLiteral(resourceName: "avatar2G")
                            cell.imgUser.image = #imageLiteral(resourceName: "avatar2G")
                            break
                        case "emoti3.jpg":
                            student.img = #imageLiteral(resourceName: "avatar3G")
                            cell.imgUser.image = #imageLiteral(resourceName: "avatar3G")
                            break
                        case "emoti4.jpg":
                            student.img = #imageLiteral(resourceName: "avatar4G")
                            cell.imgUser.image = #imageLiteral(resourceName: "avatar4G")
                            break
                        case "emoti5.jpg":
                            student.img = #imageLiteral(resourceName: "avatar5G")
                            cell.imgUser.image = #imageLiteral(resourceName: "avatar5G")
                            break
                        case "emoti6.jpg":
                            student.img = #imageLiteral(resourceName: "avatar6G")
                            cell.imgUser.image = #imageLiteral(resourceName: "avatar6G")
                            break
                        case "emoti7.jpg":
                            student.img = #imageLiteral(resourceName: "avatar7G")
                            cell.imgUser.image = #imageLiteral(resourceName: "avatar7G")
                            break
                        case "emoti8.jpg":
                            student.img = #imageLiteral(resourceName: "avatar8G")
                            cell.imgUser.image = #imageLiteral(resourceName: "avatar8G")
                            break
                        case "emoti9.jpg":
                            student.img = #imageLiteral(resourceName: "avatar9G")
                            cell.imgUser.image = #imageLiteral(resourceName: "avatar9G")
                            break
                        default:
                            let request = Request(url: URL(string:urlString)!)
                            if student.uuid != nil{
                                cell.imgUser.image = student.img
                                Nuke.loadImage(with: request, into: cell.imgUser) { [weak view] response, _ in
                                    //                view?.image = response.value
                                    if let error = response.error{
                                        print(response.error.debugDescription)
                                    }else{
                                        let resizedImage = Toucan.Resize.resizeImage(response.value!, size: CGSize(width: 200, height: 200))
                                        cell.imgUser.image = resizedImage
                                        student.img = resizedImage
                                        _ = self.saveImage(resizedImage, path: self.fileInDocumentsDirectory("\(student.uuid!).jpg"))
                                    }
                                }
                            }
                            break
                        }

                    }
                }
                cell.nameLAbel.text = student.name
                switch student.criterios.count {
                case 5:
//                    cell.valueLabel1.text = "\(student.criterios[0].value) %"
                    switch student.criterios[0].calif {
                    case 1:
                        cell.imgPresent.image = #imageLiteral(resourceName: "si2")
                        break
                    case 2:
                        cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                        break
                    case 3:
                        cell.imgPresent.image = #imageLiteral(resourceName: "retardo2")
                        break
                    default:
                        cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                        break
                    }
                    if student.criterios[1].calif == -1{
                        cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                    }else{
                        if student.criterios[1].calif == 1{
                            cell.imgPartic.image = #imageLiteral(resourceName: "si2")
                        }else{
                            cell.imgPartic.image = #imageLiteral(resourceName: "falta2")
                        }
                    }
                    if student.criterios[2].calif == -1{
                        cell.valueLabel3.text = "--"
                    }else{
                        cell.valueLabel3.text = "\(student.criterios[2].calif)"
                    }
                    if student.criterios[3].calif == -1{
                        cell.valueLabel4.text = "--"
                    }else{
                        cell.valueLabel4.text = "\(student.criterios[3].calif)"
                    }
                    if student.criterios[4].calif == -1{
                        cell.valueLabel5.text = "--"
                    }else{
                        cell.valueLabel5.text = "\(student.criterios[4].calif)"
                    }
                    break
                case 4:
//                    cell.valueLabel1.text = "\(student.criterios[0].value)"
                    switch student.criterios[0].calif {
                    case 1:
                        cell.imgPresent.image = #imageLiteral(resourceName: "si2")
                        break
                    case 2:
                        cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                        break
                    case 3:
                        cell.imgPresent.image = #imageLiteral(resourceName: "retardo2")
                        break
                    default:
                        cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                        break
                    }
                    if student.criterios[1].calif == -1{
                        cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                    }else{
                        if student.criterios[1].calif == 1{
                            cell.imgPartic.image = #imageLiteral(resourceName: "si2")
                        }else{
                            cell.imgPartic.image = #imageLiteral(resourceName: "falta2")
                        }
                    }
                    if student.criterios[2].calif == -1{
                        cell.valueLabel3.text = "--"
                    }else{
                        cell.valueLabel3.text = "\(student.criterios[2].calif)"
                    }
                    if student.criterios[3].calif == -1{
                        cell.valueLabel4.text = "--"
                    }else{
                        cell.valueLabel4.text = "\(student.criterios[3].calif)"
                    }
                    break
                case 3:
//                    cell.valueLabel1.text = "\(student.criterios[0].value) %"
                    switch student.criterios[0].calif {
                    case 1:
                        cell.imgPresent.image = #imageLiteral(resourceName: "si2")
                        break
                    case 2:
                        cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                        break
                    case 3:
                        cell.imgPresent.image = #imageLiteral(resourceName: "retardo2")
                        break
                    default:
                        cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                        break
                    }
                    if student.criterios[1].calif == -1{
                        cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                    }else{
                        if student.criterios[1].calif == 1{
                            cell.imgPartic.image = #imageLiteral(resourceName: "si2")
                        }else{
                            cell.imgPartic.image = #imageLiteral(resourceName: "falta2")
                        }
                    }
                    if student.criterios[2].calif == -1{
                        cell.valueLabel3.text = "--"
                    }else{
                        cell.valueLabel3.text = "\(student.criterios[2].calif)"
                    }
                    break
                case 2:
//                    cell.valueLabel1.text = "\(student.criterios[0].value) %"
                    switch student.criterios[0].calif {
                    case 1:
                        cell.imgPresent.image = #imageLiteral(resourceName: "si2")
                        break
                    case 2:
                        cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                        break
                    case 3:
                        cell.imgPresent.image = #imageLiteral(resourceName: "retardo2")
                        break
                    default:
                        cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                        break
                    }
                    if student.criterios[1].calif == -1{
                        cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                    }else{
                        if student.criterios[1].calif == 1{
                            cell.imgPartic.image = #imageLiteral(resourceName: "si2")
                        }else{
                            cell.imgPartic.image = #imageLiteral(resourceName: "falta2")
                        }
                    }
                    break
                case 1:
//                    cell.valueLabel1.text = "\(student.criterios[0].value) %"
                    switch student.criterios[0].calif {
                    case 1:
                        cell.imgPresent.image = #imageLiteral(resourceName: "si2")
                        break
                    case 2:
                        cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                        break
                    case 3:
                        cell.imgPresent.image = #imageLiteral(resourceName: "retardo2")
                        break
                    default:
                        cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                        break
                    }
                    break
                default:
                    if student.criterios.count > 0 {
                        switch student.criterios[0].calif {
                        case 1:
                            cell.imgPresent.image = #imageLiteral(resourceName: "si2")
                            break
                        case 2:
                            cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                            break
                        case 3:
                            cell.imgPresent.image = #imageLiteral(resourceName: "retardo2")
                            break
                        default:
                            cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                            break
                        }
                        if student.criterios[1].calif == -1{
                            cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                        }else{
                            if student.criterios[1].calif == 1{
                                cell.imgPartic.image = #imageLiteral(resourceName: "si2")
                            }else{
                                cell.imgPartic.image = #imageLiteral(resourceName: "falta2")
                            }
                        }
                    }
                    
                    break
                }
                return cell
            }
        }
        if indexPath.row == 0{
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cellHeader") as! HeaderReportPeriodTableViewCell
            if let s = students.first{
                switch s.criterios.count {
                case 5:
                    cell.criterioLabel3.text = "\(s.criterios[2].name!)"
                    cell.criterioLabel4.text = "\(s.criterios[3].name!)"
                    cell.criterioLabel5.text = "\(s.criterios[4].name!)"
                    break
                case 4:
                    cell.criterioLabel3.text = "\(s.criterios[2].name!)"
                    cell.criterioLabel4.text = "\(s.criterios[3].name!)"
                    break
                case 3:
                    cell.criterioLabel3.text = "\(s.criterios[2].name!)"
                    break
                default: break
                }
                
            }else{
                
            }
            return cell        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "studentCell") as! StudentDayReportTableViewCell
        let student = students[indexPath.row - 1]
        if let imag = student.img{
            cell.imgUser.image = imag
        }else{
            cell.imgUser.image = nil
            let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
            let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
            let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
            if let dirPath          = paths.first
            {
                let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("\(student.uuid!).jpg")
                let image    = UIImage(contentsOfFile: imageURL.path)
                if image != nil{
                    student.img = image
                    students[indexPath.row - 1].img = image
                    cell.imgUser.image = image
                }else{
                    student.img = nil
                }
            }else{
                student.img = nil
            }
            if let urlString = student.imgUrl ,!urlString.isEmpty {
                switch urlString {
                case "emoti1.jpg":
                    student.img = #imageLiteral(resourceName: "avatar1G")
                    cell.imgUser.image = #imageLiteral(resourceName: "avatar1G")
                    break
                case "emoti2.jpg":
                    student.img = #imageLiteral(resourceName: "avatar2G")
                    cell.imgUser.image = #imageLiteral(resourceName: "avatar2G")
                    break
                case "emoti3.jpg":
                    student.img = #imageLiteral(resourceName: "avatar3G")
                    cell.imgUser.image = #imageLiteral(resourceName: "avatar3G")
                    break
                case "emoti4.jpg":
                    student.img = #imageLiteral(resourceName: "avatar4G")
                    cell.imgUser.image = #imageLiteral(resourceName: "avatar4G")
                    break
                case "emoti5.jpg":
                    student.img = #imageLiteral(resourceName: "avatar5G")
                    cell.imgUser.image = #imageLiteral(resourceName: "avatar5G")
                    break
                case "emoti6.jpg":
                    student.img = #imageLiteral(resourceName: "avatar6G")
                    cell.imgUser.image = #imageLiteral(resourceName: "avatar6G")
                    break
                case "emoti7.jpg":
                    student.img = #imageLiteral(resourceName: "avatar7G")
                    cell.imgUser.image = #imageLiteral(resourceName: "avatar7G")
                    break
                case "emoti8.jpg":
                    student.img = #imageLiteral(resourceName: "avatar8G")
                    cell.imgUser.image = #imageLiteral(resourceName: "avatar8G")
                    break
                case "emoti9.jpg":
                    student.img = #imageLiteral(resourceName: "avatar9G")
                    cell.imgUser.image = #imageLiteral(resourceName: "avatar9G")
                    break
                default:
                    let request = Request(url: URL(string:urlString)!)
                    if student.uuid != nil{
                        cell.imgUser.image = student.img
                        Nuke.loadImage(with: request, into: cell.imgUser) { [weak view] response, _ in
                            //                view?.image = response.value
                            if let error = response.error{
                                print(response.error.debugDescription)
                            }else{
                                let resizedImage = Toucan.Resize.resizeImage(response.value!, size: CGSize(width: 200, height: 200))
                                cell.imgUser.image = resizedImage
                                student.img = resizedImage
                                _ = self.saveImage(resizedImage, path: self.fileInDocumentsDirectory("\(student.uuid!).jpg"))
                            }
                        }
                    }
                    break
                }
                
            }
        }
        cell.nameLAbel.text = student.name
        switch student.criterios.count {
        case 5:
            switch student.criterios[0].calif {
            case 1:
                cell.imgPresent.image = #imageLiteral(resourceName: "si2")
                break
            case 2:
                cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                break
            case 3:
                cell.imgPresent.image = #imageLiteral(resourceName: "retardo2")
                break
            default:
                cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                break
            }
            if student.criterios[1].calif == -1{
                cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
            }else{
                if student.criterios[1].calif == 1{
                    cell.imgPartic.image = #imageLiteral(resourceName: "si2")
                }else{
                    cell.imgPartic.image = #imageLiteral(resourceName: "falta2")
                }
            }
            if student.criterios[2].calif == -1{
                cell.valueLabel3.text = "--"
            }else{
                cell.valueLabel3.text = "\(student.criterios[2].calif)"
            }
            if student.criterios[3].calif == -1{
                cell.valueLabel4.text = "--"
            }else{
                cell.valueLabel4.text = "\(student.criterios[3].calif)"
            }
            if student.criterios[4].calif == -1{
                cell.valueLabel5.text = "--"
            }else{
                cell.valueLabel5.text = "\(student.criterios[4].calif)"
            }
            break
        case 4:
            switch student.criterios[0].calif {
            case 1:
                cell.imgPresent.image = #imageLiteral(resourceName: "si2")
                break
            case 2:
                cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                break
            case 3:
                cell.imgPresent.image = #imageLiteral(resourceName: "retardo2")
                break
            default:
                cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                break
            }
            if student.criterios[1].calif == -1{
                cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
            }else{
                if student.criterios[1].calif == 1{
                    cell.imgPartic.image = #imageLiteral(resourceName: "si2")
                }else{
                    cell.imgPartic.image = #imageLiteral(resourceName: "falta2")
                }
            }
            if student.criterios[2].calif == -1{
                cell.valueLabel3.text = "--"
            }else{
                cell.valueLabel3.text = "\(student.criterios[2].calif)"
            }
            if student.criterios[3].calif == -1{
                cell.valueLabel4.text = "--"
            }else{
                cell.valueLabel4.text = "\(student.criterios[3].calif)"
            }
            break
        case 3:
            switch student.criterios[0].calif {
            case 1:
                cell.imgPresent.image = #imageLiteral(resourceName: "si2")
                break
            case 2:
                cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                break
            case 3:
                cell.imgPresent.image = #imageLiteral(resourceName: "retardo2")
                break
            default:
                cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                break
            }
            if student.criterios[1].calif == -1{
                cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
            }else{
                if student.criterios[1].calif == 1{
                    cell.imgPartic.image = #imageLiteral(resourceName: "si2")
                }else{
                    cell.imgPartic.image = #imageLiteral(resourceName: "falta2")
                }
            }
            if student.criterios[2].calif == -1{
                cell.valueLabel3.text = "--"
            }else{
                cell.valueLabel3.text = "\(student.criterios[2].calif)"
            }
            break
        case 2:
            switch student.criterios[0].calif {
            case 1:
                cell.imgPresent.image = #imageLiteral(resourceName: "si2")
                break
            case 2:
                cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                break
            case 3:
                cell.imgPresent.image = #imageLiteral(resourceName: "retardo2")
                break
            default:
                cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                break
            }
            if student.criterios[1].calif == -1{
                cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
            }else{
                if student.criterios[1].calif == 1{
                    cell.imgPartic.image = #imageLiteral(resourceName: "si2")
                }else{
                    cell.imgPartic.image = #imageLiteral(resourceName: "falta2")
                }
            }
            break
        case 1:
            switch student.criterios[0].calif {
            case 1:
                cell.imgPresent.image = #imageLiteral(resourceName: "si2")
                break
            case 2:
                cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                break
            case 3:
                cell.imgPresent.image = #imageLiteral(resourceName: "retardo2")
                break
            default:
                cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                break
            }
            break
        default:
            if student.criterios.count > 0 {
                switch student.criterios[0].calif {
                case 1:
                    cell.imgPresent.image = #imageLiteral(resourceName: "si2")
                    break
                case 2:
                    cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                    break
                case 3:
                    cell.imgPresent.image = #imageLiteral(resourceName: "retardo2")
                    break
                default:
                    cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                    break
                }
                if student.criterios[1].calif == -1{
                    cell.imgPresent.image = #imageLiteral(resourceName: "falta2")
                }else{
                    if student.criterios[1].calif == 1{
                        cell.imgPartic.image = #imageLiteral(resourceName: "si2")
                    }else{
                        cell.imgPartic.image = #imageLiteral(resourceName: "falta2")
                    }
                }
            }
            
            break
        }

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            
            return 54
        }
        return 123
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if shouldShowTitle{
            if section == 0{
                return nil
            }
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellHeader") as! HeaderReportPeriodTableViewCell
        if let s = students.first{
            switch s.criterios.count {
            case 5:
                cell.criterioLabel3.text = s.criterios[2].name
                cell.criterioLabel4.text = s.criterios[3].name
                cell.criterioLabel5.text = s.criterios[4].name
                break
            case 4:
                cell.criterioLabel3.text = s.criterios[2].name
                cell.criterioLabel4.text = s.criterios[3].name
                break
            case 3:
                cell.criterioLabel3.text = s.criterios[2].name
                break
            default: break
            }
            
        }else{
            
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 3
    }
    
    
}

extension DayReporteViewController: UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print(">>>>> \(tableView.contentOffset.y)")
        if tableView.contentOffset.y < 38{
            UIView.animate(withDuration: 0.2, animations: { 
                self.headerConstraint.constant = -60
                self.view.layoutIfNeeded()
            })
        }else{
            UIView.animate(withDuration: 0.2, animations: {
                self.headerConstraint.constant = 0
                self.view.layoutIfNeeded()
            })
        }
    }
}

extension DayReporteViewController : MFMailComposeViewControllerDelegate{
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        case .cancelled:
            print("Cancelled")
            shouldShowTitle = false
            tableView.reloadData()
            controller.dismiss(animated: false, completion: {
                
            })
            break
        case .failed:
            print("Failed")
            shouldShowTitle = false
            tableView.reloadData()
            controller.dismiss(animated: false, completion: {
                
            })
            break
        case .saved:
            print("Saved")
            shouldShowTitle = false
            tableView.reloadData()
            controller.dismiss(animated: false, completion: {
                
            })
            break
        case .sent:
            print("Send")
            shouldShowTitle = false
            tableView.reloadData()
            controller.dismiss(animated: false, completion: {
                
            })
            break
        }
    }
}

extension DayReporteViewController: teacherDelegate{
    func didSuccessGetReportByDay(students: [Student]) {
        self.stopSpinner()
        if students.count == 0{
            self.students = classe.students
        }else{
            self.students = students
        }
        if let s = students.first{
            switch s.criterios.count {
            case 5:
                criterLabel1.text = s.criterios[2].name
                criterLabel2.text = s.criterios[3].name
                criterLabel3.text = s.criterios[4].name
                break
            case 4:
                criterLabel1.text = s.criterios[2].name
                criterLabel2.text = s.criterios[3].name
                break
            case 3:
                criterLabel1.text = s.criterios[2].name
                break
            default: break
            }
        }
        tableView.reloadData()
    }
    
    func didFailGetReportByDay(message: String) {
        self.stopSpinner()
        let errorvc = self.errorAlert(message: message)
        self.present(errorvc, animated: true) { 
            
        }
    }
    
}
