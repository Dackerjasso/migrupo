//
//  ListViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 29/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import ReverseExtension
import Nuke
import CVCalendar
import Toucan

class TakeListViewController: UIViewController {
    
    var classe : Class!
    var clssAux : Class!
    var tWs = TeacherWS()
    var period: Period!
    var date: CVDate!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var pushed = false
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
        if !internetProvider.sharedInstance.isVertical {
            print("Landscape")
            titleLabel.font = UIFont.boldSystemFont(ofSize: 28)
        } else {
            titleLabel.font = UIFont.boldSystemFont(ofSize: 50)
        }
        self.view.layoutIfNeeded()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if !internetProvider.sharedInstance.isVertical{
            print("Landscape")
            titleLabel.font = UIFont.boldSystemFont(ofSize: 28)
            internetProvider.sharedInstance.isVertical = false
        } else {
            titleLabel.font = UIFont.boldSystemFont(ofSize: 50)
            internetProvider.sharedInstance.isVertical = true
        }
        self.view.layoutIfNeeded()
        
    }
    func getDocumentsURL() -> URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsURL
    }
    
    func fileInDocumentsDirectory(_ filename: String) -> URL{
        let fileURL = getDocumentsURL().appendingPathComponent(filename)
        return fileURL
    }
    
    func saveImage (_ image: UIImage, path: URL ) -> Bool{
        let pngImageData = UIImageJPEGRepresentation(image, 1.0)
        UIGraphicsBeginImageContext(image.size)
        image.draw(in: CGRect(x:0, y:0, width:image.size.width, height:image.size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let jpgImageData = UIImageJPEGRepresentation(newImage!, 1.0)
        
        let result = (try? jpgImageData!.write(to: URL(fileURLWithPath: path.path), options: [.atomic])) != nil
        return result
        
    }
    
    func checkAll(){
        for alumno in clssAux.students{
            alumno.isHere = true
            alumno.isAlmostNotHere = false
        }
        let popUpv = self.storyboard?.instantiateViewController(withIdentifier: "popUpListView") as! PopUpListCompletedViewController
        popUpv.classe = clssAux
        popUpv.delegate = self
        popUpv.modalPresentationStyle = .overCurrentContext
        popUpv.modalTransitionStyle = .crossDissolve
        self.present(popUpv, animated: true, completion: {
            
        })

//        let editList = self.storyboard?.instantiateViewController(withIdentifier: "EditListView") as! EditListStudentsViewController
//        editList.classe = clssAux
//        self.navigationController?.pushViewController(editList, animated: true)
    }
    
    func appleTV(){
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.startSpinner()
        tableView.alpha = 0
        titleLabel.text = Localization("List")
        tWs.delegate = self
        clssAux = classe.copy() as! Class
        tWs.getReportBy(date: date.convertedDate(calendar: .current)!, classe: self.classe, period: period)
        let btn1 = UIButton(type: .custom)
        btn1.setImage(#imageLiteral(resourceName: "paseTodos"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 46.5, height: 46.5)
        btn1.addTarget(self, action: #selector(self.checkAll), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        
        
        self.navigationItem.setRightBarButtonItems([item1], animated: true)
        if classe == nil{
            classe = Class()
            for index in 1...10{
                let student = Student()
                student.name = "\(index)"
                classe.students.append(student)
            }
        }
        

        tableView.re.delegate = self
        tableView.re.scrollViewDidReachTop = { scrollView in
            print("scrollViewDidReachTop")
        }
        tableView.re.scrollViewDidReachBottom = { scrollView in
            print("scrollViewDidReachBottom")
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources tTakeListViewControllerhat can be recreated.
    }
    
    @IBAction func moveLabel(_ sender: Any) {
        let gesture = sender as! UIPanGestureRecognizer
        let label = gesture.view as! UILabel
        let point = gesture .translation(in: label)
        label.center = point
    }

    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension TakeListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return classe.students.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let studnt = self.classe.students[indexPath.row]
        if indexPath.row == 0{
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cellFirst") as! NameStudentTableViewCell
            
            if let imag = studnt.img{
                cell.userIMg.image = imag
            }else{
                cell.userIMg.image = nil
                let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
                let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
                let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
                if let dirPath          = paths.first
                {
                    let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("\(studnt.uuid!).jpg")
                    let image    = UIImage(contentsOfFile: imageURL.path)
                    if image != nil{
                        studnt.img = image
                    }else{
                        studnt.img = nil
                    }
                }else{
                    studnt.img = nil
                }
                if studnt.img != nil{
                    if let urlString = studnt.imgUrl ,!urlString.isEmpty {
                        switch urlString {
                        case "emoti1.jpg":
                            studnt.img = #imageLiteral(resourceName: "avatar1G")
                            cell.userIMg.image = #imageLiteral(resourceName: "avatar1G")
                            break
                        case "emoti2.jpg":
                            studnt.img = #imageLiteral(resourceName: "avatar2G")
                            cell.userIMg.image = #imageLiteral(resourceName: "avatar2G")
                            break
                        case "emoti3.jpg":
                            studnt.img = #imageLiteral(resourceName: "avatar3G")
                            cell.userIMg.image = #imageLiteral(resourceName: "avatar3G")
                            break
                        case "emoti4.jpg":
                            studnt.img = #imageLiteral(resourceName: "avatar4G")
                            cell.userIMg.image = #imageLiteral(resourceName: "avatar4G")
                            break
                        case "emoti5.jpg":
                            studnt.img = #imageLiteral(resourceName: "avatar5G")
                            cell.userIMg.image = #imageLiteral(resourceName: "avatar5G")
                            break
                        case "emoti6.jpg":
                            studnt.img = #imageLiteral(resourceName: "avatar6G")
                            cell.userIMg.image = #imageLiteral(resourceName: "avatar6G")
                            break
                        case "emoti7.jpg":
                            studnt.img = #imageLiteral(resourceName: "avatar7G")
                            cell.userIMg.image = #imageLiteral(resourceName: "avatar7G")
                            break
                        case "emoti8.jpg":
                            studnt.img = #imageLiteral(resourceName: "avatar8G")
                            cell.userIMg.image = #imageLiteral(resourceName: "avatar8G")
                            break
                        case "emoti9.jpg":
                            studnt.img = #imageLiteral(resourceName: "avatar9G")
                            cell.userIMg.image = #imageLiteral(resourceName: "avatar9G")
                            break
                        default:
                            let request = Request(url: URL(string:urlString)!)
                            
                            Nuke.loadImage(with: request, into: cell.userIMg) { [weak view] response, _ in
                                //                view?.image = response.value
                                if let error = response.error{
                                    cell.userIMg.backgroundColor = UIColor(red: 6.0/255.0, green: 169.0/255.0, blue: 244.0/255.0, alpha: 1)
                                }else{
                                    print(response.value!)
                                    let resizedImage = Toucan.Resize.resizeImage(response.value!, size: CGSize(width: 200, height: 200))
                                    cell.userIMg.image = resizedImage
                                    studnt.img = resizedImage
                                    _ = self.saveImage(resizedImage, path: self.fileInDocumentsDirectory("\(studnt.uuid!).jpg"))

                                }
                            }
                            break
                        }
                    }
                }
            }
            cell.userIMg.backgroundColor = UIColor(red: 6.0/255.0, green: 169.0/255.0, blue: 244.0/255.0, alpha: 1)
            cell.nameStudentLabel.text = "\(studnt.name!)"
            cell.delegate = self
            cell.index = indexPath
            cell.student = studnt
            cell.userIMg.clipsToBounds = true
            cell.userIMg.layer.cornerRadius = cell.userIMg.frame.size.width / 2
            cell.tag = indexPath.row
            return cell
        }else{
            let cell =  tableView.dequeueReusableCell(withIdentifier: "cell") as! ImageStudentTableViewCell
            cell.studentImg.clipsToBounds = true
            if let imag = studnt.img{
                cell.studentImg.image = imag
            }else{
                cell.studentImg.image = nil
                let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
                let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
                let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
                if let dirPath          = paths.first
                {
                    let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("\(studnt.uuid!).jpg")
                    let image    = UIImage(contentsOfFile: imageURL.path)
                    if image != nil{
                        studnt.img = image
                    }else{
                        studnt.img = nil
                    }
                }else{
                    studnt.img = nil
                }
                if studnt.img != nil{
                    if let urlString = studnt.imgUrl ,!urlString.isEmpty {
                        switch urlString {
                        case "emoti1.jpg":
                            studnt.img = #imageLiteral(resourceName: "avatar1G")
                            cell.studentImg.image = #imageLiteral(resourceName: "avatar1G")
                            break
                        case "emoti2.jpg":
                            studnt.img = #imageLiteral(resourceName: "avatar2G")
                            cell.studentImg.image = #imageLiteral(resourceName: "avatar2G")
                            break
                        case "emoti3.jpg":
                            studnt.img = #imageLiteral(resourceName: "avatar3G")
                            cell.studentImg.image = #imageLiteral(resourceName: "avatar3G")
                            break
                        case "emoti4.jpg":
                            studnt.img = #imageLiteral(resourceName: "avatar4G")
                            cell.studentImg.image = #imageLiteral(resourceName: "avatar4G")
                            break
                        case "emoti5.jpg":
                            studnt.img = #imageLiteral(resourceName: "avatar5G")
                            cell.studentImg.image = #imageLiteral(resourceName: "avatar5G")
                            break
                        case "emoti6.jpg":
                            studnt.img = #imageLiteral(resourceName: "avatar6G")
                            cell.studentImg.image = #imageLiteral(resourceName: "avatar6G")
                            break
                        case "emoti7.jpg":
                            studnt.img = #imageLiteral(resourceName: "avatar7G")
                            cell.studentImg.image = #imageLiteral(resourceName: "avatar7G")
                            break
                        case "emoti8.jpg":
                            studnt.img = #imageLiteral(resourceName: "avatar8G")
                            cell.studentImg.image = #imageLiteral(resourceName: "avatar8G")
                            break
                        case "emoti9.jpg":
                            studnt.img = #imageLiteral(resourceName: "avatar9G")
                            cell.studentImg.image = #imageLiteral(resourceName: "avatar9G")
                            break
                        default:
                            let request = Request(url: URL(string:urlString)!)
                            
                            Nuke.loadImage(with: request, into: cell.studentImg) { [weak view] response, _ in
                                //                view?.image = response.value
                                if let error = response.error{
                                    cell.studentImg.backgroundColor = UIColor(red: 6.0/255.0, green: 169.0/255.0, blue: 244.0/255.0, alpha: 1)
                                }else{
                                    print(response.value!)
                                    cell.studentImg.image = response.value
                                    studnt.img = response.value
                                    _ = self.saveImage(response.value!, path: self.fileInDocumentsDirectory("\(studnt.uuid!).jpg"))

                                }
                            }
                            break
                        }
                    }
                }
            }

            if indexPath.row == 1{
                cell.studentImg.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
                cell.studentImg.layer.cornerRadius = cell.studentImg.frame.size.width / (2 * 0.8)
            }else if indexPath.row == 2{
                cell.studentImg.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
                cell.studentImg.layer.cornerRadius = cell.studentImg.frame.size.width / (2 * 0.6)
            }
            return cell
        }
    }
    
    //ReverseExtension also supports handling UITableViewDelegate.
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("scrollView.contentOffset.y =", scrollView.contentOffset.y)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 650
        }else{
            return 160
        }
    }
}

extension TakeListViewController: listDelegate{
    func actionListFor(student: Student, cell: NameStudentTableViewCell) {
        print(cell.index)
        for alumno in clssAux.students{
            if alumno.uuid == student.uuid{
                alumno.isHere = student.isHere
                alumno.isAlmostNotHere = student.isAlmostNotHere
            }
        }
        cell.userIMg.image = nil
        
        classe.students.remove(at: cell.index.row)
        self.tableView.deleteRows(at: [cell.index], with: .none)
        tableView.reloadData()
        if classe.students.count == 0{
            let popUpv = self.storyboard?.instantiateViewController(withIdentifier: "popUpListView") as! PopUpListCompletedViewController
            popUpv.classe = clssAux
            popUpv.delegate = self
            popUpv.modalPresentationStyle = .overCurrentContext
            popUpv.modalTransitionStyle = .crossDissolve
            self.present(popUpv, animated: true, completion: {
                    
            })
        }
        //        self.tableView.deleteRows(at: [IndexPath(row: cell.tag, section: 0)], with: .automatic)
    }
    
    func reloadTable() {
        self.tableView.reloadData()
    }
}

extension TakeListViewController: EditListPopUpDelegate{
    func finishList() {
        self.startSpinner()
        print("a")
        self.tWs.asistencia(classe: clssAux, period: self.period, date: date.convertedDate(calendar: .current)!)
    }
    
    func editList() {
        let editList = self.storyboard?.instantiateViewController(withIdentifier: "EditListView") as! EditListStudentsViewController
        editList.classe = clssAux
        editList.period = self.period
        editList.date = date
        self.navigationController?.pushViewController(editList, animated: true)
    }
}

extension TakeListViewController: teacherDelegate{
    func didSuccessTakeList() {
        let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.stopSpinner()
            self.navigationController?.popViewController(animated: true)
        }
        print("Taken")
    }
    
    func didFailTakeList(message: String) {
        self.stopSpinner()
        classe.students = clssAux.students
        tableView.reloadData()
        let error = self.errorAlert(message: message)
        self.present(error, animated: true) {
            
        }
    }
    
    func didSuccessGetReportByDay(students: [Student]) {
//        if students.count == 0{
//            self.students = students
//        }else{
//            self.students = students
//        }
        var edit = false
        for s in students{
            let student = clssAux.students.filter{$0.uuid == s.uuid}.first
            if student?.criterios.count == 0{
                student?.criterios.append(clssAux.criterios[0])
                student?.criterios[0].calif = s.criterios[0].calif
            }
            student?.criterios.first?.calif = (s.criterios.first?.calif)!
            if (student?.criterios[0].calif)! > Double(0.0){
                edit = true
            }
            switch (student?.criterios[0].calif)! {
            case Double(0.0):
                student?.isHere = false
                student?.isAlmostNotHere = false
                break
            case Double(1.0):
                student?.isHere = true
                student?.isAlmostNotHere = false
                break
            case Double(2.0):
                student?.isHere = false
                student?.isAlmostNotHere = false
                break
            case Double(3.0):
                student?.isHere = false
                student?.isAlmostNotHere = true
                break
            default:
                student?.isHere = false
                student?.isAlmostNotHere = false
                break
            }
        }
        if edit{
            if !pushed{
                print("edit")
                let editList = self.storyboard?.instantiateViewController(withIdentifier: "EditListView") as! EditListStudentsViewController
                editList.classe = clssAux
                editList.period = self.period
                editList.date = date
                self.navigationController?.pushViewController(editList, animated: true)
                pushed = true
                self.stopSpinner()
                return
            }

        }
        tableView.alpha = 1
        tableView.reloadData()
        self.stopSpinner()
    }
    
    func didFailGetReportByDay(message: String) {
        tableView.alpha = 1
        self.stopSpinner()
        let errorvc = self.errorAlert(message: message)
        self.present(errorvc, animated: true) {
            
        }
//        self.students = classe.students
        tableView.reloadData()
    }
}
