//
//  EditPeriodsViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 12/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

class EditPeriodsViewController: UIViewController {
    
    var classe : Class!
    @IBOutlet weak var tableView: UITableView!
    var tWS = TeacherWS()
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var stackView: UIStackView!
    
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
        if !internetProvider.sharedInstance.isVertical {
            print("Landscape")
            self.stackView.axis = .horizontal
            titleLabel.font = UIFont.boldSystemFont(ofSize: 28)
        } else {
            self.stackView.axis = .vertical
            titleLabel.font = UIFont.boldSystemFont(ofSize: 50)
        }
        self.view.layoutIfNeeded()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            internetProvider.sharedInstance.isVertical = false
            self.stackView.axis = .horizontal
            titleLabel.font = UIFont.boldSystemFont(ofSize: 28)
        } else {
            self.stackView.axis = .vertical
            internetProvider.sharedInstance.isVertical = true
            titleLabel.font = UIFont.boldSystemFont(ofSize: 50)
        }
        self.view.layoutIfNeeded()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = Localization("EditPeriods")
        tWS.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EditPeriodsViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return classe.periods.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "periodCell") as! EditPeriodsTableViewCell
        let textAtt : NSMutableAttributedString  = NSMutableAttributedString(string: "\(Localization("Period")) \(indexPath.row + 1) (\(classe.periods[indexPath.row].startDate.commonDescription) - \(classe.periods[indexPath.row].endDate.commonDescription))")
        let text = NSString(string: "\(Localization("Period")) \(indexPath.row + 1) (\(classe.periods[indexPath.row].startDate.commonDescription) - \(classe.periods[indexPath.row].endDate.commonDescription))")
        if let range : NSRange = text.range(of: "(\(classe.periods[indexPath.row].startDate.commonDescription) - \(classe.periods[indexPath.row].endDate.commonDescription))") {
            print(range)
            textAtt.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 16), range: range)
        }
        cell.namePeriodLabel.attributedText = textAtt
        cell.period = classe.periods[indexPath.row]
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
}

extension EditPeriodsViewController : EditPeriodsDelegate{
    func edit(period: Period) {
        
        let epvc = self.storyboard?.instantiateViewController(withIdentifier: "editPeriodView") as! EditPeriodViewController
        epvc.period = period
        epvc.classe = self.classe
        self.navigationController?.pushViewController(epvc, animated: true)
    }
    
    func delete(period: Period) {
//        let poprv = self.storyboard?.instantiateViewController(withIdentifier: "removePeriodPopUpView") as! PopUpRemovePeriodViewController
//        poprv.delegate = self
//        poprv.period = period
//        self.navigationController?.present(poprv, animated: true, completion: {
//            
//        })
        let alert = UIAlertController(title: nil, message: Localization("DeletePeriodAlert"), preferredStyle: .alert)
        let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default) { (action) in
            self.startSpinner()
            self.tWS.delete(period: period)
        }
        let cancelAction = UIAlertAction(title: Localization("Cancelar"), style: .cancel) { (action) in
            
        }
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true) { 
            
        }
        
    }
}

extension EditPeriodsViewController: RemoveAnyDelegate{
    func remove(period: Any) {

    }
}

extension EditPeriodsViewController: teacherDelegate{
    func didSuccessDelete(period: Period) {
        self.stopSpinner()
        if let x = self.classe.periods.index(of: period){
            self.classe.periods.remove(at: x)
        }
        self.tableView.reloadData()
    }
    
    func didFailDeletePeriod(message: String) {
        self.stopSpinner()
        let error = errorAlert(message: message)
        self.present(error, animated: true) { 
            
        }
    }
}
