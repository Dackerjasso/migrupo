//
//  ScreenSaverViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 12/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import NotificationCenter
import UserNotifications
import RQShineLabel


class ScreenSaverViewController: UIViewController{
    
    var shineLabel : RQShineLabel! = nil
    var currentPhrase = 0
    //    NSAttributedString(string:Localization("A1")), NSAttributedString(string:Localization("A2")), NSAttributedString(string:Localization("A3")), NSAttributedString(string:Localization("A4")), NSAttributedString(string:Localization("A5")), NSAttributedString(string:Localization("A6")), NSAttributedString(string:Localization("A7")), NSAttributedString(string:Localization("A8")), NSAttributedString(string:Localization("A9")), NSAttributedString(string:Localization("A10")), NSAttributedString(string:Localization("A11")), NSAttributedString(string:Localization("A12")), NSAttributedString(string:Localization("A13")), NSAttributedString(string:Localization("A14"))
    //
    //    NSAttributedString(string:Localization("SV1")), NSAttributedString(string:Localization("SV2")), NSAttributedString(string:Localization("SV3")), NSAttributedString(string:Localization("SV4")), NSAttributedString(string:Localization("SV5")), NSAttributedString(string:Localization("SV6")), NSAttributedString(string:Localization("SV7")), NSAttributedString(string:Localization("SV8")), NSAttributedString(string:Localization("SV9")), NSAttributedString(string:Localization("SV10")), NSAttributedString(string:Localization("SV11")), NSAttributedString(string:Localization("SV12")), NSAttributedString(string:Localization("SV13")), NSAttributedString(string:Localization("SV14"))
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            internetProvider.sharedInstance.isVertical = false
        } else {
            internetProvider.sharedInstance.isVertical = true
            
        }
        self.view.layoutIfNeeded()
        
    }
    
    override func loadView() {
        view = UIView(frame: UIScreen.main.bounds)
        view.autoresizingMask = UIViewAutoresizing.flexibleWidth.union(.flexibleHeight)
        view.backgroundColor = .black
    }
    
    func dismissAtTap(){
        shineLabel.alpha = 0
        self.dismiss(animated: true) {
            
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.shineLabel.shine()
        
        shineLabel.alpha = 1
        let when = DispatchTime.now() + 8
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.runTimedCode()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissAtTap))
        tap.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tap)
        
        self.shineLabel = RQShineLabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width , height: 300))
        self.shineLabel.numberOfLines = 0
        self.shineLabel.textColor = self.color(y: Int(arc4random_uniform(UInt32(4)) + UInt32(0)))
        var x :  NSMutableAttributedString!
        switch currentPhrase {
        case 0:
            x = NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("SV1")))
            x.append(NSAttributedString(string: "\n"))
            x.append(NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("A1"))))
            break
        case 1:
            x = NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("SV2")))
            x.append(NSAttributedString(string: "\n"))
            x.append(NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("A2"))))
            break
        case 2:
            x = NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("SV3")))
            x.append(NSAttributedString(string: "\n"))
            x.append(NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("A3"))))
            break
        case 3:
            x = NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("SV4")))
            x.append(NSAttributedString(string: "\n"))
            x.append(NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("A4"))))
            break
        case 4:
            x = NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("SV5")))
            x.append(NSAttributedString(string: "\n"))
            x.append(NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("A5"))))
            break
        case 5:
            x = NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("SV6")))
            x.append(NSAttributedString(string: "\n"))
            x.append(NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("A6"))))
            break
        case 6:
            x = NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("SV7")))
            x.append(NSAttributedString(string: "\n"))
            x.append(NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("A7"))))
            break
        case 7:
            x = NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("SV8")))
            x.append(NSAttributedString(string: "\n"))
            x.append(NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("A8"))))
            break
        case 8:
            x = NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("SV9")))
            x.append(NSAttributedString(string: "\n"))
            x.append(NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("A9"))))
            break
        case 9:
            x = NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("SV10")))
            x.append(NSAttributedString(string: "\n"))
            x.append(NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("A10"))))
            break
        case 10:
            x = NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("SV11")))
            x.append(NSAttributedString(string: "\n"))
            x.append(NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("A11"))))
            break
        case 11:
            x = NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("SV12")))
            x.append(NSAttributedString(string: "\n"))
            x.append(NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("A12"))))
            break
        case 12:
            x = NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("SV13")))
            x.append(NSAttributedString(string: "\n"))
            x.append(NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("A13"))))
            break
        case 13:
            x = NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("SV14")))
            x.append(NSAttributedString(string: "\n"))
            x.append(NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("A14"))))
            break
        default:
            break
        }
        self.shineLabel.attributedText = x
        self.shineLabel.font = UIFont.boldSystemFont(ofSize: 20)
        self.shineLabel.backgroundColor = UIColor.clear
        self.shineLabel.center = self.view.center
        self.shineLabel.frame.origin.y = CGFloat(self.position(y: Int(arc4random_uniform(UInt32(4)) + UInt32(0))))
        self.shineLabel.textAlignment = self.align(y: Int(arc4random_uniform(UInt32(3)) + UInt32(0)))
        self.view.addSubview(shineLabel)
        shineLabel.alpha = 0
        
        
        
        
        let imageDetalle = UIImageView(frame: CGRect(x: 0, y: self.view.frame.size.height - 73 , width: 151, height: 73))
        imageDetalle.image = #imageLiteral(resourceName: "detalle")
        self.view.addSubview(imageDetalle)
        // Do any additional setup after loading the view.
    }
    
    func runTimedCode() {
        self.currentPhrase += 1
        if self.currentPhrase == 14{
            self.currentPhrase = 0
        }
        self.shineLabel.fadeOut {
            self.shineLabel.alpha = 0
            self.shineLabel.textColor = self.color(y: Int(arc4random_uniform(UInt32(4)) + UInt32(0)))
            self.shineLabel.frame.origin.y = CGFloat(self.position(y: Int(arc4random_uniform(UInt32(4)) + UInt32(0))))
            self.shineLabel.alpha = 1
            self.shineLabel.shine()
            
            var x :  NSMutableAttributedString!
            switch self.currentPhrase {
            case 0:
                x = NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("SV1")))
                x.append(NSAttributedString(string: "\n"))
                x.append(NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("A1"))))
                break
            case 1:
                x = NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("SV2")))
                x.append(NSAttributedString(string: "\n"))
                x.append(NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("A2"))))
                break
            case 2:
                x = NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("SV3")))
                x.append(NSAttributedString(string: "\n"))
                x.append(NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("A3"))))
                break
            case 3:
                x = NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("SV4")))
                x.append(NSAttributedString(string: "\n"))
                x.append(NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("A4"))))
                break
            case 4:
                x = NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("SV5")))
                x.append(NSAttributedString(string: "\n"))
                x.append(NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("A5"))))
                break
            case 5:
                x = NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("SV6")))
                x.append(NSAttributedString(string: "\n"))
                x.append(NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("A6"))))
                break
            case 6:
                x = NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("SV7")))
                x.append(NSAttributedString(string: "\n"))
                x.append(NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("A7"))))
                break
            case 7:
                x = NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("SV8")))
                x.append(NSAttributedString(string: "\n"))
                x.append(NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("A8"))))
                break
            case 8:
                x = NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("SV9")))
                x.append(NSAttributedString(string: "\n"))
                x.append(NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("A9"))))
                break
            case 9:
                x = NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("SV10")))
                x.append(NSAttributedString(string: "\n"))
                x.append(NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("A10"))))
                break
            case 10:
                x = NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("SV11")))
                x.append(NSAttributedString(string: "\n"))
                x.append(NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("A11"))))
                break
            case 11:
                x = NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("SV12")))
                x.append(NSAttributedString(string: "\n"))
                x.append(NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("A12"))))
                break
            case 12:
                x = NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("SV13")))
                x.append(NSAttributedString(string: "\n"))
                x.append(NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("A13"))))
                break
            case 13:
                x = NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("SV14")))
                x.append(NSAttributedString(string: "\n"))
                x.append(NSMutableAttributedString (attributedString: NSAttributedString(string:  Localization("A14"))))
                break
            default:
                break
            }
            self.shineLabel.attributedText = x
            self.shineLabel.textAlignment = self.align(y: Int(arc4random_uniform(UInt32(3)) + UInt32(0)))
            let when = DispatchTime.now() + 8
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.runTimedCode()
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ScreenSaverViewController {
    fileprivate func color(y: Int) -> UIColor{
        switch y {
        case 0:
            return UIColor(red: 255.0/255.0 , green: 178.0/255.0, blue: 0.0/255.0, alpha: 1)
        case 1:
            return UIColor(red: 76.0/255.0, green: 176.0/255.0, blue: 80.0/255.0, alpha: 1)
        case 2:
            return UIColor(red:  6.0/155.0, green: 169.0/255.0, blue: 244.0/155.0, alpha: 1)
        case 3:
            return UIColor(red: 221.0/255.0, green: 25.0/255.0, blue: 25.0/255.0, alpha: 1)
        default:
            return UIColor(red: 76.0/255.0, green: 176.0/255.0, blue: 80.0/255.0, alpha: 1)
        }
    }
    
    fileprivate func position(y: Int) -> Int{
        switch y {
        case 0:
            return 0
        case 1:
            return Int((self.view.frame.size.height  - 80) / 4)
        case 2:
            return Int(((self.view.frame.size.height - 80) / 4) * 2)
        case 3:
            return Int(((self.view.frame.size.height - 80) / 4) * 3)
        default:
            return Int((self.view.frame.size.height - 80) / 4)
        }
    }
    
    fileprivate func align (y: Int) -> NSTextAlignment{
        switch y {
        case 0:
            return NSTextAlignment.center
        case 1:
            return NSTextAlignment.left
        case 2:
            return NSTextAlignment.right
        default:
            return NSTextAlignment.center
        }
    }
    
}
