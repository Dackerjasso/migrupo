//
//  SelectStudentForTeamViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 06/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import Nuke
import Toucan

protocol SelectStudentDelegate {
    func createTeamWith(students: [Student])
    
}

class SelectStudentForTeamViewController: UIViewController {
    
    var delegate: SelectStudentDelegate?
    var classe : Class!
    var students : [Student] = []
    let btn1 = UIButton(type: .custom)

    func getDocumentsURL() -> URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsURL
    }
    
    func fileInDocumentsDirectory(_ filename: String) -> URL{
        let fileURL = getDocumentsURL().appendingPathComponent(filename)
        return fileURL
    }
    
    func saveImage (_ image: UIImage, path: URL ) -> Bool{
        
        let pngImageData = UIImageJPEGRepresentation(image, 1.0)
        UIGraphicsBeginImageContext(image.size)
        image.draw(in: CGRect(x:0, y:0, width:image.size.width, height:image.size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let jpgImageData = UIImageJPEGRepresentation(newImage!, 1.0)
        
        let result = (try? jpgImageData!.write(to: URL(fileURLWithPath: path.path), options: [.atomic])) != nil
        return result
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func finilize(){
        self.navigationController?.popViewController(animated: true)
        for student in students{
            classe.students.remove(at: classe.students.index(of: student)!)
        }
        self.delegate?.createTeamWith(students: self.students)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btn1.setImage(#imageLiteral(resourceName: "btnFinalizar"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 46.5, height: 46.5)
        btn1.addTarget(self, action: #selector(self.finilize), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        
        self.navigationItem.setRightBarButtonItems([item1], animated: true)
        self.navigationItem.rightBarButtonItem?.isEnabled = false

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SelectStudentForTeamViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return classe.students.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "seeListStudentCell") as! StudentSeeListTableViewCell
        let student = classe.students[indexPath.row]
        if let imag = student.img{
            cell.userImg.image = imag
        }else{
            cell.userImg.image = nil
            if let urlString = student.imgUrl ,!urlString.isEmpty {
                switch urlString {
                case "emoti1.jpg":
                    student.img = #imageLiteral(resourceName: "avatar1G")
                    cell.userImg.image = #imageLiteral(resourceName: "avatar1G")
                    break
                case "emoti2.jpg":
                    student.img = #imageLiteral(resourceName: "avatar2G")
                    cell.userImg.image = #imageLiteral(resourceName: "avatar2G")
                    break
                case "emoti3.jpg":
                    student.img = #imageLiteral(resourceName: "avatar3G")
                    cell.userImg.image = #imageLiteral(resourceName: "avatar3G")
                    break
                case "emoti4.jpg":
                    student.img = #imageLiteral(resourceName: "avatar4G")
                    cell.userImg.image = #imageLiteral(resourceName: "avatar4G")
                    break
                case "emoti5.jpg":
                    student.img = #imageLiteral(resourceName: "avatar5G")
                    cell.userImg.image = #imageLiteral(resourceName: "avatar5G")
                    break
                case "emoti6.jpg":
                    student.img = #imageLiteral(resourceName: "avatar6G")
                    cell.userImg.image = #imageLiteral(resourceName: "avatar6G")
                    break
                case "emoti7.jpg":
                    student.img = #imageLiteral(resourceName: "avatar7G")
                    cell.userImg.image = #imageLiteral(resourceName: "avatar7G")
                    break
                case "emoti8.jpg":
                    student.img = #imageLiteral(resourceName: "avatar8G")
                    cell.userImg.image = #imageLiteral(resourceName: "avatar8G")
                    break
                case "emoti9.jpg":
                    student.img = #imageLiteral(resourceName: "avatar9G")
                    cell.userImg.image = #imageLiteral(resourceName: "avatar9G")
                    break
                default:
                    let request = Request(url: URL(string:urlString)!)
                    
                    Nuke.loadImage(with: request, into: cell.userImg) { [weak view] response, _ in
                        //                view?.image = response.value
                        if let error = response.error{
                            
                        }else{
                        print(response.value!)
                        let resizedImage = Toucan.Resize.resizeImage(response.value!, size: CGSize(width: 200, height: 200))
                        cell.userImg.image = resizedImage
                        student.img = resizedImage
                            _ = self.saveImage(resizedImage, path: self.fileInDocumentsDirectory("\(student.uuid!).jpg"))

                        }
                    }
                    break
                }

            }
        }
        cell.nameLabel.text = student.name
        cell.button.tag = indexPath.row
        if students.contains(student){
            cell.button.isSelected = true
            cell.fondoImg.image = #imageLiteral(resourceName: "fondoAlumnoNoDisponible")
        }else{
            cell.button.isSelected = false
            cell.fondoImg.image = #imageLiteral(resourceName: "fondoAlumno1")
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let student = classe.students[indexPath.row]
        if students.contains(student){
            students.remove(at: students.index(of: student)!)
        }else{
            students.append(student)
        }
        if students.count > 0{
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        }else{
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        }
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let student = classe.students[indexPath.row]
        if students.contains(student){
            students.remove(at: students.index(of: student)!)
        }else{
            students.append(student)
        }
        if students.count > 0{
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        }else{
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        }
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 145
    }
}

