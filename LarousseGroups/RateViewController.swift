//
//  RateViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 01/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import SwiftyStarRatingView
import Nuke
import AVFoundation
import Toucan

protocol RateDelegate {
    func rated()
}

class RateViewController: UIViewController {
    var delegate : RateDelegate?
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var imgAnimate: UIImageView!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var rateView: SwiftyStarRatingView!
    @IBOutlet weak var teamNameLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    var player: AVAudioPlayer?
    var currentRate: Int!
    
    var team: Team!
    var student: Student!
    @IBOutlet weak var teamView: UIView!
    var classe : Class!
    var period: Int!
    var tWS = TeacherWS()
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
    }
    @IBOutlet weak var teamLabel: UILabel!
    
    func getDocumentsURL() -> URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsURL
    }
    
    func fileInDocumentsDirectory(_ filename: String) -> URL{
        let fileURL = getDocumentsURL().appendingPathComponent(filename)
        return fileURL
    }
    
    func saveImage (_ image: UIImage, path: URL ) -> Bool{
        
        let pngImageData = UIImageJPEGRepresentation(image, 1.0)
        UIGraphicsBeginImageContext(image.size)
        image.draw(in: CGRect(x:0, y:0, width:image.size.width, height:image.size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let jpgImageData = UIImageJPEGRepresentation(newImage!, 1.0)
        
        let result = (try? jpgImageData!.write(to: URL(fileURLWithPath: path.path), options: [.atomic])) != nil
        return result
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        teamLabel.text = Localization("Team")
        tWS.delegate = self
        if let st = student{
            if let imag = st.img{
                self.userImg.image = imag
            }else{
                self.userImg.image = nil
                if let urlString = student.imgUrl ,!urlString.isEmpty {
                    switch urlString {
                    case "emoti1.jpg":
                        student.img = #imageLiteral(resourceName: "avatar1G")
                        userImg.image = #imageLiteral(resourceName: "avatar1G")
                        break
                    case "emoti2.jpg":
                        student.img = #imageLiteral(resourceName: "avatar2G")
                        userImg.image = #imageLiteral(resourceName: "avatar2G")
                        break
                    case "emoti3.jpg":
                        student.img = #imageLiteral(resourceName: "avatar3G")
                        userImg.image = #imageLiteral(resourceName: "avatar3G")
                        break
                    case "emoti4.jpg":
                        student.img = #imageLiteral(resourceName: "avatar4G")
                        userImg.image = #imageLiteral(resourceName: "avatar4G")
                        break
                    case "emoti5.jpg":
                        student.img = #imageLiteral(resourceName: "avatar5G")
                        userImg.image = #imageLiteral(resourceName: "avatar5G")
                        break
                    case "emoti6.jpg":
                        student.img = #imageLiteral(resourceName: "avatar6G")
                        userImg.image = #imageLiteral(resourceName: "avatar6G")
                        break
                    case "emoti7.jpg":
                        student.img = #imageLiteral(resourceName: "avatar7G")
                        userImg.image = #imageLiteral(resourceName: "avatar7G")
                        break
                    case "emoti8.jpg":
                        student.img = #imageLiteral(resourceName: "avatar8G")
                        userImg.image = #imageLiteral(resourceName: "avatar8G")
                        break
                    case "emoti9.jpg":
                        student.img = #imageLiteral(resourceName: "avatar9G")
                        userImg.image = #imageLiteral(resourceName: "avatar9G")
                        break
                    default:
                        let request = Request(url: URL(string:urlString)!)
                        
                        Nuke.loadImage(with: request, into: self.userImg) { [weak view] response, _ in
                            //                view?.image = response.value
                            if let error = response.error{
                                
                            }else{
                            let resizedImage = Toucan.Resize.resizeImage(response.value!, size: CGSize(width: 200, height: 200))
                            print(response.value!)
                            self.userImg.image = resizedImage
                            st.img = resizedImage
                                _ = self.saveImage(resizedImage, path: self.fileInDocumentsDirectory("\(self.student.uuid!).jpg"))

                            }
                        }
                        break
                    }

                }
            }
        }

        userImg.clipsToBounds = true
        userImg.layer.cornerRadius = userImg.frame.size.width / 2
        if let s = student{
            nameLabel.text = s.name
            teamView.alpha = 0
        }else{
            teamNameLabel.text = team.name
            teamView.alpha = 1
            userImg.alpha = 0
            nameLabel.alpha = 0
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func playSound(name: String) {
        guard let url = Bundle.main.url(forResource: name, withExtension: "mp3") else {
            print("error")
            return
        }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            player = try AVAudioPlayer(contentsOf: url)
            guard let player = player else { return }
            
            player.play()
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    @IBAction func accept(_ sender: Any) {
        if rateView.value == 0{
            let alert = UIAlertController(title: nil, message: Localization("ErrorStarts"), preferredStyle: .alert)
            let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default, handler: { (action) in
                
            })
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: {
                
            })
            return
        }else{
            let button = sender as! UIButton
            button.isEnabled = false
            if let s = student{
                tWS.rate(students: [s], classe: self.classe, period: self.period)
            }else{
                var students : [Student] = []
                for s in team.students{
                    students.append(s)
                }
                tWS.rate(students: students, classe: self.classe, period: self.period)
            }

        }
    }

    
    
    func afterAnimation() {
        imgAnimate.stopAnimating()
        imgAnimate.image = nil
        imgAnimate.alpha = 0
        self.dismiss(animated: true) {
            self.delegate?.rated()
        }
//        self.dismiss(animated: false) {
//            self.delegate?.rated()
//        }
    }

    @IBAction func tapAction(_ sender: Any) {
        print(rateView.value)
        self.dismiss(animated: false) {
            
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension RateViewController: teacherDelegate{
    func didSuccessRateStudents() {
        switch rateView.value {
        case 3,4,5:
            if rateView.value == 4{
                UIView.animate(withDuration: 0.2, animations: {
                    self.rateView.alpha = 0
                })
            }
            okButton.isEnabled = false
            imgAnimate.alpha = 1
            let imgListArray :NSMutableArray = []
            self.playSound(name: "\(Int(rateView.value))_audio")
            if !internetProvider.sharedInstance.isVertical{
                for countValue in 1...18{
                    let strImageName : String = "l\(Int(rateView.value))_\(countValue)"
                    let image  = UIImage(named:strImageName)
                    imgListArray .add(image!)
                }
                
                self.imgAnimate.animationImages = (imgListArray as! [UIImage]);
                self.imgAnimate.animationDuration = 2.1
                self.imgAnimate.startAnimating()
            } else {
                for countValue in 1...18{
                    let strImageName1 : String = "\(Int(rateView.value))_\(countValue)"
                    let image  = UIImage(named:strImageName1)
                    imgListArray .add(image!)
                }
                
                self.imgAnimate.animationImages = (imgListArray as! [UIImage]);
                self.imgAnimate.animationDuration = 2.1
                self.imgAnimate.startAnimating()
            }

            
            self.perform(#selector(self.afterAnimation), with: nil, afterDelay: 2.1)
            break
        default:
            self.dismiss(animated: true) {
                self.delegate?.rated()
            }
            break
            //                dismiss(animated: false) {
            //                    self.delegate?.rated()
            //                }
        }
    }
    
    func diidFailRateStudents() {
        let error = self.errorAlert(message: "Error, por favor intente de nuevo")
    rateView.alpha = 1
        self.present(error, animated: true) { 
        }
        okButton.isEnabled = true
    }
}
