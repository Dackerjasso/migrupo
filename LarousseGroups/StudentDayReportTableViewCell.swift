//
//  StudentDayReportTableViewCell.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 14/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

class StudentDayReportTableViewCell: UITableViewCell {
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var nameLAbel: UILabel!
    @IBOutlet weak var valueLabel1: UILabel!
    @IBOutlet weak var valueLabel2: UILabel!
    @IBOutlet weak var valueLabel3: UILabel!
    @IBOutlet weak var valueLabel4: UILabel!
    @IBOutlet weak var valueLabel5: UILabel!
    @IBOutlet weak var imgPresent: UIImageView!
    @IBOutlet weak var imgPartic: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        imgUser.clipsToBounds = true
        imgUser.layer.cornerRadius = imgUser.frame.size.width / 2
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
