//
//  PercentViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 26/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

class PercentViewController: UIViewController {

    var totalPercent : Float!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var totalPErcentLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var totalBar: UISlider!
    @IBOutlet weak var stackView: UIStackView!
    var tws = TeacherWS()
    var classe : Class!
    var edit = false
    var currentBar = G8SliderStep()
    var tWS = TeacherWS()
    var isSaved = false
    var values : [Float] = []
    var changed = false
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
        if !internetProvider.sharedInstance.isVertical {
            print("Landscape")
            titleLabel.font = UIFont.boldSystemFont(ofSize: 26)
            self.stackView.axis = .horizontal
        } else {
            self.stackView.axis = .vertical
            titleLabel.font = UIFont.boldSystemFont(ofSize: 50)
        }
        self.view.layoutIfNeeded()
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            self.stackView.axis = .horizontal
            internetProvider.sharedInstance.isVertical = false
            titleLabel.font = UIFont.boldSystemFont(ofSize: 26)
        } else {
            self.stackView.axis = .vertical
            internetProvider.sharedInstance.isVertical = true
            titleLabel.font = UIFont.boldSystemFont(ofSize: 50)
        }
        self.view.layoutIfNeeded()
        
    }
    
    func back(sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go back to the previous ViewController
        if Int(totalPercent) < 20{
            let error = UIAlertController(title: nil, message: Localization("PercentAlert"), preferredStyle: .alert)
            let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default) { (action) in
            }

            error.addAction(okAction)
            self.present(error, animated: true) {
                
            }
            return
        }
        if !changed{
            for (index,c) in classe.criterios.enumerated(){
                if let v : Float = values[index]{
                    if Float(c.value) != v{
                        changed = true
                    }
                }else{
                    changed = true
                }
                //  (cell as! PercentTableViewCell).percentLAbel.text = "\((cell as! PercentTableViewCell).percentBar.value * 10)"
            }
        }
        if changed && !isSaved {
            let error = UIAlertController(title: nil, message: Localization("LostData"), preferredStyle: .alert)
            let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default) { (action) in
                _ = self.navigationController?.popToRootViewController(animated: true)
            }
            let cancelAction = UIAlertAction(title: Localization("Cancelar"), style: .cancel) { (action) in
                
            }
            error.addAction(okAction)
            error.addAction(cancelAction)
            self.present(error, animated: true) {
                
            }
        }else{
            _ = self.navigationController?.popToRootViewController(animated: true)
        }

    }
    
    override func viewDidAppear(_ animated: Bool) {
        updateValue()
    }
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var totalLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = Localization("AsignPercents")
        totalLabel.text = Localization("Total")
        acceptButton.setTitle(Localization("Aceptar"), for: .normal)
        for c in classe.criterios{
            values.append(Float(c.value))
            //  (cell as! PercentTableViewCell).percentLAbel.text = "\((cell as! PercentTableViewCell).percentBar.value * 10)"
        }
        
        
       
        
        
        tWS.delegate = self
        
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(self.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        
        
        
        totalPErcentLabel.textColor = UIColor(red: 255.0/255.0, green: 178.0/255.0, blue: 0.0/255.0, alpha: 1)
        tws.delegate = self
        totalBar.setThumbImage(#imageLiteral(resourceName: "circuloAzul"), for: .normal)
        totalBar.setThumbImage(#imageLiteral(resourceName: "circuloAzul"), for: .highlighted)
        totalBar.setThumbImage(#imageLiteral(resourceName: "circuloAzul"), for: .selected)
        totalBar.setMaximumTrackImage(#imageLiteral(resourceName: "campoTareas")
            .resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 0.0, bottom: 0, right: 6.0)),
                                        for: .normal)
        totalBar.setMinimumTrackImage(#imageLiteral(resourceName: "azul")
            .withRenderingMode(.alwaysTemplate)
            .resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 6.0, bottom: 0, right: 0)),
                                        for: .normal)
        
        totalPercent = 0
        if classe.criterios.count >= 5{
            plusButton.alpha = 0
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.stopSpinner()
    }
    
    @IBAction func acceptAction(_ sender: Any) {
        if totalPercent == 20{
            self.startSpinner()
           tWS.criterios(classe: self.classe)
        }else{
            let errorAlert = self.errorAlert(message: Localization("PercentAlert"))
            self.present(errorAlert, animated: true, completion: {
                
            })
        }
        
//        tws.addCriterios(classe: self.classe)
//        ARSLineProgress.show()
//        self.view.isUserInteractionEnabled = false
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func plusAction(_ sender: Any) {
        let criter = Criterio()
        criter.name = Localization("nameCriter")
        if classe.criterios.count < 5{
            classe.criterios.append(criter)
            if classe.criterios.count == 5{
                plusButton.alpha = 0
            }
        }else{
            plusButton.alpha = 0
        }
        changed = true
        edit = true
        self.tableView.reloadData()
    }
    
    func updateValue(){
        totalPercent = 0
        for c in classe.criterios{
            totalPercent = totalPercent + Float(c.value)
         
            //  (cell as! PercentTableViewCell).percentLAbel.text = "\((cell as! PercentTableViewCell).percentBar.value * 10)"
        }
        
        print(totalPercent)
        totalBar.value = totalPercent
        if ((totalPercent).truncatingRemainder(dividingBy: 5)) == 0{
            if Int(totalPercent) < 20{
                totalPErcentLabel.textColor = UIColor(red: 255.0/255.0, green: 178.0/255.0, blue: 0.0/255.0, alpha: 1)
            }else if Int(totalPercent ) == 20{
                totalPErcentLabel.textColor = UIColor(red: 6.0/255.0, green: 169.0/255.0, blue: 244.0/255.0, alpha: 1)
            }else{
                totalPErcentLabel.textColor = UIColor(red: 230.0/255.0, green: 19.0/255.0, blue: 34.0/255.0, alpha: 1)
                
            }
        }
        totalPErcentLabel.text = "\(Int(totalPercent * 5)) %"
    }
}

extension PercentViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        totalPercent = 0
        for (index, cell) in tableView.visibleCells.enumerated(){
//            totalPercent = totalPercent + (cell as! PercentTableViewCell).percentBar.value
            //  (cell as! PercentTableViewCell).percentLAbel.text = "\((cell as! PercentTableViewCell).percentBar.value * 10)"
        }
//        print(totalPercent)
//        totalBar.value = totalPercent
//        if ((totalPercent * 5).truncatingRemainder(dividingBy: 5)) == 0{
//            if Int(totalPercent * 5) < 100{
//                totalPErcentLabel.textColor = UIColor(red: 255.0/255.0, green: 178.0/255.0, blue: 0.0/255.0, alpha: 1)
//            }else if Int(totalPercent * 5) == 100{
//                totalPErcentLabel.textColor = UIColor(red: 6.0/255.0, green: 169.0/255.0, blue: 244.0/255.0, alpha: 1)
//            }else{
//                totalPErcentLabel.textColor = UIColor(red: 230.0/255.0, green: 19.0/255.0, blue: 34.0/255.0, alpha: 1)
//            }
//        }
//        totalPErcentLabel.text = "\(Int(totalPercent * 5)) %"
        return classe.criterios.count <= 5 ? classe.criterios.count : 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! PercentTableViewCell
        cell.delegate = self
        if indexPath.row == 0{
            cell.titleTF.text = Localization("Asistence")
        }else if indexPath.row == 1{
            cell.titleTF.text = Localization("Participation")
        }else{
            cell.titleTF.text = classe.criterios[indexPath.row].name
        }
        cell.percentBar.delegate = self
        cell.editButton.isSelected = false
        cell.okButton.alpha = 0
        cell.criterio = classe.criterios[indexPath.row]
        cell.percentBar.tag = indexPath.row
        cell.titleTF.isEnabled = false
        cell.percentBar.value = Float(classe.criterios[indexPath.row].value)
        cell.titleTF.textColor = UIColor(red: 173.0/255.0, green: 173.0/255.0, blue: 173.0/255.0, alpha: 1)
//        cell.percentBar.addTarget(self, action: #selector(self.change(_:)), for: .valueChanged)
        if indexPath.row == 0 || indexPath.row == 1{
            cell.editButton.alpha = 0
        }else{
            cell.okButton.tag = indexPath.row
            cell.okButton.addTarget(self, action: #selector(self.deleteCell(_:)), for: .touchUpInside)
        }
        return cell
    }
    
//    func change(_ sender : Any){
//        print((sender as! G8SliderStep).tag)
//        currentBar = (sender as! G8SliderStep)
//        criterios[currentBar.tag].value = Double((sender as! G8SliderStep).value)
//    }
    
    func deleteCell(_ sender: Any?) {
        let c = classe.criterios[(sender as! UIButton).tag]
        if let idd = c.dbid{
            print(idd)
            tws.delete(creiterio: c)
        }else{
            classe.criterios.remove(at: (sender as! UIButton).tag)
            if classe.criterios.count < 5{
                plusButton.alpha = 1
            }
            tableView.reloadData()
            changed = true
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return tableView.frame.size.height / 5
    }
}

extension PercentViewController: G8SliderDelegate{
    func change(value: Int, bar: G8SliderStep) {
        edit = true
        print(">>>>>>  \(bar.tag)")
        classe.criterios[bar.tag].value = Double(value)
        updateValue()
    }
}

extension PercentViewController: teacherDelegate{
    func didSuccessDelete(criterio: Criterio) {
        self.stopSpinner()
        if classe.criterios.contains(criterio){
            classe.criterios.remove(at: classe.criterios.index(of: criterio)!)
        }
        if classe.criterios.count < 5{
            plusButton.alpha = 1
        }
        for c in UserProvider.sharedInstance.classes{
            if c.dbid == classe.dbid{
                for cc in c.criterios{
                    if cc.dbid == criterio.dbid{
                        let i = c.criterios.index(of: cc)
                        c.criterios.remove(at: i!)
                    }
                }
            }
        }
        tableView.reloadData()
        changed = true
        updateValue()
    }
    
    func didFailDeleteCriterio(message: String) {
        self.stopSpinner()
        let error = self.errorAlert(message: message)
        self.present(error, animated: true) { 
            
        }
    }
    
    func didSuccessUpdateCriteriosOn(classe: Class) {
        let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.stopSpinner()
            NotificationCenter.default.post(name: Notification.Name(rawValue: "BackFromPercent"), object: self)
            self.navigationController?.popViewController(animated: true)
            self.tableView.reloadData()
        }
        isSaved = true
    }
    
    func didFailUpdateCriteriosOnClasse(message: String) {
        self.stopSpinner()
        let errorV = self.errorAlert(message: message)
        self.present(errorV, animated: true) {
            
        }
    }
}

extension PercentViewController: nameChangedDelegate{
    func nameChanged() {
        changed = true
    }
}

