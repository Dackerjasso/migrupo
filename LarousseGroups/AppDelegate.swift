//
//  AppDelegate.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso on 22/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SQLite


//@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    
    func applicationDidTimout(notification: NSNotification) {
        if let vc = self.window?.rootViewController as? UINavigationController {
            if let myTableViewController = vc.visibleViewController {
                // Call a function defined in your view controller.
                print (myTableViewController.classForCoder)
                
                if myTableViewController.classForCoder == LoginBlockViewController.self{
                    return
                }
                myTableViewController.lock()
                
            } else {
                // We are not on the main view controller. Here, you could segue to the desired class.
               print("Here")
                
            }
        }
    }
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let defaults = UserDefaults.standard

        if let x = defaults.object(forKey: "language"), x as! String == "ingles"{
            defaults.set("ingles", forKey: "language")
            defaults.synchronize()
        }else{
            defaults.set("español", forKey: "language")
            defaults.synchronize()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(AppDelegate.applicationDidTimout(notification:)), name: NSNotification.Name(rawValue: TimerUIApplication.ApplicationDidTimoutNotification), object: nil)
        IQKeyboardManager.sharedManager().enable = true
//        IQKeyboardManager.sharedManager().disabledToolbarClasses = [LoginBlockViewController.self, ViewController.self]
        let image = #imageLiteral(resourceName: "back")
        
        UINavigationBar.appearance().backIndicatorImage = image
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = image
        UINavigationBar.appearance().tintColor = UIColor(red: 0.0/255.0, green: 170.0/255.0, blue: 240.0/255.0, alpha: 1)
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -66), for: .default)
        
        
        let path = NSSearchPathForDirectoriesInDomains(
            .documentDirectory, .userDomainMask, true
            ).first!
        do {
            let db = try Connection("\(path)/db.sqlite3")
            print(db)
                let users = Table("users")
                try db.run(users.create(block: { (t) in
                    print(t)
                    t.column(Constants.id, defaultValue: 0)
                    t.column(Constants.uuid, defaultValue: "0")
                    t.column(Constants.name, primaryKey: false)
                    t.column(Constants.password, primaryKey: false)
                    t.column(Constants.email, primaryKey: false)
                    t.column(Constants.lastName, primaryKey: false)
                    t.column(Constants.localId, primaryKey: .autoincrement)
                    t.column(Constants.updated, primaryKey: false)
                    t.column(Constants.schoolName, primaryKey: false)
                    t.column(Constants.username, primaryKey: false)
                    t.column(Constants.imageStudent, defaultValue: "")
                    debugPrint(db)
                }))
                let classes = Table("classes")
                try db.run(classes.create(block: { (t) in
                    print(t)
                    t.column(Constants.id, defaultValue: 0)
                    t.column(Constants.uuid, defaultValue: "0")
                    t.column(Constants.name, primaryKey: false)
                    t.column(Constants.localId, primaryKey: .autoincrement)
                    t.column(Constants.updated, primaryKey: false)
                    t.column(Constants.nivel, primaryKey: false)
                    t.column(Constants.grado, primaryKey: false)
                    t.column(Constants.claseColor, defaultValue: "1")
                    t.column(Constants.teacherId, defaultValue: 0)
                    t.column(Constants.tUuid, primaryKey: false)
                    debugPrint(db)
                }))
            let students = Table("students")
                try db.run(students.create(block: { (t) in
                    print(t)
                    t.column(Constants.id, defaultValue: 0)
                    t.column(Constants.uuid, defaultValue: "0")
                    t.column(Constants.name, primaryKey: false)
                    t.column(Constants.birthday, primaryKey: false)
                    t.column(Constants.email, primaryKey: false)
                    t.column(Constants.gender, primaryKey: false)
                    t.column(Constants.idClasse, defaultValue: 0)
                    t.column(Constants.localId, primaryKey: .autoincrement)
                    t.column(Constants.updated, primaryKey: false)
                    t.column(Constants.cUuid, defaultValue: "0")
                    t.column(Constants.imageStudent, defaultValue: "")
                    debugPrint(db)
                }))
            let periods = Table("periods")
            try db.run(periods.create(block: { (t) in
                print(t)
                t.column(Constants.id, defaultValue: 0)
                t.column(Constants.uuid, defaultValue: "0")
                t.column(Constants.nameId, defaultValue: 0)
                t.column(Constants.startDate, primaryKey: false)
                t.column(Constants.endDate, primaryKey: false)
                t.column(Constants.idClasse, defaultValue: 0)
                t.column(Constants.localId, primaryKey: .autoincrement)
                t.column(Constants.updated, primaryKey: false)
                t.column(Constants.cUuid, defaultValue: "0")
                t.column(Constants.notifications, defaultValue: true)
                debugPrint(db)
            }))
            let criterios = Table("criterios")
            try db.run(criterios.create(block: { (t) in
                print(t)
                t.column(Constants.id, defaultValue: 0)
                t.column(Constants.uuid, defaultValue: "0")
                t.column(Constants.name, primaryKey: false)
                t.column(Constants.valueC, defaultValue: 0.0)
                t.column(Constants.idClasse, defaultValue: 0)
                t.column(Constants.localId, primaryKey: .autoincrement)
                t.column(Constants.updated, primaryKey: false)
                t.column(Constants.cUuid, defaultValue: "0")
                debugPrint(db)
            }))
            let deleted = Table("deleted")
            try db.run(deleted.create(block: { (t) in
                print(t)
                t.column(Constants.localId, primaryKey: .autoincrement)
                t.column(Constants.uuid, defaultValue: "0")
                t.column(Constants.typeDeleted, primaryKey: false)
                t.column(Constants.updated, primaryKey: false)
                debugPrint(db)
            }))
            let list = Table("list")
            try db.run(list.create(block: { (t) in
                print(t)
                t.column(Constants.localId, primaryKey: .autoincrement)
                t.column(Constants.pUuid, defaultValue: "0")
                t.column(Constants.sUuid, defaultValue: "0")
                t.column(Constants.tUuid, defaultValue: "0")
                t.column(Constants.cUuid, defaultValue: "0")
                t.column(Constants.criUuid, defaultValue: "0")
                t.column(Constants.present, defaultValue: 1)
                t.column(Constants.day, defaultValue: Date().stringBy(date: Date()))
                t.column(Constants.updated, primaryKey: false)
                debugPrint(db)
            }))
            let participation = Table("participation")
            try db.run(participation.create(block: { (t) in
                print(t)
                t.column(Constants.localId, primaryKey: .autoincrement)
                t.column(Constants.sUuid, defaultValue: "0")
                t.column(Constants.tUuid, defaultValue: "0")
                t.column(Constants.cUuid, defaultValue: "0")
                t.column(Constants.pUuid, defaultValue: "0")
                t.column(Constants.criUuid, defaultValue: "0")
                t.column(Constants.particip, defaultValue: false)
                t.column(Constants.day, defaultValue: Date().stringBy(date: Date()))
                t.column(Constants.updated, primaryKey: false)
                debugPrint(db)
            }))
            
            let rateDayTable = Table("rateDay")
            try db.run(rateDayTable.create(block: { (t) in
                print(t)
                t.column(Constants.localId, primaryKey: .autoincrement)
                t.column(Constants.sUuid, defaultValue: "0")
                t.column(Constants.tUuid, defaultValue: "0")
                t.column(Constants.cUuid, defaultValue: "0")
                t.column(Constants.pUuid, defaultValue: "0")
                t.column(Constants.criUuid, defaultValue: "0")
                t.column(Constants.valueC, defaultValue: 0.0)
                t.column(Constants.day, defaultValue: Date().stringBy(date: Date()))
                t.column(Constants.updated, primaryKey: false)
                debugPrint(db)
            }))
        } catch {
            print(error)
        }
        
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}
