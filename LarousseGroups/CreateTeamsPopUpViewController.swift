//
//  CreateTeamsPopUpViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 06/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

protocol CreateTeamsPopUpDelegate {
    func isManual(a : Bool)
}

class CreateTeamsPopUpViewController: UIViewController {
    var delegate : CreateTeamsPopUpDelegate?
    
    @IBOutlet weak var automaticButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var manualButton: UIButton!
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        automaticButton.setTitle(Localization("Automatic"), for: .normal)
        manualButton.setTitle(Localization("Manual"), for: .normal)
        titleLabel.text = Localization("MakeTeams")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func manualAction(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.isManual(a: true)
        }
    }
    
    @IBAction func randomAction(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.isManual(a: false)
        }
    }
    

    @IBAction func dismissAction(_ sender: Any) {
        self.dismiss(animated: false) { 
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
