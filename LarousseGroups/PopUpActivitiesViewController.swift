//
//  PopUpActivitiesViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 31/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

protocol ActivitiesDelegate {
    func roulette()
    func groups()
}

class PopUpActivitiesViewController: UIViewController {
    var delegate: ActivitiesDelegate?
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
    }
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var rouletteButton: UIButton!
    @IBOutlet weak var teamsButtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = Localization("ActivitiesM")
        rouletteButton.setTitle(Localization("Rpullete"), for: .normal)
        teamsButtn.setTitle(Localization("TeamsM"), for: .normal)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func ruletaAction(_ sender: Any) {
        print("Ruleta")
        self.dismiss(animated: false) {
            self.delegate?.roulette()
        }
    }

    @IBAction func equiposAction(_ sender: Any) {
        print("Equipos")
        self.dismiss(animated: false) {
            self.delegate?.groups()
        }
    }
    
    @IBAction func tapAction(_ sender: Any) {
        self.dismiss(animated: false) { 
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
