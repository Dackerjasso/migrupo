//
//  ImageStudentTableViewCell.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 29/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

class ImageStudentTableViewCell: UITableViewCell {

    @IBOutlet weak var studentImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
