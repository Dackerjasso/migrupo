//
//  CalendarJTViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 16/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import JTAppleCalendar
import CVCalendar

class CalendarJTViewController: UIViewController {
    var delegate : calendarDeleate?
    let formatter = DateFormatter()
    @IBOutlet weak var calendarView: JTAppleCalendarView!
    @IBOutlet weak var stack: UIStackView!
    @IBOutlet weak var monthYearLabel: UILabel!
    @IBOutlet weak var viewYears: UIView!
    @IBOutlet weak var yearsCollection: UICollectionView!
    var years : [Date] = []
    let cellsPerRow = 3
    @IBOutlet weak var backButton: UIButton!

    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: false) {
            
        }
    }
    
    @IBAction func tapAction(_ sender: Any) {
        self.viewYears.alpha = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
        if (self.isBeingPresented) {
            // being presented
            print("Presented")
            backButton.alpha = 1
        } else if (self.isMovingToParentViewController) {
            // being pushed
            print("Pushed")
            
            backButton.alpha = 0
        } else {
            // simply showing again because another VC was dismissed
            print("No sé")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupCalendar()
        viewYears.alpha = 0
        let dateNow = Date()
        var time = -45
        for _ in 1...90{
            let newDate = Calendar.current.date(byAdding: .year, value: time, to: dateNow)
            time += 1
            years.append(newDate!)
        }
        print(years)
        
        // Do any additional setup after loading the view.
    }
    
    func dismissAction(){
        self.dismiss(animated: false) {
            
        }
    }

    func setupCalendar(){
        calendarView.minimumLineSpacing = 0
        calendarView.minimumInteritemSpacing = 0
        calendarView.visibleDates { (vd) in
            self.calendarView.scrollToDate(Date())
            let date = vd.monthDates.first!
            self.formatter.dateFormat = "MMMM yyyy"
            let fullString = NSMutableAttributedString(string: self.formatter.string(from: date.date))

            self.monthYearLabel.attributedText = fullString
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func habdleCellSelected(view: JTAppleCell?, cellState: CellState){
        guard let validCell = view as? CustomCell else {return}
        if validCell.isSelected {
            validCell.selectedView.isHidden = false
        }else{
            validCell.selectedView.isHidden = true
        }
    }
    
    func handleCellTextColor(view: JTAppleCell?, cellState: CellState){
        guard let validCell = view as? CustomCell else {return}
        if cellState.isSelected{
            validCell.dateLabel.textColor = UIColor.white
        }else{
            if cellState.dateBelongsTo == .thisMonth{
                validCell.dateLabel.textColor = UIColor.black
            }else{
                validCell.dateLabel.textColor = UIColor.lightGray
            }
        }
    }
    
    
    @IBAction func nextMonth(_ sender: Any) {
       (sender as! UIButton).isEnabled = false
        calendarView.visibleDates { (vd) in
            let date = vd.monthDates.first?.date
            let dateNM = Calendar.current.date(byAdding: .month, value: 1, to: date!)
            self.calendarView.scrollToDate(dateNM!)
            (sender as! UIButton).isEnabled = true

        }
    }
    

    
    @IBAction func prevMonth(_ sender: Any) {
        (sender as! UIButton).isEnabled = false
        calendarView.visibleDates { (vd) in
            let date = vd.monthDates.first?.date
            let datePM = Calendar.current.date(byAdding: .month, value: -1, to: date!)
            self.calendarView.scrollToDate(datePM!)
            (sender as! UIButton).isEnabled = true

        }
    }
    
    @IBAction func tapYearAction(_ sender: Any) {
        print("Year")
        yearsCollection.contentOffset = CGPoint(x: 0, y: yearsCollection.contentSize.height / 2)
        yearsCollection.reloadData()
        UIView.animate(withDuration: 0.2) {
            self.viewYears.alpha = 1
        }
    }

    
    
}



extension CalendarJTViewController: JTAppleCalendarViewDelegate{
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        let date = visibleDates.monthDates.first!
        self.formatter.dateFormat = "MMMM yyyy"
        let fullString = NSMutableAttributedString(string: "\(self.formatter.string(from: date.date))  ")
        let image1Attachment = NSTextAttachment()
        image1Attachment.image = #imageLiteral(resourceName: "cambiarFecha")
        let image1String = NSAttributedString(attachment: image1Attachment)
        fullString.append(image1String)
        
        self.monthYearLabel.attributedText = fullString
        
    }
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = Calendar.current.timeZone
        formatter.locale = Calendar.current.locale
        let startDate = formatter.date(from: "1950 01 01")
        let endDate = formatter.date(from: "2100 12 31")
        let parameters = ConfigurationParameters(startDate: startDate!, endDate: endDate!)
        return parameters
    }
}


extension CalendarJTViewController: JTAppleCalendarViewDataSource{
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "CustomCell", for: indexPath) as! CustomCell
        cell.dateLabel.text = cellState.text
//        cell.backgroundColor = UIColor.random()
        self.habdleCellSelected(view: cell, cellState: cellState)
        self.handleCellTextColor(view: cell, cellState: cellState)
        return cell
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        self.habdleCellSelected(view: cell, cellState: cellState)
        self.handleCellTextColor(view: cell, cellState: cellState)
        print(">>>>>> Selected Date: \(date)")
        if self.navigationController != nil{
            self.delegate?.selectedDay(date: CVDate(date: date, calendar: .current))
        }else{
            self.dismiss(animated: false) {
                self.delegate?.selectedDay(date: CVDate(date: date, calendar: .current))
            }
        }

//        self.navigationController?.popViewController(animated: true)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        self.habdleCellSelected(view: cell, cellState: cellState)
        self.handleCellTextColor(view: cell, cellState: cellState)
    }
    
}

extension CalendarJTViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 90
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "yearCell", for: indexPath) as! YearCollectionViewCell
        cell.backimg.clipsToBounds = true
        cell.backimg.layer.cornerRadius = cell.backimg.frame.size.width / 2
        cell.backimg.backgroundColor = UIColor(red: 255.0/255.0, green: 178.0/255.0, blue: 0.0/255.0, alpha: 1)
        formatter.dateFormat = "yyyy"
        cell.yearLabel.textColor = UIColor.white
        cell.date = years[indexPath.row]
        cell.yearLabel.text = formatter.string(from: years[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let datetoJump = (collectionView.cellForItem(at: indexPath) as! YearCollectionViewCell).date
        print(datetoJump!)
        calendarView.visibleDates { (vd) in
            let date = vd.monthDates.first?.date
            let components = Calendar.current.dateComponents([.year, .month, .day], from: date!)
            var newDate = Calendar.current.dateComponents([.year, .month, .day], from: datetoJump!)
            newDate.month = components.month
            let cell = collectionView.cellForItem(at: indexPath)
            UIView.animate(withDuration: 0.2
                , animations: { 
                    cell?.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            }, completion: { (finished) in
                if finished{
                    let ddate = Calendar.current.date(from: newDate)
                    UIView.animate(withDuration: 0.2) {
                        self.viewYears.alpha = 0
                        self.calendarView.scrollToDate(ddate!)
                    }
                }
            })

        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let marginsAndInsets = flowLayout.sectionInset.left + flowLayout.sectionInset.right + flowLayout.minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
        let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
        return CGSize(width: itemWidth, height: itemWidth)
    }
}




extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}



extension UIColor {
    static func random() -> UIColor {
        return UIColor(red:   .random(),
                       green: .random(),
                       blue:  .random(),
                       alpha: 1.0)
    }
}














