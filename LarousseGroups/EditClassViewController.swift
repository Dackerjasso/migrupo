//
//  EditClassViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 26/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import CVCalendar

class EditClassViewController: UIViewController {
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var nivelTF: UITextField!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var gradoTF: UITextField!
    @IBOutlet weak var colorTF: UITextField!
    @IBOutlet weak var titleLAbel: UILabel!
    @IBOutlet weak var colorCollection: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var notButton: UIButton!
    var selectedColor : Constants.colorClass!
    @IBOutlet weak var editPeriodsButton: UIButton!
    @IBOutlet weak var notLabel: UILabel!
    let colors : [Constants.colorClass] = [.red, .orange, .blue1, .pink, .purple, .yellow, .aqua, .green, .green2, .blue2, .purple2]
    let listSec = [Int](1...6)
    let listPri = [Int](1...3)
    var star : Bool!
    let margin: CGFloat = 1
    let cellsPerRow = 6
    var classe: Class!
    var isNew = false
    var edit = false
    var tWS = TeacherWS()
    var studentsRegistered = false
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        if classe.periods.count == 0{
            notLabel.alpha = 0
            notButton.alpha = 0
            editPeriodsButton.alpha = 0
        }else{
            notButton.isSelected = classe.periods[0].notifications
        }
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
        if !internetProvider.sharedInstance.isVertical {
            print("Landscape")
            self.stackView.axis = .horizontal
            titleLabel.font = UIFont.boldSystemFont(ofSize: 28)
        } else {
            self.stackView.axis = .vertical
            titleLabel.font = UIFont.boldSystemFont(ofSize: 50)
        }
        self.view.layoutIfNeeded()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        colorCollection?.collectionViewLayout.invalidateLayout()
        super.viewWillTransition(to: size, with: coordinator)
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            internetProvider.sharedInstance.isVertical = false
            self.stackView.axis = .horizontal
            titleLabel.font = UIFont.boldSystemFont(ofSize: 28)
        } else {
            self.stackView.axis = .vertical
            internetProvider.sharedInstance.isVertical = true
            titleLabel.font = UIFont.boldSystemFont(ofSize: 50)
        }
        self.view.layoutIfNeeded()
        
    }
    func back(sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go back to the previous ViewController
        if edit || 	nameTF.text != classe.name{
            let error = UIAlertController(title: nil, message: Localization("LostData"), preferredStyle: .alert)
            let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default) { (action) in
                _ = self.navigationController?.popToRootViewController(animated: true)
            }
            let cancelAction = UIAlertAction(title: Localization("Cancelar"), style: .cancel) { (action) in
                
            }
            error.addAction(okAction)
            error.addAction(cancelAction)
            self.present(error, animated: true) {
                
            }
        }else{
            _ = self.navigationController?.popViewController(animated: true)
        }
        
    }
    @IBOutlet weak var evaluationLabel: UILabel!
    @IBOutlet weak var activateLabel: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editPeriodsButton.setTitle(Localization("EditPeriods"), for: .normal)
        titleLabel.text = Localization("EditClass")
        nameTF.placeholder = Localization("ClassName")
        nivelTF.placeholder = Localization("Nivel")
        gradoTF.placeholder = Localization("Grado")
        evaluationLabel.text = Localization("Evaluation")
        colorTF.placeholder = Localization("Color")
        activateLabel.text = Localization("ActivatedNotify")
        acceptButton.setTitle(Localization("Aceptar"), for: .normal)
        
        
        
        if isNew{
            titleLAbel.text = Localization("DuplicateClass")
            editPeriodsButton.alpha = 0
            editPeriodsButton.isEnabled = false
        }
        tWS.delegate = self
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(self.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        nameTF.setLeftPaddingPoints(30)
        nameTF.setRightPaddingPoints(30)
        nameTF.text = classe.name
        nivelTF.setLeftPaddingPoints(30)
        nivelTF.setRightPaddingPoints(30)
        if let niv = classe.nivel, niv == "Primary" || niv == "Primaria"{
            self.nivelTF.text = Localization("Primary")
        }else{
            self.nivelTF.text = Localization("Secondary")
        }
        gradoTF.setLeftPaddingPoints(30)
        gradoTF.setRightPaddingPoints(30)
        if let grado = classe.grado{
            gradoTF.text = "\(grado)"
        }else{
            gradoTF.text = ""
        }
        colorTF.setLeftPaddingPoints(30)
        colorTF.setRightPaddingPoints(30)
        colorTF.attributedPlaceholder = NSAttributedString(string: colorTF.placeholder!, attributes: [NSForegroundColorAttributeName : classe.colorClass.color])
        if let color = classe.colorClass{
            selectedColor = color
        }else{
            selectedColor = Constants.colorClass.aqua
        }
        nivelTF.setRightPaddingPoints(10, image: #imageLiteral(resourceName: "flecha3"))
        gradoTF.setRightPaddingPoints(10, image: #imageLiteral(resourceName: "flecha3"))
        colorTF.setRightPaddingPoints(10, image: #imageLiteral(resourceName: "flecha3"))
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func nivelAction(_ sender: Any) {
        nameTF.resignFirstResponder()
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let primariaAction = UIAlertAction(title: Localization("Primary"), style: .default) { (action) in
            self.nivelTF.text = Localization("Primary")
            self.gradoTF.text = ""
            self.edit = true
        }
        let secundariaAction = UIAlertAction(title: Localization("Secondary"), style: .default) { (action) in
            self.nivelTF.text = Localization("Secondary")
            self.gradoTF.text = ""
            self.edit = true
        }
        alert.addAction(primariaAction)
        alert.addAction(secundariaAction)
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = nivelTF
            presenter.sourceRect = nivelTF.bounds
        }
        
        self.present(alert, animated: true) {
            
        }

    }

    @IBAction func gradoAction(_ sender: Any) {
        nameTF.resignFirstResponder()
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        if nivelTF.text == Localization("Primary"){
            for (index, _) in listSec.enumerated(){
                let a = UIAlertAction(title: "\(index + 1)", style: .default, handler: { (action) in
                    self.gradoTF.text = action.title
                    self.edit = true
                })
                alert.addAction(a)
            }
        }else if nivelTF.text == Localization("Secondary"){
            for (index, _) in listPri.enumerated(){
                let a = UIAlertAction(title: "\(index + 1)", style: .default, handler: { (action) in
                    self.gradoTF.text = action.title
                    self.edit = true
                })
                alert.addAction(a)
            }
        }else{
            let alertError = UIAlertController(title: nil, message: Localization("SelectNivel"), preferredStyle: .alert)
            let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default, handler: { (action) in
                
            })
            alertError.addAction(okAction)
            self.present(alertError, animated: true, completion: {
                
            })
        }
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = gradoTF
            presenter.sourceRect = gradoTF.bounds
        }
        
        self.present(alert, animated: true) {
            
        }

    }
    
    @IBAction func accptAction(_ sender: Any) {
        
        if isNew{
            for cls in UserProvider.sharedInstance.classes{
                if nameTF.text == cls.name{
                    let erroralert = self.errorAlert(message: Localization("ClasseExists"))
                    self.present(erroralert, animated: true
                        , completion: {
                            
                    })
                    return
                }
            }
            let errorString = NSMutableString(string: "")
            var isError = false
            if nameTF.text?.trimmingCharacters(in: NSCharacterSet.whitespaces).characters.count == 0{
                isError = true
            }
            if nivelTF.text?.trimmingCharacters(in: NSCharacterSet.whitespaces).characters.count == 0{
                isError = true
            }
            if gradoTF.text?.trimmingCharacters(in: NSCharacterSet.whitespaces).characters.count == 0{
                isError = true
            }
            if selectedColor == nil{
                isError = true
            }
            if isError{
                errorString.append(Localization("CreateClassAlert"))
                let alert = self.errorAlert(message: errorString as String)
                self.present(alert, animated: true, completion: {
                    
                })
            }else{
                let newClass = self.classe.copy() as! Class
                newClass.name = self.nameTF.text
                newClass.nivel = self.nivelTF.text
                newClass.grado = Int(self.gradoTF.text!)
                newClass.colorClass = self.selectedColor
                newClass.criterios = []
                newClass.students = self.classe.students
                let newPeriod = Period()
                newClass.uuid = ""
                if newClass.periods.count > 0{
                    newClass.periods.removeAll()
                    newPeriod.endDate = self.classe.periods[0].endDate
                    newPeriod.startDate = self.classe.periods[0].startDate
                    newPeriod.notifications = self.classe.periods[0].notifications
                    newClass.periods.append(newPeriod)
                    newClass.periods[0].notifications = notButton.isSelected
                }else{
                    newPeriod.endDate = CVDate(date: Date(), calendar: .current)
                    newPeriod.startDate = CVDate(date: Date(), calendar: .current)
                    newClass.periods.append(newPeriod)
                }
                newClass.dbid = 0
                self.startSpinner()
                self.tWS.register(classe: newClass, for: UserProvider.sharedInstance)
            
            }
           
//            UserProvider.sharedInstance.classes.insert(newClass, at: 0)
////            self.startSpinner()
//            self.navigationController?.popViewController(animated: true)
            //CREAR CLASE
            //CREAR CRITERIOS
            //CREARALUMNOS
            
            
            
            
        }else{
            for cls in UserProvider.sharedInstance.classes{
                if cls == self.classe{
                    continue
                }
                if nameTF.text == cls.name{
                    let erroralert = self.errorAlert(message: Localization("ClasseExists"))
                    self.present(erroralert, animated: true
                        , completion: {
                            
                    })
                    return
                }
            }
            let errorString = NSMutableString(string: "")
            var isError = false
            if nameTF.text?.trimmingCharacters(in: NSCharacterSet.whitespaces).characters.count == 0{
                isError = true
            }
            if nivelTF.text?.trimmingCharacters(in: NSCharacterSet.whitespaces).characters.count == 0{
                isError = true
            }
            if gradoTF.text?.trimmingCharacters(in: NSCharacterSet.whitespaces).characters.count == 0{
                isError = true
            }
            if selectedColor == nil{
                isError = true
            }
            if isError{
                errorString.append(Localization("CreateClassAlert"))
                let alert = self.errorAlert(message: errorString as String)
                self.present(alert, animated: true, completion: {
                    
                })
            }else{
                self.classe.name = self.nameTF.text
                self.classe.nivel = self.nivelTF.text
                self.classe.grado = Int(self.gradoTF.text!)
                self.classe.colorClass = self.selectedColor
                if classe.periods.count > 0{
                    classe.periods[0].notifications = notButton.isSelected
                }
                self.startSpinner()
                tWS.update(classe: classe)
            }

        }

    }
    
    @IBAction func colorAction(_ sender: Any) {
        nameTF.resignFirstResponder()
        if colorCollection.alpha == 0{
            colorCollection.alpha = 1
        }else{
            colorCollection.alpha = 0
        }
    }

    @IBAction func notifAction(_ sender: Any) {
        (sender as! UIButton).isSelected = !(sender as! UIButton).isSelected
    }

    @IBAction func editPeriodsAction(_ sender: Any) {
        let epvc = self.storyboard?.instantiateViewController(withIdentifier: "editPeriodsView") as! EditPeriodsViewController
        epvc.classe = self.classe
        self.navigationController?.pushViewController(epvc, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EditClassViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let marginsAndInsets = flowLayout.sectionInset.left + flowLayout.sectionInset.right + flowLayout.minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
        let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
        return CGSize(width: itemWidth, height: itemWidth)
    }
    

    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 11
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ColorCollectionViewCell
        cell.clipsToBounds = true
        cell.backgroundColor = colors[indexPath.row].color
        cell.layer.cornerRadius = cell.frame.size.width / 2
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.alpha = 0
        self.edit = true
        colorTF.attributedPlaceholder = NSAttributedString(string: colorTF.placeholder!, attributes: [NSForegroundColorAttributeName : colors[indexPath.row].color])
        selectedColor = colors[indexPath.row]
    }
}

extension EditClassViewController: teacherDelegate{
    func didSuccessUpdate(period: Period) {
        
    }
    
    func didFailUpdatePeriod(message: String) {
        
    }
    
    func didSuccessRegister(classe: Class) {
            if isNew{
                if classe.students.count > 0{
                    tWS.delegate = self
                    if !studentsRegistered{
                        for s in classe.students{
                            s.uuid = nil
                            s.dbid = nil
                        }
                        tWS.register(student: nil, at: classe, students: classe.students)
                        studentsRegistered = true
                    }
                    
                }else{
                    let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        self.navigationController?.popViewController(animated: true)
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "BackFromPercent"), object: self)
                        self.stopSpinner()
                    }
                }
            }
        }
    
    func didFailRegisterClass(statusCode: Int, message: String!) {
        self.stopSpinner()
        let errorvc = self.errorAlert(message: message)
        self.present(errorvc, animated: true) { 
            
        }
    }
    
    func getDocumentsURL() -> URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsURL
    }
    
    func fileInDocumentsDirectory(_ filename: String) -> URL{
        let fileURL = getDocumentsURL().appendingPathComponent(filename)
        return fileURL
    }
    
    func saveImage (_ image: UIImage, path: URL ) -> Bool{
        
        let pngImageData = UIImageJPEGRepresentation(image, 1.0)
        UIGraphicsBeginImageContext(image.size)
        image.draw(in: CGRect(x:0, y:0, width:image.size.width, height:image.size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let jpgImageData = UIImageJPEGRepresentation(newImage!, 1.0)
        
        let result = (try? jpgImageData!.write(to: URL(fileURLWithPath: path.path), options: [.atomic])) != nil
        return result
    }
    
    func didSuccessRegister(student: Student, classe: Class) {
        if student.localId != nil{
            print(student.name)
            print(student.img)
            if student.img != nil{
                _ = self.saveImage(student.img, path: self.fileInDocumentsDirectory("\(student.uuid!).jpg"))
                let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
                let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
                let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
                if let dirPath          = paths.first
                {
                    let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("\(student.uuid!).jpg")
                    let image    = UIImage(contentsOfFile: imageURL.path)
                    if image != nil{
                        self.tWS.updateImage(url: imageURL, student: nil)
                    }else{
                    }
                }else{
                    
                }
            }
            let when = DispatchTime.now() + 2 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.navigationController?.popViewController(animated: true)
                self.stopSpinner()
            }
            NotificationCenter.default.post(name: Notification.Name(rawValue: "BackFromPercent"), object: self)
        }else{
            for s in classe.students{
                print(s.name)
                print(s.img)
                if s.uuid == nil{
                    s.uuid = "\(s.localId!)"
                }
                if s.img != nil{
                    _ = self.saveImage(s.img, path: self.fileInDocumentsDirectory("\(s.uuid!).jpg"))
                    let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
                    let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
                    let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
                    if let dirPath          = paths.first
                    {
                        let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("\(s.uuid!).jpg")
                        let image    = UIImage(contentsOfFile: imageURL.path)
                        if image != nil{
                            self.tWS.updateImage(url: imageURL, student: nil)
                        }else{
                        }
                    }else{
                    }
                }
                
            }
            let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.navigationController?.popViewController(animated: true)
                self.stopSpinner()
            }
            NotificationCenter.default.post(name: Notification.Name(rawValue: "BackFromPercent"), object: self)
        }
    }
    
    func didFailRegisterStudent(statusCode: Int, message: String!) {
        let errorvc = self.errorAlert(message: message)
        self.present(errorvc, animated: true) { 
            
        }
    }

    func didSuccessUpdate(classe: Class) {
        
        self.classe.name = classe.name
        self.classe.colorClass = classe.colorClass
        self.classe.grado = classe.grado
        self.classe.nivel = classe.nivel
        let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "BackFromPercent"), object: self)
            self.navigationController?.popViewController(animated: true)
            self.stopSpinner()
        }
    }
    
    func didFailUpdateClass(message: String) {
        self.stopSpinner()
        let error = self.errorAlert(message: message)
        self.present(error, animated: true) { 
            
        }
    }
    
    func didSuccessAddPeriodToClass() {
        
    }
    
    func didFailAddPeriodToClass(message: String) {
        
    }
    
    func didSuccessUpdateCriteriosOn(classe: Class) {
        
    }
    
    func didFailUpdateCriteriosOnClasse(message: String) {
        
    }
    
    func didSuccessUploadImage() {
        
    }
    
    func didFailUpdateImage(message: String) {
        
    }
}

extension EditClassViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 35 // Bool
    }
}
