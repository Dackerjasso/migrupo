//
//  SeeListViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 30/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

class SeeListViewController: UIViewController {
    var classe : Class!
    @IBOutlet weak var titleLabel: UILabel!

    func edit(){
    
    }
    
    func porcentajeGral(){
    
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            titleLabel.font = UIFont.boldSystemFont(ofSize: 28)
            internetProvider.sharedInstance.isVertical = false
        } else {
            titleLabel.font = UIFont.boldSystemFont(ofSize: 50)
            internetProvider.sharedInstance.isVertical = true
        }
        self.view.layoutIfNeeded()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
        if !internetProvider.sharedInstance.isVertical{
            print("Landscape")
            titleLabel.font = UIFont.boldSystemFont(ofSize: 28)
        } else {
            titleLabel.font = UIFont.boldSystemFont(ofSize: 50)
        }
        self.view.layoutIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = Localization("List")
        let btn1 = UIButton(type: .custom)
        btn1.setImage(#imageLiteral(resourceName: "editar2"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 46.5, height: 46.5)
        btn1.addTarget(self, action: #selector(self.edit), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        
        
        let btn2 = UIButton(type: .custom)
        btn2.setImage(#imageLiteral(resourceName: "porcentajeGral"), for: .normal)
        btn2.frame = CGRect(x: 0, y: 0, width: 46.5, height: 46.5)
        btn2.addTarget(self, action: #selector(self.porcentajeGral), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)
        
        
        self.navigationItem.setRightBarButtonItems([item2, item1], animated: true)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SeeListViewController: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return classe.students.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "seeListStudentCell") as! StudentSeeListTableViewCell
        cell.nameLabel.text = classe.students[indexPath.row].name
        if let img = classe.students[indexPath.row].img{
            cell.userImg.image = img
        }else{
            cell.userImg.image = nil
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 145
    }
}
