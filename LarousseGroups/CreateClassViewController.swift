//
//  CreateClassViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso on 22/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import CVCalendar

class CreateClassViewController: UIViewController {
    @IBOutlet weak var viewBackCreate: UIView!
    @IBOutlet weak var nameClassTF: UITextField!
    @IBOutlet weak var nivelTF: UITextField!
    @IBOutlet weak var gradoTF: UITextField!
    @IBOutlet weak var inicioTF: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var finTF: UITextField!
    @IBOutlet weak var ColorTF: UITextField!
    let listSec = [Int](1...6)
    @IBOutlet weak var colorCollection: UICollectionView!
    @IBOutlet weak var stackView: UIStackView!
    let listPri = [Int](1...3)
    var star : Bool!
    @IBOutlet weak var notButton: UIButton!
    var startDate: CVDate!
    var endDate : CVDate!
    var selectedColor : Constants.colorClass!
    var not = true
    var edit = false
    let margin: CGFloat = 1
    let cellsPerRow = 6
    var tWS = TeacherWS()
    let colors : [Constants.colorClass] = [.red, .orange, .blue1, .pink, .purple, .yellow, .aqua, .green, .green2, .blue2, .purple2]
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "BackFromPercent"), object: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
        if !internetProvider.sharedInstance.isVertical {
            print("Landscape")
            self.stackView.axis = .horizontal
            titleLabel.font = UIFont.boldSystemFont(ofSize: 28)
        } else {
            self.stackView.axis = .vertical
            titleLabel.font = UIFont.boldSystemFont(ofSize: 50)
        }
        self.view.layoutIfNeeded()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        colorCollection?.collectionViewLayout.invalidateLayout()
        super.viewWillTransition(to: size, with: coordinator)
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            internetProvider.sharedInstance.isVertical = false
            self.stackView.axis = .horizontal
            titleLabel.font = UIFont.boldSystemFont(ofSize: 28)
        } else {
            self.stackView.axis = .vertical
            internetProvider.sharedInstance.isVertical = true
            titleLabel.font = UIFont.boldSystemFont(ofSize: 50)

        }
        self.view.layoutIfNeeded()
        
    }
    
    @IBAction func createClass(_ sender: Any) {
        
        let errorString = NSMutableString(string: "")
        for cls in UserProvider.sharedInstance.classes{
            if nameClassTF.text == cls.name{
                let erroralert = self.errorAlert(message: Localization("ClasseExists"))
                self.present(erroralert, animated: true
                    , completion: { 
                        
                })
                return
            }
        }
        var isError = false
        if nameClassTF.text?.trimmingCharacters(in: NSCharacterSet.whitespaces).characters.count == 0{
            isError = true
        }
        if nivelTF.text?.trimmingCharacters(in: NSCharacterSet.whitespaces).characters.count == 0{
            isError = true
        }
        if gradoTF.text?.trimmingCharacters(in: NSCharacterSet.whitespaces).characters.count == 0{
            isError = true
        }
        if startDate == nil{
            isError = true
        }
        if endDate == nil{
            isError = true
        }
        if selectedColor == nil{
            isError = true
        }
        if isError{
            errorString.append(Localization("CreateClassAlert"))
            let alert = self.errorAlert(message: errorString as String)
            self.present(alert, animated: true, completion: { 
                
            })
        }else{
            if endDate.convertedDate(calendar: .current)! < startDate.convertedDate(calendar: .current)!{
                let alert = UIAlertController(title: nil, message: Localization("AlertDates"), preferredStyle: .alert)
                let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default, handler: { (action) in
                    
                })
                alert.addAction(okAction)
                self.present(alert, animated: true, completion: {
                    
                })
                return
            }
            print("Right")
            let clase = Class()
            clase.name = nameClassTF.text?.trimmingCharacters(in: NSCharacterSet.whitespaces)
            clase.nivel = nivelTF.text?.trimmingCharacters(in: NSCharacterSet.whitespaces)
            clase.grado = Int((gradoTF.text)!)
            let period = Period()
            period.startDate = startDate
            period.endDate = endDate
            period.notifications = not
            clase.periods.append(period)
            clase.colorClass = selectedColor
            self.startSpinner()
            self.view.isUserInteractionEnabled = false
            tWS.register(classe: clase, for: UserProvider.sharedInstance)
        }
    }
    
    @IBOutlet weak var evaluationLabel: UILabel!
    @IBOutlet weak var activateLabel: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    
    func back(sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go back to the previous ViewController
        if edit{
            let error = UIAlertController(title: nil, message: Localization("LostData"), preferredStyle: .alert)
            let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default) { (action) in
                _ = self.navigationController?.popViewController(animated: true)
            }
            let cancelAction = UIAlertAction(title: Localization("Cancelar"), style: .cancel) { (action) in
                
            }
            error.addAction(okAction)
            error.addAction(cancelAction)
            self.present(error, animated: true) {
                
            }
        }else{
            _ = self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(image: #imageLiteral(resourceName: "back"), style: .plain, target: self, action: #selector(self.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        titleLabel.text = Localization("MakeClass")
        nameClassTF.placeholder = Localization("ClassName")
        nivelTF.placeholder = Localization("Nivel")
        gradoTF.placeholder = Localization("Grado")
        evaluationLabel.text = Localization("Evaluation")
        inicioTF.placeholder = Localization("StartDate")
        finTF.placeholder = Localization("EndDate")
        ColorTF.placeholder = Localization("Color")
        activateLabel.text = Localization("ActivatedNotify")
        acceptButton.setTitle(Localization("Aceptar"), for: .normal)
        UINavigationBar.appearance().backIndicatorImage = #imageLiteral(resourceName: "back")
        tWS.delegate = self
        nameClassTF.setLeftPaddingPoints(30)
        nameClassTF.setRightPaddingPoints(30)
        nivelTF.setLeftPaddingPoints(30)
        nivelTF.setRightPaddingPoints(30)
        gradoTF.setLeftPaddingPoints(30)
        gradoTF.setRightPaddingPoints(30)
        inicioTF.setLeftPaddingPoints(30)
        inicioTF.setRightPaddingPoints(30)
        finTF.setLeftPaddingPoints(30)
        finTF.setRightPaddingPoints(30)
        ColorTF.setLeftPaddingPoints(30)
        ColorTF.setRightPaddingPoints(30)
        nivelTF.setRightPaddingPoints(10, image: #imageLiteral(resourceName: "flecha3"))
        gradoTF.setRightPaddingPoints(10, image: #imageLiteral(resourceName: "flecha3"))
        inicioTF.setRightPaddingPoints(10, image: #imageLiteral(resourceName: "flecha3"))
        finTF.setRightPaddingPoints(10, image: #imageLiteral(resourceName: "flecha3"))
        ColorTF.setRightPaddingPoints(10, image: #imageLiteral(resourceName: "flecha3"))
        notButton.isSelected = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    @IBAction func createClass(_ sender: Any) {
//        UserProvider.sharedInstance.classes.append(Class())
//        self.navigationController?.popViewController(animated: true)
//    }
    @IBAction func selectNivel(_ sender: Any) {
        nameClassTF.resignFirstResponder()
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let primariaAction = UIAlertAction(title: Localization("Primary"), style: .default) { (action) in
            self.nivelTF.text = Localization("Primary")
            self.gradoTF.text = ""
            self.edit = true
        }
        let secundariaAction = UIAlertAction(title: Localization("Secondary"), style: .default) { (action) in
            self.nivelTF.text = Localization("Secondary")
            self.gradoTF.text = ""
            self.edit = true
        }
        alert.addAction(primariaAction)
        alert.addAction(secundariaAction)
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = nivelTF
            presenter.sourceRect = nivelTF.bounds
        }
        
        self.present(alert, animated: true) {
            
        }
    }

    @IBAction func selectGrade(_ sender: Any) {
        nameClassTF.resignFirstResponder()
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        if nivelTF.text == Localization("Primary"){
            for (index, _) in listSec.enumerated(){
                let a = UIAlertAction(title: "\(index + 1)", style: .default, handler: { (action) in
                    self.gradoTF.text = action.title
                    self.edit = true
                })
                alert.addAction(a)
            }
        }else if nivelTF.text == Localization("Secondary"){
            for (index, _) in listPri.enumerated(){
                let a = UIAlertAction(title: "\(index + 1)", style: .default, handler: { (action) in
                    self.gradoTF.text = action.title
                    self.edit = true
                })
                alert.addAction(a)
            }
        }else{
            let alertError = UIAlertController(title: nil, message: Localization("SelectNivel"), preferredStyle: .alert)
            let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default, handler: { (action) in
                
            })
            alertError.addAction(okAction)
            self.present(alertError, animated: true, completion: { 
                
            })
        }
        if let presenter = alert.popoverPresentationController {
            presenter.sourceView = gradoTF
            presenter.sourceRect = gradoTF.bounds
        }
        
        self.present(alert, animated: true) {
            
        }

    }
    
    @IBAction func notifAction(_ sender: Any) {
        (sender as! UIButton).isSelected = !(sender as! UIButton).isSelected
        not = !not
        self.edit = true
    }
    
    @IBAction func colorAction(_ sender: Any) {
        nameClassTF.resignFirstResponder()
        if colorCollection.alpha == 0{
            colorCollection.alpha = 1
        }else{
            colorCollection.alpha = 0
        }
    }
    @IBAction func starTime(_ sender: Any) {
        nameClassTF.resignFirstResponder()
        let calendarStoryBoard = UIStoryboard(name: "Calendar", bundle: nil)
        let cvc = calendarStoryBoard.instantiateInitialViewController() as! CalendarJTViewController
        cvc.modalPresentationStyle = .overCurrentContext
        cvc.modalTransitionStyle = .crossDissolve
        cvc.delegate = self
        star = true
        self.present(cvc, animated: true) {
            
        }
    }
    @IBAction func endTime(_ sender: Any) {
        nameClassTF.resignFirstResponder()
        let calendarStoryBoard = UIStoryboard(name: "Calendar", bundle: nil)
        let cvc = calendarStoryBoard.instantiateInitialViewController() as! CalendarJTViewController
        cvc.modalPresentationStyle = .overCurrentContext
        cvc.modalTransitionStyle = .crossDissolve
        cvc.delegate = self
        star = false
        self.present(cvc, animated: true) {
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CreateClassViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let marginsAndInsets = flowLayout.sectionInset.left + flowLayout.sectionInset.right + flowLayout.minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
        let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
        return CGSize(width: itemWidth, height: itemWidth)
    }
    

    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 11
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ColorCollectionViewCell
        cell.clipsToBounds = true
        cell.backgroundColor = colors[indexPath.row].color
        cell.layer.cornerRadius = cell.frame.size.width / 2
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            collectionView.alpha = 0
            edit = true
            ColorTF.attributedPlaceholder = NSAttributedString(string: ColorTF.placeholder!, attributes: [NSForegroundColorAttributeName : colors[indexPath.row].color])
            selectedColor = colors[indexPath.row]
    }
}

extension CreateClassViewController: calendarDeleate{
    func selectedDay(date: CVDate) {
        self.edit = true
        if star{
            inicioTF.text = date.commonDescription
            startDate = date
        }else{
            finTF.text = date.commonDescription
            endDate = date
        }
    }
}

extension CreateClassViewController: teacherDelegate{
    func didSuccessRegister(classe: Class) {
        if UserProvider.sharedInstance.classes.contains(classe){
            let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                self.stopSpinner()
                self.navigationController?.popViewController(animated: true)
            }
        }
        self.view.isUserInteractionEnabled = false
        if UserProvider.sharedInstance.classes.count == 0{
            UserProvider.sharedInstance.classes.append(classe)
        }else{
            if UserProvider.sharedInstance.classes[0] != classe{
                UserProvider.sharedInstance.classes.insert(classe, at: 0)
            }
        }
        let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.stopSpinner()
            self.navigationController?.popViewController(animated: true)
        }

    }
    
    func didFailRegisterClass(statusCode: Int, message: String!) {
        self.stopSpinner()
        self.view.isUserInteractionEnabled = true
        let alert = self.errorAlert(message: message)
        self.present(alert, animated: true) {
            
        }
    }
    func didSuccessAddPeriodToClass() {
        
    }
    
    func didFailAddPeriodToClass(message: String) {
        
    }
    
    func didFailUpdateCriteriosOnClasse(message: String) {
        
    }
    
    func didSuccessUpdateCriteriosOn(classe: Class) {
        
    }
}

extension UIView{
    var screenshot: UIImage{
        UIGraphicsBeginImageContext(self.bounds.size);
        let context = UIGraphicsGetCurrentContext();
        self.layer.render(in: context!)
        let screenShot = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return screenShot!
    }
}

extension CreateClassViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.edit = true
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 35 // Bool
    }
}
