//
//  YearCollectionViewCell.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 19/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

class YearCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var backimg: UIImageView!
    @IBOutlet weak var yearLabel: UILabel!
    var date : Date!
}
