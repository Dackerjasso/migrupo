//
//  TeamsViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 06/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import CircleMenu

class TeamsViewController: UIViewController {
    
    @IBOutlet weak var centerAddButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    let margin: CGFloat = 10
    let cellsPerRow = 2
    var isLong = false
    @IBOutlet weak var tramsLabelUp: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var teamsLabel: UILabel!
    @IBOutlet weak var teamsTextLabel: UILabel!
    var currentTeam : Team!
    var teams : [Team] = []
    var colors : [Constants.colorClass] = []
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    var classe : Class!
    var manual: Bool!
    @IBOutlet weak var addBottomButton: UIButton!
    var curentTeam : Team!
    var period : Int!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var helpMenuButton: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false

        self.view.layoutIfNeeded()
        collectionView.reloadData()
    }
    
    @IBAction func helpMenuAction(_ sender: Any) {
        let url = URL(string: "http://migrupo.redlarousse.mx/manual-de-uso/")!
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        blurView.alpha = 0
        collectionView?.collectionViewLayout.invalidateLayout()
        super.viewWillTransition(to: size, with: coordinator)
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            heightConstraint.constant = 600
            internetProvider.sharedInstance.isVertical = false
        } else {
            heightConstraint.constant = 766
            internetProvider.sharedInstance.isVertical = true
        }
        self.view.layoutIfNeeded()
        collectionView.reloadData()
        
    }
    
    func checkAll(){
        let etvc = self.storyboard?.instantiateViewController(withIdentifier: "editTeamsView") as! EditTeamsViewController
        etvc.teams = teams
        self.navigationController?.pushViewController(etvc, animated: true)
    }
    
    @IBAction func tapAction(_ sender: Any) {
        for cell in collectionView.visibleCells{
            if (cell as! ClassButtonCollectionViewCell).button.buttonsIsShown(){
                (cell as! ClassButtonCollectionViewCell).button.onTap()
            }
        }
    }
    @IBAction func helpAction(_ sender: Any) {

    }
    
    @IBAction func closeHelpAction(_ sender: Any) {
        self.navigationController?.navigationBar.isHidden = false
        blurView.alpha = 0
    }
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var editTeamsLabel: UILabel!
    @IBOutlet weak var seeTeamsLabel: UILabel!
    
    
    let yourAttributes : [String : Any] = [
        NSForegroundColorAttributeName : UIColor.white,
        NSUnderlineStyleAttributeName : NSUnderlineStyle.styleSingle.rawValue]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let attributeString = NSMutableAttributedString(string: Localization("menuAyuda"),
                                                        attributes: yourAttributes)
        helpMenuButton.setAttributedTitle(attributeString, for: .normal)
        
//        helpMenuButton.attributedTitle = NSAttributedString(string: Localization("menuAyuda"), attributes: [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue])
        rateLabel.attributedText = NSAttributedString (string: Localization("ToRateTeams"))
        seeTeamsLabel.attributedText = NSAttributedString(string: Localization("SeeTeams"))
        editTeamsLabel.attributedText = NSAttributedString(string: Localization("ToEditStudents"))
        rateLabel.textAlignment = .center
        seeTeamsLabel.textAlignment = .center
        editTeamsLabel.textAlignment = .center
        tramsLabelUp.text = Localization("Teams")
        subTitle.text = Localization("AssignStudents")
        teamsLabel.text = Localization("Teams")
        teamsTextLabel.text = Localization("AssignStudents")
        if !internetProvider.sharedInstance.isVertical{
            print("Landscape")
            heightConstraint.constant = 600
        } else {
            heightConstraint.constant = 766
        }
        self.view.layoutIfNeeded()
        colors = [Constants.colorClass.red, Constants.colorClass.orange, Constants.colorClass.blue1, Constants.colorClass.pink, Constants.colorClass.purple, Constants.colorClass.yellow, Constants.colorClass.aqua, Constants.colorClass.green, Constants.colorClass.green2, Constants.colorClass.blue2, Constants.colorClass.purple2]
        let btn1 = UIButton(type: .custom)
        btn1.setImage(#imageLiteral(resourceName: "editar2"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 46.5, height: 46.5)
        btn1.addTarget(self, action: #selector(self.checkAll), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        
        let btn2 = UIButton(type: .custom)
        btn2.setImage(#imageLiteral(resourceName: "ayuda"), for: .normal)
        btn2.frame = CGRect(x: 0, y: 0, width: 46.5, height: 46.5)
        btn2.addTarget(self, action: #selector(self.ayuda), for: .touchUpInside)
        let item2 = UIBarButtonItem(customView: btn2)
        
        self.navigationItem.setRightBarButtonItems([item1, item2], animated: true)
        // Do any additional setup after loading the view.
    }
    
    func ayuda(){
        self.navigationController?.navigationBar.isHidden = true
        blurView.alpha = 1
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addButton(_ sender: Any) {
        let cpopvc = self.storyboard?.instantiateViewController(withIdentifier: "createTeamsPopUpView") as! CreateTeamsPopUpViewController
        cpopvc.delegate = self
        cpopvc.modalPresentationStyle = .overCurrentContext
        cpopvc.modalTransitionStyle = .crossDissolve
        self.present(cpopvc, animated: true) {
            
        }
    }
    
    @IBAction func addBottomButton(_ sender: Any) {
        let rvc = self.storyboard?.instantiateViewController(withIdentifier: "selectStudentView") as! SelectStudentForTeamViewController
        rvc.classe = self.classe
        rvc.delegate = self
        self.navigationController?.pushViewController(rvc, animated: true)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension TeamsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowLayout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let marginsAndInsets = flowLayout.sectionInset.left + flowLayout.sectionInset.right + flowLayout.minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
        let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
        return CGSize(width: itemWidth, height: itemWidth)
    }
    

    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if classe.students.count == 0{
            self.addBottomButton.alpha = 0
        }else{
            if teams.count == 0{
                self.addBottomButton.alpha = 0
            }else{
                self.addBottomButton.alpha = 1
            }
        }
        if teams.count == 0{
            teamsLabel.alpha = 1
            teamsTextLabel.alpha = 1
            tramsLabelUp.alpha = 0
            subTitle.alpha = 0
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        }else{
            teamsLabel.alpha = 0
            teamsTextLabel.alpha = 0
            tramsLabelUp.alpha = 1
            subTitle.alpha = 1
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        }
        return teams.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ClassButtonCollectionViewCell
        let team = teams[indexPath.row]
        cell.button.delegate = self
        cell.button.duration = 2
        cell.button.distance = 145
        //        cell.backgroundColor = UIColor.green
        cell.button.setBackgroundImage(UIImage(), for: .selected)
        //        cell.button.backgroundColor = UIColor.yellow
        if let n = team.name{
            cell.nameLabel.text = "\(n) (\(teams[indexPath.row].students.count))"
        }else{
            cell.nameLabel.text = "(\(teams[indexPath.row].students.count))"
        }
        cell.nameLabel.textColor = UIColor.white
        cell.circleImg.image = team.color.image
        if cell.button.buttonsIsShown(){
            cell.button.onTap()
        }
        cell.circleImg.tag = indexPath.row
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(normalTap(_:)))
        cell.circleImg.isUserInteractionEnabled = true
        cell.button.isUserInteractionEnabled = false
        cell.nameLabel.isUserInteractionEnabled = false
        cell.circleImg.tag = indexPath.row
        cell.circleImg.addGestureRecognizer(tapGesture)
        let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(longTap(_:)))
        cell.circleImg.addGestureRecognizer(longGesture)

        return cell
    }
    
    func normalTap(_ sender: UIGestureRecognizer){
        let cell = collectionView.cellForItem(at: IndexPath(row: (sender.view as! UIImageView).tag, section: 0)) as! ClassButtonCollectionViewCell
        for cell in collectionView.visibleCells{
            if (cell as! ClassButtonCollectionViewCell).button.buttonsIsShown(){
                (cell as! ClassButtonCollectionViewCell).button.onTap()
            }
        }
        isLong = false
        currentTeam = teams[(sender.view as! UIImageView).tag]
        print(currentTeam.students.count)
        print((sender.view as! UIImageView).tag)
        print("Normal tap")
//        self.startSpinner()
//        self.perform(#selector(self.afterAnimation), with: nil, afterDelay: 3.0)
        let rvc = self.storyboard?.instantiateViewController(withIdentifier: "rateView") as! RateViewController
        rvc.team = currentTeam
        rvc.modalPresentationStyle = .overCurrentContext
        rvc.modalTransitionStyle = .crossDissolve
        rvc.period = self.period
        rvc.classe = self.classe
        self.present(rvc, animated: true) {
            
        }
    }
    
    func afterAnimation(){
        self.stopSpinner()
    }
    
    func longTap(_ sender: UIGestureRecognizer){
        print("Long tap \((sender.view as! UIImageView).tag)")
        if sender.state == .ended {
            print("UIGestureRecognizerStateEnded")
            //Do Whatever You want on End of Gesture
        }else if sender.state == .began {
            print("UIGestureRecognizerStateBegan.")
            let cell = collectionView.cellForItem(at: IndexPath(row: (sender.view as! UIImageView).tag, section: 0)) as! ClassButtonCollectionViewCell
            if cell.button.buttonsCount == 2 && cell.button.buttonsIsShown(){
                
            }else{
                for cell in collectionView.visibleCells{
                    if (cell as! ClassButtonCollectionViewCell).button.buttonsIsShown(){
                        (cell as! ClassButtonCollectionViewCell).button.onTap()
                    }
                }
                isLong = true
                cell.button.buttonsCount = 3
                self.currentTeam = teams[(sender.view as! UIImageView).tag]
                print((sender.view as! UIImageView).tag)
                cell.button.onTap()
            }
            
            
            
            //Do Whatever You want on Began of Gesture
        }
    }
}


extension TeamsViewController: CircleMenuDelegate{
    func circleMenu(_ circleMenu: CircleMenu, willDisplay button: UIButton, atIndex: Int) {
        button.backgroundColor = UIColor.clear
        button.setBackgroundImage(UIImage(), for: .selected)
        print(circleMenu.tag)
        button.layer.zPosition = -1000
        
        if isLong{
            if atIndex == 0{
                button.setBackgroundImage(#imageLiteral(resourceName: "editarOn"), for: .normal)
            }
            if atIndex == 1{
                button.setBackgroundImage(#imageLiteral(resourceName: "verIntegrantesOn"), for: .normal)
            }
            if atIndex == 2{
                button.setBackgroundImage(#imageLiteral(resourceName: "eliminarOn"), for: .normal)
            }
            print("Long")
        }else{
            print("Not long")
        }

        
        }
    
    func circleMenu(_ circleMenu: CircleMenu, buttonDidSelected button: UIButton, atIndex: Int) {
        print(atIndex)
    }
    
    func circleMenu(_ circleMenu: CircleMenu, buttonWillSelected button: UIButton, atIndex: Int) {
        print(atIndex)
        if atIndex == 0{
            presentAlert()
        }else if atIndex == 2{
            let alert = UIAlertController(title: nil, message: Localization("DeleteTeamAlert"), preferredStyle: .alert)
            let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default, handler: { (action) in
                self.classe.students.append(contentsOf: self.currentTeam.students)
                self.colors.append(self.currentTeam.color)
                self.teams.remove(at: self.teams.index(of: self.currentTeam)!)
                self.currentTeam = nil
                if self.teams.count == 0{
                    self.collectionView.alpha = 0
                    self.centerAddButton.alpha = 1
                    self.addBottomButton.alpha = 0
                }else{
                    self.collectionView.alpha = 1
                    self.centerAddButton.alpha = 0
                    self.addBottomButton.alpha = 1
                }

                self.collectionView.reloadData()
                
            })
            let cancelAction = UIAlertAction(title: Localization("Cancelar"), style: .cancel, handler: { (action) in
                
            })
            alert.addAction(cancelAction)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: { 
                
            })
        }else{
            let sspp = self.storyboard?.instantiateViewController(withIdentifier: "studentsView") as! StudentsByTeamPopUpViewController
            sspp.team = currentTeam
            sspp.modalPresentationStyle = .overCurrentContext
            sspp.modalTransitionStyle = .crossDissolve
            self.present(sspp, animated: true, completion: {
                
            })
            
        }
    }
    
    func menuCollapsed(_ circleMenu: CircleMenu) {
        circleMenu.alpha = 1
    }
    
    func presentAlert() {
        let alertController = UIAlertController(title: nil, message:Localization("NameTeamAlert"), preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: Localization("Save"), style: .default, handler: {
            alert -> Void in
            if ((alertController.textFields?[0])?.text?.trimmingCharacters(in: .whitespacesAndNewlines).characters.count)! > 0{
                let textField = alertController.textFields![0] as UITextField
                self.currentTeam.name = textField.text
                self.collectionView.reloadData()
            }
        }))
        alertController.addAction(UIAlertAction(title: Localization("Cancelar"), style: .cancel, handler: nil))
        
        alertController.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            if let n = self.currentTeam.name{
                textField.placeholder = n
            }else{
                textField.placeholder = ""
            }
        })
        
        self.present(alertController, animated: true, completion: nil)
    }
}

extension TeamsViewController: CreateTeamsDelegate{
    func selected(teams: [Team]) {
        self.teams = teams
        for _ in 0...teams.count - 1{
            colors.remove(at: 0)
        }
        self.classe.students.removeAll()
        collectionView.reloadData()
        collectionView.alpha = 1
        centerAddButton.alpha = 0
        addBottomButton.alpha = 0
    }
}

extension TeamsViewController: CreateTeamsPopUpDelegate{
    func isManual(a: Bool) {
        manual = a
        if a{
            let rvc = self.storyboard?.instantiateViewController(withIdentifier: "selectStudentView") as! SelectStudentForTeamViewController
            rvc.classe = self.classe
            rvc.delegate = self
            self.navigationController?.pushViewController(rvc, animated: true)
        }else{
            let rvc = self.storyboard?.instantiateViewController(withIdentifier: "createTeamsView") as! CreateTeamsViewController
            rvc.delegate = self
            rvc.classe = self.classe
            rvc.modalPresentationStyle = .overCurrentContext
            rvc.modalTransitionStyle = .crossDissolve
            self.present(rvc, animated: true) {
                
            }
        }
    }
}

extension TeamsViewController: SelectStudentDelegate{
    func createTeamWith(students: [Student]) {
        if teams.count == 11{
            let alert = self.errorAlert(message: Localization("11ErrorTeams"))
            self.present(alert, animated: true, completion: { 
            })
            classe.students.append(contentsOf: students)
            return
        }
        let team = Team()
        team.students = students
        teams.append(team)
        team.color = colors[0]
        colors.remove(at: 0)
        team.name = team.color.nameTeam
        collectionView.reloadData()
        collectionView.alpha = 1
        centerAddButton.alpha = 0
        addBottomButton.alpha = 1
    }
}
