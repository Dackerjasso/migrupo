//
//  StudentInformationTableViewCell.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 31/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

class StudentInformationTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var sexLabel: UILabel!
    @IBOutlet weak var birthdayLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var selectedImg: UIButton!
    @IBOutlet weak var fondoImg: UIImageView!
    @IBOutlet weak var editButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        userImg.clipsToBounds = true
        userImg.layer.cornerRadius = userImg.frame.size.width / 2
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
