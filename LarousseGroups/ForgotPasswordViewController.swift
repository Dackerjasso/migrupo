//
//  ForgotPasswordViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso on 22/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {
    @IBOutlet weak var emailTF: UITextField!
    let tWS = TeacherWS()
    @IBOutlet weak var backButton: UIButton!
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if UIDevice.current.orientation.isLandscape {
            print("Landscape")
            internetProvider.sharedInstance.isVertical = false
        } else {
            internetProvider.sharedInstance.isVertical = true
            
        }
        self.view.layoutIfNeeded()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
        if (self.isBeingPresented) {
            // being presented
            print("Presented")
            backButton.alpha = 1
        } else if (self.isMovingToParentViewController) {
            // being pushed
            print("Pushed")
            backButton.alpha = 0
        } else {
            // simply showing again because another VC was dismissed
            print("No sé")
        }
        titleLabel.text = Localization("Forgot")
        emailTF.placeholder = Localization("Email")
        getPassButton.setTitle(Localization("GetPAssword"), for: .normal)
    }
    @IBOutlet weak var getPassButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        emailTF.setLeftPaddingPoints(30)
        emailTF.setRightPaddingPoints(30)
        emailTF.setRightPaddingPoints(30, image: #imageLiteral(resourceName: "check1"))
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        self.dismiss(animated: true) { 
            
        }
    }
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBAction func sendPassword(_ sender: Any) {
        
        if emailTF.isValidEmail(){
            tWS.delegate = self
            self.startSpinner()
            self.view.isUserInteractionEnabled = false
            tWS.delegate = self
            tWS.restorePassWordTo(email: emailTF.text)
        }else{
            let alert = self.errorAlert(message: Localization("EmailNotValid"))
            self.present(alert, animated: true, completion: { 
                
            })
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ForgotPasswordViewController: teacherDelegate{
    func didFailSendEmailToRestorePassword(message: String) {
        let alert = self.errorAlert(message: message)
        self.stopSpinner()
        self.view.isUserInteractionEnabled = true
        self.present(alert, animated: true) {
            
        }
    }
    
    func didSuccessSendMailToRestorePassword() {
        self.stopSpinner()
        let alert = UIAlertController(title: Localization("ChangePassAlert") , message: nil, preferredStyle: .alert)
        let okAction = UIAlertAction(title: Localization("Aceptar"), style: .default) { (action) in
            
        }
        alert.addAction(okAction)
        self.view.isUserInteractionEnabled = true
        self.present(alert, animated: true) {
            
        }
    }
}

extension ForgotPasswordViewController: UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.characters.count + string.characters.count - range.length
        return newLength <= 35 // Bool
    }
}
