//
//  PopupListViewController.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 29/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

protocol PopUpListDelegate {
    func takeList()
    func seeList()
    func activities()
    func rateDay()
    func repsorts()
}

class PopupListViewController: UIViewController {
    var delegate: PopUpListDelegate?
    @IBOutlet weak var titileLabel: UILabel!
    @IBOutlet weak var takeListButton: UIButton!
    @IBOutlet weak var rateDayButton: UIButton!
    @IBOutlet weak var activitiesButton: UIButton!
    @IBOutlet weak var reportsButton: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.presentTransparentNavigationBar()
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titileLabel.text = Localization("Todo")
        takeListButton.setTitle(Localization("TakeListM"), for: .normal)
        rateDayButton.setTitle(Localization("RateDayM"), for: .normal)
        activitiesButton.setTitle(Localization("ActivitiesM"), for: .normal)
        reportsButton.setTitle(Localization("ReportsM"), for: .normal)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func takeListAction(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.takeList()
        }
    }
    
    @IBAction func rateDayAction(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.rateDay()
        }
    }
    
    @IBAction func seeListAction(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.seeList()
        }
    }
    
    @IBAction func activitiesAction(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.activities()
        }
    }
    
    @IBAction func reportsAction(_ sender: Any) {
        self.dismiss(animated: false) {
            self.delegate?.repsorts()
        }
    }

    @IBAction func tapAction(_ sender: Any) {
        self.dismiss(animated: false) {
            
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
