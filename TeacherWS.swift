
//
//  TeacherWS.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso on 22/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import Alamofire
import SQLite
import CVCalendar
import SystemConfiguration

@objc protocol teacherDelegate : class{
    @objc optional func didSuccessLogin(teacher: Teacher)
    @objc optional func didFailSuccessLogin(statusCode: Int, message: String!)
    
    @objc optional func didSuccessRegister(teacher: Teacher)
    @objc optional func didFailRegisterTeacher(statusCode: Int, message: String!)
    
    @objc optional func didSuccessRegister(classe: Class)
    @objc optional func didFailRegisterClass(statusCode: Int, message: String!)
    
    @objc optional func didSuccessRegister(student: Student, classe: Class)
    @objc optional func didFailRegisterStudent(statusCode: Int, message: String!)
    
    @objc optional func didSuccessGet(classes: [Class])
    @objc optional func didFailGetClasses(statusCode: Int, message: String!)
    
    @objc optional func didSuccessSendMailToRestorePassword()
    @objc optional func didFailSendEmailToRestorePassword(message: String!)
    
    @objc optional func didSuccessChangePassword ()
    @objc optional func didFailChangePassword(message: String!)
    
    @objc optional func didSuccessUpdate(classe : Class)
    @objc optional func didFailUpdateClass(message: String)
    
    @objc optional func didSuccessAddPeriodToClass()
    @objc optional func didFailAddPeriodToClass(message: String)
    
    @objc optional func didSuccessDelete(criterio: Criterio)
    @objc optional func didFailDeleteCriterio(message: String)
    
    @objc optional func didSuccessDelete(students: [Student])
    @objc optional func didFailDeleteStudents(message: String!)
    
    @objc optional func didSuccessDelete(classe: Class)
    @objc optional func didFailDeleteClass(message: String!)
    
    @objc optional func didSuccessDelete(period: Period)
    @objc optional func didFailDeletePeriod(message: String)
    
    @objc optional func didSuccessUpdate(period: Period)
    @objc optional func didFailUpdatePeriod(message: String)
    
    @objc optional func didSuccessUpdate(student : Student)
    @objc optional func didFailUpdateStudent(message: String)
    
    @objc optional func didSuccessTakeList()
    @objc optional func didFailTakeList(message: String)
    
    @objc optional func didSuccessUpdateCriteriosOn(classe: Class)
    @objc optional func didFailUpdateCriteriosOnClasse(message: String)
    
    @objc optional func didSuccessUserUpdated(teacher: Teacher)
    @objc optional func didFailUserUpdated(message: String)
    
    @objc optional func didSuccessUploadImage()
    @objc optional func didFailUpdateImage(message: String)
    
    @objc optional func didSuccessRateStudents()
    @objc optional func diidFailRateStudents()
    
    @objc optional func didSuccessGetRepostByPeriod(students: [Student])
    @objc optional func didFailGetRepostByPeriod(message: String)
    
    @objc optional func didSuccessGetReportByDay(students: [Student])
    @objc optional func didFailGetReportByDay(message: String)
    
    @objc optional func didSuccessRateDay()
    @objc optional func didFailRateDay(message: String)
    
    @objc optional func userAlreadyExists()
    
    
    //    @objc optional func didSuccessAddPeriodTo
}

public enum HTTPMethod: String {
    case options = "OPTIONS"
    case get     = "GET"
    case head    = "HEAD"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
    case trace   = "TRACE"
    case connect = "CONNECT"
}

class TeacherWS: NSObject {
    var delegate : teacherDelegate?
    let defaults = UserDefaults.standard
    
    
    func hasInternet(completionHandler:@escaping (Bool) -> ()) {
        let url = NSURL(string: "http://google.com")
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "HEAD"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData
        request.timeoutInterval = 2
        
        //            var data = NSURLConnection.sendSynchronousRequest(request, returningResponse: &response, error: error) as NSData?
        let session = URLSession.shared
        let d = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
            if (error != nil) {
                completionHandler(false)
                return
            }else{
                if response != nil{
                    if (response as! HTTPURLResponse).statusCode == 200{
                        completionHandler(true)
                        return
                    }else{
                        completionHandler(false)
                        return
                    }
                }else{
                    completionHandler(false)
                    return
                }
            }

        }
        d.resume()
    }
    
    func rateDay(students: [Student], classe: Class, period: Period, c1: Bool, c2:Bool, c3:Bool, date: Date){
        switch currentReachabilityStatus {
        case .notReachable:
            self.rateDayOnDB(students: students, classe: classe, period: period, c1: c1, c2: c2, c3: c3, date: date)
            break
        case .reachableViaWiFi, .reachableViaWWAN:
                if internetProvider.sharedInstance.hasInternet{
                    let header : [String: String] = [
                        "Content-Type":"application/json",
                        "user":UserProvider.sharedInstance.uuid,
                        "idioma": self.defaults.object(forKey: "language") as! String
                    ]
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeStyle = DateFormatter.Style.none
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    dateFormatter.dateFormat = "y-MM-dd"
                    let bDate = dateFormatter.string(from: date)  // "2010-01-27"
                    
                    var x = [[:]]
                    x.removeAll()
                    
                    for s in students{
                        var z = [[:]]
                        z.removeAll()
                        
                        for (index,c) in s.criterios.enumerated(){
                            switch index {
                            case 0:
                                continue
                            case 1:
                                continue
                            case 2:
                                if c1 {
                                    let a = [
                                        "criterioId" : c.dbid,
                                        "calif": c.calif,
                                        ] as [String : Any]
                                    z.append(a)
                                }
                                break
                            case 3:
                                if c2 {
                                    let a = [
                                        "criterioId" : c.dbid,
                                        "calif": c.calif,
                                        ] as [String : Any]
                                    z.append(a)
                                }
                                break
                            case 4:
                                if c3 {
                                    let a = [
                                        "criterioId" : c.dbid,
                                        "calif": c.calif,
                                        ] as [String : Any]
                                    z.append(a)
                                }
                                break
                            default: break
                            }
                        }
                        print (z)
                        
                        let a = [
                            "alumnoId" : s.dbid,
                            "califInfo": z,
                            ] as [String : Any]
                        x.append(a)
                    }
                    print(x)
                    
                    
                    
                    let parameters = [
                        "periodoId":period.dbid,
                        "fecha":bDate,
                        "alumnos":x
                        ] as [String : Any]
                    
                    print(parameters)
                    
                    Alamofire.request("\(Constants.baseUrl)/\(Constants.clase)/\(Constants.calificacion)", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                        debugPrint(response)
                        switch response.result{
                        case .success:
                            switch Int((response.response?.statusCode)!){
                            case 200, 201, 202:
                                self.rateDayOnDB(students: students, classe: classe, period: period, c1: c1, c2: c2, c3: c3, date: date)
                                self.delegate?.didSuccessRateDay!()
                                break
                            case 300...500:
                                self.delegate?.didFailRateDay!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            default:
                                self.delegate?.didFailRateDay!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            }
                            break
                        case .failure(let error):
                            self.delegate?.didFailRateDay!(message: Localization("GenericError"))
                            print (error)
                            break
                        }
                    }
                }else{
                    self.rateDayOnDB(students: students, classe: classe, period: period, c1: c1, c2: c2, c3: c3, date: date)
                }
            break
        default:
            break
        }
    }
    
    
    func getReportBy(date: Date, classe: Class, period: Period){
        switch currentReachabilityStatus {
        case .notReachable:
            self.getReportOnDBBy(date: date, classe: classe, period: period)
            break
        case .reachableViaWiFi, .reachableViaWWAN:
            if internetProvider.sharedInstance.hasInternet{
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeStyle = DateFormatter.Style.none
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    dateFormatter.dateFormat = "y-MM-dd"
                    let bDate = dateFormatter.string(from: date)  // "2010-01-27"
                    
                    let header : [String: String] = [
                        "Content-Type":"application/json",
                        "user":UserProvider.sharedInstance.uuid,
                        "idioma": self.defaults.object(forKey: "language") as! String
                    ]
                    
                    Alamofire.request("\(Constants.baseUrl)/\(Constants.clase)/\(Constants.porcentajeDia)/\(period.dbid!)/\(bDate)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                        debugPrint(response)
                        switch response.result{
                        case .success:
                            var students : [Student] = []
                            switch Int((response.response?.statusCode)!){
                            case 200, 201, 202:
                                if let lmns = ((response.result.value as! NSDictionary).value(forKey: "PorcentajesPorDia") as! NSDictionary).value(forKey: "alumnos"){
                                    for student in lmns as! NSArray{
                                        print(student as! NSDictionary)
                                        let s = Student()
                                        s.dbid = (student as! NSDictionary).value(forKey: "id") as! Int
                                        s.uuid = (student as! NSDictionary).value(forKey: "alumnoUuid") as! String
                                        s.imgUrl = (student as! NSDictionary).value(forKey: "pictureUrl") as! String
                                        //                                s.image = classe.students.first(where: {$0.uuid == s.uuid}).image
                                        if let criterios = (student as! NSDictionary).value(forKey: "criterios"){
                                            for c in criterios as! NSArray{
                                                print (c)
                                                let criterio = Criterio()
                                                criterio.name = (c as! NSDictionary).value(forKey: "actividad") as! String
                                                if let v = (c as! NSDictionary).value(forKey: "valor"){
                                                    criterio.calif = v as! Double
                                                }else{
                                                    criterio.calif = -1
                                                }
                                                s.criterios.append(criterio)
                                            }
                                        }
                                        let stn = classe.students.first(where: {$0.dbid == s.dbid})
                                        s.name = stn?.name
                                        students.append(s)
                                    }
                                    self.delegate?.didSuccessGetReportByDay!(students: students)
                                }else{
                                    self.delegate?.didFailGetReportByDay!(message: Localization("GenericError"))
                                }
                                break
                            case 300...500:
                                self.delegate?.didFailGetReportByDay!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            default:
                                self.delegate?.didFailGetReportByDay!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            }
                            break
                        case .failure(let error):
                            self.delegate?.didFailGetReportByDay!(message: Localization("GenericError"))
                            print (error)
                            break
                        }
                    }
                }else{
                    self.getReportOnDBBy(date: date, classe: classe, period: period)
                }
            break
        }
    }
    

    
    
    func getReport(period: Period, classe: Class){
        switch currentReachabilityStatus {
        case .notReachable:
            self.getReportONDB(period: period, classe: classe)
            break
        case .reachableViaWiFi, .reachableViaWWAN:
            if internetProvider.sharedInstance.hasInternet{
                    let header : [String: String] = [
                        "Content-Type":"application/json",
                        "user":UserProvider.sharedInstance.uuid,
                        "idioma": self.defaults.object(forKey: "language") as! String
                    ]
                    
                    Alamofire.request("\(Constants.baseUrl)/\(Constants.clase)/\(Constants.participaciones)/\(period.dbid!)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                        debugPrint(response)
                        switch response.result{
                        case .success:
                            switch Int((response.response?.statusCode)!){
                            case 200, 201, 202:
                                var students : [Student] = []
                                if let lmns = ((response.result.value as! NSDictionary).value(forKey: "PorcentajesPorPeriodo") as! NSDictionary).value(forKey: "alumnos"){
                                    for student in lmns as! NSArray{
                                        print(student as! NSDictionary)
                                        let s = Student()
                                        let std = classe.students.filter{ $0.uuid == (student as! NSDictionary).value(forKey: "alumnoUuid") as! String }.first
                                        let std2 = std?.copy() as! Student
                                        s.img = std2.img
                                        s.dbid = (student as! NSDictionary).value(forKey: "id") as! Int
                                        s.uuid = (student as! NSDictionary).value(forKey: "alumnoUuid") as! String
                                        s.imgUrl = (student as! NSDictionary).value(forKey: "pictureUrl") as! String
                                        if let criterios = (student as! NSDictionary).value(forKey: "criterios"){
                                            for c in criterios as! NSArray{
                                                print (c)
                                                let criterio = Criterio()
                                                criterio.name = (c as! NSDictionary).value(forKey: "actividad") as! String
                                                criterio.calif = Double((c as! NSDictionary).value(forKey: "porcentaje") as! Float)
                                                s.criterios.append(criterio)
                                            }
                                        }
                                        let stn = classe.students.first(where: {$0.dbid == s.dbid})
                                        s.name = stn?.name
                                        students.append(s)
                                    }
                                    self.getReport2(period: period, classe: classe, students: students)
                                }else{
                                    self.delegate?.didFailGetRepostByPeriod!(message: Localization("GenericError"))
                                }
                                break
                            case 300...500:
                                self.delegate?.didFailGetRepostByPeriod!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            default:
                                self.delegate?.didFailGetRepostByPeriod!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            }
                            break
                        case .failure(let error):
                            self.delegate?.didFailGetRepostByPeriod!(message: Localization("GenericError"))
                            print (error)
                            break
                        }
                    }
                }else{
                    self.getReportONDB(period: period, classe: classe)
                }
        break
        }
        
    }
    
    func getReport2(period: Period!, classe: Class!, students: [Student]){
        let header : [String: String] = [
            "Content-Type":"application/json",
            "user":UserProvider.sharedInstance.uuid,
            "idioma": defaults.object(forKey: "language") as! String
        ]
        
        Alamofire.request("\(Constants.baseUrl)/\(Constants.clase)/\(Constants.porcentaje)/\(period.dbid!)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            debugPrint(response)
            switch response.result{
            case .success:
                switch Int((response.response?.statusCode)!){
                case 200, 201, 202:
                    if let lmns = ((response.result.value as! NSDictionary).value(forKey: "PorcentajesPorperiodo") as! NSDictionary).value(forKey: "alumnos"){
                        for student in lmns as! NSArray{
                            let stn = students.first(where: {$0.dbid == ((student as! NSDictionary).value(forKey: "id") as! Int)})
                            if let criterios = (student as! NSDictionary).value(forKey: "criterios"){
                                for c in criterios as! NSArray{
                                    print (c)
                                    let criterio = Criterio()
                                    criterio.name = (c as! NSDictionary).value(forKey: "actividad") as! String
                                    if let v = (c as! NSDictionary).value(forKey: "valor"){
                                        criterio.value = Double(v as! Float)
                                    }
                                    if let g = (c as! NSDictionary).value(forKey: "porcentaje"){
                                        criterio.calif = Double(g as! Float)
                                    }
                                    stn?.criterios.append(criterio)
                                }
                            }
                        }
                        self.delegate?.didSuccessGetRepostByPeriod!(students: students)
                    }else{
                        self.delegate?.didFailGetRepostByPeriod!(message: Localization("GenericError"))
                    }
                    print(response.result.value!)
                    break
                case 300...500:
                    self.delegate?.didFailGetRepostByPeriod!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                    break
                default:
                    self.delegate?.didFailGetRepostByPeriod!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                    break
                }
                break
            case .failure(let error):
                self.delegate?.didFailGetRepostByPeriod!(message: Localization("GenericError"))
                print (error)
                break
            }
        }
        
    }
    
    
    func rate(students : [Student], classe: Class, period: Int){
        switch currentReachabilityStatus {
        case .notReachable:
            self.rateOnDB(students : students, classe: classe, period: period)
            break
        case .reachableViaWiFi, .reachableViaWWAN:
            if internetProvider.sharedInstance.hasInternet{
                    let header : [String: String] = [
                        "user":UserProvider.sharedInstance.uuid,
                        "idioma": self.defaults.object(forKey: "language") as! String
                    ]
                    var x = [[ : ]]
                    x.removeAll()
                    
                    for s in students{
                        let a = [
                            "alumnoId" : s.dbid,
                            "participo": 1,
                            ] as [String : Any]
                        x.append(a)
                    }
                    
                    let date = Date()
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeStyle = DateFormatter.Style.none
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    dateFormatter.dateFormat = "y-MM-dd"
                    dateFormatter.string(from: date)
                    let bDate = dateFormatter.string(from: date)  // "2010-01-27"
                    
                    let parameters = [
                        "periodoId":period,
                        "fecha":bDate,
                        "criterioUuid":classe.criterios[1].uuid,
                        "alumnos":x
                        ] as [String : Any]
                    
                    print(parameters)
                    Alamofire.request("\(Constants.baseUrl)/\(Constants.clase)/\(Constants.participacion)", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                        debugPrint(response)
                        switch response.result{
                        case .success:
                            switch Int((response.response?.statusCode)!){
                            case 200, 201, 202:
                                self.rateOnDB(students : students, classe: classe, period: period)
                                self.delegate?.didSuccessRateStudents!()
                                break
                            case 300...500:
                                self.delegate?.diidFailRateStudents!()
                                break
                            default:
                                self.delegate?.diidFailRateStudents!()
                                break
                            }
                            break
                        case .failure(let error):
                            self.delegate?.diidFailRateStudents!()
                            print (error)
                            break
                        }
                    }
                }else{
                    self.rateOnDB(students : students, classe: classe, period: period)
                }
        break
        }
    }
    
    func getDocumentsURL() -> URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        return documentsURL
    }
    
    func fileInDocumentsDirectory(_ filename: String) -> URL{
        let fileURL = getDocumentsURL().appendingPathComponent(filename)
        return fileURL
    }
    
    func saveImage (_ image: UIImage, path: URL ) -> Bool{
        
        let pngImageData = UIImageJPEGRepresentation(image, 1.0)
        UIGraphicsBeginImageContext(image.size)
        image.draw(in: CGRect(x:0, y:0, width:image.size.width, height:image.size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let jpgImageData = UIImageJPEGRepresentation(newImage!, 1.0)
        
        let result = (try? jpgImageData!.write(to: URL(fileURLWithPath: path.path), options: [.atomicWrite])) != nil
        return result
        
    }
    
    func updateImage(url: URL, student: Student?){
        switch currentReachabilityStatus {
        case .notReachable:
            self.delegate?.didSuccessUploadImage!()
            break
        case .reachableViaWiFi, .reachableViaWWAN:
            if internetProvider.sharedInstance.hasInternet{
                    let header : [String: String] = [
                        "user":UserProvider.sharedInstance.uuid,
                        "idioma": self.defaults.object(forKey: "language") as! String
                    ]
                    var toUrl = ""
                    if let s = student{
                        toUrl = "\(Constants.baseUrl)/\(Constants.alumno)/\(s.uuid!)/\(Constants.photo)"
                    }else{
                        toUrl = "\(Constants.baseUrl)/\(Constants.users)/\(UserProvider.sharedInstance.uuid!)/\(Constants.photo)"
                    }
                    
                    Alamofire.upload(multipartFormData: { (multipartFormData) in
                        multipartFormData.append(url, withName: "file")
                    }, usingThreshold: UInt64.init(), to: toUrl, method: .put, headers: header) { (result) in
                        debugPrint(result)
                        switch result{
                        case .success (let upload, _, _):
                            upload.responseJSON(completionHandler: { (response) in
                                debugPrint(response)
                                switch response.result{
                                case .success:
                                    switch Int((response.response?.statusCode)!){
                                    case 200, 201, 202:
                                        self.delegate?.didSuccessUploadImage!()
                                        break
                                    case 300...500:
                                        self.delegate?.didFailUpdateImage!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                        break
                                    default:
                                        self.delegate?.didFailUpdateImage!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                        break
                                    }
                                    break
                                case .failure(let error):
                                    self.delegate?.didFailUpdateImage!(message: Localization("GenericError"))
                                    print (error)
                                    break
                                }
                            })
                            break
                        case .failure(let error):
                            self.delegate?.didFailUpdateImage!(message: Localization("GenericError"))
                            print(error)
                            break
                        }
                    }
                }else{
                    self.delegate?.didSuccessUploadImage!()
                }
            break
        }
        
        
    }
    
    func update(teacher: Teacher){
        switch currentReachabilityStatus {
        case .notReachable:
            let db : Connection!
            do{
                db = try Connection("\(Constants.path)/db.sqlite3")
            }catch{
                print ("Error: \(error)")
                db = nil
            }
            
            let users = Table("users")
            
            var countFunction = 0
            do{
                if db != nil{
                    countFunction = try db.scalar( users.filter( Constants.email == UserProvider.sharedInstance.email ).count )
                }
            }catch{
                print(error)
            }
            
            
            if countFunction > 0{
                do {
                    if db != nil{
                        let query = users.filter( Constants.email == UserProvider.sharedInstance.email)
                        try db.run(query.update(Constants.id <- teacher.dbid, Constants.name <- teacher.name, Constants.email <- teacher.email, Constants.lastName <- teacher.lastName, Constants.username <- teacher.userName, Constants.schoolName <- teacher.schoolName ,Constants.uuid <- teacher.uuid, Constants.updated <- false))
                    }
                    print("update")
                    self.delegate?.didSuccessUserUpdated!(teacher: teacher)
                } catch {
                    print(error)
                    self.delegate?.didFailUserUpdated!(message: Localization("GenericError"))
                }
            }
            break
        case .reachableViaWiFi, .reachableViaWWAN:
            if internetProvider.sharedInstance.hasInternet{
                    let header : [String: String] = [
                        "Content-Type":"application/json",
                        "user":UserProvider.sharedInstance.uuid,
                        "idioma": self.defaults.object(forKey: "language") as! String
                    ]
                    let parameters = [
                        "name":teacher.name,
                        "lastName":teacher.lastName,
                        "alias":teacher.userName,
                        "escuela":teacher.schoolName,
                        "email" : teacher.email
                    ]
                    debugPrint(parameters)
                    Alamofire.request("\(Constants.baseUrl)/\(Constants.users)/\(teacher.uuid!)/\(Constants.update)", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                        debugPrint(response)
                        switch response.result{
                        case .success:
                            switch Int((response.response?.statusCode)!){
                            case 200, 201, 202:
                                self.delegate?.didSuccessUserUpdated!(teacher: teacher)
                                break
                            case 300...500:
                                break
                            default:
                                self.delegate?.didFailUserUpdated!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            }
                            break
                        case .failure(let error):
                            self.delegate?.didFailUserUpdated!(message: Localization("GenericError"))
                            print (error)
                            break
                        }
                    }
                }else{
                    let db : Connection!
                    do{
                        db = try Connection("\(Constants.path)/db.sqlite3")
                    }catch{
                        print ("Error: \(error)")
                        db = nil
                    }
                    
                    let users = Table("users")
                    
                    var countFunction = 0
                    do{
                        if db != nil{
                            countFunction = try db.scalar( users.filter( Constants.email == UserProvider.sharedInstance.email ).count )
                        }
                    }catch{
                        print(error)
                    }
                    
                    
                    if countFunction > 0{
                        do {
                            if db != nil{
                                let query = users.filter( Constants.email == UserProvider.sharedInstance.email)
                                try db.run(query.update(Constants.id <- teacher.dbid, Constants.name <- teacher.name, Constants.email <- teacher.email, Constants.lastName <- teacher.lastName, Constants.username <- teacher.userName, Constants.schoolName <- teacher.schoolName ,Constants.uuid <- teacher.uuid, Constants.updated <- false))
                            }
                            print("update")
                            self.delegate?.didSuccessUserUpdated!(teacher: teacher)
                        } catch {
                            print(error)
                            self.delegate?.didFailUserUpdated!(message: Localization("GenericError"))
                        }
                    }
                }
            break
        }
    }
    
    func criterios(classe: Class){
        switch currentReachabilityStatus {
        case .notReachable:
            self.updateCriterios(classe: classe)
            break
        case .reachableViaWiFi, .reachableViaWWAN:
            if internetProvider.sharedInstance.hasInternet{
                    let header : [String: String] = [
                        "Content-Type":"application/json",
                        "user":UserProvider.sharedInstance.uuid,
                        "idioma": self.defaults.object(forKey: "language") as! String
                    ]
                    
                    var x = [[ : ]]
                    x.removeAll()
                    
                    for criter in classe.criterios{
                        print(criter)
                        let a = [
                            "actividad" : criter.name,
                            "valor":Int(criter.value),
                            "criterioUuid":criter.uuid != nil ? criter.uuid : ""
                            ] as [String : Any]
                        x.append(a)
                    }
                    
                    let parameters = [
                        "criterios":[
                            [
                                "claseId":classe.dbid,
                                "criterioInfo":x
                            ]
                        ]
                    ]
                    debugPrint(parameters)
                    Alamofire.request("\(Constants.baseUrl)/\(Constants.clase)/\(Constants.criterios)", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                        debugPrint(response)
                        switch response.result{
                        case .success:
                            switch Int((response.response?.statusCode)!){
                            case 200, 201, 202:
                                let db : Connection!
                                do{
                                    db = try Connection("\(Constants.path)/db.sqlite3")
                                }catch{
                                    print ("Error: \(error)")
                                    db = nil
                                }
                                //                        do{
                                //                            let criterios = Table("criterios")
                                //                            let query = criterios.filter((Constants.cUuid == classe.uuid || Constants.cUuid == "\(classe.localId)") && Constants.idClasse == 0)
                                //                            try db.run(query.delete())
                                //                            for cr in classe.criterios{
                                //                                if let x = cr.uuid{
                                //                                    continue
                                //                                }else{
                                //                                    classe.criterios.remove(at: classe.criterios.index(of: cr)!)
                                //                                }
                                //
                                //
                                //                            }
                                //                            self.saveOrUpdateCriteriosOn(classe: UserProvider.sharedInstance.classes)
                                //                        }catch{
                                //                            print(error)
                                //                        }
                                
                                classe.criterios.removeAll()
                                for criter in (((response.result.value  as! NSDictionary).value(forKey: "Success") as! NSArray)[0] as! NSDictionary).value(forKey: "criterioInfo") as! NSArray{
                                    let criterio = Criterio()
                                    criterio.value = (criter as! NSDictionary).value(forKey: "valor") as! Double
                                    criterio.uuid = (criter as! NSDictionary).value(forKey: "criterioUuid") as! String
                                    criterio.name = (criter as! NSDictionary).value(forKey: "actividad") as! String
                                    //                            Constants.id <- c.dbid, Constants.name <- c.name, Constants.valueC <- c.value, Constants.uuid <- c.uuid, Constants.idClasse <- classe.dbid, Constants.cUuid <- classe.uuid
                                    criterio.dbid = 0
                                    
                                    classe.criterios.append(criterio)
                                }
                                self.saveOrUpdateCriteriosOn(classe: classe)
                                
                                self.delegate?.didSuccessUpdateCriteriosOn!(classe: classe)
                                break
                            case 300...500:
                                self.delegate?.didFailUpdateCriteriosOnClasse!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            default:
                                self.delegate?.didFailUpdateCriteriosOnClasse!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            }
                            break
                        case .failure(let error):
                            self.delegate?.didFailUpdateCriteriosOnClasse!(message: Localization("GenericError"))
                            print (error)
                            break
                        }
                    }
                }else{
                    self.updateCriterios(classe: classe)
                }
        break
        }
    }
    
    func asistencia(classe: Class, period: Period, date: Date){
        switch currentReachabilityStatus {
        case .notReachable:
            self.takeListOnDB(classe: classe, period: period, date: date)
            break
        case .reachableViaWiFi, .reachableViaWWAN:
            if internetProvider.sharedInstance.hasInternet{
                    let header : [String: String] = [
                        "Content-Type":"application/json",
                        "user":UserProvider.sharedInstance.uuid,
                        "idioma": self.defaults.object(forKey: "language") as! String
                    ]
                    
                    var x = [[ : ]]
                    x.removeAll()
                    for student in classe.students{
                        var asistencia = 0
                        
                        if student.isHere  && !student.isAlmostNotHere{
                            asistencia = 1
                        }
                        if !student.isHere && student.isAlmostNotHere{
                            asistencia = 3
                        }
                        
                        if !student.isAlmostNotHere && !student.isHere{
                            asistencia = 2
                        }
                        
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.timeStyle = DateFormatter.Style.none
                        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                        dateFormatter.dateFormat = "y-MM-dd"
                        let bDate = dateFormatter.string(from: date)
                        
                        let a = [
                            "alumnouuid" : student.uuid,
                            "claseuuid":classe.uuid,
                            "asistencia":asistencia,
                            "fecha":bDate
                            ] as [String : Any]
                        x.append(a)
                    }
                    let parameters = [
                        "lista": x
                        
                    ]
                    print(parameters)
                    Alamofire.request("\(Constants.baseUrl)/\(Constants.alumno)/\(Constants.asistencia)", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                        debugPrint(response)
                        switch response.result{
                        case .success:
                            switch Int((response.response?.statusCode)!){
                            case 200, 201, 202:
                                self.takeListOnDB(classe: classe, period: period, date: date)
                                self.delegate?.didSuccessTakeList!()
                                break
                            case 300...500:
                                self.delegate?.didFailTakeList!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            default:
                                self.delegate?.didFailTakeList!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            }
                            break
                        case .failure(let error):
                            self.delegate?.didFailTakeList!(message: Localization("GenericError"))
                            print (error)
                            break
                        }
                    }
                }else{
                    self.takeListOnDB(classe: classe, period: period, date: date)
                }
        break
        }
    }
    
    
    func update(student: Student){
        switch currentReachabilityStatus {
        case .notReachable:
            self.updateOnDB(student: student)
            break
        case .reachableViaWiFi, .reachableViaWWAN:
            if internetProvider.sharedInstance.hasInternet{
                    let header : [String: String] = [
                        "Content-Type":"application/json",
                        "user":UserProvider.sharedInstance.uuid,
                        "idioma": self.defaults.object(forKey: "language") as! String
                    ]
                    let date = Date()
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeStyle = DateFormatter.Style.none
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    dateFormatter.dateFormat = "y-MM-dd"
                    dateFormatter.string(from: date)
                    let bDate = dateFormatter.string(from: student.birthdayDate.convertedDate(calendar: .current)!)  // "2010-01-27"
                    
                    let parameters : [String: Any] = [
                        "nombre": student.name,
                        "genero": student.gender == Localization("man") ? "M" : "F",
                        "cumpleanos":bDate,
                        "mail": student.emailTutor,
                        ]
                    
                    Alamofire.request("\(Constants.baseUrl)/\(Constants.alumno)/\(student.uuid!)/\(Constants.update)", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                        debugPrint(response)
                        switch response.result{
                        case .success:
                            switch Int((response.response?.statusCode)!){
                            case 200, 201, 202:
                                self.updateOnDB(student: student)
                                self.delegate?.didSuccessUpdate!(student: student)
                                break
                            case 300...500:
                                self.delegate?.didFailUpdateStudent!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            default:
                                self.delegate?.didFailUpdateStudent!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            }
                            break
                        case .failure(let error):
                            self.delegate?.didFailUpdateStudent!(message: Localization("GenericError"))
                            print (error)
                            break
                        }
                    }
                }else{
                    self.updateOnDB(student: student)
                }
            break
        }
    }
    
    func update(period: Period, strtDate: CVDate, ndDate: CVDate, classe: Class){
        //        updateOnDB
        switch currentReachabilityStatus {
        case .notReachable:
            self.updateOnDB(period: period, strtDate: strtDate, ndDate: ndDate, classe: classe)
            break
        case .reachableViaWiFi, .reachableViaWWAN:
            if internetProvider.sharedInstance.hasInternet{
                    let header : [String: String] = [
                        "Content-Type":"application/json",
                        "user":UserProvider.sharedInstance.uuid,
                        "idioma": self.defaults.object(forKey: "language") as! String
                    ]
                    
                    let date = Date()
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeStyle = DateFormatter.Style.none
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    dateFormatter.dateFormat = "y-MM-dd"
                    dateFormatter.string(from: date)
                    let startDate = dateFormatter.string(from: strtDate.convertedDate(calendar: .current)!)
                    let endDate = dateFormatter.string(from: ndDate.convertedDate(calendar: .current)!)
                    
                    let parameters : [String : Any] = [
                        "id":period.dbid,
                        "fechaInicio" : startDate,
                        "fechaFin": endDate,
                        "claseId": classe.dbid
                    ]
                    print(parameters)
                    Alamofire.request("\(Constants.baseUrl)/\(Constants.clase)/\(Constants.updatePeriodo)", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                        debugPrint(response)
                        switch response.result{
                        case .success:
                            switch Int((response.response?.statusCode)!){
                            case 200, 201, 202:
                                period.startDate = strtDate
                                period.endDate = ndDate
                                self.updateOnDB(period: period, strtDate: strtDate, ndDate: ndDate, classe: classe)
                                self.delegate?.didSuccessUpdate!(period: period)
                                break
                            case 300...500:
                                self.delegate?.didFailUpdatePeriod!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            default:
                                self.delegate?.didFailUpdatePeriod!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            }
                            break
                        case .failure(let error):
                            self.delegate?.didFailUpdatePeriod!(message: Localization("GenericError"))
                            print (error)
                            break
                        }
                    }
                }else{
                    self.updateOnDB(period: period, strtDate: strtDate, ndDate: ndDate, classe: classe)
                }
            break
        }
    }
    
    func delete(period: Period){
        switch currentReachabilityStatus {
        case .notReachable:
            self.deleteOnDB(period: period)
            break
        case .reachableViaWiFi, .reachableViaWWAN:
            if internetProvider.sharedInstance.hasInternet{
                    let header : [String: String] = [
                        "Content-Type":"application/json",
                        "user":UserProvider.sharedInstance.uuid,
                        "idioma": self.defaults.object(forKey: "language") as! String
                    ]
                    Alamofire.request("\(Constants.baseUrl)/\(Constants.clase)/\(period.uuid!)/\(Constants.deletePeriod)", method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                        debugPrint(response)
                        switch response.result{
                        case .success:
                            switch Int((response.response?.statusCode)!){
                            case 200, 201, 202:
                                self.deleteOnDB(period: period)
                                self.delegate?.didSuccessDelete!(period: period)
                                break
                            case 300...500:
                                self.delegate?.didFailDeletePeriod!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            default:
                                self.delegate?.didFailDeletePeriod!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            }
                            break
                        case .failure(let error):
                            self.delegate?.didFailDeletePeriod!(message: Localization("GenericError"))
                            print (error)
                            break
                        }
                    }
                }else{
                    self.deleteOnDB(period: period)
                }
            break
        }
        
    }
    
    func delete(classe: Class){
        switch currentReachabilityStatus {
        case .notReachable:
            self.deleteOnDB(classe: classe)
            break
        case .reachableViaWiFi, .reachableViaWWAN:
            if internetProvider.sharedInstance.hasInternet{
                    let header : [String: String] = [
                        "Content-Type":"application/json",
                        "user":UserProvider.sharedInstance.uuid,
                        "idioma": self.defaults.object(forKey: "language") as! String
                    ]
                    
                    Alamofire.request("\(Constants.baseUrl)/\(Constants.clase)/\(classe.uuid!)/\(Constants.deleteClass)", method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                        debugPrint(response)
                        switch response.result{
                        case .success:
                            switch Int((response.response?.statusCode)!){
                            case 200, 201, 202:
                                self.deleteOnDB(classe: classe)
                                self.delegate?.didSuccessDelete!(classe: classe)
                                break
                            case 300...500:
                                self.delegate?.didFailDeleteClass!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            default:
                                self.delegate?.didFailDeleteClass!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            }
                            break
                        case .failure(let error):
                            self.delegate?.didFailDeleteClass!(message: Localization("GenericError"))
                            print (error)
                            break
                        }
                    }
                }else{
                    self.deleteOnDB(classe: classe)
                }
            break
        }
    }
    
    func remove(students : [Student], toClass: Class){
        switch currentReachabilityStatus {
        case .notReachable:
            self.deletedOnDB(students: students)
            break
        case .reachableViaWiFi, .reachableViaWWAN:
            if internetProvider.sharedInstance.hasInternet{
                    let header : [String: String] = [
                        "Content-Type":"application/json",
                        "user":UserProvider.sharedInstance.uuid,
                        "idioma": self.defaults.object(forKey: "language") as! String
                    ]
                    
                    var x = [[ : ]]
                    
                    for student in students{
                        let a = ["uuid" : student.uuid]
                        x.append(a)
                    }
                    let parameters = [
                        "alumnos": x
                        
                    ]
                    print(parameters)
                    Alamofire.request("\(Constants.baseUrl)/\(Constants.alumno)/\(Constants.delete)", method: .delete, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                        debugPrint(response)
                        switch response.result{
                        case .success:
                            switch Int((response.response?.statusCode)!){
                            case 200, 201, 202:
                                self.deletedOnDB(students: students)
                                self.delegate?.didSuccessDelete!(students: students)
                                break
                            case 300...500:
                                self.delegate?.didFailDeleteStudents!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            default:
                                self.delegate?.didFailDeleteStudents!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            }
                            break
                        case .failure(let error):
                            self.delegate?.didFailDeleteStudents!(message: Localization("GenericError"))
                            print (error)
                            break
                        }
                    }
                }else{
                    self.deletedOnDB(students: students)
                }
            break
        }
    }
    
    func delete(creiterio: Criterio){
        switch currentReachabilityStatus {
        case .notReachable:
            self.deleteOnDB(criterio: creiterio)
            break
        case .reachableViaWiFi, .reachableViaWWAN:
            if internetProvider.sharedInstance.hasInternet{
                    let header : [String: String] = [
                        "Content-Type":"application/json",
                        "user":UserProvider.sharedInstance.uuid,
                        "idioma": self.defaults.object(forKey: "language") as! String
                    ]
                    
                    Alamofire.request("\(Constants.baseUrl)/\(Constants.clase)/\(creiterio.uuid!)/\(Constants.deletecriterio)", method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                        debugPrint(response)
                        switch response.result{
                        case .success:
                            switch Int((response.response?.statusCode)!){
                            case 200, 201, 202:
                                self.deleteOnDB(criterio: creiterio)
                                self.delegate?.didSuccessDelete!(criterio: creiterio)
                                break
                            case 300...500:
                                self.delegate?.didFailDeleteCriterio!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            default:
                                self.delegate?.didFailDeleteCriterio!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            }
                            break
                        case .failure(let error):
                            self.delegate?.didFailDeleteCriterio!(message: Localization("GenericError"))
                            print (error)
                            break
                        }
                    }
                }else{
                    self.deleteOnDB(criterio: creiterio)
                }
            break
        }
    }
    
    func add(period : Period, toClass : Class){
        switch currentReachabilityStatus {
        case .notReachable:
            self.createOnDB(period: period, at: toClass, isNew: false)
            break
        case .reachableViaWiFi, .reachableViaWWAN:
            if internetProvider.sharedInstance.hasInternet{
                    let date = Date()
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeStyle = DateFormatter.Style.none
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    dateFormatter.dateFormat = "y-MM-dd"
                    dateFormatter.string(from: date)
                    let startDate = dateFormatter.string(from: period.startDate.convertedDate(calendar: .current)!)
                    let endDate = dateFormatter.string(from: period.endDate.convertedDate(calendar: .current)!)
                    
                    let header : [String: String] = [
                        "Content-Type":"application/json",
                        "user":UserProvider.sharedInstance.uuid,
                        "idioma": self.defaults.object(forKey: "language") as! String
                    ]
                    let parameters : [String: Any] = [
                        "claseId":toClass.dbid,
                        "fechaInicio":startDate,
                        "fechaFin":endDate
                    ]
                    Alamofire.request("\(Constants.baseUrl)/\(Constants.clase)/\(Constants.period)", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                        debugPrint(response)
                        switch response.result{
                        case .success:
                            switch Int((response.response?.statusCode)!){
                            case 200, 201, 202:
                                let dic = (response.result.value as! NSDictionary).value(forKey: "PeriodoInfo:") as! NSDictionary
                                period.dbid = dic.value(forKey: "id") as! Int
                                period.uuid = dic.value(forKey: "uuid") as! String
                                toClass.periods.append(period)
                                self.createOnDB(period: period, at: toClass, isNew: false)
                                self.delegate?.didSuccessAddPeriodToClass!()
                                break
                            case 300...500:
                                self.delegate?.didFailAddPeriodToClass!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            default:
                                self.delegate?.didFailAddPeriodToClass!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            }
                            break
                        case .failure(let error):
                            self.delegate?.didFailAddPeriodToClass!(message: Localization("GenericError"))
                            print (error)
                            break
                        }
                    }
                }else{
                    self.createOnDB(period: period, at: toClass, isNew: false)
                }
            break
        }
    }
    
    func update(classe: Class){
        switch currentReachabilityStatus {
        case .notReachable:
            self.updateOnDB(classe: classe)
            break
        case .reachableViaWiFi, .reachableViaWWAN:
            if internetProvider.sharedInstance.hasInternet{
                    let header : [String: String] = [
                        "Content-Type":"application/json",
                        "user":UserProvider.sharedInstance.uuid,
                        "idioma": self.defaults.object(forKey: "language") as! String
                    ]
                    let parameters : [String: Any] = [
                        "nombreMateria" :classe.name,
                        "nivel" : classe.nivel,
                        "grado" : classe.grado,
                        "color" : classe.colorClass.idString,
                        "idMaestro" : UserProvider.sharedInstance.dbid
                    ]
                    Alamofire.request("\(Constants.baseUrl)/\(Constants.clase)/\(classe.uuid!)/\(Constants.classe)", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                        debugPrint(response)
                        switch response.result{
                        case .success:
                            switch Int((response.response?.statusCode)!){
                            case 200, 201, 202:
                                self.updateOnDB(classe: classe)
                                self.delegate?.didSuccessUpdate!(classe: classe)
                                break
                            case 300...500:
                                self.delegate?.didFailUpdateClass!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            default:
                                self.delegate?.didFailUpdateClass!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            }
                            break
                        case .failure(let error):
                            print (error)
                            self.delegate?.didFailUpdateClass!(message: Localization("GenericError"))
                            break
                        }
                    }
                }else{
                    self.updateOnDB(classe: classe)
                }
            break
        }
        
    }
    
    func updateProfile(password: String){
        switch currentReachabilityStatus {
        case .notReachable:
            self.delegate?.didFailChangePassword!(message: Localization("GenericError"))
            break
        case .reachableViaWiFi, .reachableViaWWAN:
            if internetProvider.sharedInstance.hasInternet{
                    let header : [String: String] = [
                        "Content-Type":"application/json",
                        "user":UserProvider.sharedInstance.uuid,
                        "idioma": self.defaults.object(forKey: "language") as! String
                    ]
                    let parameters : [String: String] = [
                        "email" : UserProvider.sharedInstance.email,
                        "password" : UserProvider.sharedInstance.password,
                        "newpassword" : password
                    ]
                    Alamofire.request("\(Constants.baseUrl)/\(Constants.users)/\(UserProvider.sharedInstance.uuid!)/\(Constants.updatePassword)", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                        debugPrint(response)
                        switch response.result{
                        case .success:
                            switch Int((response.response?.statusCode)!){
                            case 200, 201, 202:
                                UserProvider.sharedInstance.password = password
                                let defaults = UserDefaults()
                                defaults.set(password, forKey: "password")
                                defaults.synchronize()
                                self.updatePasswordOnDB(password: password)
                                self.delegate?.didSuccessChangePassword!()
                                break
                            case 300...500:
                                self.delegate?.didFailChangePassword!(message: (response.result.value as! NSDictionary).value(forKey: "observations") as! String)
                                break
                            default:
                                break
                            }
                            break
                        case .failure(let error):
                            print (error)
                            self.delegate?.didFailChangePassword!(message: Localization("GenericError"))
                            break
                        }
                    }
                }else{
                    self.delegate?.didFailChangePassword!(message: Localization("GenericError"))
                }
            break
        }
    }
    
    func deleteSinc(data : NSDictionary){
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            db = nil
        }
        
        let usersTable = Table("users")
        let classesTable = Table("classes")
        let studentsTable = Table("students")
        let periodsTable = Table("periods")
        let criteriosTable = Table("criterios")
        
        let deleteThings = data.value(forKey: "Borradas") as! NSDictionary
        for row in deleteThings.value(forKey: "maestros") as! NSArray{
            var countFunction = 0
            do{
                if db != nil{
                    countFunction = try db.scalar( usersTable.filter( Constants.uuid == row as! String).count )
                }
            }catch{
                print(error)
            }
            
            do{
                if countFunction > 0{
                    let query = usersTable.filter( Constants.uuid == row as! String)
                    try db.run(query.delete())
                }
            }catch{
                print(error)
            }
            
        }
        
        for row in deleteThings.value(forKey: "clases") as! NSArray{
            var countFunction = 0
            do{
                if db != nil{
                    countFunction = try db.scalar( classesTable.filter( Constants.uuid == row as! String).count )
                }
            }catch{
                print(error)
            }
            
            do{
                if countFunction > 0{
                    let query = classesTable.filter( Constants.uuid == row as! String)
                    try db.run(query.delete())
                }
            }catch{
                print(error)
            }
            
        }
        
        for row in deleteThings.value(forKey: "criterios") as! NSArray{
            var countFunction = 0
            do{
                if db != nil{
                    countFunction = try db.scalar( criteriosTable.filter( Constants.uuid == row as! String).count )
                }
            }catch{
                print(error)
            }
            
            do{
                if countFunction > 0{
                    let query = criteriosTable.filter( Constants.uuid == row as! String)
                    try db.run(query.delete())
                }
            }catch{
                print(error)
            }
            
        }
        for row in deleteThings.value(forKey: "alumnos") as! NSArray{
            var countFunction = 0
            do{
                if db != nil{
                    countFunction = try db.scalar( studentsTable.filter( Constants.uuid == row as! String).count )
                }
            }catch{
                print(error)
            }
            
            do{
                if countFunction > 0{
                    let query = studentsTable.filter( Constants.uuid == row as! String)
                    try db.run(query.delete())
                }
            }catch{
                print(error)
            }
            
        }
        for row in  deleteThings.value(forKey: "periodos") as! NSArray{
            var countFunction = 0
            do{
                if db != nil{
                    countFunction = try db.scalar( periodsTable.filter( Constants.uuid == row as! String).count )
                }
            }catch{
                print(error)
            }
            
            do{
                if countFunction > 0{
                    let query = periodsTable.filter( Constants.uuid == row as! String)
                    try db.run(query.delete())
                }
            }catch{
                print(error)
            }
            
        }
    }
    
    func getClasses(){
        switch currentReachabilityStatus {
        case .notReachable:
            self.getClassesByDB()
            break
        case .reachableViaWiFi, .reachableViaWWAN:
            if internetProvider.sharedInstance.hasInternet{
                    let header : [String: String] = [
                        "Content-Type":"application/json",
                        "user":UserProvider.sharedInstance.uuid,
                        "idioma": self.defaults.object(forKey: "language") as! String
                    ]
                    Alamofire.request("\(Constants.baseUrl)/\(Constants.users)/\(Constants.classes)/\(UserProvider.sharedInstance.dbid!)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                        debugPrint(response)
                        switch response.result{
                        case .success:
                            switch Int((response.response?.statusCode)!){
                            case 200, 201, 202:
                                self.deleteSinc(data: response.result.value as! NSDictionary)
                                var classess : [Class] = []
                                if let clases = (response.result.value as! NSDictionary).value(forKey: "Clases"){
                                    for clase in clases as! NSArray{
                                        print(clase as! NSDictionary)
                                        let classe = Class()
                                        classe.name = (clase as! NSDictionary).value(forKey: "claseNombre") as! String
                                        classe.dbid = (clase as! NSDictionary).value(forKey: "claseId") as! Int
                                        classe.grado = (clase as! NSDictionary).value(forKey: "claseGrado") as! Int
                                        classe.nivel = (clase as! NSDictionary).value(forKey: "claseNivel") as! String
                                        if let uuid = (clase as! NSDictionary).value(forKey: "uuid"){
                                            classe.uuid = uuid as! String
                                        }
                                        if let c = (clase as! NSDictionary).value(forKey: "claseColor"){
                                            switch "\(c)"{
                                            case "1":
                                                classe.colorClass = .red
                                                break
                                            case "2":
                                                classe.colorClass = .orange
                                                break
                                            case "3":
                                                classe.colorClass = .blue1
                                                break
                                            case "4":
                                                classe.colorClass = .pink
                                                break
                                            case "5":
                                                classe.colorClass = .purple
                                                break
                                            case "6":
                                                classe.colorClass = .yellow
                                                break
                                            case "7":
                                                classe.colorClass = .aqua
                                                break
                                            case "8":
                                                classe.colorClass = .green
                                                break
                                            case "9":
                                                classe.colorClass = .green2
                                                break
                                            case "10":
                                                classe.colorClass = .blue2
                                                break
                                            case "11":
                                                classe.colorClass = .purple2
                                                break
                                            default:
                                                break
                                            }
                                        }
                                        
                                        self.saveOrUpdate(classe: classe)
                                        
                                        
                                        if let criterios = (clase as! NSDictionary).value(forKey: "periodoCrit"){
                                            for criterio in criterios as! NSArray{
                                                print (criterio as! NSDictionary)
                                                let c = Criterio()
                                                c.name = (criterio as! NSDictionary).value(forKey: "actividad") as! String
                                                c.dbid = (criterio as! NSDictionary).value(forKey: "criterioId") as! Int
                                                c.value = (criterio as! NSDictionary).value(forKey: "valor") as! Double
                                                if let uuid = (criterio as! NSDictionary).value(forKey: "criterioUuid") {
                                                    c.uuid = uuid as! String
                                                }
                                                classe.criterios.append(c)
                                                
                                            }
                                        }
                                        
                                        self.saveOrUpdateCriteriosOn(classe: classe)
                                        
                                        if let alumnos = (clase as! NSDictionary).value(forKey: "alumnos"){
                                            for alumno in alumnos as! NSArray{
                                                print(alumno as! NSDictionary)
                                                let studnt = Student()
                                                studnt.name = (alumno as! NSDictionary).value(forKey: "nombre") as! String
                                                studnt.dbid = (alumno as! NSDictionary).value(forKey: "id") as! Int
                                                studnt.gender = (alumno as! NSDictionary).value(forKey: "genero") as! String
                                                studnt.emailTutor = (alumno as! NSDictionary).value(forKey: "mail") as! String
                                                let bd = ((alumno as! NSDictionary).value(forKey: "cumpleanos") as! String).components(separatedBy: "-")
                                                studnt.uuid = (alumno as! NSDictionary).value(forKey: "uuid") as! String
                                                if let imgUrl = (alumno as! NSDictionary).value(forKey: "pictureUrl"){
                                                    studnt.imgUrl = imgUrl as! String
                                                }
                                                print(bd)
                                                let components = NSDateComponents()
                                                components.year = Int(bd[0])!
                                                components.month = Int(bd[1])!
                                                components.day = Int(bd[2])!
                                                let bdDate = NSCalendar.current.date(from: components as DateComponents)
                                                print(bdDate!)
                                                studnt.birthdayDate = CVDate(date: bdDate!, calendar: .current)
                                                studnt.birthday = (alumno as! NSDictionary).value(forKey: "cumpleanos") as! String
                                                classe.students.append(studnt)
                                            }
                                        }
                                        
                                        self.saveOrUpdateStudentsOn(classe: classe)
                                        
                                        
                                        if let periods = (clase as! NSDictionary).value(forKey: "periodos"){
                                            for p in periods as! NSArray{
                                                print(p)
                                                let period = Period()
                                                period.dbid = (p as! NSDictionary).value(forKey: "id") as! Int
                                                period.uuid = (p as! NSDictionary).value(forKey: "uuid") as! String
                                                let splitEndDate = ((p as! NSDictionary).value(forKey: "fechaFin") as! String).components(separatedBy: "-")
                                                print(splitEndDate)
                                                let componentsEnd = NSDateComponents()
                                                componentsEnd.year = Int(splitEndDate[0])!
                                                componentsEnd.month = Int(splitEndDate[1])!
                                                componentsEnd.day = Int(splitEndDate[2])!
                                                let endDate = NSCalendar.current.date(from: componentsEnd as DateComponents)
                                                print(endDate!)
                                                
                                                let splitDate = ((p as! NSDictionary).value(forKey: "fechaInicio") as! String).components(separatedBy: "-")
                                                print(splitDate)
                                                let components = NSDateComponents()
                                                components.year = Int(splitDate[0])!
                                                components.month = Int(splitDate[1])!
                                                components.day = Int(splitDate[2])!
                                                let startDate = NSCalendar.current.date(from: components as DateComponents)
                                                print(startDate!)
                                                
                                                period.startDate = CVDate(date: startDate!, calendar: .current)
                                                period.endDate = CVDate(date: endDate!, calendar: .current)
                                                print(period)
                                                period.classId = classe.dbid
                                                classe.periods.append(period)
                                            }
                                        }
                                        
                                        self.saveOrUpdatePeriodsOn(classe: classe)
                                        
                                        classess.append(classe)
                                    }
                                }
                                //                        self.delegate?.didSuccessGet!(classes: classess)
                                self.getClassesByDB()
                                break
                            case 300...500:
                                let users = Table("users")
                                self.delegate?.didFailGetClasses!(statusCode: (response.response?.statusCode)!, message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            default:
                                self.delegate?.didFailGetClasses!(statusCode: (response.response?.statusCode)!, message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            }
                            break
                        case .failure(let error):
                            self.delegate?.didFailGetClasses!(statusCode: 500, message: Localization("GenericError"))
                            print(error)
                            break
                        }
                    }
                }else{
                    self.getClassesByDB()
                }
            break
        }
    }
    
    
    func restorePassWordTo(email: String!){
        switch currentReachabilityStatus{
        case .notReachable:
            self.delegate?.didFailSendEmailToRestorePassword!(message: "No internet connection detected")
            break
        case .reachableViaWiFi, .reachableViaWWAN:
            if internetProvider.sharedInstance.hasInternet{
                    let header : [String: String] = [
                        "Content-Type":"application/json",
                        "idioma": self.defaults.object(forKey: "language") as! String
                    ]
                    let parameters =  [
                        "email" : email
                    ]
                    Alamofire.request("\(Constants.baseUrl)/\(Constants.users)/\(Constants.restorepass)", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                        debugPrint(response)
                        switch response.result{
                        case .success:
                            switch Int((response.response?.statusCode)!){
                            case 200, 201, 202:
                                self.delegate?.didSuccessSendMailToRestorePassword!()
                                break
                            case 300...500:
                                self.delegate?.didFailSendEmailToRestorePassword!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String )
                                break
                            default:
                                self.delegate?.didFailSendEmailToRestorePassword!(message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            }
                            break
                        case .failure(let error):
                            print(error)
                            self.delegate?.didFailSendEmailToRestorePassword!(message: Localization("GenericError"))
                            break
                        }
                    }
                }else{
                    self.delegate?.didFailSendEmailToRestorePassword!(message: "No internet connection detected")
                }
            break
        }
        
        
    }
    
    
    func register(student: Student?, at classe: Class, students: [Student]?){
        switch currentReachabilityStatus {
        case .notReachable:
            if student != nil{
                self.create(student: student!, at: classe)
            }else{
                for s in students!{
                    let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
                    DispatchQueue.main.asyncAfter(deadline: when) {
                        self.create(student: s, at: classe)
                    }
                }
            }
            break
        case .reachableViaWiFi, .reachableViaWWAN:
            if internetProvider.sharedInstance.hasInternet{
                    var parameters : [String: Any] = [:]
                    if let s = student{
                        let date = Date()
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.timeStyle = DateFormatter.Style.none
                        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                        dateFormatter.dateFormat = "y-MM-dd"
                        dateFormatter.string(from: date)
                        let bDate = dateFormatter.string(from: (student?.birthdayDate.convertedDate(calendar: .current)!)!)  // "2010-01-27"
                        var x = [[ : ]]
                        x.removeAll()
                        let a = [
                            "nombre": s.name,
                            "genero": s.gender == Localization("man") ? "M" : "F",
                            "cumpleanos":bDate,
                            "mail": s.emailTutor,
                            "claseId": classe.dbid
                            ] as [String : Any]
                        x.append(a)
                        parameters = [
                            "alumnos": x
                        ]
                    }else if let ss = students{
                        var x = [[ : ]]
                        x.removeAll()
                        for student in ss{
                            let date = Date()
                            
                            let dateFormatter = DateFormatter()
                            dateFormatter.timeStyle = DateFormatter.Style.none
                            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                            dateFormatter.dateFormat = "y-MM-dd"
                            dateFormatter.string(from: date)
                            let bDate = dateFormatter.string(from: (student.birthdayDate.convertedDate(calendar: .current)!))  // "2010-01-27"
                            let a = [
                                "nombre": student.name,
                                "genero": student.gender == Localization("man") ? "M" : "F",
                                "cumpleanos":bDate,
                                "mail": student.emailTutor,
                                "claseId": classe.dbid
                                ] as [String : Any]
                            x.append(a)
                        }
                        parameters = [
                            "alumnos": x
                        ]
                    }
                    
                    let header : [String: String] = [
                        "Content-Type":"application/json",
                        "user":UserProvider.sharedInstance.uuid,
                        "idioma": self.defaults.object(forKey: "language") as! String
                    ]
                    print(parameters)
                    Alamofire.request("\(Constants.baseUrl)/\(Constants.alumno)/\(Constants.register)", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                        debugPrint(response)
                        switch response.result{
                        case .success:
                            switch Int((response.response?.statusCode)!){
                            case 200, 201, 202:
                                if let s = student{
                                    s.uuid = (((response.result.value as! NSDictionary).value(forKey: "Success") as! NSArray)[0] as! NSDictionary).value(forKey: "uuid") as! String
                                    s.claseId = (((response.result.value as! NSDictionary).value(forKey: "Success") as! NSArray)[0] as! NSDictionary).value(forKey: "claseId") as! Int
                                }else{
                                    let auxClass = classe.copy() as! Class
                                    classe.students.removeAll()
                                    for s in  (response.result.value as! NSDictionary).value(forKey: "Success") as! NSArray{
                                        let st = Student()
                                        st.uuid = (s as! NSDictionary).value(forKey: "uuid") as! String
                                        st.claseId = (s as! NSDictionary).value(forKey: "claseId") as! Int
                                        st.birthday = (s as! NSDictionary).value(forKey: "cumpleanos") as! String
                                        let bd = st.birthday.components(separatedBy: "-")
                                        let components = NSDateComponents()
                                        components.year = Int(bd[0])!
                                        components.month = Int(bd[1])!
                                        components.day = Int(bd[2])!
                                        let bdDate = NSCalendar.current.date(from: components as DateComponents)
                                        print(bdDate!)
                                        st.birthdayDate = CVDate(date: bdDate!, calendar: .current)
                                        st.gender = (s as! NSDictionary).value(forKey: "genero") as! String
                                        st.dbid = (s as! NSDictionary).value(forKey: "id") as! Int
                                        st.emailTutor = (s as! NSDictionary).value(forKey: "mail") as! String
                                        st.name = (s as! NSDictionary).value(forKey: "nombre") as! String
                                        let auxS = auxClass.students.filter{$0.name == st.name}.first
                                        st.img = auxS?.img
                                        classe.students.append(st)
                                    }
                                    self.delegate?.didSuccessRegister!(student: Student(), classe: classe)
                                }
                                
                                if let a = (((response.result.value as! NSDictionary).value(forKey: "Success") as! NSArray)[0] as! NSDictionary).value(forKey: "id"){
                                    student?.dbid = a  as! Int
                                }else{
                                    student?.dbid = 0
                                }
                                
                                if let s = student{
                                    self.create(student: s, at: classe)
                                    self.delegate?.didSuccessRegister!(student: s, classe: classe)
                                }else{
                                    for s in classe.students{
                                        self.create(student: s, at: classe)
                                    }
                                    self.delegate?.didSuccessRegister!(student: Student(), classe: classe)
                                }
                                break
                            case 300...500:
                                self.delegate?.didFailRegisterStudent!(statusCode: (response.response?.statusCode)!, message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            default:
                                self.delegate?.didFailRegisterStudent!(statusCode: (response.response?.statusCode)!, message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            }
                            break
                        case .failure(let error):
                            self.delegate?.didFailRegisterStudent!(statusCode: 500, message:Localization("GenericError"))
                            print(error)
                            break
                        }
                    }
                }else{
                    if student != nil{
                        self.create(student: student!, at: classe)
                    }else{
                        for s in students!{
                            let when = DispatchTime.now() + 1 // change 2 to desired number of seconds
                            DispatchQueue.main.asyncAfter(deadline: when) {
                                self.create(student: s, at: classe)
                            }
                        }
                    }
                }
            break
        }
    }
    
    
    func register(classe: Class, for teacher: Teacher){
        switch currentReachabilityStatus {
        case .notReachable:
            self.creatOnDB(classe: classe)
            break
        case .reachableViaWiFi, .reachableViaWWAN:
            if internetProvider.sharedInstance.hasInternet{
                    print(teacher)
                    print(classe)
                    let date = Date()
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeStyle = DateFormatter.Style.none
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    dateFormatter.dateFormat = "y-MM-dd"
                    dateFormatter.string(from: date)
                    var startDate = dateFormatter.string(from: NSDate() as Date)
                    var endDate = dateFormatter.string(from: NSDate() as Date)
                    if classe.periods.count > 0{
                        startDate = dateFormatter.string(from: classe.periods[0].startDate.convertedDate(calendar: .current)!)
                        endDate = dateFormatter.string(from: classe.periods[0].endDate.convertedDate(calendar: .current)!)
                    }
                    
                    let parameters : [String: Any] = [
                        "nombreMateria": classe.name,
                        "nivel": classe.nivel,
                        "grado":classe.grado,
                        "color": classe.colorClass.idString,
                        "idMaestro": teacher.dbid,
                        "fechaInicio" : startDate,
                        "fechaFin" : endDate
                    ]
                    
                    let header : [String: String] = [
                        "Content-Type":"application/json",
                        "user":UserProvider.sharedInstance.uuid,
                        "idioma": self.defaults.object(forKey: "language") as! String
                    ]
                    print(parameters)
                    Alamofire.request("\(Constants.baseUrl)/\(Constants.clase)/\(Constants.register)", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                        debugPrint(response)
                        switch response.result{
                        case .success:
                            switch Int((response.response?.statusCode)!){
                            case 200, 201, 202:
                                classe.periods.first?.dbid = (response.result.value as! NSDictionary).value(forKey: "periodoId") as! Int
                                classe.periods.first?.uuid = (response.result.value as! NSDictionary).value(forKey: "periodoUuid") as! String
                                classe.dbid = ((((response.result.value as! NSDictionary).value(forKey: "InfoCriterios") as! NSDictionary).value(forKey: "Success") as! NSArray)[0] as! NSDictionary).value(forKey: "claseId") as! Int
                                classe.uuid = (response.result.value as! NSDictionary).value(forKey: "uuid") as! String
                                
                                let crit = ((((response.result.value as! NSDictionary).value(forKey: "InfoCriterios") as! NSDictionary).value(forKey: "Success") as! NSArray)[0] as! NSDictionary).value(forKey: "criterioInfo")
                                
                                classe.dbid = ((((response.result.value as! NSDictionary).value(forKey: "InfoCriterios") as! NSDictionary).value(forKey: "Success") as! NSArray)[0] as! NSDictionary).value(forKey: "claseId") as! Int
                                for c in crit as! NSArray{
                                    let criterio = Criterio()
                                    criterio.name = (c as! NSDictionary).value(forKey: "actividad") as! String
                                    criterio.value = (c as! NSDictionary).value(forKey: "valor") as! Double
                                    criterio.uuid = (c as! NSDictionary).value(forKey: "criterioUuid") as! String
                                    print(c)
                                    classe.criterios.append(criterio)
                                }
                                self.delegate?.didSuccessRegister!(classe: classe)
                                self.creatOnDB(classe: classe)
                                break
                            case 300...500:
                                self.delegate?.didFailRegisterClass!(statusCode: (response.response?.statusCode)!, message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            default:
                                self.delegate?.didFailRegisterClass!(statusCode: (response.response?.statusCode)!, message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            }
                            break
                        case .failure(let error):
                            print(error)
                            self.delegate?.didFailRegisterClass!(statusCode: 50, message: Localization("GenericError"))
                            break
                        }
                    }
                }else{
                    self.creatOnDB(classe: classe)
                }
        }
    }
    
    
    func login(teacher: Teacher){
        let parameters : [String: String] = [
            "email": teacher.email,
            "password": teacher.password
        ]
        
        let header : [String: String] = [
            "Content-Type":"application/json",
            "idioma": defaults.object(forKey: "language") as! String
        ]
        print(parameters)
        switch currentReachabilityStatus {
            case .notReachable:
            self.loginWithOutInternet(teacher: teacher, message: Localization("MailOrPasswordErrorAlert"))
            break
        case .reachableViaWiFi, .reachableViaWWAN:
//            let manager = NetworkReachabilityManager(host: "www.google.com")
//            manager?.listener = { status in
//                
//                print("Network Status Changed: \(status)")
//                print("network reachable \(manager!.isReachable)")
//            }
            if internetProvider.sharedInstance.hasInternet{
                    Alamofire.request("\(Constants.baseUrl)/\(Constants.users)/\(Constants.login)", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                        debugPrint(response)
                        switch response.result{
                        case .success:
                            switch Int((response.response?.statusCode)!){
                            case 200, 201, 202:
                                teacher.dbid = (response.result.value as! NSDictionary).value(forKey: "id") as! Int
                                teacher.uuid = (response.result.value as! NSDictionary).value(forKey: "uuid") as! String
                                teacher.name = (response.result.value as! NSDictionary).value(forKey: "name") as! String
                                teacher.lastName = (response.result.value as! NSDictionary).value(forKey: "lastName") as! String
                                teacher.email = (response.result.value as! NSDictionary).value(forKey: "email") as! String
                                if let img = (response.value as! NSDictionary).value(forKey: "pictureURL"){
                                    teacher.imageUrl = img as! String
                                }
                                teacher.userName = (response.result.value as! NSDictionary).value(forKey: "alias") as! String
                                teacher.schoolName = (response.result.value as! NSDictionary).value(forKey: "escuela") as! String
                                
                                UserProvider.sharedInstance.token = teacher.token
                                UserProvider.sharedInstance.dbid = teacher.dbid
                                UserProvider.sharedInstance.imageUrl = teacher.imageUrl
                                UserProvider.sharedInstance.uuid = teacher.imageUrl
                                UserProvider.sharedInstance.email = teacher.email
                                UserProvider.sharedInstance.name = teacher.name
                                UserProvider.sharedInstance.lastName = teacher.lastName
                                UserProvider.sharedInstance.uuid = teacher.uuid
                                UserProvider.sharedInstance.schoolName = teacher.schoolName
                                UserProvider.sharedInstance.userName = teacher.userName
                                UserProvider.sharedInstance.password = teacher.password
                                UserProvider.sharedInstance.classes = []
                                UserProvider.sharedInstance.isnew = teacher.isnew
                                self.saveOrUpdate(teacher: teacher)

                                self.loginWithOutInternet(teacher
                                    : teacher, message: Localization("MailOrPasswordErrorAlert"))

                                break
                            case 300...500:
                                self.loginWithOutInternet(teacher: teacher, message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            default:
                                self.loginWithOutInternet(teacher: teacher, message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            }
                            break
                        case .failure(let error):
                            self.loginWithOutInternet(teacher: teacher, message: Localization("MailOrPasswordErrorAlert"))
                            print(error)
                            break
                        }
                    }
                }else{
                    self.loginWithOutInternet(teacher: teacher, message: Localization("MailOrPasswordErrorAlert"))
                }
            break
        }
    }
    
    func register(teacher: Teacher){
        
        switch currentReachabilityStatus {
        case .notReachable:
            teacher.isnew = true
            self.creatOnDB(teacher: teacher)
            break
        case .reachableViaWiFi, .reachableViaWWAN:
            if internetProvider.sharedInstance.hasInternet{
                    let parameters  = [
                        "email":teacher.email,
                        "password":teacher.password,
                        "name":teacher.name,
                        "lastName": teacher.lastName,
                        "alias": teacher.userName,
                        "escuela": teacher.schoolName
                        ] as [String : Any]
                    let header = [
                        "Content-Type":"application/json",
                        "idioma": self.defaults.object(forKey: "language") as! String
                    ]
                    print(parameters)
                    Alamofire.request("\(Constants.baseUrl)/\(Constants.users)/\(Constants.register)", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                        debugPrint(response)
                        switch response.result{
                        case .success:
                            switch Int((response.response?.statusCode)!){
                            case 200, 201, 202:
                                UserProvider.sharedInstance.email = teacher.email
                                UserProvider.sharedInstance.userName = teacher.userName
                                UserProvider.sharedInstance.name = teacher.name
                                UserProvider.sharedInstance.lastName = teacher.lastName
                                UserProvider.sharedInstance.password = teacher.password
                                UserProvider.sharedInstance.schoolName = teacher.schoolName
                                UserProvider.sharedInstance.uuid = (((response.result.value as! NSDictionary).value(forKey: "Success")as! NSArray)[0] as! NSDictionary).value(forKey: "uuid") as! String
                                UserProvider.sharedInstance.dbid = (((response.result.value as! NSDictionary).value(forKey: "Success")as! NSArray)[0] as! NSDictionary).value(forKey: "id") as! Int
                                UserProvider.sharedInstance.classes = []
                                UserProvider.sharedInstance.localPath = teacher.localPath
                                teacher.isnew = true
                                UserProvider.sharedInstance.image = teacher.image
                                teacher.uuid = UserProvider.sharedInstance.uuid
                                UserProvider.sharedInstance.isnew = teacher.isnew
                                self.saveOrUpdate(teacher: UserProvider.sharedInstance)
                                self.delegate?.didSuccessRegister!(teacher:  UserProvider.sharedInstance)
                                break
                            case 300...999:
                                self.delegate?.didFailRegisterTeacher!(statusCode: (response.response?.statusCode)!, message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            default:
                                self.delegate?.didFailRegisterTeacher!(statusCode: (response.response?.statusCode)!, message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                                break
                            }
                            break
                        case .failure(let error):
                            self.delegate?.didFailRegisterTeacher!(statusCode: 500, message: Localization("GenericError"))
                            print(error)
                            break
                        }
                    }
                }else{
                    teacher.isnew = true
                    self.creatOnDB(teacher: teacher)
                }
            break
        }
    }
    
    
    
    //DB
    
    func loginWithOutInternet(teacher: Teacher, message: String?){
        let users = Table("users")
        do{
            let defaults = UserDefaults()
            let db = try Connection("\(Constants.path)/db.sqlite3")
            for user in try db.prepare(users) {
                if user.get(Constants.email) == teacher.email{
                    if user.get(Constants.password) == teacher.password{
                        print("Yes")
                        teacher.dbid = user.get(Constants.id)
                        teacher.uuid = user.get(Constants.uuid)
                        teacher.name =  user.get(Constants.name)
                        teacher.lastName = user.get(Constants.lastName)
                        teacher.email = user.get(Constants.email)
                        teacher.userName = user.get(Constants.username)
                        teacher.schoolName = user.get(Constants.schoolName)
                        teacher.userName = user.get(Constants.username)
                        teacher.localID = user.get(Constants.localId)
                        teacher.password = user.get(Constants.password)
                        teacher.updated = user.get(Constants.updated)
                        teacher.imageUrl = user.get(Constants.imageStudent)
                        UserProvider.sharedInstance.token = teacher.token
                        UserProvider.sharedInstance.dbid = teacher.dbid
                        UserProvider.sharedInstance.imageUrl = teacher.imageUrl
                        UserProvider.sharedInstance.uuid = teacher.imageUrl
                        UserProvider.sharedInstance.email = teacher.email
                        UserProvider.sharedInstance.name = teacher.name
                        UserProvider.sharedInstance.lastName = teacher.lastName
                        UserProvider.sharedInstance.uuid = teacher.uuid
                        UserProvider.sharedInstance.schoolName = teacher.schoolName
                        UserProvider.sharedInstance.userName = teacher.userName
                        UserProvider.sharedInstance.classes = []
                        UserProvider.sharedInstance.localID = teacher.localID
                        UserProvider.sharedInstance.password = teacher.password
                        UserProvider.sharedInstance.updated = teacher.updated
                        if !teacher.isnew{
                            self.delegate?.didSuccessLogin!(teacher:  teacher)
                            return
                        }else{
                            self.delegate?.didSuccessRegister!(teacher: teacher)
                            return
                        }
                    }
                }
            }
            self.delegate?.didFailSuccessLogin!(statusCode: 500, message: message)
        }catch{
            print(error)
            self.delegate?.didFailSuccessLogin!(statusCode: 500, message: message)
        }
        
    }
    
    func saveOrUpdate(classe: Class){
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            db = nil
        }
        
        let classesTable = Table("classes")
        
        var countFunction = 0
        do{
            if db != nil{
                countFunction = try db.scalar( classesTable.filter( Constants.uuid == classe.uuid ).count )
            }
        }catch{
            print(error)
        }
        
        
        if countFunction > 0{
            do {
                if db != nil{
                    let query = classesTable.filter( Constants.uuid == classe.uuid)
                    try db.run(query.update(Constants.id <- classe.dbid, Constants.name <- classe.name, Constants.updated <- true, Constants.uuid <- classe.uuid, Constants.claseColor <- classe.colorClass.idString, Constants.nivel <- classe.nivel, Constants.grado <- classe.grado, Constants.teacherId <- UserProvider.sharedInstance.dbid))
                }
                print("update")
                self.delegate?.didSuccessRegister!(classe: classe)
            } catch {
                print(error)
            }
        }else{
            do {
                if db != nil{
                    
                    try db.run(classesTable.insert(Constants.id <- classe.dbid, Constants.name <- classe.name, Constants.updated <- true, Constants.uuid <- classe.uuid, Constants.claseColor <- classe.colorClass.idString, Constants.nivel <- classe.nivel, Constants.grado <- classe.grado, Constants.teacherId <- UserProvider.sharedInstance.dbid, Constants.tUuid <- UserProvider.sharedInstance.uuid))
                }
                print("insert")
                self.delegate?.didFailRegisterClass!(statusCode: 500, message: "")
            } catch {
                print(error)
            }
        }
        
    }
    
    func saveOrUpdate(teacher: Teacher){
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            db = nil
        }
        
        let users = Table("users")
        
        var countFunction = 0
        do{
            if db != nil{
                countFunction = try db.scalar( users.filter( Constants.email == UserProvider.sharedInstance.email ).count )
            }
        }catch{
            print(error)
        }
        
        
        if countFunction > 0{
            do {
                if db != nil{
                    let query = users.filter( Constants.email == UserProvider.sharedInstance.email)
                    try db.run(query.update(Constants.id <- teacher.dbid, Constants.name <- teacher.name, Constants.password <- teacher.password, Constants.email <- teacher.email, Constants.lastName <- teacher.lastName, Constants.username <- teacher.userName, Constants.schoolName <- teacher.schoolName ,Constants.uuid <- teacher.uuid, Constants.updated <- true, Constants.imageStudent <- teacher.imageUrl != nil ? teacher.imageUrl : ""))
                    UserProvider.sharedInstance.updated = true
                }
                print("update")
            } catch {
                print(error)
            }
        }else{
            do {
                if db != nil{
                    try db.run(users.insert(Constants.id <- teacher.dbid, Constants.name <- teacher.name, Constants.password <- teacher.password, Constants.email <- teacher.email, Constants.lastName <- teacher.lastName, Constants.username <- teacher.userName, Constants.schoolName <- teacher.schoolName ,Constants.uuid <- teacher.uuid, Constants.updated <- true, Constants.imageStudent <- teacher.imageUrl != nil ? teacher.imageUrl : ""))
                    UserProvider.sharedInstance.updated = true
                }
                print("insert")
            } catch {
                print(error)
            }
        }
        
    }
    
    func saveOrUpdateCriteriosOn(classe: Class){
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            db = nil
        }
        let criterios = Table("criterios")
        
        for c in classe.criterios{
            
            var countFunction = 0
            do{
                if db != nil{
                    if c.uuid != nil{
                        let x = c.localId != nil ? "\(c.localId!)" : ""
                        countFunction = try db.scalar( criterios.filter( Constants.uuid == c.uuid || Constants.uuid == x).count )
                    }
                }
            }catch{
                print(error)
                self.delegate?.didFailUpdateCriteriosOnClasse!(message: Localization("GenericError"))
            }
            
            
            if countFunction > 0{
                do {
                    if db != nil{
                        let x = c.localId != nil ? "\(c.localId!)" : ""
                        let query = criterios.filter( Constants.uuid == c.uuid || Constants.uuid == x )
                        classe.dbid = classe.dbid != nil ? classe.dbid : 0
                        try db.run(query.update(Constants.id <- c.dbid, Constants.name <- c.name, Constants.valueC <- c.value, Constants.uuid <- c.uuid, Constants.idClasse <- classe.dbid, Constants.cUuid <- classe.uuid))
                    }
                    print("update")
                    self.delegate?.didSuccessUpdateCriteriosOn!(classe: classe)
                } catch {
                    print(error)
                    self.delegate?.didFailUpdateCriteriosOnClasse!(message: Localization("GenericError"))
                }
            }else{
                do {
                    if db != nil{
                        if c.uuid != nil{
                            try db.run(criterios.insert(Constants.id <- c.dbid != nil ? c.dbid : 0,Constants.name <- c.name, Constants.valueC <- c.value, Constants.uuid <- c.uuid, Constants.idClasse <- classe.dbid, Constants.updated <- true, Constants.cUuid <- classe.uuid))
                        }else{
                            try db.run(criterios.insert(Constants.id <- 0, Constants.name <- c.name, Constants.valueC <- c.value, Constants.uuid <- "0", Constants.cUuid <- classe.uuid, Constants.updated <- true))
                            do{
                                let stmt = try db.prepare("SELECT * FROM criterios where uuid == 0")
                                
                                for data in stmt {
                                    print (data[5])
                                    let s = "\(data[5]!)"
                                    let alice = criterios.filter(Constants.localId == NSString(string: s).integerValue)
                                    if let x = c.uuid, x != ""{
                                        try db.run(alice.update(Constants.uuid <- x, Constants.updated <- true))
                                    }else{
                                        try db.run(alice.update(Constants.uuid <- "\(data[5]!)", Constants.updated <- false))
                                        c.uuid = "\(data[5]!)"
                                    }
                                    c.localId = NSString(string: s).integerValue
                                }
                            }catch{
                                print(error)
                                self.delegate?.didFailUpdateCriteriosOnClasse!(message: Localization("GenericError"))
                            }
                            
                            
                        }
                        
                    }
                    print("insert")
                    self.delegate?.didSuccessUpdateCriteriosOn!(classe: classe)
                } catch {
                    print(error)
                    self.delegate?.didFailUpdateCriteriosOnClasse!(message: Localization("GenericError"))
                }
            }
        }
        
    }
    
    
    func saveOrUpdateStudentsOn(classe: Class){
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            db = nil
        }
        let students = Table("students")
        
        for student in classe.students{
            
            var countFunction = 0
            do{
                if db != nil{
                    countFunction = try db.scalar( students.filter( Constants.uuid == student.uuid ).count )
                }
            }catch{
                print(error)
                self.delegate?.didSuccessRegister!(student: student, classe: classe)
            }
            
            
            if countFunction > 0{
                do {
                    if db != nil{
                        let query = students.filter( Constants.uuid == student.uuid)
                        let dbDate = student.birthdayDate.convertedDate(calendar: .current)
                        try db.run(query.update(Constants.id <- student.dbid, Constants.uuid <- student.uuid, Constants.name <- student.name, Constants.birthday <- dbDate!, Constants.email <- student.emailTutor, Constants.gender <- student.gender, Constants.idClasse <- classe.dbid, Constants.updated <- true, Constants.cUuid <- classe.uuid, Constants.imageStudent <- student.imgUrl != nil ? student.imgUrl : ""))
                    }
                    print("update")
                    self.delegate?.didSuccessUpdate!(student: student)
                } catch {
                    print(error)
                }
            }else{
                do {
                    if db != nil{
                        let dbDate = student.birthdayDate.convertedDate(calendar: .current)
                        try db.run(students.insert(Constants.id <- student.dbid, Constants.uuid <- student.uuid, Constants.name <- student.name, Constants.birthday <- dbDate!, Constants.email <- student.emailTutor, Constants.gender <- student.gender, Constants.idClasse <- classe.dbid, Constants.updated <- true,  Constants.cUuid <- classe.uuid, Constants.imageStudent <- student.imgUrl != nil ? student.imgUrl : ""))
                    }
                    print("insert")
                    self.delegate?.didSuccessRegister!(student: student, classe: classe)
                } catch {
                    print(error)
                    self.delegate?.didSuccessRegister!(student: student, classe: classe)
                }
            }
        }
        
        
    }
    
    
    func saveOrUpdatePeriodsOn(classe: Class){
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            db = nil
        }
        let periods = Table("periods")
        
        for period in classe.periods{
            
            var countFunction = 0
            do{
                if db != nil{
                    countFunction = try db.scalar( periods.filter( Constants.uuid == period.uuid ).count )
                }
            }catch{
                print(error)
            }
            
            
            if countFunction > 0{
                do {
                    if db != nil{
                        let query = periods.filter( Constants.uuid == period.uuid)
                        let startDate = period.startDate.convertedDate(calendar: .current)
                        let endDate = period.endDate.convertedDate(calendar: .current)
                        try db.run(query.update(Constants.id <- period.dbid, Constants.startDate <- startDate!, Constants.endDate <- endDate!, Constants.idClasse <- period.classId, Constants.updated <- true, Constants.uuid <- period.uuid))
                    }
                    print("update")
                } catch {
                    print(error)
                }
            }else{
                do {
                    if db != nil{
                        let startDate = period.startDate.convertedDate(calendar: .current)
                        let endDate = period.endDate.convertedDate(calendar: .current)
                        try db.run(periods.insert(Constants.id <- period.dbid, Constants.startDate <- startDate!, Constants.endDate <- endDate!, Constants.idClasse <- period.classId, Constants.cUuid <- classe.uuid, Constants.updated <- true, Constants.uuid <- period.uuid, Constants.notifications <- period.notifications))
                    }
                    print("insert")
                } catch {
                    print(error)
                }
            }
        }
        
    }
    
    func getClassesByDB(){
        let classes = Table("classes")
        let students = Table("students")
        let periods = Table("periods")
        let criterios = Table("criterios")
        var classess : [Class] = []
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print(error)
            db = nil
        }
        do{
            if db != nil{
                let queryClasses = classes.filter(Constants.tUuid == UserProvider.sharedInstance.uuid)
                for clss in try db.prepare(queryClasses) {
                    print(clss.get(Constants.id))
                    print(clss.get(Constants.name))
                    let classe = Class()
                    classe.name = clss.get(Constants.name)
                    classe.dbid = clss.get(Constants.id)
                    classe.uuid = clss.get(Constants.uuid)
                    classe.grado = clss.get(Constants.grado)
                    classe.nivel = clss.get(Constants.nivel)
                    classe.updated = clss.get(Constants.updated)
                    switch clss.get(Constants.claseColor){
                    case "1":
                        classe.colorClass = .red
                        break
                    case "2":
                        classe.colorClass = .orange
                        break
                    case "3":
                        classe.colorClass = .blue1
                        break
                    case "4":
                        classe.colorClass = .pink
                        break
                    case "5":
                        classe.colorClass = .purple
                        break
                    case "6":
                        classe.colorClass = .yellow
                        break
                    case "7":
                        classe.colorClass = .aqua
                        break
                    case "8":
                        classe.colorClass = .green
                        break
                    case "9":
                        classe.colorClass = .green2
                        break
                    case "10":
                        classe.colorClass = .blue2
                        break
                    case "11":
                        classe.colorClass = .purple2
                        break
                    default:
                        break
                    }
                    if clss.get(Constants.id) == 0{
                        let query = students.filter(Constants.cUuid == clss.get(Constants.uuid))
                        do{
                            for student in try db.prepare(query) {
                                print(student)
                                let studnt = Student()
                                studnt.updated = student.get(Constants.updated)
                                studnt.dbid = student.get(Constants.id)
                                studnt.uuid = student.get(Constants.uuid)
                                studnt.name = student.get(Constants.name)
                                studnt.birthdayDate = CVDate(date: student.get(Constants.birthday), calendar: .current)
                                studnt.emailTutor = student.get(Constants.email)
                                studnt.gender = student.get(Constants.gender)
                                studnt.claseId = student.get(Constants.idClasse)
                                studnt.localId = student.get(Constants.localId)
                                classe.students.append(studnt)
                            }
                        }catch{
                            print(error)
                        }
                    }else{
                        let query = students.filter(Constants.cUuid == clss.get(Constants.uuid))
                        do{
                            for student in try db.prepare(query) {
                                print(student)
                                let studnt = Student()
                                studnt.updated = student.get(Constants.updated)
                                studnt.dbid = student.get(Constants.id)
                                studnt.uuid = student.get(Constants.uuid)
                                studnt.name = student.get(Constants.name)
                                studnt.birthdayDate = CVDate(date: student.get(Constants.birthday), calendar: .current)
                                studnt.emailTutor = student.get(Constants.email)
                                studnt.gender = student.get(Constants.gender)
                                studnt.claseId = student.get(Constants.idClasse)
                                studnt.localId = student.get(Constants.localId)
                                studnt.imgUrl = student.get(Constants.imageStudent)
                                let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
                                let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
                                let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
                                if let dirPath          = paths.first
                                {
                                    let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("\(studnt.uuid!).jpg")
                                    let image    = UIImage(contentsOfFile: imageURL.path)
                                    if image != nil{
                                        studnt.img = image
                                    }else{
                                        studnt.img = nil
                                    }
                                }else{
                                    studnt.img = nil
                                }
                                
                                // Do whatever you want with the image
                                
                                classe.students.append(studnt)
                            }
                        }catch{
                            print(error)
                        }
                    }
                    
                    
                    let query = criterios.filter(Constants.cUuid == clss.get(Constants.uuid))
                    do{
                        for row in try db.prepare(query) {
                            let c = Criterio()
                            c.updated = row.get(Constants.updated)
                            c.dbid = row.get(Constants.id)
                            c.uuid = row.get(Constants.uuid)
                            c.name = row.get(Constants.name)
                            c.value = row.get(Constants.valueC)
                            c.localId = row.get(Constants.localId)
                            classe.criterios.append(c)
                        }
                    }catch{
                        print(error)
                    }
                    
                    
                    let queryPeriods = periods.filter(Constants.cUuid == clss.get(Constants.uuid))
                    do{
                        for p in try db.prepare(queryPeriods) {
                            print(p)
                            let period = Period()
                            period.updated = p.get(Constants.updated)
                            period.dbid = p.get(Constants.id)
                            period.startDate = CVDate(date: p.get(Constants.startDate), calendar: .current)
                            period.endDate = CVDate(date: p.get(Constants.endDate), calendar: .current)
                            period.classId = p.get(Constants.idClasse)
                            period.uuid = p.get(Constants.uuid)
                            period.localId = p.get(Constants.localId)
                            period.notifications = p.get(Constants.notifications)
                            classe.periods.append(period)
                        }
                    }catch{
                        print(error)
                    }
                    classe.localId = clss.get(Constants.localId)
                    classess.append(classe)
                }
                UserProvider.sharedInstance.classes.removeAll()
                UserProvider.sharedInstance.classes.append(contentsOf: classess)
                self.delegate?.didSuccessGet!(classes: classess)
            }
        }catch{
            print(error)
        }
    }
    
    func creatOnDB(teacher: Teacher){
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            db = nil
        }
        
        let users = Table("users")
        
        var countFunction = 0
        do{
            if db != nil{
                countFunction = try db.scalar( users.filter( Constants.email == teacher.email ).count )
            }
        }catch{
            print(error)
        }
        
        
        if countFunction > 0{
            self.delegate?.userAlreadyExists!()
        }else{
            do {
                if db != nil{
                    try db.run(users.insert(Constants.name <- teacher.name, Constants.password <- teacher.password, Constants.email <- teacher.email, Constants.lastName <- teacher.lastName, Constants.username <- teacher.userName, Constants.schoolName <- teacher.schoolName , Constants.updated <- false))
                }
                do{
                    let query = users.filter(Constants.email == teacher.email)
                    for user in try db.prepare(query) {
                        print(user.get(Constants.name))
                        do{
                            try db.run(query.update(Constants.uuid <- "\(user.get(Constants.localId))"))
                        }catch{
                            print(error)
                        }
                    }
                }catch{
                    print(error)
                }
                print("insert")
                self.loginWithOutInternet(teacher: teacher, message: Localization("MailOrPasswordErrorAlert"))
            } catch {
                print(error)
            }
        }
    }
    
    
    
    
    func creatOnDB(classe: Class){
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            self.delegate?.didFailRegisterClass!(statusCode: 500, message: Localization("GenericError"))
            db = nil
        }
        
        let classesTable = Table("classes")
        
        do {
            if db != nil{
                try db.run(classesTable.insert(Constants.id <- classe.dbid != nil ? classe.dbid : 0 ,Constants.name <- classe.name, Constants.updated <- false, Constants.claseColor <- classe.colorClass.idString, Constants.nivel <- classe.nivel, Constants.grado <- classe.grado, Constants.teacherId <- UserProvider.sharedInstance.dbid, Constants.tUuid <- UserProvider.sharedInstance.uuid))
            }
            if classe.criterios.count == 0{
                let cri = Criterio()
                cri.name = "asistencia"
                cri.value = 10
                let cri2 = Criterio()
                cri2.name = "participación"
                cri2.value = 10
                classe.criterios.append(cri)
                classe.criterios.append(cri2)
            }
            do{
                let stmt = try db.prepare("SELECT * FROM Classes ORDER BY localId DESC LIMIT 1")
                
                for data in stmt {
                    print (data[3])
                    let s = "\(data[3]!)"
                    let alice = classesTable.filter(Constants.localId == NSString(string: s).integerValue)
                    if let x = classe.uuid, x != ""{
                        try db.run(alice.update(Constants.uuid <- x, Constants.updated <- true))
                    }else{
                        try db.run(alice.update(Constants.uuid <- "\(data[3]!)", Constants.updated <- false))
                        classe.uuid = "\(data[3]!)"
                    }
                    classe.localId = NSString(string: s).integerValue
                }
                self.delegate?.didSuccessRegister!(classe: classe)
            }catch{
                print(error)
                self.delegate?.didFailRegisterClass!(statusCode: 500, message: Localization("GenericError"))
            }
            self.createOnDB(period: classe.periods.first!, at: classe, isNew: true)
            self.saveOrUpdateCriteriosOn(classe: classe)
        }catch{
            print(error)
            self.delegate?.didFailRegisterClass!(statusCode: 500, message: Localization("GenericError"))
        }
    }
    
    func create(student: Student, at classe: Class){
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            self.delegate?.didFailRegisterStudent!(statusCode: 500, message: Localization("GenericError") )
            db = nil
        }
        
        let studentsTable = Table("students")
        do {
            if db != nil{
                if let bd = student.birthdayDate.convertedDate(calendar: .current){
                    try db.run(studentsTable.insert(Constants.id <- student.dbid != nil ? student.dbid : 0, Constants.name <- student.name, Constants.birthday <- bd, Constants.email <- student.emailTutor, Constants.gender <- student.gender, Constants.cUuid <- classe.uuid, Constants.updated <- false, Constants.cUuid <- classe.uuid, Constants.imageStudent <- student.imgUrl != nil ? student.imgUrl : "" ))
                }else{
                    try db.run(studentsTable.insert(Constants.id <- student.dbid != nil ? student.dbid : 0, Constants.name <- student.name, Constants.birthday <- student.birthdayDate.convertedDate(calendar: .current)!, Constants.email <- student.emailTutor, Constants.gender <- student.gender, Constants.cUuid <- classe.uuid, Constants.updated <- false, Constants.cUuid <- classe.uuid, Constants.imageStudent <- student.imgUrl != nil ? student.imgUrl : "" ))
                }
                
                
            }
            do{
                let stmt = try db.prepare("SELECT * FROM students ORDER BY localId DESC LIMIT 1")
                for data in stmt {
                    print (data[7])
                    let s = "\(data[7]!)"
                    let alice = studentsTable.filter(Constants.localId == NSString(string: s).integerValue)
                    if let x = student.uuid{
                        try db.run(alice.update(Constants.uuid <- x, Constants.updated <- true))
                    }else{
                        try db.run(alice.update(Constants.uuid <- "\(data[7]!)", Constants.updated <- false))
                        student.uuid = "\(data[7]!)"
                    }
                    student.localId = NSString(string: s).integerValue
                }
                self.delegate?.didSuccessRegister!(student: student, classe: classe)
            }catch{
                print(error)
                self.delegate?.didFailRegisterStudent!(statusCode: 500, message: Localization("GenericError") )
            }
        }catch{
            self.delegate?.didFailRegisterStudent!(statusCode: 500, message: Localization("GenericError"))
            print(error)
        }
        
    }
    
    func createOnDB(period: Period, at classe: Class, isNew: Bool){
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            self.delegate?.didFailAddPeriodToClass!(message: Localization("GenericError"))
            db = nil
        }
        
        let periodsTable = Table("periods")
        if !isNew{
            do{
                var t = false
                for p in classe.periods{
                    if p == period{
                        continue
                    }
                    let start = p.startDate.convertedDate(calendar: .current)
                    let end = p.endDate.convertedDate(calendar: .current)
                    let start1 = Calendar.current.date(byAdding: .day, value: 0, to: start!)
                    let end1 = Calendar.current.date(byAdding: .day, value: 0, to: end!)
                    if ((start1!...end1!).contains(period.startDate.convertedDate(calendar: .current)!)){
                        self.delegate?.didFailAddPeriodToClass!(message: Localization("GenericError"))
                        t = true
                    }else if ((start1!...start!).contains(period.endDate.convertedDate(calendar: .current)!)){
                        self.delegate?.didFailAddPeriodToClass!(message: Localization("GenericError"))
                        t = true
                    }
                }
                if !t {
                    if db != nil{
                        let pStart = period.startDate.convertedDate(calendar: .current)
                        let pEnd = period.endDate.convertedDate(calendar: .current)
                        try db.run(periodsTable.insert(Constants.id <- period.dbid != nil ? period.dbid : 0 ,Constants.startDate <- pStart!, Constants.endDate <- pEnd!,Constants.cUuid <- classe.uuid , Constants.updated <- false, Constants.cUuid <- classe.uuid, Constants.notifications <- period.notifications))
                    }
                    do{
                        
                        let stmt = try db.prepare("SELECT * FROM periods ORDER BY localId DESC LIMIT 1")
                        
                        for data in stmt {
                            print (data[6])
                            let s = "\(data[6]!)"
                            let alice = periodsTable.filter(Constants.localId == NSString(string: s).integerValue)
                            if let u = period.uuid{
                                try db.run(alice.update(Constants.uuid <- u,  Constants.updated <- true))
                            }else{
                                try db.run(alice.update(Constants.uuid <- "\(data[6]!)", Constants.updated <- false))
                                period.uuid = "\(data[6]!)"
                            }
                            period.localId = NSString(string: s).integerValue
                        }
                        if !classe.periods.contains(period){
                            classe.periods.append(period)
                        }
                        self.delegate?.didSuccessAddPeriodToClass!()
                    }catch{
                        self.delegate?.didFailAddPeriodToClass!(message: Localization("GenericError"))
                        print(error)
                    }
                    
                }
            }catch{
                self.delegate?.didFailAddPeriodToClass!(message: Localization("GenericError"))
                
                print(error)
            }
        }else{
            do{
                let pStart = period.startDate.convertedDate(calendar: .current)
                let pEnd = period.endDate.convertedDate(calendar: .current)
                try db.run(periodsTable.insert(Constants.id <- period.dbid != nil ? period.dbid : 0 , Constants.startDate <- pStart!, Constants.endDate <- pEnd!,Constants.idClasse <- classe.localId, Constants.cUuid <- classe.uuid, Constants.updated <- true, Constants.notifications <- period.notifications))
                do{
                    let stmt = try db.prepare("SELECT * FROM periods ORDER BY localId DESC LIMIT 1")
                    
                    for data in stmt {
                        print (data[6])
                        let s = "\(data[6]!)"
                        let alice = periodsTable.filter(Constants.localId == NSString(string: s).integerValue)
                        if let u = period.uuid{
                            try db.run(alice.update(Constants.uuid <- u, Constants.updated <- true))
                        }else{
                            try db.run(alice.update(Constants.uuid <- "\(data[6]!)", Constants.updated <- false))
                            period.uuid = "\(data[6]!)"
                        }
                        period.localId = NSString(string: s).integerValue
                    }
                    if !classe.periods.contains(period){
                        classe.periods.append(period)
                    }
                    self.delegate?.didSuccessAddPeriodToClass!()
                }catch{
                    self.delegate?.didFailAddPeriodToClass!(message: Localization("GenericError"))
                    print(error)
                }
            }catch{
                print(error)
            }
        }
    }
    
    func updateOnDB(student: Student){
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            self.delegate?.didFailUpdateStudent!(message: Localization("GenericError"))
            db = nil
        }
        
        let users = Table("students")
        
        var countFunction = 0
        do{
            if db != nil{
                countFunction = try db.scalar( users.filter( Constants.uuid == student.uuid).count )
            }
        }catch{
            print(error)
            self.delegate?.didFailUpdateStudent!(message: Localization("GenericError"))
        }
        
        if countFunction > 0{
            do {
                if db != nil{
                    let query = users.filter( Constants.uuid == student.uuid)
                    let dbDate = student.birthdayDate.convertedDate(calendar: .current)
                    try db.run(query.update( Constants.name <- student.name, Constants.birthday <- dbDate!, Constants.email <- student.emailTutor, Constants.gender <- student.gender, Constants.updated <- false, Constants.imageStudent <- student.imgUrl != nil ? student.imgUrl : ""))
                }
                print("update")
                self.delegate?.didSuccessUpdate!(student: student)
            } catch {
                print(error)
                self.delegate?.didFailUpdateStudent!(message: Localization("GenericError"))
            }
        }
    }
    
    func updateOnDB(classe: Class){
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            self.delegate?.didFailUpdateClass!(message: Localization("GenericError"))
            db = nil
        }
        
        let classesTable = Table("classes")
        
        var countFunction = 0
        do{
            if db != nil{
                countFunction = try db.scalar( classesTable.filter( Constants.uuid == classe.uuid).count )
            }
        }catch{
            print(error)
            self.delegate?.didFailUpdateClass!(message: Localization("GenericError"))
        }
        if countFunction > 0{
            do {
                if db != nil{
                    let query = classesTable.filter( Constants.uuid == classe.uuid)
                    try db.run(query.update(Constants.name <- classe.name, Constants.updated <- false, Constants.claseColor <- classe.colorClass.idString, Constants.nivel <- classe.nivel, Constants.grado <- classe.grado))
                }
                print("update")
                if classe.periods.count > 0{
                    self.updateOnDB(period: classe.periods[0], strtDate: classe.periods[0].startDate, ndDate: classe.periods[0].endDate, classe: classe)
                }
            } catch {
                print(error)
                self.delegate?.didFailUpdateClass!(message: Localization("GenericError"))
            }
            self.delegate?.didSuccessUpdate!(classe: classe)
        }
    }
    
    func updateOnDB(period: Period, strtDate: CVDate, ndDate: CVDate, classe: Class){
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            self.delegate?.didFailUpdatePeriod!(message: Localization("GenericError"))
            db = nil
        }
        
        let periodTable = Table("periods")
        
        var countFunction = 0
        do{
            if db != nil{
                countFunction = try db.scalar( periodTable.filter( Constants.uuid == period.uuid).count )
            }
        }catch{
            print(error)
            self.delegate?.didFailUpdatePeriod!(message: Localization("GenericError"))
        }
        if countFunction > 0{
            do {
                if db != nil{
                    let query = periodTable.filter( Constants.uuid == period.uuid)
                    let startDate = strtDate.convertedDate(calendar: .current)
                    let endDate = ndDate.convertedDate(calendar: .current)
                    try db.run(query.update(Constants.updated <- false, Constants.startDate <- startDate!, Constants.endDate <- endDate!, Constants.cUuid <- classe.uuid, Constants.notifications <- period.notifications))
                }
                print("update")
                self.delegate?.didSuccessUpdate!(period: period)
            } catch {
                print(error)
                self.delegate?.didFailUpdatePeriod!(message: Localization("GenericError"))
            }
        }
    }
    
    func deleteOnDB(classe: Class){
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            self.delegate?.didFailDeleteClass!(message: Localization("GenericError"))
            db = nil
        }
        
        let classesTable = Table("classes")
        let criteriosTable = Table("criterios")
        let studentTable = Table("students")
        let periodsTable = Table("periods")
        
        var countFunction = 0
        do{
            if db != nil{
                countFunction = try db.scalar( classesTable.filter( Constants.uuid == classe.uuid).count )
            }
        }catch{
            print(error)
            self.delegate?.didFailDeleteClass!(message: Localization("GenericError"))
        }
        if countFunction > 0{
            do {
                if db != nil{
                    let query = classesTable.filter( Constants.uuid == classe.uuid)
                    try db.run(query.delete())
                }
                print("update")
                self.delegate?.didSuccessDelete!(classe: classe)
                self.deleted(uuid: classe.uuid, type: "classe")
            } catch {
                print(error)
                self.delegate?.didFailDeleteClass!(message: Localization("GenericError"))
            }
            do{
                let query = criteriosTable.filter( Constants.cUuid == classe.uuid)
                try db.run(query.delete())
                let query2 = studentTable.filter( Constants.cUuid == classe.uuid)
                try db.run(query2.delete())
                let query3 = periodsTable.filter( Constants.cUuid == classe.uuid)
                try db.run(query3.delete())
            }catch{
                print(error)
            }
        }
    }
    
    func deleteOnDB(criterio: Criterio){
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            db = nil
            self.delegate?.didFailDeleteCriterio!(message: Localization("GenericError"))
        }
        
        let classesTable = Table("criterios")
        
        var countFunction = 0
        do{
            if db != nil{
                countFunction = try db.scalar( classesTable.filter( Constants.uuid == criterio.uuid).count )
            }
        }catch{
            print(error)
            self.delegate?.didFailDeleteCriterio!(message: Localization("GenericError"))
        }
        if countFunction > 0{
            do {
                if db != nil{
                    let query = classesTable.filter( Constants.uuid == criterio.uuid)
                    try db.run(query.delete())
                }
                print("update")
                self.delegate?.didSuccessDelete!(criterio: criterio)
                self.deleted(uuid: criterio.uuid, type: "criterio")
            } catch {
                print(error)
                self.delegate?.didFailDeleteCriterio!(message: Localization("GenericError"))
            }
        }
    }
    
    
    func deletedOnDB(students: [Student]){
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            self.delegate?.didSuccessDelete!(students: students)
            db = nil
        }
        
        let studentTable = Table("students")
        for s in students{
            var countFunction = 0
            do{
                if db != nil{
                    countFunction = try db.scalar( studentTable.filter( Constants.uuid == s.uuid).count )
                }
            }catch{
                print(error)
                self.delegate?.didFailDeleteStudents!(message: Localization("GenericError"))
            }
            if countFunction > 0{
                do {
                    if db != nil{
                        let query = studentTable.filter( Constants.uuid == s.uuid)
                        try db.run(query.delete())
                    }
                    print("update")
                    self.deleted(uuid: s.uuid, type: "students")
                    self.delegate?.didSuccessDelete!(students: students)
                } catch {
                    print(error)
                    self.delegate?.didFailDeleteStudents!(message: Localization("GenericError"))
                }
            }
        }
    }
    
    func deleteOnDB(period: Period){
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            self.delegate?.didSuccessDelete!(period: period)
            db = nil
        }
        
        let periodsTable = Table("periods")
        var countFunction = 0
        do{
            if db != nil{
                if period.dbid != 0{
                    countFunction = try db.scalar( periodsTable.filter( Constants.id == period.dbid).count )
                }else{
                    countFunction = try db.scalar( periodsTable.filter( Constants.uuid == period.uuid).count )
                }
            }
        }catch{
            print(error)
            self.delegate?.didSuccessDelete!(period: period)
        }
        if countFunction > 0{
            do {
                if db != nil{
                    if period.dbid != 0{
                        let query = periodsTable.filter( Constants.id == period.dbid)
                        try db.run(query.delete())
                    }else{
                        let query = periodsTable.filter( Constants.uuid == period.uuid)
                        try db.run(query.delete())
                    }
                }
                print("update")
                self.deleted(uuid: period.uuid, type: "period")
            } catch {
                print(error)
                self.delegate?.didSuccessDelete!(period: period)
            }
        }
        self.delegate?.didSuccessDelete!(period: period)
    }
    
    func deleted(uuid: String, type: String){
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            db = nil
        }
        
        let deletedTable = Table("deleted")
        
        do {
            if db != nil{
                try db.run(deletedTable.insert(Constants.uuid <- uuid, Constants.typeDeleted <- type, Constants.updated <- false))
            }
        }catch{
            print(error)
        }
    }
    
    func takeListOnDB(classe: Class, period: Period, date: Date){
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            self.delegate?.didFailTakeList!(message: Localization("GenericError"))
            db = nil
        }
        
        let listTable = Table("list")
        //        let sTable = Table("students")
        for student in classe.students{
            
            var countFunction = 0
            do{
                if db != nil{
                    countFunction = try db.scalar( listTable.filter( Constants.sUuid == student.uuid && Constants.day ==  Date().stringBy(date: date)).count)
                }
            }catch{
                print(error)
                self.delegate?.didFailTakeList!(message: Localization("GenericError"))
            }
            
            if countFunction > 0 {
                do{
                    let query = listTable.filter(Constants.sUuid == student.uuid && Constants.day == Date().stringBy(date: date))
                    var asistencia = 0
                    if student.isHere  && !student.isAlmostNotHere{
                        asistencia = 1
                    }
                    if !student.isHere && student.isAlmostNotHere{
                        asistencia = 3
                    }
                    
                    if !student.isAlmostNotHere && !student.isHere{
                        asistencia = 2
                    }
                    try db.run(query.update(Constants.present <- asistencia))
                }catch{
                    print(error)
                    self.delegate?.didFailTakeList!(message: Localization("GenericError"))
                }
            }else{
                do {
                    if db != nil{
                        var asistencia = 0
                        if student.isHere  && !student.isAlmostNotHere{
                            asistencia = 1
                        }
                        if !student.isHere && student.isAlmostNotHere{
                            asistencia = 3
                        }
                        
                        if !student.isAlmostNotHere && !student.isHere{
                            asistencia = 2
                        }
                        try db.run(listTable.insert(Constants.criUuid <- classe.criterios[0].uuid ,Constants.pUuid <- period.uuid, Constants.updated <- false, Constants.sUuid <- student.uuid, Constants.tUuid <- UserProvider.sharedInstance.uuid, Constants.cUuid <- classe.uuid, Constants.present <- asistencia, Constants.day <- Date().stringBy(date: date)))
                    }
                }catch{
                    print(error)
                    self.delegate?.didFailTakeList!(message: Localization("GenericError"))
                }
            }
            
        }
        self.delegate?.didSuccessTakeList!()
    }
    
    func updateCriterios(classe: Class){
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            db = nil
            self.delegate?.didFailUpdateCriteriosOnClasse!(message: Localization("GenericError"))
        }
        
        let classesTable = Table("criterios")
        
//        var countFunction = 0
//        do{
//            if db != nil{
//                countFunction = try db.scalar( classesTable.filter( Constants.cUuid == classe.uuid).count )
//            }
//        }catch{
//            print(error)
//        }
//        if countFunction > 0{
//            do {
//                if db != nil{
//                    let query = classesTable.filter( Constants.cUuid == classe.uuid)
//                    try db.run(query.delete())
//                }
//                print("update")
//                //                        self.delegate?.didSuccessDelete!(criterio: criterio)
//            } catch {
//                print(error)
//                self.delegate?.didFailUpdateCriteriosOnClasse!(message: Localization("GenericError"))
//                //                        self.delegate?.didFailDeleteCriterio!(message: Localization("GenericError"))
//            }
//        }
        saveOrUpdateCriteriosOn(classe: classe)
    }
    
    func rateOnDB(students : [Student], classe: Class, period: Int){
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            db = nil
        }
        
        let participationTable = Table("participation")
        //        let sTable = Table("students")
        for student in students{
            
            var countFunction = 0
            do{
                if db != nil{
                    countFunction = try db.scalar( participationTable.filter( Constants.sUuid == student.uuid && Constants.day == Date().stringBy(date: Date()) ).count )
                }
            }catch{
                print(error)
                self.delegate?.diidFailRateStudents!()
                return
            }
            
            if countFunction > 0 {
                do{
                    let prd = classe.periods.first(where: {$0.dbid == period})
                    let query = participationTable.filter(Constants.sUuid == student.uuid && Constants.day == Date().stringBy(date: Date()) && Constants.pUuid == (prd?.uuid)!)
                    try db.run(query.update(Constants.particip <- true))
                }catch{
                    print(error)
                    self.delegate?.diidFailRateStudents!()
                    return
                }
            }else{
                do {
                    if db != nil{
                        let prd = classe.periods.first(where: {$0.dbid == period})
                        try db.run(participationTable.insert(Constants.updated <- false, Constants.pUuid <- (prd?.uuid)!, Constants.sUuid <- student.uuid, Constants.tUuid <- UserProvider.sharedInstance.uuid, Constants.cUuid <- classe.uuid, Constants.particip <- true, Constants.day <- Date().stringBy(date: Date()), Constants.criUuid <- classe.criterios[1].uuid))
                    }
                }catch{
                    print(error)
                    self.delegate?.diidFailRateStudents!()
                    return
                }
            }
            
        }
        self.delegate?.didSuccessRateStudents!()
        
    }
    
    
    func updatePasswordOnDB(password: String){
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            db = nil
            self.delegate?.didFailChangePassword!(message: Localization("GenericError"))
        }
        
        let users = Table("users")
        
        var countFunction = 0
        do{
            if db != nil{
                countFunction = try db.scalar( users.filter( Constants.email == UserProvider.sharedInstance.email ).count )
            }
        }catch{
            print(error)
            self.delegate?.didFailChangePassword!(message: Localization("GenericError"))
        }
        
        
        if countFunction > 0{
            do {
                if db != nil{
                    let query = users.filter( Constants.email == UserProvider.sharedInstance.email)
                    try db.run(query.update(Constants.password <- password))
                }
                self.delegate?.didSuccessChangePassword!()
                print("update")
            } catch {
                self.delegate?.didFailChangePassword!(message: Localization("GenericError"))
                print(error)
            }
        }
        self.delegate?.didSuccessChangePassword!()
    }
    
    func rateDayOnDB(students: [Student], classe: Class, period: Period, c1: Bool, c2:Bool, c3:Bool, date: Date){
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            self.delegate?.didFailRateDay!(message: Localization("GenericError"))
            db = nil
        }
        
        let rateDayTable = Table("rateDay")
        for student in students{
            print(student.name)
            for (index, c) in student.criterios.enumerated(){
                switch index {
                case 2:
                    if c1{
                        var countFunction = 0
                        do{
                            if db != nil{
                                countFunction = try db.scalar( rateDayTable.filter( Constants.sUuid == student.uuid && Constants.day == Date().stringBy(date: date) && Constants.criUuid == student.criterios[2].uuid ).count)
                            }
                        }catch{
                            print(error)
                            self.delegate?.didFailRateDay!(message: Localization("GenericError"))
                        }
                        
                        if countFunction > 0 {
                            do{
                                let query = rateDayTable.filter(Constants.sUuid == student.uuid && Constants.day == Date().stringBy(date: date) && Constants.criUuid == student.criterios[2].uuid)
                                try db.run(query.update(Constants.valueC <- student.criterios[2].calif))
                            }catch{
                                print(error)
                                self.delegate?.didFailRateDay!(message: Localization("GenericError"))
                            }
                        }else{
                            do {
                                if db != nil{
                                    try db.run(rateDayTable.insert(Constants.sUuid <- student.uuid, Constants.tUuid <- UserProvider.sharedInstance.uuid, Constants.cUuid <- classe.uuid, Constants.pUuid <- period.uuid, Constants.criUuid <- student.criterios[2].uuid, Constants.day <- Date().stringBy(date: date), Constants.updated <- false, Constants.valueC <- student.criterios[2].calif))
                                }
                            }catch{
                                print(error)
                                self.delegate?.didFailRateDay!(message: Localization("GenericError"))
                            }
                        }
                        
                    }
                    break
                case 3:
                    if c2{
                        var countFunction = 0
                        do{
                            if db != nil{
                                countFunction = try db.scalar( rateDayTable.filter( Constants.sUuid == student.uuid && Constants.day == Date().stringBy(date: date) && Constants.criUuid == student.criterios[3].uuid ).count)
                            }
                        }catch{
                            print(error)
                            self.delegate?.didFailRateDay!(message: Localization("GenericError"))
                        }
                        
                        if countFunction > 0 {
                            do{
                                let query = rateDayTable.filter(Constants.sUuid == student.uuid && Constants.day == Date().stringBy(date: date) && Constants.criUuid == student.criterios[3].uuid)
                                try db.run(query.update(Constants.valueC <- student.criterios[3].calif))
                            }catch{
                                print(error)
                                self.delegate?.didFailRateDay!(message: Localization("GenericError"))
                            }
                        }else{
                            do {
                                if db != nil{
                                    try db.run(rateDayTable.insert(Constants.sUuid <- student.uuid, Constants.tUuid <- UserProvider.sharedInstance.uuid, Constants.cUuid <- classe.uuid, Constants.pUuid <- period.uuid, Constants.criUuid <- student.criterios[3].uuid, Constants.day <- Date().stringBy(date: date), Constants.updated <- false, Constants.valueC <- student.criterios[3].calif))
                                }
                            }catch{
                                print(error)
                                self.delegate?.didFailRateDay!(message: Localization("GenericError"))
                            }
                        }
                        
                    }
                    break
                case 4:
                    if c3{
                        var countFunction = 0
                        do{
                            if db != nil{
                                countFunction = try db.scalar( rateDayTable.filter( Constants.sUuid == student.uuid && Constants.day == Date().stringBy(date: date) && Constants.criUuid == student.criterios[4].uuid ).count)
                            }
                        }catch{
                            print(error)
                        }
                        
                        if countFunction > 0 {
                            do{
                                let query = rateDayTable.filter(Constants.sUuid == student.uuid && Constants.day == Date().stringBy(date: date) && Constants.criUuid == student.criterios[4].uuid)
                                try db.run(query.update(Constants.valueC <- student.criterios[4].calif))
                            }catch{
                                print(error)
                                self.delegate?.didFailRateDay!(message: Localization("GenericError"))
                            }
                        }else{
                            do {
                                if db != nil{
                                    try db.run(rateDayTable.insert(Constants.sUuid <- student.uuid, Constants.tUuid <- UserProvider.sharedInstance.uuid, Constants.cUuid <- classe.uuid, Constants.pUuid <- period.uuid, Constants.criUuid <- student.criterios[4].uuid, Constants.day <- Date().stringBy(date: date), Constants.updated <- false, Constants.valueC <- student.criterios[4].calif))
                                }
                            }catch{
                                print(error)
                                self.delegate?.didFailRateDay!(message: Localization("GenericError"))
                            }
                        }
                        
                    }
                    break
                default:
                    break
                }
            }
        }
        self.delegate?.didSuccessRateDay!()
    }
    
    func getReportOnDBBy(date: Date, classe: Class, period: Period){
        print (date)
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            db = nil
        }
        
        let rateDayTable = Table("rateDay")
        let studentsTable = Table("students")
        
        for s in classe.students{
            s.criterios.removeAll()
            for (index,cr) in classe.criterios.enumerated(){
                cr.calif = 0
                s.criterios.append(cr.copy() as! Criterio)
            }
            for (index,crit) in s.criterios.enumerated(){
                if index == 0{
                    let listTable = Table("list")
                    let queryL = listTable.filter(Constants.sUuid == s.uuid && Constants.day == Date().stringBy(date: date) )
                    do{
                        for row in try db.prepare(queryL) {
                            crit.calif = Double(row.get(Constants.present))
                        }
                    }catch{
                        print(error)
                    }
                    continue
                }else if index == 1{
                    let pTable = Table("participation")
                    let queryL = pTable.filter(Constants.sUuid == s.uuid && Constants.day == Date().stringBy(date: date) )
                    do{
                        for row in try db.prepare(queryL) {
                            if row.get(Constants.particip){
                                crit.calif = 1
                            }else{
                                crit.calif = 0
                            }
                        }
                    }catch{
                        print(error)
                    }
                }else{
                    do{
                        let query1 = rateDayTable.filter(Constants.sUuid == s.uuid  && Constants.criUuid == crit.uuid && Constants.day == Date().stringBy(date: date) && Constants.pUuid == period.uuid)
                        do{
                            for row in try db.prepare(query1) {
                                crit.calif = row.get(Constants.valueC)
                            }
                        }catch{
                            print(error)
                        }
                    }
                }
            }
            
            
            
            //        do {
            //            if db != nil{
            //                var students : [Student] = []
            //                do{
            //                    let query = rateDayTable.filter( Constants.day == Date().stringBy(date: date) && Constants.pUuid == period.uuid)
            //                    for q in try db.prepare(query){
            //                        print(q.get(Constants.valueC))
            //                        let student = Student()
            //                        student.criterios = classe.criterios
            //                        do{
            //                            let query = studentsTable.filter( Constants.uuid == q.get(Constants.sUuid))
            //                            for s in try db.prepare(query){
            //                                student.dbid = s.get(Constants.id)
            //                                student.uuid = s.get(Constants.uuid)
            //                                student.name = s.get(Constants.name)
            //                                student.birthdayDate = CVDate(date: s.get(Constants.birthday), calendar: .current)
            //                                student.emailTutor = s.get(Constants.email)
            //                                student.gender = s.get(Constants.gender)
            //                                student.claseId = s.get(Constants.idClasse)
            //                                students.append(student)
            //                                for (index,crit) in student.criterios.enumerated(){
            //                                    if index == 0{
            //                                        let listTable = Table("list")
            //                                        let queryL = listTable.filter(Constants.sUuid == student.uuid && Constants.day == Date().stringBy(date: date) )
            //                                        do{
            //                                            for row in try db.prepare(queryL) {
            //                                                crit.calif = Double(row.get(Constants.present))
            //                                            }
            //                                        }catch{
            //                                            print(error)
            //                                        }
            //                                        continue
            //                                    }else if index == 1{
            //                                        let pTable = Table("participation")
            //                                        let queryL = pTable.filter(Constants.sUuid == student.uuid && Constants.day == Date().stringBy(date: date) )
            //                                        do{
            //                                            for row in try db.prepare(queryL) {
            //                                                if row.get(Constants.particip){
            //                                                    crit.value = 1
            //                                                }else{
            //                                                    crit.value = 0
            //                                                }
            //                                            }
            //                                        }catch{
            //                                            print(error)
            //                                        }
            //                                    }
            //                                    do{
            //                                        let query1 = rateDayTable.filter(Constants.sUuid == student.uuid && Constants.criUuid == crit.uuid && Constants.day == Date().stringBy(date: date) && Constants.pUuid == period.uuid)
            //                                        do{
            //                                            for row in try db.prepare(query1) {
            //                                                crit.value = row.get(Constants.valueC)
            //                                            }
            //                                        }catch{
            //                                            print(error)
            //                                        }
            //                                    }
            //                                }
            //                            }
            //                        }catch{
            //                            print(error)
            //                        }
            //
            //                    }
            //                    self.delegate?.didSuccessGetReportByDay!(students: students)
            //                }catch{
            //                    print (error)
            //                    self.delegate?.didFailGetReportByDay!(message: Localization("GenericError"))
            //                }
            //            }
        }
        self.delegate?.didSuccessGetReportByDay!(students: classe.students)
        
    }
    
    
    func getReportONDB(period: Period, classe: Class){
        let start = period.startDate.convertedDate(calendar: .current)!
        let end = period.endDate.convertedDate(calendar: .current)!
        var totalInscidencias = [0,0,0,0,0]
        var date = start
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            db = nil
        }
        let criterios = Table("criterios")
        let rateDayTable = Table("rateDay")
        let listTable = Table("list")
        
        for s in classe.students{
            s.criterios.removeAll()
            for (index,cr) in classe.criterios.enumerated(){
                cr.calif = 0
                s.criterios.append(cr.copy() as! Criterio)
            }
        }
        
        
        for s in classe.students{
            for (index, crit) in s.criterios.enumerated(){
                switch index {
                case 0:
                    let listTable = Table("list")
                    var countFunction = 0
                    do{
                        if db != nil{
                            countFunction = try db.scalar( listTable.filter( Constants.pUuid == period.uuid && (Constants.sUuid == s.uuid || Constants.sUuid == "\(s.localId!)") && Constants.cUuid == classe.uuid && Constants.criUuid == crit.uuid ).count )
                        }
                    }catch{
                        print(error)
                    }
                    
                    do{
                        let stmt = try db.prepare("select count (*) from (SELECT * FROM list where periodUuid = (?) GROUP BY day)", period.uuid)
                        for data in stmt {
                            let s = "\(data[0]!)"
                            crit.inscidencias = NSString(string: s).integerValue
                        }
                    }catch{
                        print(error)
                    }
                    
                    crit.totalCalif = 0
                    if countFunction > 0{
                        let queryL = listTable.filter(Constants.pUuid == period.uuid && Constants.sUuid == s.uuid && Constants.cUuid == classe.uuid && Constants.criUuid == crit.uuid )
                        print(queryL)
                        do{
                            for row in try db.prepare(queryL){
                                switch row.get(Constants.present){
                                case 1:
                                    crit.totalCalif += 1
                                    break
                                case 2:
                                    crit.totalCalif += 0
                                    break
                                case 3:
                                    crit.totalCalif += 0.5
                                    break
                                default: break
                                    
                                }
                            }
                        }catch{
                            print(error)
                        }
                        
                    }else{
                        crit.totalCalif = 0
                    }
                    break
                case 1:
                    let pTable = Table("participation")
                    var countFunction = 0
                    do{
                        if db != nil{
                            countFunction = try db.scalar( pTable.filter( Constants.pUuid == period.uuid && Constants.sUuid == s.uuid  && Constants.cUuid == classe.uuid && Constants.criUuid == crit.uuid ).count )
                        }
                    }catch{
                        print(error)
                    }
                    do{
                        let stmt = try db.prepare("select count (*) from (SELECT * FROM participation where periodUuid = (?) GROUP BY day)", period.uuid)
                        for data in stmt {
                            let s = "\(data[0]!)"
                            crit.inscidencias = NSString(string: s).integerValue
                        }
                    }catch{
                        print(error)
                    }
                    crit.totalCalif = 0
                    if countFunction > 0{
                        let queryL = pTable.filter(Constants.pUuid == period.uuid && Constants.sUuid == s.uuid && Constants.cUuid == classe.uuid && Constants.criUuid == crit.uuid )
                        print(queryL)
                        do{
                            for row in try db.prepare(queryL){
                                crit.totalCalif += row.get(Constants.particip) == true ? 1 : 0
                            }
                        }catch{
                            print(error)
                        }
                        
                    }else{
                        crit.totalCalif = 0
                    }
                    break
                case 2:
                    var countFunction = 0
                    do{
                        if db != nil{
                            countFunction = try db.scalar( rateDayTable.filter( Constants.pUuid == period.uuid && Constants.sUuid == s.uuid && Constants.cUuid == classe.uuid && Constants.criUuid == crit.uuid ).count )
                        }
                    }catch{
                        print(error)
                    }
                    crit.totalCalif = 0
                    crit.inscidencias = countFunction
                    if countFunction > 0{
                        let queryL = rateDayTable.filter(Constants.pUuid == period.uuid && Constants.sUuid == s.uuid && Constants.cUuid == classe.uuid && Constants.criUuid == crit.uuid )
                        print(queryL)
                        do{
                            for row in try db.prepare(queryL){
                                crit.totalCalif += row.get(Constants.valueC)
                            }
                        }catch{
                            print(error)
                        }
                        
                    }else{
                        crit.totalCalif = 0
                    }
                    break
                case 3:
                    var countFunction = 0
                    do{
                        if db != nil{
                            countFunction = try db.scalar( rateDayTable.filter( Constants.pUuid == period.uuid && Constants.sUuid == s.uuid && Constants.cUuid == classe.uuid && Constants.criUuid == crit.uuid ).count )
                        }
                    }catch{
                        print(error)
                    }
                    crit.inscidencias = countFunction
                    crit.totalCalif = 0
                    if countFunction > 0{
                        let queryL = rateDayTable.filter(Constants.pUuid == period.uuid && Constants.sUuid == s.uuid && Constants.cUuid == classe.uuid && Constants.criUuid == crit.uuid )
                        print(queryL)
                        do{
                            for row in try db.prepare(queryL){
                                crit.totalCalif += row.get(Constants.valueC)
                            }
                        }catch{
                            print(error)
                        }
                        
                    }else{
                        crit.totalCalif = 0
                    }
                    break
                case 4:
                    var countFunction = 0
                    do{
                        if db != nil{
                            countFunction = try db.scalar( rateDayTable.filter( Constants.pUuid == period.uuid && Constants.sUuid == s.uuid && Constants.cUuid == classe.uuid && Constants.criUuid == crit.uuid ).count )
                        }
                    }catch{
                        print(error)
                    }
                    crit.inscidencias = countFunction
                    crit.totalCalif = 0
                    if countFunction > 0{
                        let queryL = rateDayTable.filter(Constants.pUuid == period.uuid && Constants.sUuid == s.uuid && Constants.cUuid == classe.uuid && Constants.criUuid == crit.uuid )
                        print(queryL)
                        do{
                            for row in try db.prepare(queryL){
                                crit.totalCalif += row.get(Constants.valueC)
                            }
                        }catch{
                            print(error)
                        }
                        
                    }else{
                        crit.totalCalif = 0
                    }
                    break
                default:
                    break
                }
                
            }
        }
        
        for student in classe.students{
            let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
            let nsUserDomainMask    = FileManager.SearchPathDomainMask.userDomainMask
            let paths               = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
            if let dirPath          = paths.first
            {
                let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("\(student.uuid!).jpg")
                let image    = UIImage(contentsOfFile: imageURL.path)
                if image != nil{
                    student.img = image
                }else{
                    student.img = nil
                }
            }else{
                student.img = nil
            }
            if student.img != nil{
                if let urlString = student.imgUrl ,!urlString.isEmpty {
                    switch urlString {
                    case "emoti1.jpg":
                        student.img = #imageLiteral(resourceName: "avatar1G")
                        break
                    case "emoti2.jpg":
                        student.img = #imageLiteral(resourceName: "avatar2G")
                        break
                    case "emoti3.jpg":
                        student.img = #imageLiteral(resourceName: "avatar3G")
                        break
                    case "emoti4.jpg":
                        student.img = #imageLiteral(resourceName: "avatar4G")
                        break
                    case "emoti5.jpg":
                        student.img = #imageLiteral(resourceName: "avatar5G")
                        break
                    case "emoti6.jpg":
                        student.img = #imageLiteral(resourceName: "avatar6G")
                        break
                    case "emoti7.jpg":
                        student.img = #imageLiteral(resourceName: "avatar7G")
                        break
                    case "emoti8.jpg":
                        student.img = #imageLiteral(resourceName: "avatar8G")
                        break
                    case "emoti9.jpg":
                        student.img = #imageLiteral(resourceName: "avatar9G")
                        break
                    default:
                                               break
                    }
                }
            }

            for (index,cri) in student.criterios.enumerated(){
                let query = criterios.filter(Constants.uuid == cri.uuid)
                do{
                    for row in try db.prepare(query){
                        cri.value = row.get(Constants.valueC)
                    }
                }catch{
                    print (error)
                }
                
                print("\(student.name!) - \(cri.name) - \(cri.totalCalif) de \(cri.inscidencias)")
                switch index {
                case 0:
                    if (cri.totalCalif * (cri.value)) == 0 && Double(cri.inscidencias) == 0{
                        cri.calif = 0.0
                    }else{
                        cri.calif = (cri.totalCalif * (cri.value * 5)) / Double(cri.inscidencias)
                    }
                    break
                case 1:
                    if (cri.totalCalif * (cri.value)) == 0 && Double(cri.inscidencias) == 0{
                        cri.calif = 0.0
                    }else{
                        cri.calif = (cri.totalCalif * (cri.value * 5)) / Double(cri.inscidencias)
                    }
                    break
                case 2:
                    if (cri.totalCalif * (cri.value)) == 0 && Double((cri.inscidencias * 10)) == 0{
                        cri.calif = 0.0
                    }else{
                        cri.calif = (cri.totalCalif * (cri.value * 5)) / (Double((cri.inscidencias * 10)))
                    }
                    break
                case 3:
                    if (cri.totalCalif * (cri.value)) == 0 && Double((cri.inscidencias * 10)) == 0{
                        cri.calif = 0.0
                    }else{
                        cri.calif = (cri.totalCalif * (cri.value * 5)) / (Double((cri.inscidencias * 10)))
                    }
                    break
                case 4:
                    if (cri.totalCalif * (cri.value)) == 0 && Double((cri.inscidencias * 10)) == 0{
                        cri.calif = 0.0
                    }else{
                        cri.calif = (cri.totalCalif * (cri.value * 5)) / (Double((cri.inscidencias * 10)))
                    }
                    break
                default: break
                }
                print(cri.percentage)
            }
        }
        self.delegate?.didSuccessGetRepostByPeriod!(students: classe.students)
    }

}

class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}




