//
//  Period.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso on 24/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import CVCalendar

class Period: NSObject {
    var dbid : Int!
    var startDate : CVDate!
    var endDate : CVDate!
    var namedId: Int!
    var classId : Int!
    var notifications = true
    var uuid : String!
    var updated: Bool!
    var localId : Int!

}


