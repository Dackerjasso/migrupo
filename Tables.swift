//
//  Tables.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 07/07/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import SQLite

class Tables: NSObject {
    //Profes
    static let id = Expression<Int?>("id")
    static let localId = Expression<Int?>("localId")
    static let name = Expression<String?>("name")
    static let email = Expression<String?>("email")
    static let username = Expression<String?>("username")
    static let password = Expression<String?>("password")
    static let lastName = Expression<String?>("lastName")
    static let uuid = Expression<String>("uuid")
    static let schoolName = Expression<String?>("schoolName")
    
    //Classes
    static let grado = Expression<Int?>("grado")
    static let nivel = Expression<String?>("nivel")
    static let claseColor = Expression<String?>("claseColor")
    
    //Students
    static let gender = Expression<String?>("gender")
    static let birthday = Expression<Date?>("birthday")
    static let idClasse = Expression<Int?>("idClasse")
    
    //Periods
    static let startDate = Expression<Date?>("startDate")
    static let endDate = Expression<Date?>("endDate")
    static let nameId = Expression<Int?>("nameId")
    
    //GENERAL
    static let updated = Expression<Bool?>("updated")
}
