//
//  TablesProvider.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 07/07/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

public class TablesProvider {
    class var sharedInstance: Tables {
        struct Static {
            static let instance: Tables = Tables()
        }
        return Static.instance
    }
}
