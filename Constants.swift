//
//  Constants.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso on 22/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import SQLite

class Constants: NSObject {
    
//    Production
    static let baseUrl = "http://dev.red-larousse.com.mx:8080/larousse/api1.0"
//    Desarrollo
//    static let baseUrl = "http://45.33.12.221:8080/larousse/api1.0"
//    Luis local
//      static let baseUrl = "http://192.168.7.104:8080/larousseservices/api1.0"

    static let users = "users"
    static let login = "login"
    static let register = "registro"
    static let clase = "clase"
    static let alumno = "alumno"
    static let restorepass = "recoverPasword"
    static let criterios = "criterios"
    static let classes = "classes"
    static let updatePassword = "updatePasword"
    static let photo = "photo"
    static let classe = "class"
    static let period = "periodo"
    static let deletecriterio = "deletecriterio"
    static let delete = "delete"
    static let deleteClass = "deleteClass"
    static let deletePeriod = "deletePeriod"
    static let updatePeriodo = "updatePeriodo"
    static let update = "update"
    static let asistencia = "asistencia"
    static let participacion = "participacion"
    static let participaciones = "participaciones"
    static let porcentaje = "porcentaje"
    static let porcentajeDia = "porcentajeDia"
    static let calificacion = "calificacion"
    static let sincronizar = "sincronizar"
    
    //Profes
    static let id = Expression<Int>("id")
    static let localId = Expression<Int>("localId")
    static let name = Expression<String>("name")
    static let email = Expression<String>("email")
    static let username = Expression<String>("username")
    static let password = Expression<String>("password")
    static let lastName = Expression<String>("lastName")
    static let uuid = Expression<String>("uuid")
    static let schoolName = Expression<String>("schoolName")

    //Classes
    static let grado = Expression<Int>("grado")
    static let nivel = Expression<String>("nivel")
    static let claseColor = Expression<String>("claseColor")
    static let teacherId = Expression<Int>("teacherId")

    //Students
    static let gender = Expression<String>("gender")
    static let birthday = Expression<Date>("birthday")
    static let idClasse = Expression<Int>("idClasse")
    static let imageStudent = Expression<String>("image")

    
    //Periods
    static let startDate = Expression<Date>("startDate")
    static let endDate = Expression<Date>("endDate")
    static let nameId = Expression<Int>("nameId")

    //GENERAL
    static let updated = Expression<Bool>("updated")
    static let typeDeleted = Expression<String>("typeDeleted")
    
    //List
    static let sUuid = Expression<String>("studentUuid")
    static let tUuid = Expression<String>("teacherUuid")
    static let cUuid = Expression<String>("classeUuid")
    static let criUuid = Expression<String>("criterioUuid")
    static let present = Expression<Int>("present")
    static let day = Expression<String>("day")
    static let pUuid = Expression<String>("periodUuid")
    static let particip = Expression<Bool>("participation")
    static let notifications = Expression<Bool>("notifications")
    
    //Criterios
    static let valueC = Expression<Double>("value")
    
    static let ssb = UIStoryboard(name: "Spinner", bundle: nil)
    static let svc = ssb.instantiateViewController(withIdentifier: "spinnerView") as! SpinnerViewController
    
    
    static let path = NSSearchPathForDirectoriesInDomains(
        .documentDirectory, .userDomainMask, true
        ).first!
    
    public enum colorClass{
        case red
        case orange
        case blue1
        case pink
        case purple
        case yellow
        case aqua
        case green
        case green2
        case blue2
        case purple2
    }
}


extension Constants.colorClass{
    
    var idString : String{
        get{
            switch self {
            case .red:
                return "1"
            case .orange:
                return   "2"
            case .blue1:
                return  "3"
            case .pink:
                return  "4"
            case .purple:
                return  "5"
            case .yellow:
                return "6"
            case .aqua:
                return "7"
            case .green:
                return "8"
            case .green2:
                return "9"
            case .blue2:
                return "10"
            case .purple2:
                return "11"
            }

        }
    }
    
    var nameTeam : String{
        get{
            switch self {
            case .red:
                return "Team 1"
            case .orange:
                return   "Team 2"
            case .blue1:
                return  "Team 3"
            case .pink:
                return  "Team 4"
            case .purple:
                return  "Team 5"
            case .yellow:
                return "Team 6"
            case .aqua:
                return "Team 7"
            case .green:
                return "Team 8"
            case .green2:
                return "Team 9"
            case .blue2:
                return "Team 10"
            case .purple2:
                return "Team 11"
            }
            
        }
    }
    
    var color: UIColor {
        get {
            switch self {
            case .red:
                return UIColor(red: 178.0/255.0, green: 59.0/255.0, blue: 59.0/255.0, alpha: 1)
            case .orange:
                return   UIColor(red: 188.0/255.0, green: 69.0/255.0, blue: 36.0/255.0, alpha: 1)
            case .blue1:
                return  UIColor(red: 57.0/255.0, green: 135.0/255.0, blue: 188.0/255.0, alpha: 1)
            case .pink:
                return  UIColor(red: 214.0/255.0, green: 29.0/255.0, blue: 86.0/255.0, alpha: 1)
            case .purple:
                return  UIColor(red: 83.0/255.0, green: 44.0/255.0, blue: 191.0/255.0, alpha: 1)
            case .yellow:
                return UIColor(red: 239.0/255.0, green: 217.0/255.0, blue: 55.0/255.0, alpha: 1)
            case .aqua:
                return UIColor(red: 68.0/255.0, green: 193.0/255.0, blue: 181.0/255.0, alpha: 1)
            case .green:
                return UIColor(red: 51.0/255.0, green: 168.0/255.0, blue: 51.0/255.0, alpha: 1)
            case .green2:
                return UIColor(red: 184.0/255.0, green: 188.0/255.0, blue: 17.0/255.0, alpha: 1)
            case .blue2:
                return UIColor(red: 60.0/255.0, green: 87.0/255.0, blue: 221.0/255.0, alpha: 1)
            case .purple2:
                return UIColor(red: 141.0/255.0, green: 41.0/255.0, blue: 193.0/255.0, alpha: 1)
            }
        }
    }

    
    var image : UIImage{
        switch self{
        case .red:
             return #imageLiteral(resourceName: "burbuja1")
        case .orange:
            return #imageLiteral(resourceName: "burbuja2")
        case .blue1:
            return #imageLiteral(resourceName: "burbuja3")
        case .pink:
            return #imageLiteral(resourceName: "burbuja4")
        case .purple:
            return #imageLiteral(resourceName: "burbuja5")
        case .yellow:
            return #imageLiteral(resourceName: "burbuja6")
        case .aqua:
            return #imageLiteral(resourceName: "burbuja7")
        case .green:
            return #imageLiteral(resourceName: "burbuja8")
        case .green2:
            return #imageLiteral(resourceName: "burbuja9")
        case .blue2:
            return #imageLiteral(resourceName: "burbuja10")
        case .purple2:
            return #imageLiteral(resourceName: "burbuja11")

        }
    }
}

