//
//  Team.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 06/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

class Team: NSObject {
    var students : [Student] = []
    var color: Constants.colorClass!
    var name: String!
}
