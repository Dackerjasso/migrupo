     //
//  Sincronizacion.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 18/07/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import Alamofire
import SQLite
import CVCalendar

protocol sincronizacionDelegate {
    func didSuccessSincronizado(teacher: Teacher)
    func didFailSincronizadoTeacher(message: String)
    func noInternetconnection()
    func didSuccessCLassesSincronizacion(classes: [Class])
    func didsuccessSincronizacion2()
    func updatedDB()
}

class Sincronizacion: NSObject {
    let defaults = UserDefaults.standard
    var delegate: sincronizacionDelegate?
    
    func register(teacher: Teacher){
        let parameters  = [
            "email":teacher.email,
            "password":teacher.password,
            "name":teacher.name,
            "lastName": teacher.lastName,
            "alias": teacher.userName,
            "escuela": teacher.schoolName
            ] as [String : Any]
        let header = [
            "Content-Type":"application/json",
            "idioma": defaults.object(forKey: "language") as! String
        ]
        print(parameters)
        Alamofire.request("\(Constants.baseUrl)/\(Constants.users)/\(Constants.register)", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            debugPrint(response)
            switch response.result{
            case .success:
                switch Int((response.response?.statusCode)!){
                case 200, 201, 202:
                    UserProvider.sharedInstance.email = teacher.email
                    UserProvider.sharedInstance.userName = teacher.userName
                    UserProvider.sharedInstance.name = teacher.name
                    UserProvider.sharedInstance.lastName = teacher.lastName
                    UserProvider.sharedInstance.password = teacher.password
                    UserProvider.sharedInstance.schoolName = teacher.schoolName
                    UserProvider.sharedInstance.uuid = (((response.result.value as! NSDictionary).value(forKey: "Success")as! NSArray)[0] as! NSDictionary).value(forKey: "uuid") as! String
                    UserProvider.sharedInstance.dbid = (((response.result.value as! NSDictionary).value(forKey: "Success")as! NSArray)[0] as! NSDictionary).value(forKey: "id") as! Int
                    UserProvider.sharedInstance.classes = []
                    UserProvider.sharedInstance.localPath = teacher.localPath
                    teacher.isnew = true
                    UserProvider.sharedInstance.image = teacher.image
                    teacher.uuid = UserProvider.sharedInstance.uuid
                    UserProvider.sharedInstance.isnew = teacher.isnew
                    self.saveOrUpdate(teacher: teacher)
                    self.delegate?.didSuccessSincronizado(teacher: teacher)
                    break
                case 300...999:
                    self.delegate?.didFailSincronizadoTeacher( message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                    break
                default:
                    self.delegate?.didFailSincronizadoTeacher( message: (response.result.value as! NSDictionary).value(forKey: "fullMessage") as! String)
                    break
                }
                break
            case .failure(let error):
                self.delegate?.didFailSincronizadoTeacher( message: Localization("GenericError"))
                print(error)
                break
            }
        }
    }
    
    func saveOrUpdate(teacher: Teacher){
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            db = nil
        }
        
        let users = Table("users")
        
        var countFunction = 0
        do{
            if db != nil{
                countFunction = try db.scalar( users.filter( Constants.email == UserProvider.sharedInstance.email ).count )
            }
        }catch{
            print(error)
        }
        
        
        if countFunction > 0{
            do {
                if db != nil{
                    let query = users.filter( Constants.email == UserProvider.sharedInstance.email)
                    try db.run(query.update(Constants.id <- teacher.dbid, Constants.name <- teacher.name, Constants.password <- teacher.password, Constants.email <- teacher.email, Constants.lastName <- teacher.lastName, Constants.username <- teacher.userName, Constants.schoolName <- teacher.schoolName ,Constants.uuid <- teacher.uuid, Constants.updated <- true))
                }
                print("update")
            } catch {
                print(error)
            }
        }else{
            do {
                if db != nil{
                    try db.run(users.insert(Constants.id <- teacher.dbid, Constants.name <- teacher.name, Constants.password <- teacher.password, Constants.email <- teacher.email, Constants.lastName <- teacher.lastName, Constants.username <- teacher.userName, Constants.schoolName <- teacher.schoolName ,Constants.uuid <- teacher.uuid, Constants.updated <- true))
                }
                print("insert")
            } catch {
                print(error)
            }
        }
        
    }
    
    func sincronizar(classes: [Class]){
        switch currentReachabilityStatus{
        case .notReachable:
            self.delegate?.noInternetconnection()
            break
        case .reachableViaWiFi, .reachableViaWWAN:
            let header : [String: String] = [
                "Content-Type":"application/json",
                "idioma": defaults.object(forKey: "language") as! String,
                "user":UserProvider.sharedInstance.uuid
            ]
            var clasessss = [[:]]
            clasessss.removeAll()
            let letters = CharacterSet.letters

            for c in classes{
                var x = [[ : ]]
                x.removeAll()
                var cr = [[ : ]]
                cr.removeAll()
                var p = [[ : ]]
                p.removeAll()
                var rates = [[:]]
                rates.removeAll()

                for s in c.students{
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeStyle = DateFormatter.Style.none
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    dateFormatter.dateFormat = "y-MM-dd"
                    let bDate = dateFormatter.string(from: s.birthdayDate.convertedDate(calendar: .current)!)  // "2010-01-27"
                    let a = [
                        "uuid": s.uuid.rangeOfCharacter(from: letters) != nil ? s.uuid : "",
                        "nombre": s.name,
                        "genero": s.gender == Localization("man") ? "M" : "F",
                        "cumpleanos": bDate,
                        "mail": s.emailTutor,
                        "claseId": "",
                        "pictureUrl":"",
                        "localId": s.localId
                        ] as [String : Any]
                    print(a)
                    x.append(a)
                }
                
                for cri in c.criterios{
                    let b = [
                        "criterioUuid": cri.uuid.rangeOfCharacter(from: letters) != nil ? cri.uuid : "",
                        "actividad": cri.name,
                        "valor" : cri.value,
                        "localId": cri.localId
                        ] as [String : Any]
                    cr.append(b)
                }
                
                for per in c.periods{
                    let dateFormatter = DateFormatter()
                    dateFormatter.timeStyle = DateFormatter.Style.none
                    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
                    dateFormatter.dateFormat = "y-MM-dd"
                    let startDate = dateFormatter.string(from: per.startDate.convertedDate(calendar: .current)!)
                    let endDate = dateFormatter.string(from: per.endDate.convertedDate(calendar: .current)!)
                    
                    let c = [
                        "uuid": per.uuid.rangeOfCharacter(from: letters) != nil ? per.uuid : "",
                        "fechaInicio":startDate,
                        "fechaFin":endDate,
                        "localId": per.localId,
                        "id": per.dbid != nil ? per.dbid : ""
                        ] as [String : Any]
                    p.append(c)
                }
                
            
                let abc = [
                    "uuid" : c.uuid.rangeOfCharacter(from: .letters) != nil ? c.uuid : "",
                    "claseId" : c.dbid != 0 ? c.dbid : "",
                    "nombreMateria": c.name,
                    "nivel": c.nivel,
                    "grado": c.grado,
                    "color": c.colorClass.idString,
                    "idMaestro": UserProvider.sharedInstance.dbid,
                    "localId": c.localId,
                    "alumnos": x,
                    "criterioInfo" : cr,
                    "periodos":p,
                    "calificaciones":[
                    ],
                    "participaciones":[
                    ],
                    "asistencias":[
                    ],
                    "calificacionesUpdate" : [

                    ],
                    "lista":[
                        
                    ]
                    ] as [String : Any]
                
                clasessss.append(abc)
            }
            
            
            
            print(classes)
            
            var cDeleted = [[:]]
            cDeleted.removeAll()
            var sDeleted = [[:]]
            sDeleted.removeAll()
            var pDeleted = [[:]]
            pDeleted.removeAll()
            
            let db : Connection!
            do{
                db = try Connection("\(Constants.path)/db.sqlite3")
            }catch{
                print ("Error: \(error)")
                db = nil
            }
            let deletedTable = Table("deleted")
            
            do{
                for row in try db.prepare(deletedTable){
                    print(row.get(Constants.typeDeleted))
                    if row.get(Constants.typeDeleted) == "classe"{
                        if row.get(Constants.uuid).rangeOfCharacter(from: letters) != nil{
                            let a = [
                                "uuid" : row.get(Constants.uuid)
                            ]
                            cDeleted.append(a)
                        }
                        
                    }else if row.get(Constants.typeDeleted) == "students"{
                        if row.get(Constants.uuid).rangeOfCharacter(from: letters) != nil{
                            let a = [
                                "uuid" : row.get(Constants.uuid)
                            ]
                            sDeleted.append(a)
                        }
                    }else if row.get(Constants.typeDeleted) == "period"{
                        if row.get(Constants.uuid).rangeOfCharacter(from: letters) != nil{
                            let a = [
                                "uuid" : row.get(Constants.uuid)
                            ]
                            pDeleted.append(a)
                        }
                    }
                }
            }catch{
                print(error)
            }

            
            let parameters = [
                "clases":clasessss,
                "borrarAlumnos": sDeleted,
                "borrarCriterios":[],
                "borrarClases":
                cDeleted,
                "borrarPeriodos":
                pDeleted
            ] as [String : Any]
            
            print(parameters)
            
            Alamofire.request("\(Constants.baseUrl)/\(Constants.clase)/\(Constants.sincronizar)", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
                debugPrint(response)
                switch response.result{
                case .success:
                        let db : Connection!
                        do{
                            db = try Connection("\(Constants.path)/db.sqlite3")
                        }catch{
                            print ("Error: \(error)")
                            db = nil
                        }
                        
                    let classesTable = Table("classes")
                    let criteriosTable = Table("criterios")
                    let periodsTable = Table("periods")
                    let participTable = Table("participation")
                    let rateDayTable = Table("rateDay")
                    let studentsTable = Table("students")
                    let listTable = Table("list")

                    switch Int((response.response?.statusCode)!){
                    case 200, 201, 202:
                        
                        let classesDic = ((response.result.value as! NSDictionary).value(forKey: "ClasesNuevas") as! NSArray)
                        for c in classesDic{
                            let lid = (c as! NSDictionary).value(forKey: "localId") as! Int
                            let cid = (c as! NSDictionary).value(forKey: "id") as! Int
                            
                            let cls = UserProvider.sharedInstance.classes.filter{ $0.localId == lid }.first
                            cls?.uuid = (c as! NSDictionary).value(forKey: "uuid") as! String
                            cls?.dbid = cid
                            let alice = classesTable.filter(Constants.localId == (cls?.localId)!)
                            do{
                                try db.run(alice.update(Constants.id <- cid, Constants.uuid <- (cls?.uuid)!, Constants.updated <- true))
                                
                            }catch{
                                print(error)
                            }
                            do{
                                let alice = periodsTable.filter(Constants.cUuid == "\((c as! NSDictionary).value(forKey: "localId") as! Int)")
                                try db.run(alice.update(Constants.cUuid <- (cls?.uuid)!))
                            }catch{
                                print(error)
                            }
                            do{
                                let alice = criteriosTable.filter(Constants.cUuid == "\((c as! NSDictionary).value(forKey: "localId") as! Int)")
                                try db.run(alice.update(Constants.cUuid <- (cls?.uuid)!))
                            }catch{
                                print(error)
                            }
                            do{
                                let alice = studentsTable.filter(Constants.cUuid == "\((c as! NSDictionary).value(forKey: "localId") as! Int)")
                                try db.run(alice.update(Constants.cUuid <- (cls?.uuid)!))
                            }catch{
                                print(error)
                            }
                        }
                        
                        
                        
                        if let criteriosArray = (response.result.value as! NSDictionary).value(forKey: "CriteriosNuevos") {
                            for cri in criteriosArray  as! NSArray{
                                if  let criteArray = (cri as! NSDictionary).value(forKey: "Success"){
                                    let info = ((criteArray as! NSArray)[0] as! NSDictionary).value(forKey: "criterioInfo") as! NSArray
                                    for (index,cr) in info.enumerated(){
                                        let criuuid = (cr as! NSDictionary).value(forKey: "criterioUuid") as! String
                                        let alice = criteriosTable.filter(Constants.localId == (cr as! NSDictionary).value(forKey: "localId") as! Int)
                                        do{
                                            try db.run(alice.update(Constants.uuid <- criuuid, Constants.updated <- true))
                                        }catch{
                                            print(error)
                                        }
                                    }
                                }
                            }
                        }
                        
                        //
                        let perdic = (response.result.value as! NSDictionary).value(forKey: "PeriodosNuevos")
                        for per in perdic as! NSArray{
                            if  let x = (per as! NSDictionary).value(forKey: "PeriodoInfo:"){
                                let peruuid = (x as! NSDictionary).value(forKey: "uuid") as! String
                                let alice = periodsTable.filter(Constants.localId == (x as! NSDictionary).value(forKey: "localId") as! Int)
                                do{
                                    try db.run(alice.update(Constants.uuid <- peruuid, Constants.updated <- true))
                                }catch{
                                    print(error)
                                }
                            }
                            
                        }
                        
                        if let studntsDic = (response.result.value as! NSDictionary).value(forKey: "AlumnosNuevos"){
                            
                            for stdn in studntsDic as! NSArray{
                                if let x = (stdn as! NSDictionary).value(forKey: "Success"){
                                    for s in x as! NSArray{
                                        do {
                                            let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                                            let documentDirectory = URL(fileURLWithPath: path)
                                            let originPath = documentDirectory.appendingPathComponent("\((s as! NSDictionary).value(forKey: "localId") as! Int).jpg")
                                            let destinationPath = documentDirectory.appendingPathComponent("\((s as! NSDictionary).value(forKey: "uuid") as! String).jpg")
                                            try FileManager.default.moveItem(at: originPath, to: destinationPath)
                                        } catch {
                                            print(error)
                                        }
                                        let stuuuid = (s as! NSDictionary).value(forKey: "uuid") as! String
                                        let alice = studentsTable.filter(Constants.localId == (s as! NSDictionary).value(forKey: "localId") as! Int)
                                        do{
                                            try db.run(alice.update(Constants.uuid <- stuuuid, Constants.updated <- true))
                                        }catch{
                                            print(error)
                                        }

                                    }
                                }
                            }

                        }
                        
                        self.delegate?.didSuccessCLassesSincronizacion(classes: classes)
                        
                        print(response.result.value as! NSDictionary)
                        
                        break
                    case 300...500:
                        break
                    default:
                        break
                    }
                    break
                case .failure(let error):
                    print(error)
                    break
                }
            }
            break
        }

    }
    
    
    func sincronizacion2(){
        
        
        let header : [String: String] = [
            "Content-Type":"application/json",
            "idioma": defaults.object(forKey: "language") as! String,
            "user":UserProvider.sharedInstance.uuid
        ]
        var clasessss = [[:]]
        clasessss.removeAll()
        
        let participationTable = Table("participation")
        let rateDayTable = Table("rateDay")
        let listTable = Table("list")

        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            db = nil
        }
        
        
        for c in UserProvider.sharedInstance.classes{
            var x = [[ : ]]
            x.removeAll()
            var cr = [[ : ]]
            cr.removeAll()
            var p = [[ : ]]
            p.removeAll()
            var rates = [[:]]
            rates.removeAll()
            var califUpdate = [[:]]
            califUpdate.removeAll()
            var list = [[:]]
            list.removeAll()
            let dateFormatter = DateFormatter()
            dateFormatter.timeStyle = DateFormatter.Style.none
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.dateFormat = "y-MM-dd"
//            let bDate = dateFormatter.string(from: s.birthdayDate.convertedDate(calendar: .current)!)  // "2010-01-27"
            var countFunction = 0
            do{
                if db != nil{
                    countFunction = try db.scalar( participationTable.filter((Constants.cUuid == c.uuid || Constants.cUuid == "\(c.localId)" ) && Constants.tUuid == UserProvider.sharedInstance.uuid).count  )
                }
            }catch{
                print(error)
            }
            
            
            if countFunction > 0{
                do {
                    if db != nil{
                        let querryParticipation = participationTable.filter(( Constants.cUuid == c.uuid || Constants.cUuid == "\(c.localId)" ) && Constants.tUuid == UserProvider.sharedInstance.uuid)
                        for row in try db.prepare(querryParticipation) {
                            let dateString = row.get(Constants.day)
                            let dateArray = dateString.components(separatedBy: "/")
                            let year = "\(dateArray[0])"
                            let month = dateArray[1].characters.count == 1 ? "0\(dateArray[1])" : dateArray[1]
                            let day = dateArray[2].characters.count == 1 ? "0\(dateArray[2])" : dateArray[2]
                            let r = [
                                "periodoUuid":row.get(Constants.pUuid),
                                "alumnoUuid":row.get(Constants.sUuid),
                                "participo":row.get(Constants.particip) == true ? 1 : 0,
                                "fecha":"\(year)-\(month)-\(day)",
                                "criterioUuid":row.get(Constants.criUuid)
                            ] as [String : Any]
                            rates.append(r)
                        }
                    }
                } catch {
                    print(error)
                }
            }
            
            
            countFunction = 0
            do{
                if db != nil{
                    countFunction = try db.scalar( rateDayTable.filter(( Constants.cUuid == c.uuid || Constants.cUuid == "\(c.localId)" ) && Constants.tUuid == UserProvider.sharedInstance.uuid).count   )
                }
            }catch{
                print(error)
            }
            
            
            
            if countFunction > 0{
                do {
                    if db != nil{
                        let querryParticipation = rateDayTable.filter((Constants.cUuid == c.uuid || Constants.cUuid == "\(c.localId)" ) && Constants.tUuid == UserProvider.sharedInstance.uuid)
                        for row in try db.prepare(querryParticipation) {
                            let dateString = row.get(Constants.day)
                            let dateArray = dateString.components(separatedBy: "/")
                            let year = "\(dateArray[0])"
                            let month = dateArray[1].characters.count == 1 ? "0\(dateArray[1])" : dateArray[1]
                            let day = dateArray[2].characters.count == 1 ? "0\(dateArray[2])" : dateArray[2]
                            let r = [
                                "periodoUuid":row.get(Constants.pUuid),
                                "alumnoUuid":row.get(Constants.sUuid),
                                "calificacion":row.get(Constants.valueC),
                                "fecha":"\(year)-\(month)-\(day)",
                                "criterioUuid":row.get(Constants.criUuid)
                                ] as [String : Any]
                            califUpdate.append(r)
                        }
                    }
                } catch {
                    print(error)
                }
            }
            
            countFunction = 0
            do{
                if db != nil{
                    countFunction = try db.scalar( listTable.filter( (Constants.cUuid == c.uuid || Constants.cUuid == "\(c.localId)" ) && Constants.tUuid == UserProvider.sharedInstance.uuid).count  )
                }
            }catch{
                print(error)
            }
            
            
            
            if countFunction > 0{
                do {
                    if db != nil{
                        let querryParticipation = listTable.filter((Constants.cUuid == c.uuid || Constants.cUuid == "\(c.localId)" ) && Constants.tUuid == UserProvider.sharedInstance.uuid)
                        for row in try db.prepare(querryParticipation) {
                            let dateString = row.get(Constants.day)
                            let dateArray = dateString.components(separatedBy: "/")
                            let year = "\(dateArray[0])"
                            let month = dateArray[1].characters.count == 1 ? "0\(dateArray[1])" : dateArray[1]
                            let day = dateArray[2].characters.count == 1 ? "0\(dateArray[2])" : dateArray[2]
                            let r = [
                                "alumnouuid":row.get(Constants.sUuid),
                                "claseuuid":row.get(Constants.cUuid),
                                "asistencia":row.get(Constants.present),
                                "fecha":"\(year)-\(month)-\(day)",
                                ] as [String : Any]
                            list.append(r)
                        }
                    }
                } catch {
                    print(error)
                }
            }
            
            let abc = [
                "uuid" : c.uuid,
                "claseId" : c.dbid,
                "nombreMateria": c.name,
                "nivel": c.nivel,
                "grado": c.grado,
                "color": c.colorClass.idString,
                "idMaestro": UserProvider.sharedInstance.dbid,
                "localId": c.localId,
                "alumnos": x,
                "criterioInfo" : cr,
                "periodos":p,
                "calificaciones":[
                ],
                "participaciones":rates,
                "asistencias":[
                ],
                "calificacionesUpdate" : califUpdate,
                "lista":list
                ] as [String : Any]
            
            clasessss.append(abc)
        }
        
        
        
        
        var cDeleted = [[:]]
        cDeleted.removeAll()
        var sDeleted = [[:]]
        sDeleted.removeAll()
        var pDeleted = [[:]]
        pDeleted.removeAll()
        
        
        let parameters = [
            "clases":clasessss,
            "borrarAlumnos": sDeleted,
            "borrarCriterios":[],
            "borrarClases":
            cDeleted,
            "borrarPeriodos":
            pDeleted
            ] as [String : Any]
        
        print(parameters)
        
        Alamofire.request("\(Constants.baseUrl)/\(Constants.clase)/\(Constants.sincronizar)", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            debugPrint(response)
            switch response.result{
            case .success:
                
                switch Int((response.response?.statusCode)!){
                case 200, 201, 202:
                    let db : Connection!
                    do{
                        db = try Connection("\(Constants.path)/db.sqlite3")
                    }catch{
                        print ("Error: \(error)")
                        db = nil
                    }
                    
                    let participationTable = Table("participation")
                    let rateDayTable = Table("rateDay")
                    do{
                        let alice = participationTable.filter(Constants.updated == false)
                        try db.run(alice.update(Constants.updated <- true))
                    }catch{
                        print(error)
                    }
                    do{
                        let alice = rateDayTable.filter(Constants.updated == false)
                        try db.run(alice.update(Constants.updated <- true))
                    }catch{
                        print(error)
                    }
                    print(response.result.value!)
                    self.delegate?.didsuccessSincronizacion2()
                    break
                case 300...500:
                    break
                default:
                    break
                }
                break
            case .failure(let error):
                print(error)
                break
            }
        }
    }

    func sincronizarPlus(classes: [Class]){
        self.saveOrUpdate(classes: classes){response in
            print(response)
            if response{
                self.saveOrUpdatePeriodsOn(classes: classes){response in
                    if response{
                        self.saveOrUpdateStudentsOn(classes: classes){response in
                            print(response)
                        }
                    }
                }
            }
        }
    }
    
    
    //UPDATE
    func saveOrUpdate(classes: [Class], completionHandler:@escaping (Bool) -> ()){
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            db = nil
        }
        
        let classesTable = Table("classes")
        
        for c in classes{
            var countFunction = 0
            do{
                if db != nil{
                    countFunction = try db.scalar( classesTable.filter( Constants.localId == c.localId ).count )
                }
            }catch{
                print(error)
                completionHandler(false)
            }
            
            
            if countFunction > 0{
                do {
                    if db != nil{
                        let query = classesTable.filter( Constants.localId == c.localId)
                        try db.run(query.update(Constants.id <- c.dbid, Constants.name <- c.name, Constants.updated <- true, Constants.uuid <- c.uuid, Constants.claseColor <- c.colorClass.idString, Constants.nivel <- c.nivel, Constants.grado <- c.grado, Constants.teacherId <- UserProvider.sharedInstance.dbid))
                    }
                    print("update")
                    completionHandler(true)
                } catch {
                    print(error)
                    completionHandler(false)
                }
            }

        }
        
    }
    
    func saveOrUpdateStudentsOn(classes: [Class], completionHandler:@escaping (Bool) -> ()){
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            db = nil
        }
        
        let students = Table("students")
        for classe in classes{
            for student in classe.students{
                
                var countFunction = 0
                do{
                    if db != nil{
                        countFunction = try db.scalar( students.filter( Constants.localId == student.localId ).count )
                    }
                }catch{
                    print(error)
                    completionHandler(false)
                }
                
                
                if countFunction > 0{
                    do {
                        if db != nil{
                            let query = students.filter( Constants.uuid == student.uuid)
                            let dbDate = student.birthdayDate.convertedDate(calendar: .current)
                            try db.run(query.update(Constants.id <- student.dbid, Constants.uuid <- student.uuid, Constants.name <- student.name, Constants.birthday <- dbDate!, Constants.email <- student.emailTutor, Constants.gender <- student.gender, Constants.idClasse <- classe.dbid, Constants.updated <- true, Constants.cUuid <- classe.uuid))
                        }
                        print("update")
                        completionHandler(true)
                    } catch {
                        print(error)
                        completionHandler(false)
                    }
                }
                
            }
        }
    }
    
    func saveOrUpdatePeriodsOn(classes: [Class], completionHandler:@escaping (Bool) -> ()){
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            db = nil
        }
        let periods = Table("periods")
        
        for c in classes{
            for period in c.periods{
                
                var countFunction = 0
                do{
                    if db != nil{
                        countFunction = try db.scalar( periods.filter( Constants.localId == period.localId ).count )
                    }
                }catch{
                    print(error)
                    completionHandler(false)
                }
                
                
                if countFunction > 0{
                    do {
                        if db != nil{
                            let query = periods.filter( Constants.id == period.dbid)
                            let startDate = period.startDate.convertedDate(calendar: .current)
                            let endDate = period.endDate.convertedDate(calendar: .current)
                            try db.run(query.update(Constants.id <- period.dbid, Constants.nameId <- period.namedId, Constants.startDate <- startDate!, Constants.endDate <- endDate!, Constants.idClasse <- period.classId, Constants.updated <- true, Constants.uuid <- period.uuid))
                        }
                        print("update")
                        completionHandler(true)
                    } catch {
                        print(error)
                        completionHandler(false)
                    }
                }
            }
        }
    }
    
    func updateDB(){
        let db : Connection!
        do{
            db = try Connection("\(Constants.path)/db.sqlite3")
        }catch{
            print ("Error: \(error)")
            db = nil
        }
        
        let classesTable = Table("classes")
        let criteriosTable = Table("criterios")
        let periodsTable = Table("periods")
        let participTable = Table("participation")
        let rateDayTable = Table("rateDay")
        let studentsTable = Table("students")
        let listTable = Table("list")
        
        for c in UserProvider.sharedInstance.classes{
            let querryList = listTable.filter(Constants.cUuid == "\(c.localId!)")
            let querryParticipation = participTable.filter(Constants.cUuid == "\(c.localId!)")
            let querryrateDay = rateDayTable.filter(Constants.cUuid == "\(c.localId!)")
            
            do{
                try db.run(querryList.update(Constants.cUuid <- c.uuid))
                try db.run(querryParticipation.update(Constants.cUuid <- c.uuid))
                try db.run(querryrateDay.update(Constants.cUuid <- c.uuid))
            }catch{
                print(error)
            }


            
            for s in c.students{
                do{
                    let querryList = listTable.filter(Constants.sUuid == "\(s.localId!)")
                    let querryParticipation = participTable.filter(Constants.sUuid == "\(s.localId!)")
                    let querryrateDay = rateDayTable.filter(Constants.sUuid == "\(s.localId!)")
                    try db.run(querryList.update(Constants.sUuid <- s.uuid))
                    try db.run(querryParticipation.update(Constants.sUuid <- s.uuid))
                    try db.run(querryrateDay.update(Constants.sUuid <- s.uuid))
                }catch{
                    print(error)
                }
            }
            
            for cri in c.criterios{
                do{
                    let querryList = listTable.filter(Constants.criUuid == "\(cri.localId!)")
                    let querryParticipation = participTable.filter(Constants.criUuid == "\(cri.localId!)")
                    let querryrateDay = rateDayTable.filter(Constants.criUuid == "\(cri.localId!)")
                    try db.run(querryList.update(Constants.criUuid <- cri.uuid))
                    try db.run(querryParticipation.update(Constants.criUuid <- cri.uuid))
                    try db.run(querryrateDay.update(Constants.criUuid <- cri.uuid))
                }catch{
                    print(error)
                }
            }
            
            do{
                for row in try db.prepare(criteriosTable){
                    let localID = row.get(Constants.localId)
                    do{
                        let querryList = listTable.filter(Constants.criUuid == "\(localID)")
                        let querryParticipation = participTable.filter(Constants.criUuid == "\(localID)")
                        let querryrateDay = rateDayTable.filter(Constants.criUuid == "\(localID)")
                        try db.run(querryList.update(Constants.criUuid <- row.get(Constants.uuid)))
                        try db.run(querryParticipation.update(Constants.criUuid <- row.get(Constants.uuid)))
                        try db.run(querryrateDay.update(Constants.criUuid <- row.get(Constants.uuid)))

                    }catch{
                        print(error)
                    }
                }
            }catch{
                print(error)
            }
            
            
            for p in c.periods{
                do{
                    let querryList = listTable.filter(Constants.pUuid == "\(p.localId!)")
                    let querryParticipation = participTable.filter(Constants.pUuid == "\(p.localId!)")
                    let querryrateDay = rateDayTable.filter(Constants.pUuid == "\(p.localId!)")
                    try db.run(querryList.update(Constants.pUuid <- p.uuid))
                    try db.run(querryParticipation.update(Constants.pUuid <- p.uuid))
                    try db.run(querryrateDay.update(Constants.pUuid <- p.uuid))
                }catch{
                    print(error)
                }
            }
        }
      self.delegate?.updatedDB()
    }
}

extension String {
    func changeDate(_ mydate:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.long
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let convertedDate = dateFormatter.date(from: mydate)
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let date = dateFormatter.string(from: convertedDate!)
        return date
    }
}
