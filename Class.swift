//
//  Class.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso on 22/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

class Class: NSObject, NSCopying {
    var dbid: Int!
    var dbidString : String!
    var name: String!
    var students: [Student] = []
    var colorClass: Constants.colorClass!
    var nivel : String!
    var grado: Int!
    var periods : [Period] = []
    var criterios: [Criterio] = []
    var uuid: String!
    var localId : Int!
    var updated: Bool!
    override init (){
    
    }
    
    init(dbid: Int?, dbidString: String?, name: String?, students : [Student]?, colorClass: Constants.colorClass?, nivel: String?, grado: Int?, periods: [Period]?, criterios: [Criterio]?, uuid: String?) {
        self.dbid = dbid
        self.dbidString = dbidString
        self.name = name
        self.students = students!
        self.colorClass = colorClass
        self.nivel = nivel
        self.grado = grado
        self.periods = periods!
        self.criterios = criterios!
        self.uuid = uuid
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = Class(dbid: dbid, dbidString: dbidString, name: name, students: students, colorClass: colorClass, nivel: nivel, grado: grado, periods: periods, criterios: criterios, uuid: uuid)
        return copy
    }
}
