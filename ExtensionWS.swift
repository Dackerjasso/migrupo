//
//  ExtensionWS.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 07/07/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit
import SQLite
import CVCalendar


extension Date{
    func stringBy(date: Date) -> String{
        
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: date)
        let month = calendar.component(.month, from: date)
        let day = calendar.component(.day, from: date)
        return "\(year)/\(month)/\(day)"

//        
//        
//        let dateFormatter = DateFormatter()
//        dateFormatter.timeStyle = DateFormatter.Style.none
//        dateFormatter.dateStyle = DateFormatter.Style.short
//        
//        return dateFormatter.string(from: date)
    }
}

