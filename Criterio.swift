//
//  Criterio.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 26/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

class Criterio: NSObject, NSCopying {
    var dbid : Int!
    var name: String!
    var value = 0.0
    var uuid : String!
    var calif = 0.0
    var localId : Int!
    var totalCalif = 0.0
    var percentage = 0.0
    var updated: Bool!
    var inscidencias = 0
    override init (){
        
    }
    
    init(dbid: Int?, name: String?, value : Double?, uuid: String?) {
        self.dbid = dbid
        self.name = name
        self.value = value!
        self.uuid = uuid
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        let copy = Criterio(dbid: dbid, name: name, value: value, uuid: uuid)
        return copy
    }
}
