//
//  main.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 09/06/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

UIApplicationMain(CommandLine.argc, UnsafeMutableRawPointer(CommandLine.unsafeArgv).bindMemory( to: UnsafeMutablePointer<Int8>.self, capacity: Int(CommandLine.argc)), NSStringFromClass(TimerUIApplication.self), NSStringFromClass(AppDelegate.self))





//    UIApplicationMain(Process.argc, Process.unsafeArgv, NSStringFromClass(TimerUIApplication), NSStringFromClass(AppDelegate))

