//
//  Teacher.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso on 22/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

class Teacher: NSObject {
    var dbid: Int!
    var dbidString : String!
    var name : String!
    var lastName: String!
    var imageUrl: String!
    var image: UIImage!
    var userName: String!
    var schoolName: String!
    var email: String!
    var password : String!
    var classes : [Class] = []
    var token : String!
    var uuid: String!
    var isnew = false
    var localPath : URL!
    var localID : Int!
    var updated : Bool!
}
