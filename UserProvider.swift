//
//  UserProvider.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso on 22/05/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

public class UserProvider {
    class var sharedInstance: Teacher {
        struct Static {
            static let instance: Teacher = Teacher()
        }
        return Static.instance
    }
}
