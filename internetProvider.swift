//
//  internetProvider.swift
//  LarousseGroups
//
//  Created by Miguel Angel Jasso Estrada on 25/08/17.
//  Copyright © 2017 Miguel Angel Jasso. All rights reserved.
//

import UIKit

public class internetProvider {
    class var sharedInstance: Internet {
        struct Static {
            static let instance: Internet = Internet()
        }
        return Static.instance
    }
}
